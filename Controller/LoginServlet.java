package controller;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import model.Constants;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String authUrl = "https://accounts.google.com/o/oauth2/auth?"
                + "client_id=" + Constants.GOOGLE_CLIENT_ID
                + "&redirect_uri=" + Constants.GOOGLE_REDIRECT_URI
                + "&response_type=code"
                + "&scope=email profile";
        response.sendRedirect(authUrl);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("pass");
        UserDAO db = new UserDAO();
        Account user = db.loginByUserName(username, password);

        if (user == null) {
            request.setAttribute("warning", "Wrong username or password");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else if (!user.isIsActive()) {
            request.setAttribute("loginStatus", "disabled");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("acc", user);
            int isPartner = user.getIsPartner();
            Integer isCenter = user.getIsCenter();

            if (isPartner == 1) {
                response.sendRedirect("partner.jsp");
            } else {
                switch (isCenter) {
                    case 1 ->
                        response.sendRedirect("center.jsp");
                    case 0 ->
                        response.sendRedirect("keeper.jsp");
                }
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
