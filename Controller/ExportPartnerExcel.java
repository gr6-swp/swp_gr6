package controller;

import dal.StatisticDAO;
import entity.PartnerItem;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import service.PartnerService;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.util.CellRangeAddress;

@WebServlet(name = "ExportExcel", urlPatterns = {"/exportExcel"})
public class ExportPartnerExcel extends HttpServlet {

    private PartnerService partnerService;

    @Override
    public void init() throws ServletException {
        StatisticDAO dbContext = new StatisticDAO();
        partnerService = new PartnerService(dbContext);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String partner = request.getParameter("partner");

        List<PartnerItem> partnerList = null;
        try {
            partnerList = partnerService.getPartnerStatistics(partner);
        } catch (SQLException ex) {
            Logger.getLogger(ExportPartnerExcel.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (partnerList == null || partnerList.isEmpty()) {
            response.getWriter().write("No data available to export.");
            return;
        }

        // Thiết lập response để xuất file Excel
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=partner_statistics.xlsx");

        try ( Workbook workbook = new XSSFWorkbook();  OutputStream out = response.getOutputStream()) {
            Sheet sheet = workbook.createSheet("Partner Statistics");

            // Tiêu đề cho các cột
            String[] columns = {"No", "Partner Name", "Order Type", "Status", "Total Quantity"};
            Row headerRow = sheet.createRow(0);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }

            // Tạo các dòng dữ liệu
            int rowNum = 1;
            for (PartnerItem pi : partnerList) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(rowNum - 1);
                row.createCell(1).setCellValue(pi.getPartner());
                row.createCell(2).setCellValue(pi.getOder());
                row.createCell(3).setCellValue(pi.getStatus());
                row.createCell(4).setCellValue(pi.getTotalquantity());
            }

            // Ghi workbook vào OutputStream
            workbook.write(out);
        } catch (Exception e) {
            throw new ServletException("Exception in creating Excel file", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String partner = request.getParameter("partnerSelected");
        List<PartnerItem> partnerList = null;
        try {
            partnerList = partnerService.getPartnerStatistics(partner);
        } catch (SQLException e) {
            response.getWriter().write("Error retrieving data from database.");
            return;
        }
        if (partnerList == null || partnerList.isEmpty()) {
            response.getWriter().write("No data available for selected partner.");
            return;
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=partner_report.xlsx");

        try ( Workbook workbook = new XSSFWorkbook();  OutputStream out = response.getOutputStream()) {
            // Tạo sheet mới
            Sheet sheet = workbook.createSheet("Partner Report");
            Row titleRow = sheet.createRow(0);
            Cell titleCell = titleRow.createCell(0);
            titleCell.setCellValue("Bảng thống kê đối tác và số lượng đơn hàng");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4)); // Từ dòng 0, cột 0 đến dòng 0, cột 4
            Row emptyRow = sheet.createRow(1);
            String[] columns = {"No", "Partner Name", "Order Type", "Status", "Total Quantity"};
            Row headerRow = sheet.createRow(2);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }

            // Tạo các dòng dữ liệu
            int rowNum = 3;
            for (PartnerItem pi : partnerList) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(rowNum - 3);
                row.createCell(1).setCellValue(pi.getPartner());
                row.createCell(2).setCellValue(pi.getOder()); 
                row.createCell(3).setCellValue(pi.getStatus());
                row.createCell(4).setCellValue(pi.getTotalquantity());
            }

            // Ghi workbook vào OutputStream
            workbook.write(out);
        } catch (Exception e) {
            throw new ServletException("Exception in creating Excel file", e);
        }
    }

    @Override
    public String getServletInfo() {
        return "Servlet to export Partner Statistics to Excel";
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doGet(request, response);
    }
}
