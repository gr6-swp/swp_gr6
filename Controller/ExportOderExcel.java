/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.StatisticDAO;
import entity.OderIteam;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import service.OrderService;

/**
 *
 * @author songl
 */
@WebServlet(name = "ExportOderExcel", urlPatterns = {"/exportOderExcel"})
public class ExportOderExcel extends HttpServlet {


    private OrderService orderService;
    @Override
    public void init() throws ServletException{
        StatisticDAO dbContext = new StatisticDAO();
        orderService  = new OrderService(dbContext);
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String warehouse = request.getParameter("warehouse");
        List<OderIteam> orderList = null;
        try {
            orderList = orderService.getOrderStatistics(warehouse);
        } catch (SQLException ex) {
            Logger.getLogger(ExportOderExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (orderList == null || orderList.isEmpty()) {
            response.getWriter().write("No data available to export");
            return;
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=order_statistics.xlsx");
        try ( Workbook workbook = new XSSFWorkbook();  OutputStream out = response.getOutputStream()) {
            Sheet sheet = workbook.createSheet("Order Statistics");
            String[] columns = {"No", "Warehouse Name", "Order Type", "Status", "Total Quantity"};
            Row headerRow = sheet.createRow(0);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }
            int rowNum = 1;
            for (OderIteam stats : orderList) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(rowNum - 1);
                row.createCell(1).setCellValue(stats.getWarehouse());
                row.createCell(2).setCellValue(stats.getOder());
                row.createCell(3).setCellValue(stats.getStatus());
                row.createCell(4).setCellValue(stats.getTotalquantity());             
            }
            workbook.write(out);
        }catch(Exception e){
            throw new ServletException("Exception in creating Excel file",e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String warehouse = request.getParameter("warehouseSelected");
       List<OderIteam> orderList = null;
        try {
            orderList = orderService.getOrderStatistics(warehouse);
        } catch (SQLException e) {
            response.getWriter().write("Error retrieving data from database.");
            return;
        }
        if(orderList == null || orderList.isEmpty()){
            response.getWriter().write("No data available for selected warehouse.");
            return;
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=order_report.xlsx");
        try (Workbook workbook = new XSSFWorkbook(); OutputStream out = response.getOutputStream()){
            Sheet sheet = workbook.createSheet("Order Report");
            Row titRow = sheet.createRow(0);
            Cell titleCell=titRow.createCell(0);
            titleCell.setCellValue("Bảng thống kê đơn hàng ");
            sheet.addMergedRegion(new CellRangeAddress(0,0,0,4));
            Row emptyRow = sheet.createRow(1);
            String[] columns = {"No", "Warehouse Name", "Order Type", "Status", "Total Quantity"};
            Row headerRow = sheet.createRow(2);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }
            int rowNum =3;
            for (OderIteam stats : orderList) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(rowNum - 1);
                row.createCell(1).setCellValue(stats.getWarehouse());
                row.createCell(2).setCellValue(stats.getOder());
                row.createCell(3).setCellValue(stats.getStatus());
                row.createCell(4).setCellValue(stats.getTotalquantity());             
            }
            workbook.write(out);
        } catch (Exception e) {
             throw new ServletException("Exception in creating Excel file", e);
        }
    }
    @Override
    public String getServletInfo() {
        return "Servlet to export Order Statistics to Excel";
    }// </editor-fold>

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);      
    }
}
