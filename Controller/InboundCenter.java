/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.center;

import dao.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import model.Partner;
import model.ProOrCenter;

/**
 *
 * @author X P S
 */
@WebServlet(name = "InboundCenter", urlPatterns = {"/inboundcenter"})
public class InboundCenter extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InboundCenter</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InboundCenter at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao = new DAO();

        // Lấy danh sách tất cả sản phẩm
        List<ProOrCenter> allProOr = dao.getProductOrders2();
        String keyword = request.getParameter("keyword");

        // Tạo Map để nhóm sản phẩm theo PartnerID
        Map<Partner, List<ProOrCenter>> proOrMap = new TreeMap<>(Comparator.comparing(Partner::getPartnerID)); // Sử dụng TreeMap để sắp xếp

        if (keyword != null && !keyword.isEmpty()) {
            // Tạo danh sách sản phẩm phù hợp với từ khóa
            List<ProOrCenter> filteredProOr = new ArrayList<>();
            for (ProOrCenter proOr : allProOr) {
                // Kiểm tra từ khóa trong các trường dữ liệu của sản phẩm
                if (proOr.getProductName().toLowerCase().contains(keyword.toLowerCase())
                        || proOr.getDescription().toLowerCase().contains(keyword.toLowerCase())) {
                    filteredProOr.add(proOr);
                }
            }

            // Nếu không có sản phẩm nào phù hợp
            if (filteredProOr.isEmpty()) {
                request.setAttribute("searchResultMessage", "Không có sản phẩm nào phù hợp với từ khóa '" + keyword + "'");
                // Nhóm tất cả sản phẩm theo PartnerID
                for (Partner partner : dao.getAllPartners()) {
                    List<ProOrCenter> partnerProOrs = new ArrayList<>();
                    for (ProOrCenter proOr : allProOr) {
                        if (proOr.getPartnerID() == partner.getPartnerID()) {
                            partnerProOrs.add(proOr);
                        }
                    }
                    proOrMap.put(partner, partnerProOrs);
                }
            } else {
                // Nhóm sản phẩm phù hợp theo PartnerID
                for (Partner partner : dao.getAllPartners()) {
                    List<ProOrCenter> partnerProOrs = new ArrayList<>();
                    for (ProOrCenter proOr : filteredProOr) {
                        if (proOr.getPartnerID() == partner.getPartnerID()) {
                            partnerProOrs.add(proOr);
                        }
                    }
                    if (!partnerProOrs.isEmpty()) {
                        proOrMap.put(partner, partnerProOrs);
                    }
                }
            }
        } else {
            // Nếu không có từ khóa, nhóm tất cả sản phẩm theo PartnerID
            for (Partner partner : dao.getAllPartners()) {
                List<ProOrCenter> partnerProOrs = new ArrayList<>();
                for (ProOrCenter proOr : allProOr) {
                    if (proOr.getPartnerID() == partner.getPartnerID()) {
                        partnerProOrs.add(proOr);
                    }
                }
                proOrMap.put(partner, partnerProOrs);
            }
        }

        request.setAttribute("proor", proOrMap);
        request.getRequestDispatcher("inboundcenter.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
