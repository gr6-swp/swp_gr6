/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.StatisticDAO;
import entity.PartnerInventory;
import entity.PartnerOder;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author songl
 */
@WebServlet(name = "exparneroders", urlPatterns = {"/exparneroders"})
public class ExportPartnerOder extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ExportPartnerOder</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ExportPartnerOder at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        StatisticDAO dbcontext = new StatisticDAO();
        String productName = request.getParameter("productNameSelected");
        List<PartnerOder> partneroderlist = null;
        HttpSession session = request.getSession();
        Integer partnerID = (Integer) session.getAttribute("partnerID");
        if (partnerID == null) {
            response.sendRedirect("login.jsp?error=missingPartnerID");
            return;
        }

        try {
            partneroderlist = dbcontext.fetchPartnerOrderStatistics(partnerID, productName);
        } catch (SQLException e) {
            // Log the error for debugging purposes
            System.out.println("SQL Exception: " + e.getMessage());
            e.printStackTrace();

            response.getWriter().write("Error retrieving data from database: " + e.getMessage());
            return;
        }
        if (partneroderlist == null || partneroderlist.isEmpty()) {
            response.getWriter().write("No data available for selected productName.");
            return;
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=partnerorder_report.xlsx");
        try ( Workbook workbook = new XSSFWorkbook();  OutputStream out = response.getOutputStream()) {
            Sheet sheet = workbook.createSheet("Inventory Report");
            Row titleRow = sheet.createRow(0);
            Cell titleCell = titleRow.createCell(0);
            titleCell.setCellValue("Bảng thống kê số lượng sản phẩm tồn kho");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4)); // Từ dòng 0, cột 0 đến dòng 0, cột 4
            Row emptyRow = sheet.createRow(1);
            String[] columns = {"No", "Product Name", "Total Product","OderType","OderStatus","OderTypeQuantity"};
            Row headerRow = sheet.createRow(2);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }
            int rowNum = 3;
            for (PartnerOder item :  partneroderlist) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(rowNum - 3);
                row.createCell(1).setCellValue(item.getProduct());
                row.createCell(2).setCellValue(item.getQuantity());
                row.createCell(3).setCellValue(item.getOrderType());
                row.createCell(4).setCellValue(item.getOrderStatus());
                row.createCell(5).setCellValue(item.getOrderTypeQuantity());
            }
            workbook.write(out);
        } catch (Exception e) {
            throw new ServletException("Exception in creating Excel file", e);
        }
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
