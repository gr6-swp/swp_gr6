package controller;

import dal.ProductDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Products;

public class ProductPartner extends HttpServlet {

    private final ProductDAO dao = new ProductDAO(); // Instantiate ProductDAO

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");

        if ("editProduct".equals(action)) {
            handleEditProduct(request, response);
        } else {
            // Handle other actions if needed
            processRequest(request, response);
        }
    }

 private void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    HttpSession session = request.getSession();
    Account partner = (Account) session.getAttribute("acc"); // Get the partner object from session

    if (partner == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    int partnerId = partner.getPartnerID();

    // Get products for the specific partnerId
    List<Products> products = dao.getProductsByPartnerID(partnerId);

    request.setAttribute("products", products);
    request.getRequestDispatcher("productPartner.jsp").forward(request, response);
}

    private void handleEditProduct(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        int productId = Integer.parseInt(request.getParameter("id"));
        String productName = request.getParameter("name");
        String productDescription = request.getParameter("description");

        // Update product in database
        dao.updateProduct(productId, productName, productDescription);

        // Redirect to product list page
        response.sendRedirect("productpart");
    }
}
