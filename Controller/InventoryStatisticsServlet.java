/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;
import dal.StatisticDAO;
import entity.InventoryItem;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;
import service.InventoryService;

/**
 *
 * @author songl
 */
@WebServlet(name = "InventoryStatisticsServlet", urlPatterns = {"/inventoryStatistics"})
public class InventoryStatisticsServlet extends HttpServlet {  
    private InventoryService inventoryService;
    @Override
    public void init() throws ServletException {
        StatisticDAO dbContext = new StatisticDAO();
        inventoryService = new InventoryService(dbContext);
    }
    @Override

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String warehouse = request.getParameter("warehouse");
        String zoneName = request.getParameter("zoneName");
        try {
            List<String> warehouseNames = inventoryService.getAllWarehouseNames();
            request.setAttribute("warehouseNames", warehouseNames);
        } catch (SQLException e) {
           
            throw new ServletException("Error fetching ware statistics", e);
        }

        try {
            List<String> zoneNames = inventoryService.getAllZoneNames();
            request.setAttribute("zoneNames", zoneNames);
        } catch (SQLException e) {          
            throw new ServletException("Error fetching zone statistics", e);
        }
        try {
            List<InventoryItem> inventoryList = inventoryService.getInventoryStatistics(warehouse, zoneName);
             if ("all".equals(warehouse) || "all".equals(zoneName)) {
                inventoryList = inventoryService.getInventoryStatistics(null, null); // Fetch all partners if 'all' is selected
            } else {
                inventoryList = inventoryService.getInventoryStatistics(warehouse, zoneName);
            }
            request.setAttribute("inventoryList", inventoryList);
            request.getRequestDispatcher("iventorystatistic.jsp").forward(request, response);
        } catch (SQLException e) {        
            throw new ServletException("Error fetching inventory statistics", e);

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SignupServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SignupServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

}
