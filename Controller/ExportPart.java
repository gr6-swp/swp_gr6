/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.partner;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author X P S
 */
@WebServlet(name = "ExportPart", urlPatterns = {"/exportpart"})
public class ExportPart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ExportPart</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ExportPart at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DAO dao = new DAO();

        int partnerID = Integer.parseInt(request.getParameter("partnerID"));
        int productID = Integer.parseInt(request.getParameter("productID"));
        String productName = request.getParameter("productName");
        String description = request.getParameter("description");
        String orderType = request.getParameter("orderType");
        String orderStatus = request.getParameter("orderStatus");
        int quantityuser = Integer.parseInt(request.getParameter("quantityExport"));
        int quantityproduct = dao.getQuantityByProID(productID);
        int zoneID = Integer.parseInt(request.getParameter("zoneID"));

     
        if (quantityuser == quantityproduct) {

            dao.updaOutboundPa(productID, orderType, orderStatus);
            response.sendRedirect("inboundpart");}
        else {
            
            // Tạo đơn hàng mới
            double oldunitPrice = dao.getUnitPriceByID(productID);
            double newunitPrice = (quantityuser * oldunitPrice) / quantityproduct;
            dao.insertOrders2(partnerID, orderType, orderStatus, zoneID);
            dao.insertProduct(productName, description, quantityuser, newunitPrice);
            /////////////////////////////////////////////////////////////////
            
            // cập nhập lại quatity cho số hàng còn lại 
            int newquantity = quantityproduct - quantityuser;
           
            double unitPrice = oldunitPrice - newunitPrice;
            
            dao.updateOrders(orderType, orderStatus, productID);
            dao.updateProducts(newquantity, unitPrice, productID);
            response.sendRedirect("inboundpart");
            }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
