package controller;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import dao.NotificationsDAO;
import dal.DBConnection;
import model.Notification;

@WebServlet("/notificationsCenter")
public class NotificationsCenterServlet extends HttpServlet {

    private NotificationsDAO notificationDAO;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }

        switch (action) {
            case "create":
                showCreateForm(request, response);
                break;
            case "list":
            default:
                listNotifications(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        if (action == null) {
            action = "list";
        }

        switch (action) {
            case "createNotification":
                createNotification(request, response);
                break;
            case "updateStatus":
                updateNotificationStatus(request, response);
                break;
            default:
                listNotifications(request, response);
                break;
        }
    }

    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("new.jsp");
        dispatcher.forward(request, response);
    }

    private void createNotification(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String senderRole = "Center";
        String receiverRole = request.getParameter("receiverRole");
        String message = request.getParameter("message");
        int relatedEntityId = Integer.parseInt(request.getParameter("relatedEntityId"));

        Notification notification = new Notification();
        notification.setSenderRole(senderRole);
        notification.setReceiverRole(receiverRole);
        notification.setMessage(message);
        notification.setStatus("Unread");
        notification.setCreatedAt(LocalDateTime.now());
        notification.setRelatedEntityId(relatedEntityId);

        notificationDAO.createNotification(notification);
        response.sendRedirect("notificationsCenter?action=list");
    }

    private void listNotifications(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Notification> notifications = notificationDAO.getAllNotificationsCenter();
        request.setAttribute("notifications", notifications);
        RequestDispatcher dispatcher = request.getRequestDispatcher("notificationsCenter.jsp");
        dispatcher.forward(request, response);
    }

    private void updateNotificationStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int notificationID = Integer.parseInt(request.getParameter("notificationID"));
        String status = request.getParameter("status");

        notificationDAO.updateNotificationStatus(notificationID, status);
        response.sendRedirect("notificationsCenter?action=list");
    }
}
