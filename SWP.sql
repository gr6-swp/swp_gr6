﻿﻿CREATE DATABASE SWP

USE SWP

-- Bảng Đối tác: Lưu trữ thông tin về các đối tác.
CREATE TABLE Partners (
    PartnerID INT PRIMARY KEY IDENTITY(1,1),       -- Mã định danh duy nhất của đối tác
    PartnerName NVARCHAR(255) NOT NULL,            -- Tên đối tác
    ContactName NVARCHAR(255),                     -- Tên người liên hệ
    Email NVARCHAR(255) UNIQUE NOT NULL,           -- Email (duy nhất)
    PhoneNumber NVARCHAR(20),                      -- Số điện thoại
    Address NVARCHAR(255)                          -- Địa chỉ
);

-- Bảng Tài khoản: Lưu trữ thông tin tài khoản người dùng.
CREATE TABLE Accounts (
    UserID INT PRIMARY KEY IDENTITY(1,1),         -- Mã định danh duy nhất của tài khoản
    Username NVARCHAR(255) UNIQUE NOT NULL,       -- Tên đăng nhập (duy nhất)
    PasswordHash NVARCHAR(255) NOT NULL,          -- Mật khẩu đã băm (hash)
    Email NVARCHAR(255) UNIQUE NOT NULL,          -- Email (duy nhất)
    PhoneNumber NVARCHAR(20),                     -- Số điện thoại
	IsPartner BIT NOT NULL DEFAULT 0,             -- 1: Partner, 0: Không phải Partner. Nếu không phải là partner
												  -- thì mới kiểm tra xem có phải Center hay Keeper
    IsCenter BIT DEFAULT NULL,                    -- 1: Center, 0: Keeper, NULL: không phải là Center hay Keeper
    PartnerID INT,                                -- ID đối tác (nếu là Partner)
	isActive BIT NOT NULL DEFAULT 0,              -- nếu là 1 thì được đăng nhập nếu là 0 thì không được đăng nhập
    FOREIGN KEY (PartnerID) REFERENCES Partners(PartnerID)
);

-- Bảng Kho: Lưu trữ thông tin về các kho hàng.
CREATE TABLE Warehouses (
    WarehouseID INT PRIMARY KEY IDENTITY(1,1),    -- Mã định danh duy nhất của kho
    WarehouseName NVARCHAR(255) NOT NULL,         -- Tên kho
    Address NVARCHAR(255)                         -- Địa chỉ kho
);

-- Bảng Khu vực kho: Lưu trữ thông tin về các khu vực trong kho.
CREATE TABLE WarehouseZones (
    ZoneID INT PRIMARY KEY IDENTITY(1,1),        -- Mã định danh duy nhất của khu vực kho
    WarehouseID INT NOT NULL,                    -- ID kho chứa khu vực này
    ZoneName NVARCHAR(255) NOT NULL,             -- Tên khu vực kho
    KeeperID INT,                                -- ID thủ kho phụ trách khu vực này
    FOREIGN KEY (WarehouseID) REFERENCES Warehouses(WarehouseID),
    FOREIGN KEY (KeeperID) REFERENCES Accounts(UserID)
);

-- Bảng Sản phẩm: Lưu trữ thông tin về các sản phẩm.
CREATE TABLE Products (
    ProductID INT PRIMARY KEY IDENTITY(1,1),     -- Mã định danh duy nhất của sản phẩm
    ProductName NVARCHAR(255) NOT NULL,          -- Tên sản phẩm
    Description NVARCHAR(MAX),                   -- Mô tả sản phẩm
    Quantity INT NOT NULL,                       -- Số lượng sản phẩm
    UnitPrice DECIMAL(18,2) NOT NULL,             -- Đơn giá sản phẩm
    WarehouseZoneID int ,
FOREIGN KEY (WarehouseZoneID ) REFERENCES WarehouseZones(ZoneID)
);
-- Bảng Đơn hàng: Lưu trữ thông tin về các đơn hàng nhập/xuất kho.
CREATE TABLE Orders (
    OrderID INT PRIMARY KEY IDENTITY(1,1),       -- Mã định danh duy nhất của đơn hàng
    PartnerID INT NOT NULL,                      -- ID đối tác tạo đơn hàng
    ZoneID INT,                             -- ID kho liên quan đến đơn hàng
    ProductID INT,                               -- ID sản phẩm
    OrderType NVARCHAR(50) NOT NULL CHECK (OrderType IN (N'Nhập kho', N'Xuất kho')), -- Loại đơn hàng (nhập hoặc xuất)
    OrderStatus NVARCHAR(50) NOT NULL CHECK (OrderStatus IN (N'Chờ phê duyệt', N'Đã phê duyệt', N'Đang xử lý', N'Hoàn thành', N'Đã hủy')), -- Trạng thái đơn hàng
    CreatedAt DATETIME NOT NULL DEFAULT GETDATE(), -- Thời gian tạo đơn hàng
    UpdatedAt DATETIME,                            -- Thời gian cập nhật đơn hàng (nếu có)
    FOREIGN KEY (PartnerID) REFERENCES Partners(PartnerID),
    FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
);

-- Bảng Tồn kho: Lưu trữ thông tin về số lượng sản phẩm tồn kho tại từng khu vực.
CREATE TABLE Inventory (
    InventoryID INT PRIMARY KEY IDENTITY(1,1),    -- Mã định danh duy nhất của bản ghi tồn kho
    ProductID INT NOT NULL,                       -- ID sản phẩm
    WarehouseZoneID INT NOT NULL,                 -- ID khu vực kho
    Quantity INT NOT NULL,                        -- Số lượng tồn kho
    FOREIGN KEY (ProductID) REFERENCES Products(ProductID),
    FOREIGN KEY (WarehouseZoneID) REFERENCES WarehouseZones(ZoneID)
);

CREATE TABLE Notifications (
    ID INT PRIMARY KEY IDENTITY(1,1),
    IDSender INT NOT NULL,
    IDReceiver INT NOT NULL,
    Content NVARCHAR(MAX) NOT NULL,
    RelatedEntityID INT,
    CreatedAt DATETIME NOT NULL DEFAULT GETDATE(),
    ReadAt DATETIME,
    FOREIGN KEY (IDSender) REFERENCES Accounts(UserID),
    FOREIGN KEY (IDReceiver) REFERENCES Accounts(UserID),
    CHECK (IDSender != IDReceiver)  -- Đảm bảo người gửi và người nhận khác nhau
);

--Trigger của Trung
GO
CREATE TRIGGER trg_UpdateOrderIDOnProductInsert
ON Products
AFTER INSERT
AS
BEGIN
    -- Lấy ProductID mới nhất từ bảng Products
    DECLARE @latestProductID INT;
    SELECT TOP 1 @latestProductID = ProductID FROM Products ORDER BY ProductID DESC;

    -- Lấy OrderID mới nhất từ bảng Orders
    DECLARE @latestOrderID INT;
    SELECT TOP 1 @latestOrderID = OrderID FROM Orders ORDER BY OrderID DESC;

    -- Cập nhật ProductID trong bảng Orders với OrderID mới nhất và ProductID mới nhất
    UPDATE Orders
    SET ProductID = @latestProductID
    WHERE OrderID = @latestOrderID;
END;
GO

--Trigger của Thực
-- Trigger kiểm tra mỗi Keeper chỉ quản lý một kho duy nhất
CREATE TRIGGER trg_CheckSingleWarehouseKeeper
ON WarehouseZones
AFTER INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM inserted i
        JOIN WarehouseZones w ON i.KeeperID = w.KeeperID AND i.WarehouseID <> w.WarehouseID
        WHERE i.KeeperID IS NOT NULL
    )
    BEGIN
        RAISERROR ('Invalid KeeperID: The user is already managing a different warehouse.', 16, 1);
        ROLLBACK TRANSACTION;
    END
END;
GO

CREATE TRIGGER trg_UpdateProductWarehouseZoneID
ON Orders
AFTER UPDATE
AS
BEGIN
    UPDATE p
    SET p.WarehouseZoneID = i.ZoneID
    FROM Products p
    INNER JOIN inserted i ON p.ProductID = i.ProductID
    WHERE p.ProductID = i.ProductID;
END;

--Của Thành
-- Tạo trigger cho sự kiện INSERT vào bảng Orders
GO
CREATE TRIGGER trg_NotifyOnOrderInsert
ON Orders
AFTER INSERT
AS
BEGIN
    DECLARE @OrderID INT, @PartnerID INT, @OrderType NVARCHAR(50), @OrderStatus NVARCHAR(50);

    -- Lấy thông tin từ bảng Orders vừa được thêm
    SELECT @OrderID = OrderID, @PartnerID = PartnerID, @OrderType = OrderType, @OrderStatus = OrderStatus 
    FROM inserted;

    -- Tạo thông báo cho sự kiện INSERT
    INSERT INTO Notifications (SenderRole, ReceiverRole, Message, Status, CreatedAt, RelatedEntityID)
    VALUES ('System', 'Partner', CONCAT('A new order has been created. OrderID: ', @OrderID, ', Type: ', @OrderType, ', Status: ', @OrderStatus), 'New', GETDATE(), @OrderID);
END;
GO



--Của La
CREATE TRIGGER trg_AddToInventory
ON Orders
AFTER INSERT, UPDATE
AS
BEGIN
    -- Thêm vào bảng Inventory khi có đơn hàng nhập kho và đã hoàn thành
    INSERT INTO Inventory (ProductID, WarehouseZoneID, Quantity)
    SELECT i.ProductID, o.ZoneID, 1
    FROM inserted i
    INNER JOIN Orders o ON i.OrderID = o.OrderID
    WHERE o.OrderType = N'Nhập kho' AND o.OrderStatus = N'Hoàn thành';
END;

--Trigger tạo Notification từ Orders
CREATE TRIGGER trg_AfterOrderStatusChange
ON Orders
AFTER INSERT, UPDATE
AS
BEGIN
    DECLARE @OrderID INT, @PartnerID INT, @ZoneID INT, @KeeperID INT, @OrderType NVARCHAR(50), @OrderStatus NVARCHAR(50), @CreatedAt DATETIME, @UpdatedAt DATETIME, @WarehouseID INT;
    
    SELECT @OrderID = inserted.OrderID, @PartnerID = inserted.PartnerID, @ZoneID = inserted.ZoneID, @OrderType = inserted.OrderType, @OrderStatus = inserted.OrderStatus, @CreatedAt = inserted.CreatedAt, @UpdatedAt = inserted.UpdatedAt
    FROM inserted;
    
    SELECT @KeeperID = KeeperID
    FROM WarehouseZones
    WHERE ZoneID = @ZoneID;
    
    SELECT @WarehouseID = WarehouseID
    FROM WarehouseZones
    WHERE ZoneID = @ZoneID;
    
    IF @OrderStatus = N'Đang xử lý'
    BEGIN
        INSERT INTO Notifications (IDSender, IDReceiver, Content, RelatedEntityID)
        VALUES (@PartnerID, 1, N'Partner ' + CAST(@PartnerID AS NVARCHAR) + N' có đơn ' + @OrderType + N' ' + CAST(@OrderID AS NVARCHAR) + N' vào thời gian ' + CAST(@CreatedAt AS NVARCHAR), @OrderID);
        
        INSERT INTO Notifications (IDSender, IDReceiver, Content, RelatedEntityID)
        VALUES (@KeeperID, 1, N'Partner ' + CAST(@PartnerID AS NVARCHAR) + N' có đơn ' + @OrderType + N' ' + CAST(@OrderID AS NVARCHAR) + N' vào thời gian ' + CAST(@CreatedAt AS NVARCHAR), @OrderID);
    END
    ELSE IF @OrderStatus = N'Hoàn thành'
    BEGIN
        INSERT INTO Notifications (IDSender, IDReceiver, Content, RelatedEntityID)
        VALUES (@KeeperID, 1, N'Đã ' + @OrderType + N' thành công đơn ' + CAST(@OrderID AS NVARCHAR) + N' tại kho ' + CAST(@WarehouseID AS NVARCHAR), @OrderID);
    END
    ELSE IF @OrderStatus = N'Đã phê duyệt'
    BEGIN
        INSERT INTO Notifications (IDSender, IDReceiver, Content, RelatedEntityID)
        VALUES (1, @PartnerID, N'Đơn ' + @OrderType + N' đã được xác nhận sẽ ' + @OrderType + N' tại kho ' + CAST(@WarehouseID AS NVARCHAR) + N' vào lúc ' + CAST(@UpdatedAt AS NVARCHAR), @OrderID);
        
        INSERT INTO Notifications (IDSender, IDReceiver, Content, RelatedEntityID)
        VALUES (1, @KeeperID, N'Có đơn ' + @OrderType + N' ' + CAST(@OrderID AS NVARCHAR) + N' cần được ' + @OrderType + N' vào thời gian ' + CAST(@CreatedAt AS NVARCHAR), @OrderID);
    END
END;
