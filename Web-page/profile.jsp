<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Thông tin tài khoản</title>
        <link rel="stylesheet" type="text/css" href="css/profile.css">
        <script>
            function togglePasswordSection() {
                var passwordSection = document.getElementById("password-section");
                if (passwordSection.style.display === "none") {
                    passwordSection.style.display = "block";
                } else {
                    passwordSection.style.display = "none";
                }
            }

            function toggleEditSection(sectionId) {
                var editSection = document.getElementById(sectionId);
                if (editSection.style.display === "none") {
                    editSection.style.display = "block";
                } else {
                    editSection.style.display = "none";
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <h1>Thông tin tài khoản</h1>
            <div class="profile-info">
                <c:choose>
                    <c:when test="${not empty acc}">
                        <p>
                            <strong>Tên đăng nhập:</strong> ${acc.userName}
                        </p>
                        <p>
                            <strong>Email:</strong> ${acc.email}
                            <button onclick="toggleEditSection('email-section')" class="edit-btn">Edit</button>
                        </p>
                        <div id="email-section" class="edit-section">
                            <form action="changeEmail" method="post">
                                <p>
                                    <label for="newEmail">Enter New Email:</label>
                                    <input type="email" id="newEmail" name="newEmail" pattern="^(?=.*[a-zA-Z0-9]){4,}@gmail\\.com" title="Please enter the right form of email" required>
                                </p>
                                <button type="submit">Submit</button>
                            </form>
                        </div>
                        <p>
                            <strong>Số điện thoại:</strong> ${acc.phoneNumber}
                            <button onclick="toggleEditSection('phone-section')" class="edit-btn">Edit</button>
                        </p>
                        <div id="phone-section" class="edit-section">
                            <form action="changePhone" method="post">
                                <p>
                                    <label for="newPhoneNumber">Enter New Phone Number:</label>
                                    <input type="tel" id="newPhoneNumber" name="newPhoneNumber" pattern="^\d{10}$" title="Phone number must be 10 digits." required>
                                </p>
                                <button type="submit">Submit</button>
                            </form>
                        </div>
                        <button onclick="togglePasswordSection()">Change Password</button>
                        <div id="password-section" class="password-section">
                            <form action="changePassword" method="post">
                                <p>
                                    <label for="newPassword">Enter New Password:</label>
                                    <input type="password" id="newPassword" name="newPassword" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9]{6,}" title="Password must contain 
                                           at least 1 lowercase letter, 1 uppercase letter, 1 number and 6 characters." required>
                                </p>
                                <p>
                                    <label for="reenterPassword">Re-enter New Password:</label>
                                    <input type="password" id="reenterPassword" name="reenterPassword" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9]{6,}" title="Re-Password must contain 
                                           at least 1 lowercase letter, 1 uppercase letter, 1 number and 6 characters." required>
                                </p>
                                <button type="submit">Submit</button>
                            </form>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <p>Không có thông tin người dùng.</p>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </body>
</html>
