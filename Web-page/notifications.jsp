<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Thông báo</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h1>Thông báo</h1>

            <!-- Nút để mở popup tạo thông báo -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createNotificationModal">
                Tạo thông báo mới
            </button>


            <!-- Modal popup để tạo thông báo mới -->
            <div class="modal fade" id="createNotificationModal" tabindex="-1" role="dialog" aria-labelledby="createNotificationModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="createNotificationModalLabel">Tạo thông báo mới</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="notifications" method="post">
                                <input type="hidden" name="action" value="createNotification">

                                <div class="form-group">
                                    <label for="senderRole">Vai trò Người gửi:</label>
                                    <input type="text" class="form-control" id="senderRole" name="senderRole" required>
                                </div>
                                <div class="form-group">
                                    <label for="receiverRole">Vai trò Người nhận:</label>
                                    <input type="text" class="form-control" id="receiverRole" name="receiverRole" required>
                                </div>
                                <div class="form-group">
                                    <label for="message">Nội dung:</label>
                                    <textarea class="form-control" id="message" name="message" rows="3" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="relatedEntityId">ID Liên quan:</label>
                                    <input type="number" class="form-control" id="relatedEntityId" name="relatedEntityId" required>
                                </div>
                                <button type="submit" class="btn btn-primary">Tạo thông báo</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Danh sách thông báo -->

            <h2>Danh sách thông báo</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID Thông báo</th>
                        <th>ID Đơn hàng</th>
                        <th>Vai trò Người gửi</th>
                        <th>Vai trò Người nhận</th>
                        <th>Nội dung</th>
                        <th>Trạng thái</th>
                        <th>Ngày tạo</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="notification" items="${notifications}">
                        <tr>
                            <td>${notification.notificationID}</td>
                            <td>${notification.orderID}</td>
                            <td>${notification.senderRole}</td>
                            <td>${notification.receiverRole}</td>
                            <td>${notification.message}</td>
                            <td>${notification.status}</td>
                            <td>${notification.createdAt}</td>
                            <td>
                                <c:if test="${notification.status == 'Unread'}">
                                    <form action="notifications" method="post" style="display:inline;">
                                        <input type="hidden" name="action" value="updateStatus">
                                        <input type="hidden" name="notificationID" value="${notification.notificationID}">
                                        <input type="hidden" name="status" value="Read">
                                        <button type="submit" class="btn btn-primary">Đánh dấu là đã đọc</button>
                                    </form>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <form action="notifications" method="doPost">
                <button type="submit" class="btn btn-primary">Hiện danh sách</button>
            </form>
        </div>
    </body>
</html>
