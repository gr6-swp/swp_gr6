<%-- 
    Document   : partner
    Created on : May 30, 2024, 11:08:28 PM
    Author     : X P S
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Product List</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="assets/images/favicon.png" />
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            h2 {
                margin-bottom: 10px;
            }

            /* Kiểu dáng cho nút */
            button {
                background-color: #4CAF50; /* Màu xanh lá cây */
                color: white;
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Kiểu dáng cho ô tìm kiếm và select */
            input[type="text"], select {
                padding: 8px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* Kiểu dáng cho bảng */
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 15px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            input[type="text"] {
                padding: 10px;
                border: 2px solid #4CAF50; /* Viền xanh lá cây */
                border-radius: 5px; /* Bo góc */
                width: 200px; /* Điều chỉnh độ rộng tùy ý */
            }

            #container {
                border: 2px solid #4CAF50; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px; /* Bo góc (tùy chọn) */
                width: 125%;
            }

            label {
                display: block;
                margin-bottom: 5px;
            }
            input, select {
                width: 100%;
                padding: 8px;
                margin-bottom: 10px;
                box-sizing: border-box;
            }
            button {
                padding: 10px;
                background-color: #4CAF50;
                color: white;
                border: none;
                cursor: pointer;
            }
            #orderDetails {
                margin-top: 20px;
            }

            .form-group {
                margin-bottom: 15px;
            }



            .form-row {
                display: flex;
                justify-content: space-between;
            }
            .modal-header {
                color: white;
            }
            .modal-footer {
                justify-content: center;
            }
            .modal-body{
                border: 2px solid #4CAF50; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px;
                margin: 20px
            }
            .modal-body .form-group label {
                font-weight: bold;
            }
            .btn-custom {
                background-color: #28a745;
                color: white;
            }
            .btn-custom:hover {
                background-color: #218838;
            }
            .form-control {
                border-radius: 0;
            }
            .table th {
                background-color: #28a745;
                color: white;
            }
            .table input {
                width: 100%;
                border: none;
            }
            .form-container {
                display: flex;
                align-items: center;
                justify-content: space-between;
                border: 1px solid #ccc;
                border-radius: 5px;
                background-color: #f9f9f9;
            }
            .form-container select,
            .form-container .products-count {
                padding: 5px;
                font-size: 14px;
            }
            .form-container .sort-by {
                display: flex;
                align-items: center;
            }
            .form-container .sort-by select {
                margin-left: 5px;
            }
            .form-container .view-options {
                display: flex;
                align-items: center;
            }
            .form-container .view-options i {
                margin-left: 5px;
                cursor: pointer;
            }
            .bordered-section{
                border: 1px solid #000;
                padding: 10px;
                margin-bottom: 20px;
                border-radius: 8px;
            }

        </style>
    </head>
    <body>
       <div class="container-scroller">
            <%@include file="sidebarPartner.jsp" %>
            <div class="container-fluid page-body-wrapper">
                <div style="margin-left: 5%; margin-top: 1%; margin-right: 0%; width: 71%;">
                    <h2 style="color: #007bff">List Product</h2>
                    <div id="container">
                        <div class="search-container" style="text-align: end">
                            <input type="text" id="searchInput" placeholder="Search...">
                        </div>
                        <div class="form-container bg-gradient-success">
                            <div class="products-count text-black-50">All ${proPart.size()} Products</div>
                            <div class="sort-by ">
                                <label for="sort" class="text-black">Sort:</label>
                                <select id="sort" style="margin: 2px;">
                                    <option value="quantity">Quantity</option>
                                    <option value="unitprice">Unit Price</option>
                                    <option value="name-asc">A-Z</option>
                                    <option value="name-desc">Z-A</option>
                                </select>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover product-table">
                                <thead>
                                    <tr style="color: black; font-family: nunito-regular, sans-serif">
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>ZoneID</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="productTbody">
                                    <c:forEach items="${products}" var="product">
                                        <tr>
                                            <td>${product.productid}</td>
                                            <td>${product.productname}</td>
                                            <td>${product.description}</td>
                                            <td>${product.quantity}</td>
                                            <td>${product.unitprice}</td>
                                            <td>${product.warehouseZoneID}</td>
                                            <td>
                                                <button type="button" class="btn btn-success edit-btn"
                                                        data-id="${product.productid}"
                                                        data-name="${product.productname}"
                                                        data-description="${product.description}"
                                                        data-quantity="${product.quantity}">Edit</button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!-- Modal for Editing Product -->
                        <div class="modal fade" id="editModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form id="editForm" method="post">
                                        <div class="modal-header bg-success">
                                            <h4 class="modal-title text-black">Edit Product</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" id="editId" name="id">
                                            <div class="form-group">
                                                <label for="editName">Product Name:</label>
                                                <input type="text" class="form-control" id="editName" name="name">
                                                <small id="editNameError" class="form-text text-danger" style="display: none;">Name cannot contain special characters</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="editDescriptions">Description:</label>
                                                <input type="text" class="form-control" id="editDescriptions" name="description">
                                                <small id="editDescriptionError" class="form-text text-danger" style="display: none;">Description cannot contain special characters</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="editQuantity">Quantity:</label>
                                                <input type="number" class="form-control" id="editQuantity" name="quantity" required readonly>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success">Save Changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
                <script>
                    $(document).ready(function () {
                        // Edit product functionality
                        $(document).on('click', '.edit-btn', function () {
                            const id = $(this).data('id');
                            const name = $(this).data('name');
                            const description = $(this).data('description');
                            const quantity = $(this).data('quantity');

                            $('#editModal').modal('show');
                            $('#editId').val(id);
                            $('#editName').val(name);
                            $('#editDescriptions').val(description);
                            $('#editQuantity').val(quantity);
                        });

                        // Edit product form submission
                        $('#editForm').on('submit', function (event) {
                            event.preventDefault();

                            const id = $('#editId').val();
                            const name = $('#editName').val();
                            const description = $('#editDescriptions').val();
                            const quantity = $('#editQuantity').val();

                            let errorMessage = '';

                            // Validate name and description - allow only alphanumeric characters and spaces
                            if (!/^[a-zA-Z0-9\s]+$/.test(name)) {
                                errorMessage = 'Name cannot contain special characters';
                                $('#editNameError').text(errorMessage).show(); // Show error message
                                $('#editDescriptionError').hide(); // Hide error message if description is valid
                                return; // Prevent form submission if validation fails
                            } else {
                                $('#editNameError').hide(); // Hide error message if validation passes
                            }

                            if (!/^[a-zA-Z0-9\s]+$/.test(description)) {
                                errorMessage = 'Description cannot contain special characters';
                                $('#editDescriptionError').text(errorMessage).show(); // Show error message
                                $('#editNameError').hide(); // Hide error message if name is valid
                                return; // Prevent form submission if validation fails
                            } else {
                                $('#editDescriptionError').hide(); // Hide error message if validation passes
                            }

                            // AJAX request to update product
                            $.ajax({
                                url: 'productPartner', // Adjust URL as per your server-side endpoint
                                type: 'POST',
                                data: {
                                    action: 'editProduct',
                                    id: id,
                                    name: name,
                                    description: description,
                                    quantity: quantity
                                },
                                success: function (response) {
                                    $('#editModal').modal('hide'); // Hide modal on success
                                    location.reload(); // Reload page or update UI as needed
                                },
                                error: function (error) {
                                    alert('Failed to edit product.'); // Handle error
                                }
                            });
                        });

                        // Clear error message when editing name or description
                        $('#editName, #editDescriptions').on('input', function () {
                            $(this).next('.error').hide();
                        });

                        // Handle search input keyup event for live search
                        $('#searchInput').on('keyup', function () {
                            var filter = $(this).val().toUpperCase();
                            $('table tbody tr').each(function () {
                                var found = false;
                                $(this).find('td').each(function () {
                                    if ($(this).text().toUpperCase().indexOf(filter) > -1) {
                                        found = true;
                                        return false;
                                    }
                                });
                                $(this).toggle(found);
                            });
                        });

                        // Handle sort change event
                        $('#sort').on('change', function () {
                            var sortBy = $(this).val();
                            var rows = $('table tbody tr').get();

                            rows.sort(function (a, b) {
                                var A = $(a).find('td:eq(' + getColumnIndex(sortBy) + ')').text().toUpperCase();
                                var B = $(b).find('td:eq(' + getColumnIndex(sortBy) + ')').text().toUpperCase();

                                if (sortBy === 'name-asc') {
                                    return (A < B) ? -1 : (A > B) ? 1 : 0;
                                } else if (sortBy === 'name-desc') {
                                    return (A > B) ? -1 : (A < B) ? 1 : 0;
                                } else if (sortBy === 'quantity' || sortBy === 'unitprice') {
                                    return parseFloat(A) - parseFloat(B);
                                }
                            });

                            $.each(rows, function (index, row) {
                                $('table').children('tbody').append(row);
                            });
                        });

                        // Function to get column index based on sort criteria
                        function getColumnIndex(sortBy) {
                            if (sortBy === 'quantity') {
                                return 3;
                            } else if (sortBy === 'unitprice') {
                                return 4;
                            } else if (sortBy === 'name-asc' || sortBy === 'name-desc') {
                                return 1;
                            }
                            return 0;
                        }
                    });
                </script>
            </div>
        </div>
    </body>
                    </html>

