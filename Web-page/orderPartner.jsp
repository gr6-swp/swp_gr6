<%-- 
    Document   : partner
    Created on : May 30, 2024, 11:08:28 PM
    Author     : X P S
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Trang chủ</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="assets/images/favicon.png" />
        <style>
            .order-table {
                border-collapse: collapse;
                width: 10%;
            }

            .order-table th,
            .order-table td {
                border: 1px solid #ddd;
                padding: 8px;
            }

            .order-table tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            .order-table tr:hover {
                background-color: #ddd;
            }

            .order-table th {
                background-color: #4CAF50;
                color: white;
            }

            body {
                font-family: Arial, sans-serif;
            }
            h2 {
                margin-bottom: 10px;
            }

            /* Kiểu dáng cho nút */
            button {
                background-color: #4CAF50; /* Màu xanh lá cây */
                color: white;
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Kiểu dáng cho ô tìm kiếm và select */
            input[type="text"], input[type="number"], select {
                padding: 8px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 4px;
                padding: 10px;
                border-radius: 5px; /* Bo góc */
                width: 170px; /* Điều chỉnh độ rộng tùy ý */
            }


            /* Kiểu dáng cho bảng */
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 15px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }



            button {
                /* ... (các kiểu dáng khác của nút) */
                margin-left: 5px; /* Tạo khoảng cách giữa nút và ô tìm kiếm */
            }
            #container {
                border: 2px solid #44ce42; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px; /* Bo góc (tùy chọn) */
            }

            .popup {
                display: none; /* Ẩn popup ban đầu */
                position: fixed; /* Ở vị trí cố định so với viewport */
                z-index: 1; /* Đảm bảo popup nằm trên các phần tử khác */
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: auto;
                background-color: rgba(0, 0, 0, 0.4); /* Màu nền mờ */
            }

            .popup-content {
                background-color: #fefefe;
                margin: 10% auto; /* Căn giữa popup */
                padding: 17px;
                border: 3px solid #888;
                width: 73%;
                max-width: 833px; /* Giới hạn chiều rộng tối đa */
                min-height: 453px;
                border-radius: 10px;
                margin-left: 28%;
            }

            .close-btn {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close-btn:hover,
            .close-btn:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 15px; /* Tăng padding cho các ô */
                text-align: left;
            }

            body {
                font-family: sans-serif;
            }

            button {
                padding: 10px;
                background-color: #4CAF50;
                color: white;
                border: none;
                cursor: pointer;
            }
            input, select {
                width: 100%;
                padding: 8px;
                box-sizing: border-box;
            }
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }
            th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
            }
            th {
                background-color: #f4f4f4;
            }
            button {
                background: #28a745;
                color: #fff;
                border: none;
                padding: 10px 15px;
                cursor: pointer;
                border-radius: 5px;
            }

            .nav-link.active {
                background-color: #28a745 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>


        <%@ include file="sidebarPartner.jsp" %>




        <div style="margin-left: 5%; margin-top: 1%; margin-right: 0%; width: 71%">

            <ul class="nav nav-tabs" id="orderTabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#inbound" onclick="showTab('inbound')">Inbound</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#outbound" onclick="showTab('outbound')">Outbound</a>
                </li>
            </ul>

            <div id="inbound" class="tab-content">
                <div id="container">
                    <div class="search-container">
                        <button hef="inboundpart" type="button">Creat</button>
                        <input type="text" id="searchInput" placeholder="Tìm kiếm..." onkeydown="handleSearch(event)">
                        <button type="button" onclick="performSearch()">Search</button>
                    </div>


                    <div id="createPopup" class="popup">
                        <div class="popup-content">
                            <span class="close-btn" onclick="closePopup()">&times;</span>
                            <h2 style="color: #000">Create Order Inbound</h2>
                            <form id="createOrderForm" action="insertdonpart" method="POST">
                                <div id="container">
                                    <div>
                                        <label style="color: black" for="partnerID">ID Đối Tác:</label>
                                        <input type="number" id="partnerID" name="partnerID" required>
                                    </div>

                                    <div>
                                        <label style="color: black" for="orderType">Loại Đơn Hàng:</label>
                                        <select id="orderType" name="orderType" required>
                                            <option value="Nhập kho">Nhập kho</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label style="color: black" for="orderStatus">Trạng Thái Đơn Hàng:</label>
                                        <select id="orderStatus" name="orderStatus" required>
                                            <option value="Chờ phê duyệt">Chờ phê duyệt</option>
                                        </select>
                                    </div>
                                    <h2 style="color: black">Chi Tiết Đơn</h2>
                                    <table class="order-table">
                                        <thead>
                                            <tr style="color:black">
                                                <th>Product Name</th>
                                                <th>Description</th>
                                                <th>Quantity</th>
                                                <th>UnitPrice</th>
                                            </tr>
                                        </thead>
                                        <tbody id="orderDetails">
                                            <tr>
                                                <td>
                                                    <input type="text" id="ProductName" name="ProductName" required>
                                                </td>
                                                <td>
                                                    <input type="text" id="Description" name="Description" required>
                                                </td>
                                                <td>
                                                    <input type="number" id="Quantity" name="Quantity" required>
                                                </td>
                                                <td>
                                                    <input type="number" id="UnitPrice" name="UnitPrice" required>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button type="submit">Create</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <table style="color: black;">
                        <thead>
                            <tr>
                                <th>ID Partner</th>
                                <th>ID Product</th>
                                <th>Product Name</th>
                                <th>Description</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Time</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${product}" var="o">
                                <c:if test="${o.orderType == 'Nhập kho'}">
                                    <tr>
                                        <th>${o.partnerID}</th>
                                        <th>${o.productID}</th>
                                        <td>${o.productName}</td>
                                        <td>${o.description}</td>
                                        <td>${o.quantity}</td>
                                        <td>${o.unitPrice}</td>
                                        <td>${o.createdAt}</td>
                                        <td>${o.orderStatus}</td>
                                        <td>
                                            <c:if test="${o.orderStatus eq 'Hoàn thành'}">
                                                <button class="openExportPopupBtn" type="button" data-partner-id="${o.partnerID}" 
                                                        data-product-id="${o.productID}" data-product-name="${o.productName}"
                                                        data-description="${o.description}" data-quantity="${o.quantity}"
                                                        data-unit-price="${o.unitPrice}">Export</button>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                        </tbody>
                    </table>

                    <!-- Màn hình popup xác nhận outbound -->
                    <div id="exportPopup" class="popup" style="display: none;">
                        <div class="popup-content" style="width: 30%; margin-left: 606px; border: 20px solid #28a745;">
                            <span class="close-btn" onclick="closeExportPopup()">&times;</span>
                            <h2 style="color: #000">EXPORT PRODUCT</h2>
                            <img src="https://png.pngtree.com/png-clipart/20230925/original/pngtree-free-shipping-green-delivery-van-icon-for-your-business-vector-png-image_12858455.png" alt="Export Product Image" style="width: 87%; margin-bottom: -20px;">
                            <p style="font-size:30px; color: black">Are you sure you want to export this order?</p>
                            <form id="exportForm" action="exportpart" method="POST">
                                <input type="hidden" id="partnerIDExport" name="partnerID"> 
                                <input type="hidden" id="productIDExport" name="productID">
                                <input type="hidden" id="orderTypeExport" name="orderType" value="Xuất kho">
                                <input type="hidden" id="orderStatusExport" name="orderStatus" value="Chờ phê duyệt">
                                <input type="hidden" id="productNameExport" name="productName">
                                <input type="hidden" id="descriptionExport" name="description">
                                <input type="hidden" id="quantityExport" name="quantity">
                                <input type="hidden" id="unitPriceExport" name="unitPrice">
                                <button type="submit" style="margin-left: 115px; margin-top: 20px;">Yes</button>
                                <button type="button" onclick="closeExportPopup()">No</button>
                            </form>
                        </div>
                    </div>


                </div>
            </div>




            <div id="outbound" class="tab-content" style="display: none;">
                <div id="container">
                    <div class="search-container">                          
                        <input type="text" id="searchInput" placeholder="Tìm kiếm..." onkeydown="handleSearch(event)">
                        <button type="button" onclick="performSearch()">Search</button>
                    </div>


                    <table style="color: black;">
                        <thead>
                            <tr>
                                <th>ID Partner</th>
                                <th>ID Product</th>
                                <th>Product Name</th>
                                <th>Description</th>
                                <th>Quantity</th>
                                <th>Unit Price</th> 
                                <th>Time</th>  
                                <th>Status</th>


                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${product}" var="o">
                                <c:if test="${o.orderType == 'Xuất kho'}">
                                    <tr>
                                        <th>${o.partnerID}</th>
                                        <th>${o.productID}</th>
                                        <td>${o.productName}</td>
                                        <td>${o.description}</td>
                                        <td>${o.quantity}</td>
                                        <td>${o.unitPrice}</td>
                                        <td>${o.createdAt}</td>
                                        <td>${o.orderStatus}</td>


                                    </tr>
                                </c:if>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

            <script>
                function showTab(tabName) {
                    var tabs = document.querySelectorAll('.tab-content');
                    tabs.forEach(function (tab) {
                        if (tab.id === tabName) {
                            tab.style.display = 'block';
                        } else {
                            tab.style.display = 'none';
                        }
                    });
                }
            </script>


            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    // Các biến cho popup Create
                    var createPopup = document.getElementById("createPopup");
                    var createButton = document.querySelector(".search-container button"); // Nút Create
                    var closeButton = document.querySelector(".close-btn"); // Nút đóng popup Create

                    // Các biến cho popup Export
                    const openExportPopupBtns = document.querySelectorAll('.openExportPopupBtn');
                    const exportPopup = document.getElementById('exportPopup');
                    const exportForm = document.getElementById('exportForm');
                    const partnerIDExport = document.getElementById('partnerIDExport');
                    const productIDExport = document.getElementById('productIDExport');
                    const productNameExport = document.getElementById('productNameExport');
                    const descriptionExport = document.getElementById('descriptionExport');
                    const quantityExport = document.getElementById('quantityExport');
                    const unitPriceExport = document.getElementById('unitPriceExport');
                    const orderTypeExport = document.getElementById('orderTypeExport');
                    const orderStatusExport = document.getElementById('orderStatusExport');
                    // ... (các trường khác trong form export) ...

                    // Xử lý sự kiện click nút Create
                    createButton.addEventListener('click', function () {
                        createPopup.style.display = "block";
                    });

                    // Xử lý sự kiện click nút đóng popup Create
                    closeButton.onclick = function () {
                        createPopup.style.display = "none";
                    }

                    // Xử lý sự kiện click bên ngoài popup Create
                    window.onclick = function (event) {
                        if (event.target == createPopup) {
                            createPopup.style.display = "none";
                        }
                    }

                    // Xử lý sự kiện click các nút Export
                    openExportPopupBtns.forEach(btn => {
                        btn.addEventListener('click', function () {
                            partnerIDExport.value = this.dataset.partnerId;
                            productIDExport.value = this.dataset.productId;
                            productNameExport.value = this.dataset.productName;
                            descriptionExport.value = this.dataset.description;
                            quantityExport.value = this.dataset.quantity;
                            unitPriceExport.value = this.dataset.unitPrice;
                            // ... (gán các giá trị khác từ dataset) ...

                            exportPopup.style.display = 'block'; // Mở popup export
                        });
                    });

                    // Xử lý sự kiện submit form Export
                    exportForm.addEventListener('submit', function (event) {
                        // Xử lý logic submit form export ở đây
                        // Ví dụ: gửi dữ liệu đơn hàng đến server để xử lý xuất kho
                        // ...

                        closeExportPopup(); // Đóng popup sau khi submit
                    });

                    // Xử lý tìm kiếm
                    function handleSearch(event) {
                        if (event.key === "Enter") {
                            performSearch(); // Bạn cần định nghĩa hàm performSearch() để thực hiện tìm kiếm
                        }
                    }

                    // Các hàm khác
                    function closeExportPopup() {
                        exportPopup.style.display = 'none';
                    }

                    function initializeProductRow() {
                        let orderDetails = document.getElementById("orderDetails");
                        orderDetails.innerHTML = "";
                        addProductRow(orderDetails);
                    }

                    function addProductRow(orderDetails) {
                        let newRow = document.createElement('tr');
                        newRow.classList.add("product-row");
                        newRow.dataset.productIndex = productIndex;

                        let nameCell = document.createElement('td');
                        let descriptionCell = document.createElement('td');
                        let priceCell = document.createElement('td');

                        nameCell.innerHTML = '<input type="text" class="form-control" name="productName[]">';
                        descriptionCell.innerHTML = '<input type="text" class="form-control" name="productDescription[]">';
                        priceCell.innerHTML = '<input type="number" class="form-control" name="productPrice[]">';

                        newRow.appendChild(nameCell);
                        newRow.appendChild(descriptionCell);
                        newRow.appendChild(priceCell);
                        orderDetails.appendChild(newRow);

                        productIndex++;
                    }
                });

            </script>
        </div>

    </body>
</html>

