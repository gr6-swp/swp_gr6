<%-- 
    Document   : trogiup
    Created on : May 31, 2024, 2:00:04 PM
    Author     : X P S
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi">
    <head>
        <link rel="preconnect" href="https://www.google.com" >
        <link rel="preconnect" href="https://www.facebook.com" >
        <link rel="preconnect" href="https://www.googletagmanager.com" >
        <link rel="preconnect" href="https://www.google-analytics.com" >
        <link rel="preconnect" href="https://hotjar.com" >
        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-368497342"></script>
        <script> window.dataLayer = window.dataLayer || []; function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', 'AW-368497342');</script>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=AW-368497342"  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];w[l].push({'gtm.start':
                        new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-545R7ZM');</script>

    <script type="text/javascript">
        var clevertap = {event: [], profile: [], account: [], onUserLogin: [], notifications: [], privacy: []};
        // replace with the CLEVERTAP_ACCOUNT_ID with the actual ACCOUNT ID value from your Dashboard -> Settings page
        clevertap.account.push({"id": "RZ9-W57-576Z"});
        clevertap.privacy.push({optOut: false}); //set the flag to true, if the user of the device opts out of sharing their data
        clevertap.privacy.push({useIP: false}); //set the flag to true, if the user agrees to share their IP data
        (function ()
        {
            var wzrk = document.createElement('script');
            wzrk.type = 'text/javascript';
            wzrk.async = true;
            wzrk.src = ('https:' == document.location.protocol ? 'https://d2r1yp2w7bby2u.cloudfront.net' : 'http://static.clevertap.com') + '/js/clevertap.min.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wzrk, s);
        }
        )();
    </script>
    <title>
        Phương án vay vốn sản xuất kinh doanh hiện nay | Cẩm nang tài chính | Tin tức | Ngân hàng TMCP Đại Chúng Việt Nam - PVcomBank
    </title>
    <base href="https://www.pvcombank.com.vn/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1"/>
    <meta name="keywords" content="Phương án vay vốn sản xuất kinh doanh hiện nay"/>
    <meta property="og:title" content="Phương án vay vốn sản xuất kinh doanh hiện nay | Cẩm nang tài chính | Tin tức | Ngân hàng TMCP Đại Chúng Việt Nam - PVcomBank">

    <meta name="description" content="Bài viết sau đây của PVcomBank sẽ chia sẻ mẫu phương án vay vốn kinh doanh hiện nay và những yếu tố cần có để vay vốn."/>
    <meta property="og:description" content="Bài viết sau đây của PVcomBank sẽ chia sẻ mẫu phương án vay vốn kinh doanh hiện nay và những yếu tố cần có để vay vốn.">

    <meta property="og:image" content="https://www.pvcombank.com.vn/upload/images/news/hacC2Qyo1Bphuong-an-vay-von-san-xuat-kinh-doanh-1.jpg">
    <meta name="twitter:image" content="https://www.pvcombank.com.vn/upload/images/news/hacC2Qyo1Bphuong-an-vay-von-san-xuat-kinh-doanh-1.jpg">

    <link rel="icon" href="https://www.pvcombank.com.vn/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="https://www.pvcombank.com.vn/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="https://www.pvcombank.com.vn/ddl/styles.css" type='text/css'>
    <link rel="stylesheet" href="https://www.pvcombank.com.vn/ddl/styles-print.css" type='text/css' media="print">
    <link rel="stylesheet" href="https://www.pvcombank.com.vn/ddl/styles-tab.css" type='text/css' media="screen and (min-width: 768px)">
    <link rel="stylesheet" href="https://www.pvcombank.com.vn/ddl/styles-des.css" type='text/css' media="screen and (min-width: 992px)">
    <link rel="stylesheet" href="https://www.pvcombank.com.vn/css/pixel/style.css" type='text/css'>
    <link rel="stylesheet" href="https://www.pvcombank.com.vn/css/slick/slick.css" type='text/css'>
    <style>
        .hover-menu-right {
            position: relative;

        }
        .hover-menu-right > .dropdown-menu {
            position: absolute;
            top: 100%;
            left: 0;
            width: 100%;
            min-width: 200px;
            max-height: 0;
            overflow-y: hidden;
            transition: all .3s;
            background-color: #fff;
            opacity: .3;
        }
        .hover-menu-right:hover > .dropdown-menu {
            max-height: 90px;
            opacity: 1;
        }
        .click-menu-mobile > .dropdown-menu {
            max-height: 0;
            overflow-y: hidden;
            transition: all .3s;
            background-color: #fff;
            opacity: .3;
        }
        .click-menu-mobile > .dropdown-menu.show {
            max-height: 120px;
            opacity: 1;
        }
        .dropdown-menu a.button-text {
            text-decoration: none;
            padding: 10px 16px;
            color: var(--color-secondary) !important;
            box-shadow: 0px -1px 0px rgba(13, 34, 64, 0.05);
            transition: all .3s;
            display: flex;
        }
        .click-menu-mobile > .dropdown-menu a.button-text {
            background-color: #EFEBE2;
            padding: 16px 28px;
        }
        .click-menu-mobile > .dropdown-menu .button-text.button-text--has-icon-left.button-text--sm .icon {
            margin-right: 20px;
        }
        .dropdown-menu .button-text:hover {
            background-color: #EFEBE2;
        }
        .dropdown-menu .button-text:hover > span {
            text-decoration: none;
        }
    </style>
    <style>
        .card--description {
            display: -webkit-box !important;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .card--description>p {
            margin: 0;
        }
        .description-news {
            color: var(--color-bk);
            font-size: 1.4rem;
        }
    </style>
    <style>
        .card--description {
            display: -webkit-box !important;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .card--description>p {
            margin: 0;
        }
        .description-news {
            color: var(--color-bk);
            font-size: 1.4rem;
        }
    </style>
    <style>
        .card--description {
            display: -webkit-box !important;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .card--description>p {
            margin: 0;
        }
        .description-news {
            color: var(--color-bk);
            font-size: 1.4rem;
        }
    </style>

</head>
<body class="bg-gr-10">
    <header id="frontend-header" class="bg-wh frontend-header-home container p-0 box-shadow--lg">
        <div class="container p-0 bg-wh">
            <div id="frontend-header-wrapper" class="position-relative d-flex justify-content-between align-items-center pl-lg-3 pl-xl-5">
                <button class="button-toggle-nav-mobile d-lg-none" type="button" data-toggle="nav-mobile-container" onclick="openNavMobile(this)">
                    <i class="icon icon-menu--bk"></i>
                </button>

                <a id="frontend-logo" href="https://www.pvcombank.com.vn?reload=1717139800" class="logo pvc--original mr-0 mr-lg-4"></a>

                <a href="tel:1900 5555 92" class="button-text button-text--sm d-block d-lg-none mr-2">
                    <div class="h8 text--b text--le">1900 5555 92</div>
                </a>
                <div class="head-group d-none d-md-none d-lg-block ml-auto">
                    <div id="top-head">
                        <div id="top-head-wrapper">
                            <span>
                                <a href="http://facebook.com/PVcomBankFanpage" target="_blank" class="fb-link"></a>
                            </span>
                            <ul class="w-100 d-flex align-items-center border-bottom">
                                <li class="d-flex align-items-center mr-5">
                                    <a href="tel:1900 5555 92" class="d-flex header__hotline">
                                        <i class="icon icon-call-phone--bl mr-lg-half"></i>
                                        <div class="h9 text--b text--color-bl">1900&nbsp;5555&nbsp;92</div>
                                    </a>
                                    <a href="tel:1900 5555 92"></a>
                                </li>
                                <li class="mr-3 d-flex">
                                    <a class="h9" href=" https://www.pvcombank.com.vn/gioi-thieu-ve-pvcombank">
                                        Về chúng tôi
                                    </a>
                                </li>
                                <li class="mr-3 d-flex">
                                    <a class="h9" href="https://www.pvcombank.com.vn/tin-tuc">
                                        Tin tức
                                    </a>
                                </li>
                                <li class="mr-3 d-flex">
                                    <a class="h9" href="https://www.pvcombank.com.vn/co-dong.html">
                                        Quan hệ cổ đông
                                    </a>
                                </li>    
                                <li class="mr-3 d-flex">
                                    <a class="h9" href="https://www.pvcombank.com.vn/mang-luoi">
                                        ATM & Chi nhánh
                                    </a>
                                </li>
                                <li class="mr-3 d-flex">
                                    <a class="h9" href="https://www.pvcombank.com.vn/bieu-lai-suat">
                                        Lãi suất
                                    </a>
                                </li>
                                <li class="mr-3 d-flex">
                                    <a class="h9"  href="https://www.pvcombank.com.vn/ti-gia-ngoai-te">
                                        Tỷ giá
                                    </a>
                                </li>
                                <li class="mr-3 d-flex">
                                    <a class="h9" href="http://tuyendung.pvcombank.com.vn">
                                        Tuyển dụng
                                    </a>
                                </li>
                                <li class="ml-auto"></li>
                                <li id="menu-language" class="ml-auto">
                                    <select id="menu-language-select" autocomplete="off"
                                            class="menu-language-select form-control form-control--sm border-left">
                                        <option value="en" >EN</option>
                                        <option value="vi" selected>VI</option>
                                    </select>
                                </li>
                                <li class="p-1 border-left d-flex align-items-center search-box" id="search-box">
                                    <a href="/tim-kiem.html">
                                        <i class="icon icon-search-find-magnifier--bk"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <ul id="main-menu" class="d-flex align-items-center">

                        <li class="dropdown-cat mr-3" id="beau-person">
                            <a class="h6 text--b d-flex dropdown__item align-items-center">
                                Cá nhân
                                <i class="icon icon-arrow-down--bl icon--size-3"></i>
                            </a>
                            <div class="mega-dropdown-menu box-shadow--lg" id="person-drop">
                                <div class="divider-h divider-h--gr"></div>
                                <div class="mega-dropdown-menu-wrapper d-lg-flex">
                                    <div class="flex-grow-1 d-flex flex-wrap">
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/dich-vu-the" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-credit-card--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Dịch vụ Thẻ</h6>
                                        </a>
                                        <div class="divider-v divider-v--gr"></div>
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/san-pham-vay" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-money-stack--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Sản phẩm Vay</h6>
                                        </a>
                                        <div class="divider-v divider-v--gr"></div>
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/tiet-kiem" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-bank-locked-secure--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Tiết kiệm</h6>
                                        </a>
                                        <div class="divider-h divider-h--gr"></div>
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/ngan-hang-so" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-computer-mobile-phone--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Ngân hàng số</h6>
                                        </a>
                                        <div class="divider-v divider-v--gr"></div>
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/tai-khoan" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-account-personal-secure--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Tài khoản</h6>
                                        </a>
                                        <div class="divider-v divider-v--gr"></div>
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/bao-hiem" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-insurance-shield--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Bảo hiểm</h6>
                                        </a>
                                    </div>
                                    <div class="menu-ads flex-shrink-0 pt-3 px-4 d-none d-lg-block">
                                        <img  src="/upload/images/ads/mt9zu2KDhU.jpg" srcset="/upload/images/ads/mt9zu2KDhU.jpg 1x, /upload/images/ads/mt9zu2KDhU@2x.jpg 2x" alt="PVOne<br>Tích điểm nhanh<br>Đổi quà chất">
                                        <h5 class="text--color-bl mb-2">PVOne<br>Tích điểm nhanh<br>Đổi quà chất</h5>
                                        <a href="/khuyen-mai/uu-dai-khac/chi-tiet/pvone-1-cho-tat-ca" class="button-solid button-solid--sm button-solid--on-dark">Tìm hiểu thêm</a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="dropdown-cat mr-3" id="beau-business">
                            <a class="h6 text--b d-flex dropdown__item align-items-center">
                                Doanh nghiệp
                                <i class="icon icon-arrow-down--bl icon--size-3"></i>
                            </a>
                            <div class="mega-dropdown-menu box-shadow--lg" id="business-drop">
                                <div class="divider-h divider-h--gr"></div>
                                <div class="mega-dropdown-menu-wrapper d-lg-flex">
                                    <div class="flex-grow-1 d-flex flex-wrap">
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/tin-dung-doanh-nghiep" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-money-coin-tower--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Tín dụng</h6>
                                        </a>
                                        <div class="divider-v divider-v--gr"></div>
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/tai-khoan-doanh-nghiep" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-account-business-secure--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Tài khoản</h6>
                                        </a>
                                        <div class="divider-v divider-v--gr"></div>
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/thanh-toan-trong-nuoc" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-bank-locked-secure--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Thanh toán trong nước</h6>
                                        </a>
                                        <div class="divider-h divider-h--gr"></div>
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/bao-lanh" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-bill-receipt--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Bảo lãnh</h6>
                                        </a>
                                        <div class="divider-v divider-v--gr"></div>
                                        <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/ngan-hang-giao-dich" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center">
                                            <i class="icon icon-bank-international--color-multi mb-1"></i>
                                            <h6 class="text--b text--mi text--color-bk">Thanh toán Quốc tế</h6>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </li>
                        <li class="dropdown-cat mr-3" id="beau-premier">
                            <a class="h6 text--b d-flex dropdown__item align-items-center">
                                PVcomBank Premier
                                <i class="icon icon-arrow-down--bl icon--size-3"></i>
                            </a>
                            <div class="mega-dropdown-menu box-shadow--lg" id="premier-drop">
                                <div class="divider-h divider-h--gr"></div>
                                <div class="mega-dropdown-menu-wrapper d-lg-flex">
                                    <div class="flex-grow-1 d-flex flex-wrap">
                                        <a href="https://premier.pvcombank.com.vn/" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center  main-premier-item ">
                                            <i class="icon logo-pvc-premier mb-1"></i>
                                            <div class="text--b text--mi text--color-wh  d-none ">Premier</div>
                                        </a>
                                        <div class="divider-v divider-v--wh"></div>
                                        <a href="https://premier.pvcombank.com.vn/vi/giai-phap-gia-tang-tai-san" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center ">
                                            <i class="icon icon-presentation mb-1"></i>
                                            <div class="text--b text--mi text--color-wh ">Giải pháp Gia tăng tài sản</div>
                                        </a>
                                        <div class="divider-v divider-v--wh"></div>
                                        <a href="https://premier.pvcombank.com.vn/vi/giai-phap-chu-doanh-nghiep" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center ">
                                            <i class="icon icon-business-user-prem mb-1"></i>
                                            <div class="text--b text--mi text--color-wh ">Đặc quyền Chủ doanh nghiệp</div>
                                        </a>
                                        <div class="divider-v divider-v--wh"></div>
                                        <a href="https://premier.pvcombank.com.vn/vi/giai-phap-tich-luy-ben-vung" class="mega-dropdown-menu__category pt-3 pb-4 d-flex flex-column justify-content-center align-items-center ">
                                            <i class="icon icon-pig-saving mb-1"></i>
                                            <div class="text--b text--mi text--color-wh ">Giải pháp Tích lũy bền vững</div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </li>
                        <li class="mr-3 d-none divider--vertical"></li>
                        <li class="border-left ml-auto">
                            <div class="hover-menu-right" id="main-menu__item-group-1">
                                <a href="javascript:void(0)" class="button-text button-text--lg button-text--has-icon-left login-button">
                                    <div class="h8 text--le text--b"><span class="optional">Đăng nhập </span>Ngân hàng trực tuyến</div>
                                    <i class="icon icon-lock-secured--color-multi"></i>
                                </a>
                                <div class="dropdown-menu">
                                    <a href="https://digitalbanking.pvcombank.com.vn/"
                                       class="button-text button-text--sm button-text--has-icon-left">
                                        <span>Cá nhân</span>
                                        <i class="icon icon-arrow-right--bl"></i>
                                    </a>
                                    <a href="https://ibanking.pvcombank.com.vn/WebIB/MainIB"
                                       class="button-text button-text--sm button-text--has-icon-left">
                                        <span>Doanh nghiệp</span>
                                        <i class="icon icon-arrow-right--bl"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="d-none" id="main-menu__item-group-2">
                                <a href="tel:1900 5555 92" class="d-flex align-items-center mr-2 header__hotline">
                                    <i class="icon icon-call-phone--bk mr-lg-half"></i>
                                    <div class="h9 text--b text--color-bk">1900 5555 92</div>
                                </a>
                                <div class="border-left">
                                    <div class="hover-menu-right d-flex align-items-center h-100">
                                        <a href="javascript:void(0)" class="login-button p-1 d-flex"> <i class="icon icon-lock-secured--color-multi"></i></a>
                                        <div class="dropdown-menu">
                                            <a href="https://digitalbanking.pvcombank.com.vn/"
                                               class="button-text button-text--sm button-text--has-icon-left">
                                                <span>Cá nhân</span>
                                                <i class="icon icon-arrow-right--bl"></i>
                                            </a>
                                            <a href="https://ibanking.pvcombank.com.vn/WebIB/MainIB"
                                               class="button-text button-text--sm button-text--has-icon-left">
                                                <span>Doanh nghiệp</span>
                                                <i class="icon icon-arrow-right--bl"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div id="menu-language-2" class="border-left d-flex">
                                    <select id="menu-language-select-2" autocomplete="off"
                                            class="menu-language-select form-control form-control--sm">
                                        <option value="en" >EN</option>
                                        <option value="vi" selected>VI</option>
                                    </select>
                                </div>
                                <div id="search-box" class="border-left border-right px-1 d-flex ml-half search-box d-flex align-items-center">
                                    <a href="/tim-kiem.html" class="m-half">
                                        <i class="icon icon-search-find-magnifier--bk"></i>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>



    <div id="nav-mobile-container" class="nav-mobile-container d-none align-items-start collapsed">


        <button class="btn-close-nav-mobile p-1 bg-wh" data-toggle="nav-mobile-container" onclick="closeNavMobile(this)">
            <i class="icon icon-close-x--bk"></i>
        </button>


        <div class="nav-mobile-backdrop bg-gr-60" data-toggle="nav-mobile-container"></div>

        <div class="nav-mobile">

            <div class="nav-mobile__header d-flex justify-content-between bg-ye">
                <a href="/" class="logo pvc--on-yellow ml-3 mt-2 mb-3"></a>
                <a href="/" class="logo pvc--mobile-menu"></a>
            </div>

            <ul class="nav-mobile__menu pb-6">
                <li class="nav-mobile__menu-item click-menu-mobile">
                    <a href="javascript:void(0)" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-lock-secured--color-multi mr-2"></i>
                        Đăng nhập Ngân hàng trực tuyến
                    </a>
                    <div class="divider-h divider-h--gr"></div>

                    <div class="dropdown-menu">
                        <a href="https://digitalbanking.pvcombank.com.vn/"
                           class="button-text button-text--sm button-text--has-icon-left">
                            <span>Cá nhân</span>
                            <i class="icon icon-arrow-right--bl"></i>
                        </a>
                        <a href="https://ibanking.pvcombank.com.vn/WebIB/MainIB"
                           class="button-text button-text--sm button-text--has-icon-left">
                            <span>Doanh nghiệp</span>
                            <i class="icon icon-arrow-right--bl"></i>
                        </a>
                    </div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="/" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-home--color-multi mr-2"></i>
                        Trang chủ
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>

                <div class="h8 text--b text--color-gr pl-4 pt-4 pb-1">CÁ NHÂN</div>
                <div class="divider-h divider-h--gr"></div>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/dich-vu-the"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-credit-card--color-multi mr-2"></i>
                        Dịch vụ Thẻ
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/san-pham-vay"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-money-stack--color-multi mr-2"></i>
                        Sản phẩm Vay
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/tiet-kiem"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-bank-locked-secure--color-multi mr-2"></i>
                        Tiết kiệm
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/ngan-hang-so"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-computer-mobile-phone--color-multi mr-2"></i>
                        Ngân hàng số
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/tai-khoan"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-account-personal-secure--color-multi mr-2"></i>
                        Tài khoản
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/bao-hiem"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-insurance-shield--color-multi mr-2"></i>
                        Bảo hiểm
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>

                <div class="h8 text--b text--color-gr pl-4 pt-4 pb-1">DOANH NGHIỆP</div>
                <div class="divider-h divider-h--gr"></div>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/tin-dung-doanh-nghiep"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-money-coin-tower--color-multi mr-2"></i>
                        Tín dụng
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/tai-khoan-doanh-nghiep"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-account-business-secure--color-multi mr-2"></i>
                        Tài khoản
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/thanh-toan-trong-nuoc"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-bank-locked-secure--color-multi mr-2"></i>
                        Thanh toán trong nước
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/bao-lanh"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-bill-receipt--color-multi mr-2"></i>
                        Bảo lãnh
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/ngan-hang-giao-dich"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-bank-international--color-multi mr-2"></i>
                        Thanh toán Quốc tế
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <div class="h8 text--b text--color-da-bl pl-4 pt-4 pb-1">PVCOMBANK PREMIER</div>
                <div class="divider-h divider-h--gr"></div>
                <li class="nav-mobile__menu-item">
                    <a href="https://premier.pvcombank.com.vn/vi/giai-phap-gia-tang-tai-san" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-presentation mr-2"></i>
                        Giải pháp Gia tăng tài sản
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://premier.pvcombank.com.vn/vi/giai-phap-chu-doanh-nghiep" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-business-user-prem mr-2"></i>
                        Đặc quyền Chủ doanh nghiệp
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://premier.pvcombank.com.vn/vi/giai-phap-tich-luy-ben-vung" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-pig-saving mr-2"></i>
                        Giải pháp Tích lũy bền vững
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>

                <li class="nav-mobile__menu-item mt-5">
                    <div class="divider-h divider-h--gr"></div>
                    <a href="https://www.pvcombank.com.vn/lead_form_1?returnUrl=http://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/phuong-an-vay-von-san-xuat-kinh-doanh" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-mail-contact--color-multi mr-2"></i>
                        Đăng ký nhận tư vấn
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/mang-luoi" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-branch-building--color-multi mr-2"></i>
                        Tìm chi nhánh gần bạn
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/ti-gia-ngoai-te"
                       class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-earth-currency--color-multi mr-2"></i>
                        Tra cứu tỉ giá
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/bieu-lai-suat" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-calculator--color-multi mr-2"></i>
                        Biểu lãi suất
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item mb-5">
                    <a href="/khuyen-mai/uu-dai-wow" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        <i class="icon icon-gift-box--color-multi mr-2"></i>
                        Thế giới ưu đãi
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>

                <li class="nav-mobile__menu-item">
                    <div class="divider-h divider-h--gr"></div>
                    <a href="https://www.pvcombank.com.vn/gioi-thieu-ve-pvcombank" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        Về chúng tôi
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/tin-tuc" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        Tin tức
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="https://www.pvcombank.com.vn/co-dong.html" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        Quan hệ cổ đông
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item">
                    <a href="http://tuyendung.pvcombank.com.vn" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        Tuyển dụng
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
                <li class="nav-mobile__menu-item mb-4">
                    <a href="https://www.pvcombank.com.vn/lien-he" class="nav-mobile__menu-link d-flex align-items-center h8 text--b text--color-bk none-underline">
                        Liên hệ
                    </a>
                    <div class="divider-h divider-h--gr"></div>
                </li>
            </ul>
        </div>
    </div>


    <div id="main-banner" class="has-header-margin mt-lg-0"></div>

    <div class="container bg-wh mt-5 mt-lg-15">
        <div class="pb-6" id="frontend-wrapper">
            <!-- main -->
            <div id="tin-tuc-detail-wrapper" class="pb-3 mb-3 pt-2">
                <nav aria-label="breadcrumb">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="https://www.pvcombank.com.vn/tin-tuc">Tin tức</a></li>
                        <li class="breadcrumb-item pb-half mx-1 arrow">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.2034 4.2034C6.45373 3.95306 6.84764 3.93381 7.12007 4.14563L7.18549 4.2034L10.5188 7.53673C10.7692 7.78707 10.7884 8.18098 10.5766 8.4534L10.5188 8.51882L7.18549 11.8522C6.91429 12.1234 6.4746 12.1234 6.2034 11.8522C5.95306 11.6018 5.93381 11.2079 6.14563 10.9355L6.2034 10.8701L9.04578 8.02778L6.2034 5.18549C5.95306 4.93515 5.93381 4.54124 6.14563 4.26882L6.2034 4.2034Z" fill="#292525"/>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh">Cẩm nang tài chính</a>
                        </li>
                    </ul>
                </nav>
                <div id="tin-tuc-content">
                    <h3 class="d-none d-lg-block pb-7 pt-3 text--color-bl">Phương án vay vốn sản xuất kinh doanh hiện nay</h3>
                    <h4 class="d-block d-lg-none pb-7 pt-3 text--color-bl">Phương án vay vốn sản xuất kinh doanh hiện nay</h4>
                    <div class="divider-h divider-h--gr"></div>
                    <div class="py-3 row">
                        <div class="col-4 col-md-8 col-lg-6">
                            <div class="h9 text--b d-flex align-items-center"><i class="icon icon-calculator--bl icon--size-3 mr-1"></i>Ngày đăng: 04/04/2024</div>
                        </div>
                        <div class="col-4 col-md-8 col-lg-6">
                            <div class="h9 text--b d-flex align-items-center justify-content-lg-end mt-2 mt-lg-0 mt-md-2">
                                <span class="d-inline-flex">Chia sẻ:</span>

                                <a class="d-flex text-bk none-underline" href="https://www.facebook.com/sharer/sharer.php?u=https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/phuong-an-vay-von-san-xuat-kinh-doanh" target="_blank">
                                    <span class="d-inline-flex align-items-center ml-2"><img class="icon-social" src="https://www.pvcombank.com.vn/images/icon-social-facebook@2x.png" alt="icon-social-facebook
                                                                                             "></i>Facebook</span>
                                </a>
                                <div class="zalo-share-button d-flex ml-2 "
                                     data-href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/phuong-an-vay-von-san-xuat-kinh-doanh"
                                     data-oaid="110884890514649778"
                                     data-layout="1"
                                     data-color="blue"
                                     data-customize=true>
                                    <img class="icon-social" src="https://www.pvcombank.com.vn/images/icon-social-zalo@2x.png" alt="icon-social-zalo">
                                    <a href="javascript:void(0);"
                                       class="menu-item text-bk none-underline">Zalo</a>
                                </div>

                                <a class="d-flex text-bk none-underline" href="mailto:?subject=Phương án vay vốn sản xuất kinh doanh hiện nay&amp;body=https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/phuong-an-vay-von-san-xuat-kinh-doanh"
                                   title="Share by Email">
                                    <span class="d-inline-flex ml-2">
                                        <img class="icon-social" src="https://www.pvcombank.com.vn/images/icon-social-email@2x.png" alt="icon-social-zalo">Email</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="divider-h divider-h--gr"></div>
                    <div id="noi-dung-post" class="mt-3 h8">
                        <p style="text-align: justify;">Để c&oacute; thể r&uacute;t ngắn thời gian tiếp cận nguồn vốn vay từ ng&acirc;n h&agrave;ng, doanh nghiệp cần chuẩn bị đầy đủ hồ sơ, c&aacute;c yếu tố ch&iacute;nh như ph&aacute;p l&yacute;, mục đ&iacute;ch vay, năng lực tạo lợi nhuận, năng lực t&agrave;i ch&iacute;nh, t&agrave;i sản thế chấp v&agrave; hộ kinh doanh cần phải chuẩn bị kế hoạch hoặc phương &aacute;n vay vốn cụ thể theo quy định. C&ugrave;ng theo d&otilde;i b&agrave;i viết sau đ&acirc;y của PVcomBank để t&igrave;m hiểu về <strong>phương &aacute;n vay vốn sản xuất kinh doanh</strong> hiện nay.</p>
                        <h6 style="text-align: justify;"><strong>1. Những yếu tố cần c&oacute; trong phương &aacute;n vay vốn sản xuất kinh doanh</strong></h6>
                        <p style="text-align: justify;"><strong>1.1 Th&ocirc;ng tin ph&aacute;p l&yacute;</strong></p>
                        <p style="text-align: justify;">Để đảm bảo độ tin cậy của kh&aacute;ch h&agrave;ng trong qu&aacute; tr&igrave;nh vay vốn, doanh nghiệp cần cung cấp đầy đủ v&agrave; ch&iacute;nh x&aacute;c c&aacute;c th&ocirc;ng tin về c&ocirc;ng ty<strong> (</strong>địa chỉ c&ocirc;ng ty, giấy chứng nhận đăng k&yacute; kinh doanh, ng&agrave;nh nghề kinh doanh, th&ocirc;ng tin về chủ sở hữu, người đại diện theo ph&aacute;p luật,...), th&ocirc;ng tin về sản xuất, bộ m&aacute;y tổ chức (địa điểm sản xuất, thiết bị v&agrave; c&ocirc;ng nghệ ứng dụng trong sản xuất, dịch vụ, sơ đồ tổ chức v&agrave; vận h&agrave;nh dịch vụ, m&ocirc; tả bộ phận điều h&agrave;nh v&agrave; quản l&yacute; của doanh nghiệp).</p>
                        <p style="text-align: justify;">Xem th&ecirc;m: <a href="../../tin-tuc/cam-nang-tai-chinh/vay-von-kinh-doanh">Vay vốn kinh doanh</a> | Giải ph&aacute;p ph&aacute;t triển d&agrave;nh cho doanh nghiệp</p>
                        <p style="text-align: justify;"><strong>1.2 Mục đ&iacute;ch v&agrave; kế hoạch vay vốn</strong></p>
                        <p style="text-align: justify;">Để đảm bảo khả năng trả nợ của doanh nghiệp, bạn cần tr&igrave;nh b&agrave;y những th&ocirc;ng tin như: Hạn mức t&iacute;n dụng đề nghị, mục đ&iacute;ch sử dụng nguồn vốn, kế hoạch r&uacute;t vốn, phương thức giải ng&acirc;n, thời gian trả nợ,&hellip;</p>
                        <p style="text-align: justify;"><strong>1.3 Khả năng tạo lợi nhuận</strong></p>
                        <p style="text-align: justify;">Đ&acirc;y l&agrave; một trong những yếu tố quan trọng cần c&oacute; trong phương &aacute;n sản xuất kinh doanh. Để tăng độ tin cậy khi vay vốn, doanh nghiệp cần đảm bảo một số nội dung về nghi&ecirc;n cứu thị trường mục ti&ecirc;u; Khả năng cung cấp nguy&ecirc;n vật liệu/yếu tố đầu v&agrave;o để thực hiện kế hoạch kinh doanh; Doanh thu, chi ph&iacute; v&agrave; lợi nhuận dự kiến,...</p>
                        <p style="text-align: justify;"><strong>1.4 Khả năng t&agrave;i ch&iacute;nh</strong></p>
                        <p style="text-align: justify;">Hồ sơ t&agrave;i ch&iacute;nh bao gồm bảng c&acirc;n đối kế to&aacute;n, bảng b&aacute;o c&aacute;o kinh doanh, bảng lu&acirc;n chuyển tiền tệ v&agrave; c&aacute;c loại h&oacute;a đơn, sao k&ecirc;,...Bạn cần tr&igrave;nh b&agrave;y tối thiểu 3 năm gần nhất để ng&acirc;n h&agrave;ng đ&aacute;nh gi&aacute; kết quả kinh doanh của doanh nghiệp.</p>
                        <p style="text-align: justify;"><strong>1.5 T&agrave;i sản thế chấp</strong></p>
                        <p style="text-align: justify;">Doanh nghiệp cần cung cấp c&aacute;c giấy tờ về t&agrave;i sản thế chấp nếu tham gia g&oacute;i vay thế chấp. Nếu t&agrave;i sản l&agrave; đất đai, bạn cần cung cấp giấy chứng nhận quyền sử dụng đất.</p>
                        <p><em><img style="display: block; margin-left: auto; margin-right: auto;" src="http://www.pvcombank.com.vn/static/SEO/141 - Phương &aacute;n vay vốn sản xuất kinh doanh hiện nay/phuong-an-vay-von-san-xuat-kinh-doanh-1.jpg" alt="những yếu tố cần c&oacute; trong phương &aacute;n vay vốn sản xuất kinh doanh" width="90%" /></em></p>
                        <p style="text-align: center;"><em>Những yếu tố cần c&oacute; trong phương &aacute;n vay vốn sản xuất kinh doanh</em></p>
                        <h6 style="text-align: justify;"><strong>2. Mẫu phương &aacute;n vay vốn sản xuất kinh doanh hiện nay</strong></h6>
                        <p style="text-align: center;"><strong>CỘNG H&Ograve;A X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM<br /> &nbsp;Độc lập - Tự do - Hạnh ph&uacute;c<br /> &nbsp;---------------</strong></p>
                        <p style="text-align: center;"><strong>PHƯƠNG &Aacute;N VAY VỐN</strong></p>
                        <table width="592">
                            <tbody>
                                <tr>
                                    <td width="194">
                                        <p><strong>K&iacute;nh gửi:</strong></p>
                                    </td>
                                    <td width="398">
                                        <p>- &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..<br /> &nbsp;- Ng&acirc;n h&agrave;ng Ch&iacute;nh s&aacute;ch x&atilde; hội &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p><strong style="text-align: justify;">I, TH&Ocirc;NG TIN CỦA KH&Aacute;CH H&Agrave;NG</strong></p>
                        <ol style="text-align: justify;">
                            <li>T&ecirc;n cơ sở sản xuất kinh doanh:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</li>
                            <li>Họ v&agrave; t&ecirc;n người đại diện: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; Chức vụ: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</li>
                        </ol>
                        <p style="text-align: justify; padding-left: 30px;">- Chứng minh nh&acirc;n d&acirc;n/Hộ chiếu/Thẻ căn cước c&ocirc;ng d&acirc;n số:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <p style="text-align: justify; padding-left: 30px;">- Ng&agrave;y cấp: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; Nơi cấp:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <ol style="text-align: justify;" start="3">
                            <li>Giấy ủy quyền (nếu c&oacute;) số &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.. ng&agrave;y &hellip;/.../&hellip;&hellip; của&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</li>
                            <li>Địa chỉ: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</li>
                            <li>Điện thoại: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</li>
                            <li>M&atilde; số thuế: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</li>
                            <li>Giấy chứng nhận đăng k&yacute; doanh nghiệp/hợp t&aacute;c x&atilde;/hộ kinh doanh số: &hellip;&hellip;&hellip;&hellip;&hellip;.</li>
                        </ol>
                        <p style="text-align: justify; padding-left: 30px;">Do &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.. Cấp ng&agrave;y &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <ol style="text-align: justify;" start="8">
                            <li>Quyết định th&agrave;nh lập số<sup>1</sup>:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</li>
                            <li>Giấy ph&eacute;p đầu tư/Giấy chứng nhận đầu tư<sup>2</sup> số: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</li>
                        </ol>
                        <p style="text-align: justify; padding-left: 30px;">Do &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.. Cấp ng&agrave;y &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <ol style="text-align: justify;" start="10">
                            <li>Giấy ph&eacute;p kinh doanh/Giấy ph&eacute;p hoạt động/Chứng chỉ h&agrave;nh nghề<sup>3</sup> số: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. thời hạn c&ograve;n hiệu lực (th&aacute;ng, năm) &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</li>
                            <li>T&agrave;i khoản thanh to&aacute;n số: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. tại Ng&acirc;n h&agrave;ng &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</li>
                            <li>Vốn tự c&oacute;/vốn điều lệ/vốn g&oacute;p: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</li>
                        </ol>
                        <p><strong>II, TH&Ocirc;NG TIN LI&Ecirc;N QUAN ĐẾN HOẠT ĐỘNG SẢN XUẤT, KINH DOANH</strong></p>
                        <p style="padding-left: 30px;"><strong style="text-align: justify;">1. Kh&aacute;i qu&aacute;t t&igrave;nh h&igrave;nh sản xuất kinh doanh</strong></p>
                        <p style="padding-left: 30px;">a) Th&ocirc;ng tin chung về ng&agrave;nh nghề sản xuất kinh doanh: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="padding-left: 30px;">b) Hiện trạng cơ sở vật chất, kỹ thuật:</p>
                        <p style="text-align: justify; padding-left: 30px;">- Văn ph&ograve;ng, chi nh&aacute;nh, cửa h&agrave;ng kinh doanh (diện t&iacute;ch, địa chỉ):&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">- Nh&agrave; xưởng, kho b&atilde;i (số lượng, diện t&iacute;ch, địa chỉ, t&igrave;nh trạng hoạt động): &hellip;&hellip;&hellip;&hellip;.</p>
                        <p style="text-align: justify; padding-left: 30px;">- Trang thiết bị, phương tiện, m&aacute;y m&oacute;c (số lượng, gi&aacute; trị, t&igrave;nh trạng hoạt động): &hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">- Đất sản xuất (diện t&iacute;ch, địa chỉ): &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">- C&acirc;y trồng, vật nu&ocirc;i (số lượng, gi&aacute; trị): &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <ol style="text-align: justify;" start="2">
                            <li><strong> T&oacute;m tắt t&igrave;nh h&igrave;nh t&agrave;i ch&iacute;nh, hoạt động sản xuất kinh doanh<br /></strong>a) T&igrave;nh h&igrave;nh t&agrave;i ch&iacute;nh đến ng&agrave;y .... /.../20....:</li>
                        </ol>
                        <p style="text-align: justify; padding-left: 30px;">- Vốn chủ sở hữu: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">- Nợ phải thu: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">- H&agrave;ng tồn kho: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">- Nợ phải trả: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">Trong đ&oacute;, dư nợ vay tại c&aacute;c tổ chức t&iacute;n dụng: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..&hellip;&hellip;. đồng<br />b) Quan hệ t&iacute;n dụng với c&aacute;c tổ chức t&iacute;n dụng</p>
                        <p style="text-align: justify; padding-left: 30px;">- Quan hệ t&iacute;n dụng với Ng&acirc;n h&agrave;ng Ch&iacute;nh s&aacute;ch x&atilde; hội (NHCSXH)</p>
                        <p style="text-align: justify; padding-left: 30px;">Tổng dư nợ: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.. đồng, trong đ&oacute;: nợ qu&aacute; hạn: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. đồng. Cụ thể:</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Dư nợ chương tr&igrave;nh &hellip;&hellip;&hellip;&hellip;..: &hellip;&hellip;&hellip;&hellip;&hellip; đồng, trong đ&oacute;: nợ qu&aacute; hạn: &hellip;.. đồng;</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Dư nợ chương tr&igrave;nh &hellip;&hellip;&hellip;&hellip;..: &hellip;&hellip;&hellip;&hellip;&hellip; đồng, trong đ&oacute;: nợ qu&aacute; hạn: &hellip;.. đồng.</p>
                        <p style="text-align: justify; padding-left: 30px;">- Quan hệ t&iacute;n dụng với c&aacute;c tổ chức t&iacute;n dụng kh&aacute;c</p>
                        <table width="592">
                            <tbody>
                                <tr>
                                    <td rowspan="2" width="55">
                                        <p>STT</p>
                                    </td>
                                    <td rowspan="2" width="67">
                                        <p>T&ecirc;n Tổ chức t&iacute;n dụng</p>
                                    </td>
                                    <td rowspan="2" width="92">
                                        <p>H&igrave;nh thức cấp t&iacute;n dụng</p>
                                    </td>
                                    <td rowspan="2" width="85">
                                        <p>Mục đ&iacute;ch cấp t&iacute;n dụng</p>
                                    </td>
                                    <td colspan="2" width="116">
                                        <p>Dư nợ</p>
                                    </td>
                                    <td rowspan="2" width="79">
                                        <p>Nh&oacute;m nợ</p>
                                    </td>
                                    <td rowspan="2" width="98">
                                        <p>T&agrave;i sản bảo đảm (loại TSBĐ, gi&aacute; trị)</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="55">
                                        <p>VNĐ (trđ)</p>
                                    </td>
                                    <td width="61">
                                        <p>Ngoại tệ</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="55">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="67">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="92">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="85">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="55">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="61">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="79">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="98">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="55">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="67">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="92">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="85">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="55">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="61">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="79">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="98">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p style="padding-left: 30px;">c) Kết quả sản xuất kinh doanh của 02 năm trước thời điểm vay vốn (nếu c&oacute;)</p>
                        <p style="text-align: justify; padding-left: 30px;">- Năm &hellip;&hellip;&hellip;&hellip;:</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Tổng doanh thu: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Tổng chi ph&iacute;: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Thuế thu nhập doanh nghiệp: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Lợi nhuận: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">- Năm &hellip;&hellip;&hellip;&hellip;:</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Tổng doanh thu: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.. đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Tổng chi ph&iacute;: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Thuế thu nhập doanh nghiệp: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;...&hellip;&hellip;&hellip;&hellip;&hellip; đồng</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Lợi nhuận: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. đồng</p>
                        <ol style="text-align: justify;" start="3">
                            <li><strong> T&igrave;nh h&igrave;nh sử dụng lao động</strong></li>
                        </ol>
                        <p style="text-align: justify; padding-left: 30px;">Tổng số lao động hiện c&oacute;: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip; lao động, trong đ&oacute;: lao động l&agrave; người d&acirc;n tộc thiểu số l&agrave; &hellip;&hellip;&hellip;&hellip;&hellip;. người (chiếm &hellip;&hellip;&hellip;&hellip;..% tổng số lao động).</p>
                        <p style="text-align: justify;"><strong>III. TH&Ocirc;NG TIN VỀ PHƯƠNG &Aacute;N VAY VỐN V&Agrave; SỬ DỤNG VỐN VAY</strong></p>
                        <ol style="text-align: justify;">
                            <li><strong> T&ecirc;n phương &aacute;n vay vốn: </strong>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</li>
                        </ol>
                        <p style="text-align: justify; padding-left: 30px;">- Địa điểm thực hiện phương &aacute;n: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">- Thời gian thực hiện phương &aacute;n: từ th&aacute;ng &hellip;./20&hellip;&hellip; đến th&aacute;ng &hellip;./20&hellip;&hellip;</p>
                        <p style="text-align: justify; padding-left: 30px;">- Th&ocirc;ng tin Dự &aacute;n ph&aacute;t triển v&ugrave;ng trồng dược liệu qu&yacute;/ Dự &aacute;n trung t&acirc;m nh&acirc;n giống kh&aacute;ch h&agrave;ng tham gia (sau đ&acirc;y gọi tắt l&agrave; Dự &aacute;n):</p>
                        <p style="text-align: justify; padding-left: 30px;">+ T&ecirc;n Dự &aacute;n: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Địa điểm thực hiện: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Tổng vốn đầu tư: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Thời gian thực hiện:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <p style="text-align: justify; padding-left: 30px;">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <p style="text-align: justify; padding-left: 30px;">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <p style="text-align: justify; padding-left: 30px;">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <p style="text-align: justify; padding-left: 30px;">+ M&ocirc; tả sơ lược về Dự &aacute;n (sản phẩm, quy tr&igrave;nh, giải ph&aacute;p kỹ thuật, c&ocirc;ng nghệ, thị trường,....): &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Vai tr&ograve;, vị tr&iacute; của kh&aacute;ch h&agrave;ng trong Dự &aacute;n: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Cơ quan c&oacute; thẩm quyền ph&ecirc; duyệt: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
                        <ol style="text-align: justify;" start="2">
                            <li><strong> T&oacute;m tắt nội dung phương &aacute;n vay vốn v&agrave; sử dụng vốn vay<br /></strong>a) Phương &aacute;n vay vốn tham gia Dự &aacute;n dược liệu<sup>4</sup></li>
                        </ol>
                        <p style="text-align: justify; padding-left: 30px;">- Mở rộng, cải tạo nh&agrave; xưởng, kho b&atilde;i:</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Diện t&iacute;ch mở rộng, cải tạo: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Chi ph&iacute; mở rộng, cải tạo: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
                        <p style="text-align: justify; padding-left: 30px;">- Mở rộng, cải tạo đất sản xuất:</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Diện t&iacute;ch mở rộng, cải tạo: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Chi ph&iacute; mở rộng, cải tạo: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
                        <p style="text-align: justify; padding-left: 30px;">- Đầu tư trang thiết bị:</p>
                        <p style="text-align: justify; padding-left: 30px;">+ M&aacute;y m&oacute;c, thiết bị (chủng loại, số lượng, gi&aacute; trị): &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">+ Phương tiện (chủng loại, số lượng, gi&aacute; trị): &hellip;&hellip;&hellip;&hellip;&hellip;.&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">- Đầu tư c&acirc;y, con giống (chủng loại, số lượng, gi&aacute; trị): &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">- Đầu tư vốn lưu động: Vật tư, nguy&ecirc;n, nhi&ecirc;n liệu, h&agrave;ng h&oacute;a, dịch vụ (chủng loại, số lượng, gi&aacute; trị): &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
                        <p style="text-align: justify; padding-left: 30px;">- Đầu tư kh&aacute;c: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
                        <p style="text-align: justify; padding-left: 30px;">b) Phương &aacute;n sử dụng lao động</p>
                        <p style="text-align: justify; padding-left: 30px;">Tổng số lao động tham gia: &hellip;&hellip;&hellip;&hellip;&hellip; lao động, trong đ&oacute;: lao động l&agrave; người d&acirc;n tộc thiểu số l&agrave; &hellip;&hellip;&hellip;&hellip;. người (chiếm &hellip;&hellip;&hellip;.% tổng số lao động).<br /><br />c) Tổng nguồn vốn thực hiện phương &aacute;n v&agrave; cơ cấu nguồn vốn</p>
                        <p style="text-align: justify; padding-left: 30px;">Tổng nguồn vốn thực hiện phương &aacute;n: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. đồng. Trong đ&oacute;:</p>
                        <p style="text-align: justify; padding-left: 30px;">- Vốn tự c&oacute;: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; đồng, tỷ lệ: &hellip;&hellip;&hellip;.&hellip;.%;</p>
                        <p style="text-align: justify; padding-left: 30px;">- Vốn vay tại NHCSXH: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;......................... đồng, tỷ lệ: &hellip;&hellip;&hellip;&hellip;.%;</p>
                        <p style="text-align: justify; padding-left: 30px;">- Vốn vay tại c&aacute;c TCTD kh&aacute;c: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.&hellip; đồng, tỷ lệ: &hellip;&hellip;&hellip;&hellip;.%;</p>
                        <p style="text-align: justify; padding-left: 30px;">- Vốn hỗ trợ từ ng&acirc;n s&aacute;ch nh&agrave; nước (nếu c&oacute;): &hellip;&hellip;&hellip;.&hellip;&hellip;. đồng, tỷ lệ &hellip;&hellip;&hellip;&hellip;.%;</p>
                        <p style="text-align: justify; padding-left: 30px;">- Vốn huy động kh&aacute;c: &hellip;&hellip;&hellip;..&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. đồng, tỷ lệ: &hellip;&hellip;&hellip;&hellip;%.<br /><br />d) Hiệu quả kinh tế của phương &aacute;n <em>(theo số năm thực hiện phương &aacute;n)</em></p>
                        <p style="text-align: justify; padding-left: 30px;"><em>Đơn vị: triệu đồng</em></p>
                        <table width="593">
                            <tbody>
                                <tr>
                                    <td width="43">
                                        <p>TT</p>
                                    </td>
                                    <td width="214">
                                        <p>Chỉ ti&ecirc;u</p>
                                    </td>
                                    <td width="116">
                                        <p>Năm ...</p>
                                    </td>
                                    <td width="116">
                                        <p>Năm ...</p>
                                    </td>
                                    <td width="104">
                                        <p>&hellip;&hellip;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="43">
                                        <p>1</p>
                                    </td>
                                    <td width="214">
                                        <p>Tổng doanh thu</p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="104">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="43">
                                        <p>2</p>
                                    </td>
                                    <td width="214">
                                        <p>Tổng chi ph&iacute;</p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="104">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="43">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="214">
                                        <p><em>Trong đ&oacute;: khấu hao</em></p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="104">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="43">
                                        <p>3</p>
                                    </td>
                                    <td width="214">
                                        <p>Thuế TNDN</p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="104">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="43">
                                        <p>4</p>
                                    </td>
                                    <td width="214">
                                        <p>Lợi nhuận</p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="116">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="104">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p style="text-align: justify;">Thời gian dự kiến ho&agrave;n vốn: th&aacute;ng &hellip;&hellip;/20&hellip;&hellip;</p>
                        <p style="text-align: justify;"><strong>IV, ĐỀ NGHỊ VAY VỐN</strong></p>
                        <p style="text-align: justify; padding-left: 30px;">Đề nghị Ng&acirc;n h&agrave;ng Ch&iacute;nh s&aacute;ch x&atilde; hội cho vay để thực hiện phương &aacute;n vay vốn tham gia Dự &aacute;n dược liệu, cụ thể như sau:</p>
                        <ol style="text-align: justify;">
                            <li>Số tiền vay: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.. đồng</li>
                        </ol>
                        <p style="text-align: justify; padding-left: 30px;">(Bằng chữ:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;)</p>
                        <ol style="text-align: justify;" start="2">
                            <li>Mục đ&iacute;ch vay: &hellip;.&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</li>
                            <li>Thời hạn vay: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; th&aacute;ng.</li>
                            <li>L&atilde;i suất vay: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..%/năm.</li>
                            <li>Trả gốc theo định kỳ: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..th&aacute;ng/lần.</li>
                            <li>Trả l&atilde;i theo định kỳ: h&agrave;ng th&aacute;ng.</li>
                            <li>Nguồn v&agrave; kế hoạch trả nợ <em>(theo số năm vay vốn)</em></li>
                        </ol>
                        <table width="593">
                            <tbody>
                                <tr>
                                    <td width="42">
                                        <p>TT</p>
                                    </td>
                                    <td width="242">
                                        <p>Chỉ ti&ecirc;u</p>
                                    </td>
                                    <td width="103">
                                        <p>Năm &hellip;</p>
                                    </td>
                                    <td width="103">
                                        <p>Năm &hellip;</p>
                                    </td>
                                    <td width="103">
                                        <p>&hellip;&hellip;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="42">
                                        <p>1</p>
                                    </td>
                                    <td width="242">
                                        <p>Nguồn từ phương &aacute;n</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="42">
                                        <p><em>a</em></p>
                                    </td>
                                    <td width="242">
                                        <p><em>Khấu hao</em></p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="42">
                                        <p><em>b</em></p>
                                    </td>
                                    <td width="242">
                                        <p><em>Lợi nhuận</em></p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="42">
                                        <p>2</p>
                                    </td>
                                    <td width="242">
                                        <p>Nguồn kh&aacute;c</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="42">
                                        <p>3</p>
                                    </td>
                                    <td width="242">
                                        <p>Tổng nguồn trả nợ</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="103">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&nbsp;</p>
                        <ol style="text-align: justify;" start="8">
                            <li>Dự kiến t&agrave;i sản bảo đảm tiền vay</li>
                        </ol>
                        <table width="592">
                            <tbody>
                                <tr>
                                    <td width="43">
                                        <p>TT</p>
                                    </td>
                                    <td width="201">
                                        <p>T&ecirc;n t&agrave;i sản</p>
                                    </td>
                                    <td width="98">
                                        <p>Số lượng</p>
                                    </td>
                                    <td width="122">
                                        <p>Gi&aacute; trị ước t&iacute;nh (đồng)</p>
                                    </td>
                                    <td width="128">
                                        <p>Giấy tờ về t&agrave;i sản</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="43">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="201">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="98">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="122">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="128">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="43">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="201">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="98">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="122">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="128">
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&nbsp;</p>
                        <p><strong style="text-align: justify;">V, CAM KẾT CỦA KH&Aacute;CH H&Agrave;NG</strong></p>
                        <ol style="text-align: justify;">
                            <li>Chịu tr&aacute;ch nhiệm trước ph&aacute;p luật về sự ch&iacute;nh x&aacute;c, trung thực của c&aacute;c th&ocirc;ng tin, t&agrave;i liệu đ&atilde; cung cấp<sup>5</sup>; cung cấp kịp thời, đầy đủ cho Ng&acirc;n h&agrave;ng Ch&iacute;nh s&aacute;ch x&atilde; hội nếu c&oacute; thay đổi c&aacute;c th&ocirc;ng tin li&ecirc;n quan trong qu&aacute; tr&igrave;nh vay vốn.</li>
                            <li>Việc k&yacute; kết, thực hiện c&aacute;c thủ tục vay vốn tại Ng&acirc;n h&agrave;ng Ch&iacute;nh s&aacute;ch x&atilde; hội kh&ocirc;ng vi phạm quy định của ph&aacute;p luật, quy định nội bộ của b&ecirc;n vay vốn cũng như c&aacute;c cam kết, thỏa thuận giữa b&ecirc;n vay vốn v&agrave; bất kỳ chủ thể n&agrave;o kh&aacute;c.</li>
                            <li>Tu&acirc;n thủ c&aacute;c quy định về cho vay, chịu sự kiểm tra, gi&aacute;m s&aacute;t việc sử dụng vốn vay của Ng&acirc;n h&agrave;ng Ch&iacute;nh s&aacute;ch x&atilde; hội v&agrave; c&aacute;c cơ quan chức năng theo quy định của ph&aacute;p luật.</li>
                            <li>Thực hiện đầy đủ c&aacute;c cam kết với Ng&acirc;n h&agrave;ng Ch&iacute;nh s&aacute;ch x&atilde; hội, sử dụng tiền vay đ&uacute;ng mục đ&iacute;ch, đ&uacute;ng đối tượng, trả nợ đầy đủ, đ&uacute;ng hạn theo kế hoạch.</li>
                            <li>T&agrave;i sản bảo đảm tiền vay đ&aacute;p ứng đầy đủ c&aacute;c điều kiện, thực hiện thủ tục c&ocirc;ng chứng, chứng thực, x&aacute;c nhận v&agrave; đăng k&yacute; giao dịch bảo đảm theo quy định của ph&aacute;p luật v&agrave; Ng&acirc;n h&agrave;ng Ch&iacute;nh s&aacute;ch x&atilde; hội; phối hợp với Ng&acirc;n h&agrave;ng Ch&iacute;nh s&aacute;ch x&atilde; hội xử l&yacute; t&agrave;i sản bảo đảm theo thỏa thuận của c&aacute;c b&ecirc;n v&agrave; quy định của ph&aacute;p luật.</li>
                            <li>Tại thời điểm đề nghị vay vốn, kh&ocirc;ng c&oacute; nợ xấu tại c&aacute;c tổ chức t&iacute;n dụng, chi nh&aacute;nh ng&acirc;n h&agrave;ng nước ngo&agrave;i./.</li>
                        </ol>
                        <p style="text-align: justify;">&nbsp;</p>
                        <table width="592">
                            <tbody>
                                <tr>
                                    <td width="295">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td width="297">
                                        <p><em>&hellip;&hellip;, ng&agrave;y ... th&aacute;ng ... năm &hellip;&hellip;<br /> &nbsp;</em><strong>Người đại diện vay vốn<br /> <em>&nbsp;</em></strong><em>(K&yacute;, ghi r&otilde; họ t&ecirc;n, chức vụ, đ&oacute;ng dấu)</em></p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h6 style="text-align: justify;"><strong>3. Ưu đ&atilde;i l&atilde;i suất khi vay vốn sản xuất kinh doanh tại PVcomBank</strong> &nbsp;</h6>
                        <p style="text-align: justify;">PVcomBank l&agrave; một trong những ng&acirc;n h&agrave;ng cung cấp g&oacute;i vay vốn sản xuất kinh doanh với l&atilde;i suất ưu đ&atilde;i.</p>
                        <ul style="text-align: justify;">
                            <li>PVcomBank cung cấp nhiều g&oacute;i vay ph&ugrave; hợp với đa dạng ph&acirc;n kh&uacute;c kh&aacute;ch h&agrave;ng.</li>
                            <li>Dịch vụ chăm s&oacute;c kh&aacute;ch h&agrave;ng tốt, hỗ trợ v&agrave; tư vấn kh&aacute;ch h&agrave;ng nhiệt t&igrave;nh.</li>
                            <li>Cung cấp c&aacute;c giải ph&aacute;p chuyển đổi số gi&uacute;p kh&aacute;ch h&agrave;ng c&oacute; thể quản l&yacute; t&agrave;i ch&iacute;nh th&ocirc;ng qua ứng dụng di động, trang web, dịch vụ trực tuyến.</li>
                        </ul>
                        <p><em><img style="display: block; margin-left: auto; margin-right: auto;" src="http://www.pvcombank.com.vn/static/SEO/141 - Phương &aacute;n vay vốn sản xuất kinh doanh hiện nay/phuong-an-vay-von-san-xuat-kinh-doanh-2.jpg" alt="vay vốn sản xuất kinh doanh tại pvcombank l&atilde;i suất ưu đ&atilde;i" width="90%" height="NaN" /></em></p>
                        <p style="text-align: center;"><em>Vay vốn sản xuất kinh doanh tại PVcomBank l&atilde;i suất ưu đ&atilde;i</em></p>
                        <p style="text-align: justify;">Tham khảo bảng l&atilde;i suất kinh doanh tại ng&acirc;n h&agrave;ng PVcomBank mới nhất:</p>
                        <table width="602">
                            <tbody>
                                <tr>
                                    <td width="190">
                                        <p><strong>Chương tr&igrave;nh ưu đ&atilde;i</strong></p>
                                    </td>
                                    <td style="text-align: center;" width="119">
                                        <p><strong>L&atilde;i suất vay ưu đ&atilde;i</strong></p>
                                    </td>
                                    <td style="text-align: center;" width="104">
                                        <p><strong>L&atilde;i suất vay sau ưu đ&atilde;i</strong></p>
                                    </td>
                                    <td style="text-align: center;" width="104">
                                        <p><strong>Hạn mức vay</strong></p>
                                    </td>
                                    <td style="text-align: center;" width="85">
                                        <p><strong>Thời gian vay</strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="3" width="190">
                                        <p>Vay mục đ&iacute;ch sản xuất kinh doanh</p>
                                    </td>
                                    <td style="text-align: center;" width="119">
                                        <p>Thời hạn vay &ge; 36 th&aacute;ng</p>
                                        <p>5,99% (ưu đ&atilde;i 3 th&aacute;ng)</p>
                                    </td>
                                    <td style="text-align: center;" width="104">
                                        <p>LSCS + 1%</p>
                                    </td>
                                    <td style="text-align: center;" rowspan="3" width="104">
                                        <p>10 tỷ đồng</p>
                                    </td>
                                    <td style="text-align: center;" rowspan="3" width="85">
                                        <p>10 năm</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" width="119">
                                        <p>Thời hạn vay 6&le;THKV&le; 12</p>
                                        <p>7,99% (ưu đ&atilde;i 3 th&aacute;ng)</p>
                                    </td>
                                    <td style="text-align: center;" width="104">
                                        <p>LSCS + 1.5%</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" width="119">
                                        <p>Thời hạn vay &gt; 12 th&aacute;ng</p>
                                        <p>7,99% (ưu đ&atilde;i 6 th&aacute;ng)</p>
                                    </td>
                                    <td style="text-align: center;" width="104">
                                        <p>LSCS + 3,3%</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p style="text-align: justify;">Lưu &yacute;: Bảng l&atilde;i suất vay ng&acirc;n h&agrave;ng kinh doanh tr&ecirc;n đ&acirc;y sẽ c&oacute; thể thay đổi theo từng thời kỳ.&nbsp; Để cập nhật th&ocirc;ng tin mới nhất, bạn c&oacute; thể đăng k&yacute; nhận tư vấn <a href="../../lead_form_1?returnUrl=http://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/lead1">tại đ&acirc;y</a>.</p>
                        <p style="text-align: justify;">Xem th&ecirc;m: <a href="../../tin-tuc/tin-pvcombank/bieu-phi-dich-vu-qua-he-thong-ngan-hang-tmcp-dai-chung-viet-nam-pvcombank">Biểu ph&iacute; dịch vụ &amp; L&atilde;i suất cơ sở (LSCS)</a> tại ng&acirc;n h&agrave;ng PVcomBank</p>
                        <p style="text-align: justify;">B&agrave;i viết tr&ecirc;n l&agrave; chia sẻ của PVcomBank về mẫu phương &aacute;n sản xuất kinh doanh hiện nay. Hy vọng những th&ocirc;ng tin n&agrave;y sẽ mang lại cho bạn nhiều gi&aacute; trị. Nếu bạn c&oacute; nhu cầu vay vốn, h&atilde;y li&ecirc;n hệ ngay cho PVcomBank qua website <a href="../../">PVcomBank</a>/App PVConnect, hotline tổng đ&agrave;i <strong>1900 5555 92</strong> hoặc ph&ograve;ng giao dịch PVcomBank gần nhất.&nbsp;</p>
                    </div>
                </div>
            </div>
            <!-- related news -->
            <div id="tin-tuc-lien-quan">
                <div class="row pt-4 pb-2 pt-lg-5 mt-lg-4 mb-lg-3">
                    <div class="col-2 col-md-4 col-lg-6">
                        <div class="h7 h5--des text--b">Nội dung liên quan</div>
                    </div>
                    <div class="col-2 col-md-4 col-lg-6 d-flex justify-content-end align-items-center">
                        <a href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh" class="button-text button-text--sm button-text--has-icon-right">
                            <span>Xem tất cả</span>
                        </a>
                        <i class="icon icon-arrow-right--bl"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 col-md-4 col-lg-4 mb-3 mb-lg-0">
                        <div class="cell cell--lg h-100 bg-wh">
                            <a href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/vay-von-kinh-doanh-lai-suat-thap" class="cell__cover-image">
                                <img class="w-100" src="upload/images/news/RgrWVHmcmCvay-von-kinh-doanh-lai-suat-thap-3.jpg">
                            </a>
                            <div class="cell__body pt-3 pb-3 pl-4 pr-3">
                                <a class="none-underline description-news mb-1" href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/vay-von-kinh-doanh-lai-suat-thap">Ngân hàng nào cho vay vốn kinh doanh lãi suất thấp hiện nay?</a>
                            </div>
                            <div class="cell__footer pr-3 pl-4 pr-3 mb-3">
                                <a href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/vay-von-kinh-doanh-lai-suat-thap" class="button-text button-text--sm button-text--has-icon-right">
                                    <span>Chi tiết</span>
                                    <i class="icon icon-arrow-right--bl"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md-4 col-lg-4 mb-3 mb-lg-0">
                        <div class="cell cell--lg h-100 bg-wh">
                            <a href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/vay-tieu-thuong" class="cell__cover-image">
                                <img class="w-100" src="upload/images/news/4VXSFivtOHvay-tieu-thuong-1.jpg">
                            </a>
                            <div class="cell__body pt-3 pb-3 pl-4 pr-3">
                                <a class="none-underline description-news mb-1" href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/vay-tieu-thuong">Những điều cần biết về vay tiểu thương kinh doanh </a>
                            </div>
                            <div class="cell__footer pr-3 pl-4 pr-3 mb-3">
                                <a href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/vay-tieu-thuong" class="button-text button-text--sm button-text--has-icon-right">
                                    <span>Chi tiết</span>
                                    <i class="icon icon-arrow-right--bl"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md-4 col-lg-4 mb-3 mb-lg-0">
                        <div class="cell cell--lg h-100 bg-wh">
                            <a href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/lai-suat-vay-kinh-doanh" class="cell__cover-image">
                                <img class="w-100" src="upload/images/news/o8hrkSGqUJlai-suat-vay-kinh-doanh-3.jpg">
                            </a>
                            <div class="cell__body pt-3 pb-3 pl-4 pr-3">
                                <a class="none-underline description-news mb-1" href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/lai-suat-vay-kinh-doanh">Lãi suất vay kinh doanh tại PVcomBank hiện nay</a>
                            </div>
                            <div class="cell__footer pr-3 pl-4 pr-3 mb-3">
                                <a href="https://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/lai-suat-vay-kinh-doanh" class="button-text button-text--sm button-text--has-icon-right">
                                    <span>Chi tiết</span>
                                    <i class="icon icon-arrow-right--bl"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- pagination -->
        </div>
    </div>
    <footer class="">
        <div class="support-footer bg-gr-10">
            <div class="container">
                <div class="support-menu">
                    <div class="d-none d-lg-block pt-lg-5 pb-lg-3">
                        <h5 class="text--b text--le text--color-bk">Bạn cần hỗ trợ?</h5>
                    </div>
                    <div class="row mt-2">
                        <div class="col-4 col-md-4 col-lg-4 d-none d-lg-block">
                            <div class="d-flex align-items-center align-items-lg-start py-2 py-lg-0">
                                <div class="mr-2 mr-lg-3 icon-sp">
                                    <i class="icon icon-phone-call--color-multi"></i>
                                </div>
                                <div>
                                    <div class="link">
                                        <div class="h6 text--b text--color-bk d-none d-lg-block">Tổng đài 1900&nbsp;5555&nbsp;92</div>
                                    </div>
                                    <div class="submenu">
                                        <div class="mt-1 h8 text--color-gr">
                                            Tổng đài hỗ trợ trực tuyến 24/7 sẵn sàng trả lời mọi thắc mắc
                                        </div>
                                        <div class="mt-3 h8 text--color-bl">
                                            <a href="tel:1900 5555 92"
                                               class="button-text button-text--sm button-text--has-icon-right">
                                                <span>Bấm để gọi ngay</span>
                                                <i class="icon icon-arrow-right--bl"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4 col-sm-3 col-md-4 col-lg-4">
                            <div class="d-flex align-items-center align-items-sm-start align-items-lg-start py-2 py-lg-0">
                                <div class="mr-2 mr-lg-3 icon-sp">
                                    <i class="icon icon-mail-contact--color-multi"></i>
                                </div>
                                <div>
                                    <div class="link">
                                        <div class="h6 text--b text--color-bk d-none d-lg-block">Đăng ký nhận tư vấn</div>
                                        <a href="https://www.pvcombank.com.vn/lead_form_1?returnUrl=http://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/phuong-an-vay-von-san-xuat-kinh-doanh"
                                           class="h8 button-text text--b text--color-bk d-block d-lg-none"><span>Đăng ký nhận tư vấn</span></a>
                                    </div>
                                    <div class="submenu d-sm-block">
                                        <div class="mt-1 h8 text--color-gr">
                                            Chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất có thể
                                        </div>
                                        <div class="mt-3 h8 text--color-bl">
                                            <a href="https://www.pvcombank.com.vn/lead_form_1?returnUrl=http://www.pvcombank.com.vn/tin-tuc/cam-nang-tai-chinh/phuong-an-vay-von-san-xuat-kinh-doanh"
                                               class="button-text button-text--sm button-text--has-icon-right">
                                                <span>Đăng ký ngay</span>
                                                <i class="icon icon-arrow-right--bl"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="divider-h divider-h--gr d-block d-sm-none"></div>
                        </div>

                        <div class="col-4 col-sm-3 col-md-4 col-lg-4">
                            <div class="d-flex align-items-center align-items-sm-start align-items-lg-start py-2 py-lg-0">
                                <div class="mr-2 mr-lg-3 icon-sp">
                                    <i class="icon icon-branch-building--color-multi"></i>
                                </div>
                                <div>
                                    <div class="link">
                                        <div class="h6 text--b text--color-bk d-none d-lg-block">Tìm chi nhánh gần bạn</div>
                                        <a href="https://www.pvcombank.com.vn/mang-luoi"
                                           class="h8 button-text text--b text--color-bk d-block d-lg-none"><span>Tìm chi nhánh gần bạn</span></a>
                                    </div>
                                    <div class="submenu d-sm-block">
                                        <div class="mt-1 h8 text--color-gr">
                                            Tìm chi nhánh, ATM hoặc phòng giao dịch gần vị trí của bạn nhất
                                        </div>
                                        <div class="mt-3 h8 text--color-bl">
                                            <a href="https://www.pvcombank.com.vn/mang-luoi"
                                               class="button-text button-text--sm button-text--has-icon-right">
                                                <span>Mở danh sách</span>
                                                <i class="icon icon-arrow-right--bl"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="divider-h divider-h--gr d-block d-sm-none"></div>
                        </div>
                    </div>

                    <div class="row mt-lg-5 align-items-stretch">
                        <div class="col-4 col-sm-3 col-md-4 col-lg-4">
                            <div class="d-flex align-items-center align-items-sm-start align-items-lg-start py-2 py-lg-0 h-100">
                                <div class="mr-2 mr-lg-3 icon-sp">
                                    <i class="icon icon-earth-currency--color-multi"></i>
                                </div>
                                <div class="">
                                    <div class="link">
                                        <div class="h6 text--b text--color-bk d-none d-lg-block">Tra cứu tỉ giá</div>
                                        <a href="https://www.pvcombank.com.vn/ti-gia-ngoai-te"
                                           class="h8 button-text text--b text--color-bk d-block d-lg-none"><span>Tra cứu tỉ giá</span></a>
                                    </div>
                                    <div class="submenu d-sm-block">
                                        <div class="mt-1 h8 text--color-gr flex-grow-1">
                                            Tỉ giá tốt nhất, cập nhật liên tục từng phút cho bạn
                                        </div>
                                        <div class="mt-3 h8 text--color-bl">
                                            <a href="https://www.pvcombank.com.vn/ti-gia-ngoai-te"
                                               class="button-text button-text--sm button-text--has-icon-right">
                                                <span>Mở công cụ</span>
                                                <i class="icon icon-arrow-right--bl"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="divider-h divider-h--gr d-block d-sm-none"></div>
                        </div>

                        <div class="col-4 col-sm-3 col-md-4 col-lg-4">
                            <div class="d-flex align-items-center align-items-sm-start align-items-lg-start py-2 py-lg-0">
                                <div class="mr-2 mr-lg-3 icon-sp">
                                    <i class="icon icon-calculator--color-multi"></i>
                                </div>
                                <div>
                                    <div class="link">
                                        <div class="h6 text--b text--color-bk d-none d-lg-block">Biểu lãi suất</div>
                                        <a href="https://www.pvcombank.com.vn/bieu-lai-suat"
                                           class="h8 button-text text--b text--color-bk d-block d-lg-none"><span>Biểu lãi suất</span></a>
                                    </div>
                                    <div class="submenu d-sm-block">
                                        <div class="mt-1 h8 text--color-gr">
                                            Cập nhật lãi suất tiết kiệm theo thời gian thực
                                        </div>
                                        <div class="mt-3 h8 text--color-bl">
                                            <a href="https://www.pvcombank.com.vn/bieu-lai-suat"
                                               class="button-text button-text--sm button-text--has-icon-right">
                                                <span>Mở công cụ</span>
                                                <i class="icon icon-arrow-right--bl"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4 d-none d-lg-block col-lg-4">
                            <div class="d-flex align-items-center align-items-sm-start align-items-lg-start py-2 py-lg-0">
                                <div class="mr-2 mr-lg-3 icon-sp">
                                    <i class="icon icon-gift-box--color-multi"></i>
                                </div>
                                <div>
                                    <div class="h6 text--b text--color-bk d-none d-lg-block">Thế giới ưu đãi</div>
                                    <a href="/khuyen-mai/uu-dai-wow"
                                       class="h8 button-text text--b text--color-bk d-block d-lg-none"><span>Thế giới ưu đãi</span></a>
                                    <div class="mt-1 h8 text--le text--color-gr">
                                        Ưu đãi ngập tràn và luôn được cập nhật
                                    </div>
                                    <div class="mt-3 h8 text--color-bl">
                                        <a href="/khuyen-mai/uu-dai-wow"
                                           class="button-text button-text--sm button-text--has-icon-right">
                                            <span>Khám phá ngay</span>
                                            <i class="icon icon-arrow-right--bl"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-footer bg-gr-60">
            <div class="container accordion" id="footer-accordion">
                <div class="row pt-2 mt-2 pt-lg-5 mt-lg-5 edit-text">
                    <div class="col-4 col-md-8 col-lg-3">
                        <div class="h8 text--b text--color-wh link d-flex justify-content-between py-2 py-lg-0">
                            CÁ NHÂN
                            <div class="icon-collapse">
                                <i class="icon icon-plus--wh d-block d-lg-none"></i>
                            </div>
                        </div>
                        <ul class="submenu d-lg-block">
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/dich-vu-the"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Dịch vụ Thẻ</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/san-pham-vay"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Sản phẩm Vay</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/tiet-kiem"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Tiết kiệm</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/ngan-hang-so"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Ngân hàng số</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/tai-khoan"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Tài khoản</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/bao-hiem"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Bảo hiểm</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/ca-nhan/san-pham-khac"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Sản phẩm khác</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="divider-h divider-h--wh d-block d-lg-none divider-collapse"></div>
                    </div>

                    <div class="col-4 col-md-8 col-lg-3">
                        <div class="h8 text--b text--color-wh link d-flex justify-content-between py-2 py-lg-0">
                            DOANH NGHIỆP
                            <div class="icon-collapse">
                                <i class="icon icon-plus--wh d-block  d-lg-none"></i>
                            </div>
                        </div>
                        <ul class="submenu d-lg-block">
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/tin-dung-doanh-nghiep"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Tín dụng</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/tai-khoan-doanh-nghiep"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Tài khoản</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/thanh-toan-trong-nuoc"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Thanh toán trong nước</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/bao-lanh"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Bảo lãnh</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/danh-muc-san-pham/doanh-nghiep/ngan-hang-giao-dich"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Thanh toán Quốc tế</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="divider-h divider-h--wh d-block d-lg-none divider-collapse"></div>
                        <div class="h8 text--b text--color-wh link d-flex justify-content-between py-2 pb-lg-0 pt-lg-4">
                            PVCOMBANK PREMIER
                            <div class="icon-collapse">
                                <i class="icon icon-plus--wh d-block  d-lg-none"></i>
                            </div>
                        </div>
                        <ul class="submenu d-lg-block">
                            <li class="mb-2 mb-lg-0">
                                <a href="https://premier.pvcombank.com.vn/vi/giai-phap-gia-tang-tai-san" class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Giải pháp Gia tăng tài sản</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://premier.pvcombank.com.vn/vi/giai-phap-chu-doanh-nghiep" class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Đặc quyền Chủ doanh nghiệp</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://premier.pvcombank.com.vn/vi/giai-phap-tich-luy-ben-vung" class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Giải pháp Tích lũy bền vững</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>

                        </ul>
                        <div class="divider-h divider-h--wh d-block d-lg-none divider-collapse"></div>
                    </div>

                    <div class="col-4 col-md-8 col-lg-3">
                        <div class="h8 text--b text--color-wh link d-flex justify-content-between py-2 py-lg-0">
                            VỀ PVCOMBANK
                            <div class="icon-collapse">
                                <i class="icon icon-plus--wh d-block d-lg-none"></i>
                            </div>
                        </div>
                        <ul class="submenu d-lg-block">
                            <li class="mb-2 mb-lg-0">
                                <a href="/gioi-thieu-ve-pvcombank"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Giới thiệu</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/co-cau-quan-tri"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Cơ cấu quản trị</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/co-dong.html"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Quan hệ cổ đông</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/mang-luoi"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Mạng lưới</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="http://tuyendung.pvcombank.com.vn"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Tuyển dụng</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/lien-he"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Liên hệ</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="divider-h divider-h--wh d-block d-lg-none divider-collapse"></div>
                    </div>

                    <div class="col-4 col-md-8 col-lg-3">
                        <div class="h8 text--b text--color-wh link d-flex justify-content-between py-2 py-lg-0">
                            LIÊN KẾT NHANH
                            <div class="icon-collapse">
                                <i class="icon icon-plus--wh d-block d-lg-none"></i>
                            </div>
                        </div>
                        <ul class="submenu d-lg-block">
                            <li class="mb-2 mb-lg-0">
                                <a href="https://digitalbanking.pvcombank.com.vn/"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Ngân hàng trực tuyến dành cho KHCN</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://ibanking.pvcombank.com.vn/WebIB/MainIB"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Ngân hàng trực tuyến dành cho KHDN</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="/khuyen-mai/uu-dai-wow"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Thế giới ưu đãi WOW</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="/tin-tuc/tin-pvcombank/bieu-phi-dich-vu-qua-he-thong-ngan-hang-tmcp-dai-chung-viet-nam-pvcombank"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Biểu phí & Lãi suất cơ sở</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://www.pvcombank.com.vn/bieu-lai-suat"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Lãi suất</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://ibanking.pvcombank.com.vn/WebIB/tra-cuu-hop-dong-bds"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Tra cứu thông tin người mua BĐS</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://ibanking.pvcombank.com.vn/WebIB/tra-cuu-bao-lanh"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Xác thực chứng thư bảo lãnh online</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                            <li class="mb-2 mb-lg-0">
                                <a href="https://mail.pvcombank.com.vn"
                                   class="button-text button-text--sm button-text--has-icon-left h8 text--color-wh mt-0 mt-lg-2">
                                    <span>Webmail</span>
                                    <i class="icon icon-arrow-right--wh"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="d-lg-flex align-items-center mt-lg-6 mt-4 pb-3">
                    <div class="d-flex align-items-center pb-1 pb-lg-0">
                        <div class="d-lg-flex align-items-center mr-2">
                            <a class="logo pvc-app" href="#"></a>
                        </div>
                        <div class="h9 text--b text--color-wh mr-2">
                            Ứng dụng ngân hàng di động <br> PVConnect
                        </div>
                    </div>
                    <div class="d-lg-flex align-items-center">
                        <a class="button-badge button-badge__ios-apple mr-1" target="_blank"
                           href="https://pvconnect.onelink.me/HUyH/9s1cmxmq"></a>
                        <a class="button-badge button-badge__google-play-store" target="_blank"
                           href="https://pvconnect.onelink.me/HUyH/9s1cmxmq"></a>
                    </div>
                </div>
                <div class="divider-h divider-h--wh divider-collapse"></div>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="h10 text--le text--color-wh mr-md-5 mr-2 py-3">
                        Bản quyền &copy; 2020 thuộc về Ngân hàng TMCP Đại Chúng Việt Nam (PVcomBank)
                    </div>
                    <div class="social-icon d-flex justify-lg-content-center justify-content-start">
                        <select autocomplete="off"
                                class="d-lg-none menu-language-select form-control on-dark form-control--sm">
                            <option value="en" >EN</option>
                            <option value="vi" selected>VI</option>
                        </select>
                        <a href="https://www.facebook.com/PVcomBankFanpage" target="_blank"
                           class="button-text none-underline mr-1">
                            <i class="icon icon-social-facebook"></i></a>
                        <a href="https://www.youtube.com/channel/UCWM9M4MJkrQsMecV3m2d3AQ" target="_blank"
                           class="button-text none-underline mr-1">
                            <i class="icon icon-social-youtube"></i></a>
                        <a href="https://zalo.me/110884890514649778" target="_blank"
                           class="button-text none-underline">
                            <i class="icon icon-social-zalo"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <button class="button-back-to-top hide" style="bottom: 85px">
        <i class="icon icon-arrow-up--bk"></i>
    </button>

    <script type='text/javascript' src="https://www.pvcombank.com.vn/ddl/index.js"></script>
    <script type='text/javascript' src="https://www.pvcombank.com.vn/js/jquery-ui-1.12.1.min.js"></script>
    <script type='text/javascript' src="https://www.pvcombank.com.vn/ddl/styles.js"></script>
    <script type='text/javascript' src="https://www.pvcombank.com.vn/js/pixel_script.js"></script>
    <script type="text/javascript" src="https://www.pvcombank.com.vn/js/slick.js"></script>
    <script type="text/javascript" src="https://www.pvcombank.com.vn/js/locations.min.js"></script>
    <script src="https://www.pvcombank.com.vn/js/lazysizes.min.js" async=""></script>
    <script rel="preload" src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
            WebFontConfig = {
                custom: {
                    families: ['Montserrat:n3,n4,n5,n6', 'icomoon'],
                    urls: ['https://www.pvcombank.com.vn/ddl/styles.css']
                }
            };
    </script>
    <script type="text/javascript">
        getParameterValues = function () {
            if (window.location.href.slice(window.location.href.indexOf('?') === -1)) {
                return {};
            }
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            var params = {};
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                params[urlparam[0]] = urlparam[1];
            }
            return params;
        }
        $('.menu-language-select').change(function (event) {
            changeLanguage(event)
        });
        function changeLanguage(e) {
            var selectedLanguage = $(e.target).children("option:selected").val();
            var ajaxUrl = "https://www.pvcombank.com.vn/set-languaguage";
            $.ajax({
                method: "POST",
                url: ajaxUrl,
                data: {toLang: selectedLanguage},
                success: function () {
                    var params = getParameterValues();
                    params.reload = Date.now();
                    window.location.href = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + $.param(params);
                }
            });
        }
    </script>


    <script src="https://sp.zalo.me/plugins/sdk.js"></script>
    <script type="text/javascript">
        // $(function() {
        let isMinimumStyle = false;
        // when start
        if ($("html").scrollTop() > 0) {
            changeHeaderMinimumStyle()
        }
        // when scroll
        $(window).on('scroll', function () {
            // console.log(window.innerWidth);
            var docScrollTop = $("html").scrollTop();
            // console.log(docScrollTop);
            if (docScrollTop > 0 && window.innerWidth >= 1024 && !isMinimumStyle) {
                isMinimumStyle = true;
                changeHeaderMinimumStyle();
            } else if (docScrollTop === 0 && isMinimumStyle) {
                isMinimumStyle = false;
                changeHeaderStyleBack();
            }
        });
        // });
        function changeHeaderMinimumStyle() {
            // container
            $('#frontend-header').addClass('bg-wh').addClass('container-full-width').removeClass('container');
            // top-head
            $('#top-head').addClass('disappear');
            // logo
            // $('#frontend-logo').addClass('opacity-0');
            $('#frontend-header-wrapper').removeClass('border-bottom');
            // $('#frontend-header-wrapper').addClass('pl-lg-3');
            setTimeout(function () {
                $('#main-menu__item-group-1').addClass('d-none');
                $('#main-menu__item-group-2').removeClass('d-none').addClass('d-flex');
                $('#frontend-logo')
                        // .addClass('pvc--icon-only').removeClass('pvc--original')
                        .addClass('smaller').addClass('ml-lg-3').removeClass('opacity-0');
            }, 0);
            $('#main-menu li:last-child').removeClass('border-left');
            $('#main-menu .divider--vertical').removeClass('d-none');
            // beau-person, beau-business
            $("#beau-business a.dropdown__item").removeClass('h6').addClass('h8');
            $("#beau-person a.dropdown__item").removeClass('h6').addClass('h8');
            $("#beau-premier a.dropdown__item").removeClass('h6').addClass('h8');

            $("#beau-person a.dropdown__item i.icon").removeClass('icon--size-3').addClass('icon--size-2');
            $("#beau-business a.dropdown__item i.icon").removeClass('icon--size-3').addClass('icon--size-2');
            $("#beau-premier a.dropdown__item i.icon").removeClass('icon--size-3').addClass('icon--size-2');

            $("#main-menu li .main-menu__secondary-title").removeClass('h8').addClass('h9');
        }
        function changeHeaderStyleBack() {
            // container
            $('#frontend-header').removeClass('bg-wh').addClass('container').removeClass('container-full-width');
            // top-head
            $('#top-head').removeClass('disappear');
            // logo
            // $('#frontend-logo').addClass('opacity-0');
            $('#frontend-header-wrapper').addClass('border-bottom');
            // $('#frontend-header-wrapper').removeClass('pl-lg-3');
            setTimeout(function () {
                $('#main-menu__item-group-1').removeClass('d-none');
                $('#main-menu__item-group-2').addClass('d-none').removeClass('d-flex');
                $('#frontend-logo')
                        // .addClass('pvc--original').removeClass('pvc--icon-only')
                        .removeClass('smaller').removeClass('ml-lg-3').removeClass('opacity-0');
            }, 0);
            $('#main-menu li:last-child').addClass('border-left');
            $('#main-menu .divider--vertical').addClass('d-none');
            // beau-person, beau-business
            $("#beau-business a.dropdown__item").addClass('h6').removeClass('h8');
            $("#beau-person a.dropdown__item").addClass('h6').removeClass('h8');
            $("#beau-premier a.dropdown__item").addClass('h6').removeClass('h8');

            $("#beau-person a.dropdown__item i.icon").removeClass('icon--size-2').addClass('icon--size-3');
            $("#beau-business a.dropdown__item i.icon").removeClass(' icon--size-2').addClass(' icon--size-3');
            $("#beau-premier a.dropdown__item i.icon").removeClass(' icon--size-2').addClass(' icon--size-3');

            $("#main-menu li .main-menu__secondary-title").removeClass('h9').addClass('h8');
        }
        // when hover in hotline
        $('.header__hotline').mouseenter(function () {
            $('.header__hotline > i.icon').removeClass('icon-call-phone--bk').addClass('icon-call-phone--bl');
            $('.header__hotline > .h9').removeClass('text--color-bk').addClass('text--color-bl');
        }).mouseleave(function () {
            $('.header__hotline > i.icon').removeClass('icon-call-phone--bl').addClass('icon-call-phone--bk');
            $('.header__hotline > .h9').removeClass('text--color-bl').addClass('text--color-bk');
        });
        $('.nav-mobile-backdrop').on('click', function () {
            closeNavMobile(this);
        })
        $('.click-menu-mobile a').on('click', function (event) {
            console.log(event.target)
            $(event.target).parent().find('.dropdown-menu').toggleClass('show')
        })
    </script>
    <input type="hidden" name="___PVCOMBANK" id="__PVCB" value="c1ca996bd8bae65732af2cecd4d4f28a"/>
</body>
</html>
