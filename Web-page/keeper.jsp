<%-- 
    Document   : keeper
    Created on : May 30, 2024, 11:07:35 PM
    Author     : X P S
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Trang chủ</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="assets/images/favicon.png" />
        
    </head>
    <body>
        <div class="container-scroller">
            <!-- partial:partials/_navbar.html -->
            <nav
                class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div
                    class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                    <a class="navbar-brand brand-logo" href="index.html"><img
                            src="assets/images/logo.svg" alt="logo" /></a>
                    <a class="navbar-brand brand-logo-mini"
                       href="index.html"><img src="assets/images/logo-mini.svg"
                                           alt="logo" /></a>
                </div>

                <div class="navbar-menu-wrapper d-flex align-items-stretch">
                    <button
                        class="navbar-toggler navbar-toggler align-self-center"
                        type="button" data-toggle="minimize">
                        <span class="mdi mdi-menu"></span>
                    </button>
                    <form class="d-flex align-items-center h-100"
                          style="font-size: larger; padding-top: 10px;">
                        <h2>Dashboard of Keeper</h2>
                    </form>
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item  dropdown d-none d-md-block">

                            <a class="nav-link dropdown-toggle"
                               id="reportDropdown" href="#"
                               data-toggle="dropdown"
                               aria-expanded="false"><svg width="24"
                                                       height="24" fill="none"
                                                       xmlns="http://www.w3.org/2000/svg"
                                                       font-size="24" class="jss220"
                                                       color="#A2A8AF"><path
                                                       d="M19.08 4.93A9.972 9.972 0 0 0 12 2a9.928 9.928 0 0 0-7.07 2.93A9.947 9.947 0 0 0 2 12a9.947 9.947 0 0 0 2.93 7.07A9.947 9.947 0 0 0 12 22a9.947 9.947 0 0 0 7.07-2.93A9.947 9.947 0 0 0 22 12a9.937 9.937 0 0 0-2.92-7.07ZM12 11.38a2.817 2.817 0 0 1 2.81 2.81c0 1.285-.86 2.374-2.048 2.701a.187.187 0 0 0-.141.185v.523c0 .338-.261.621-.6.643a.622.622 0 0 1-.653-.621v-.545a.187.187 0 0 0-.141-.185 2.83 2.83 0 0 1-2.048-2.69c0-.338.261-.632.61-.643a.626.626 0 0 1 .642.62 1.56 1.56 0 0 0 1.678 1.558 1.567 1.567 0 0 0 1.449-1.448A1.567 1.567 0 0 0 12 12.61c-1.547.022-2.81-1.242-2.81-2.8 0-1.274.871-2.363 2.048-2.701a.2.2 0 0 0 .141-.185V6.39c0-.338.261-.62.6-.643a.622.622 0 0 1 .653.621v.545c0 .087.054.163.141.185a2.83 2.83 0 0 1 2.048 2.69.637.637 0 0 1-.61.643.626.626 0 0 1-.642-.62 1.56 1.56 0 0 0-1.678-1.558 1.576 1.576 0 0 0-1.449 1.449A1.567 1.567 0 0 0 12 11.379Z"
                                                       fill="#A2A8AF"></path></svg>
                                Vay vốn kinh doanh </a>
                            <!-- <div class="dropdown-menu navbar-dropdown"
                                aria-labelledby="reportDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="mdi mdi-file-pdf mr-2"></i>PDF
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">
                                    <i class="mdi mdi-file-excel mr-2"></i>Excel
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">
                                    <i class="mdi mdi-file-word mr-2"></i>doc
                                </a>
                            </div> -->
                        </li>
                        <li class="nav-item  dropdown d-none d-md-block">
                            <a class="nav-link dropdown-toggle"
                               id="projectDropdown" href="#"
                               data-toggle="dropdown" aria-expanded="false">
                                <svg viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg"
                                     width="24" height="24" font-size="24"
                                     class="jss220" color="#A2A8AF"><path
                                     fill-rule="evenodd" clip-rule="evenodd"
                                     d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2Zm-1 17v-2h2v2h-2Zm3.17-6.83.9-.92c.57-.57.93-1.37.93-2.25 0-2.21-1.79-4-4-4S8 6.79 8 9h2c0-1.1.9-2 2-2s2 .9 2 2c0 .55-.22 1.05-.59 1.41l-1.24 1.26C11.45 12.4 11 13.4 11 14.5v.5h2c0-1.5.45-2.1 1.17-2.83Z"
                                     fill="#A2A8AF"></path></svg>
                                Trợ giúp </a>
                        </li>
                        <li
                            class="nav-item nav-language dropdown d-none d-md-block">
                            <a class="nav-link dropdown-toggle"
                               id="languageDropdown" href="#"
                               data-toggle="dropdown" aria-expanded="false">
                                <svg viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg"
                                     width="24" height="24" font-size="24"
                                     class="jss220" color="#A2A8AF"><path
                                     d="M12 21C8.387 17.773 2 13.76 2 8.395 2 5.374 4.42 3 7.5 3c1.74 0 3.41.744 4.5 2 1.09-1.256 2.76-2 4.5-2C19.58 3 22 5.374 22 8.395c0 5.356-6.379 9.396-10 12.605Z"
                                     fill="#A2A8AF"></path></svg>
                                <p class="mb-1 text-black">Góp ý</p>

                            </a>
                        </li>
                        <li class="nav-item nav-profile dropdown">
                            <a class="nav-link dropdown-toggle"
                               id="profileDropdown" href="#"
                               data-toggle="dropdown" aria-expanded="false">
                                <span
                                    style="display: table-cell; vertical-align: middle; white-space: nowrap;"></span>
                                <div class="nav-profile-text">

                                    <p class="mb-1 text-black"> Your Name </p>
                                </div>
                            </a>
                            <div
                                class="dropdown-menu navbar-dropdown dropdown-menu-right p-0 border-0 font-size-sm"
                                aria-labelledby="profileDropdown"
                                data-x-placement="bottom-end">
                                <div class="p-2">
                                    <h5
                                        class="dropdown-header text-uppercase pl-2 text-dark">User
                                        Options</h5>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>Profile</span>
                                        <span class="p-0">
                                            <span
                                                class="badge badge-success">1</span>
                                            <i
                                                class="mdi mdi-account-outline ml-1"></i>
                                        </span>
                                    </a>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="javascript:void(0)">
                                        <span>Service pack information</span>
                                        <i class="mdi mdi-settings"></i>
                                    </a>
                                    <div role="separator"
                                         class="dropdown-divider"></div>
                                    <h5
                                        class="dropdown-header text-uppercase  pl-2 text-dark mt-2">Actions</h5>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>File import and export
                                            history</span>
                                        <svg width="24" height="24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg"
                                             font-size="24"><g
                                             clip-path="url(#file-icon_svg__a)"
                                             fill="currentColor"><path
                                             d="M20.02 7.443c.209 0 .398.045.588.1v-.136c0-.94-.768-1.708-1.708-1.708h-6.462l-1.455-1.446A.856.856 0 0 0 10.378 4H5.272c-.94 0-1.709.768-1.709 1.708v1.79c.136-.028.272-.046.407-.046h16.05v-.009Z"></path><path
                                             fill-rule="evenodd"
                                             clip-rule="evenodd"
                                             d="M3.97 8.546h16.05c1.094.009 1.971.84 1.98 1.88v.027l-1.139 8.097c-.018 1.022-.904 1.853-1.988 1.853H5.136c-1.094 0-1.97-.822-1.988-1.853L2 10.453v-.027c0-1.04.886-1.871 1.97-1.88Zm7.393 3.19v1.057c2.088.118 3.588 1.944 3.597 4.429 0 .1-.082.19-.18.208h-.037a.203.203 0 0 1-.199-.145c-.47-1.4-1.51-2.115-3.172-2.169v1.04a.22.22 0 0 1-.081.162.213.213 0 0 1-.299-.027l-1.898-2.314a.206.206 0 0 1 .01-.27L11 11.6a.193.193 0 0 1 .154-.073c.118 0 .208.09.208.208Z"></path></g><defs><clippath
                                             id="file-icon_svg__a"><path
                                                fill="#fff"
                                                transform="translate(2 4)"
                                                d="M0 0h20v16.412H0z"></path></clippath></defs></svg>
                                    </a>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>Lock Account</span>
                                        <i class="mdi mdi-lock ml-1"></i>
                                    </a>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>Log Out</span>
                                        <i class="mdi mdi-logout ml-1"></i>
                                    </a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link count-indicator dropdown-toggle"
                               id="notificationDropdown" href="#"
                               data-toggle="dropdown">
                                <i class="mdi mdi-bell-outline"></i>
                                <span class="count-symbol bg-danger"></span>
                            </a>
                            <div
                                class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list"
                                aria-labelledby="notificationDropdown">
                                <h6
                                    class="p-3 mb-0 bg-primary text-white py-4">Thông
                                    báo</h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-success">
                                            <i class="mdi mdi-calendar"></i>
                                        </div>
                                    </div>
                                    <div
                                        class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6
                                            class="preview-subject font-weight-normal mb-1">Sự
                                            kiện hôm nay
                                        </h6>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-warning">
                                            <i class="mdi mdi-settings"></i>
                                        </div>
                                    </div>
                                    <div
                                        class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6
                                            class="preview-subject font-weight-normal mb-1">Cập
                                            nhật </h6>
                                        <p class="text-gray ellipsis mb-0">
                                            Update website </p>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-info">
                                            <i class="mdi mdi-link-variant"></i>
                                        </div>
                                    </div>
                                    <div
                                        class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6
                                            class="preview-subject font-weight-normal mb-1">News
                                        </h6>
                                        <p class="text-gray ellipsis mb-0"> News
                                            everyday
                                            ! </p>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <h6 class="p-3 mb-0 text-center">
                                    Tất cả thông báo</h6>
                            </div>
                        </li>
                    </ul>
                    <button
                        class="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
                        type="button" data-toggle="offcanvas">
                        <span class="mdi mdi-menu"></span>
                    </button>
                </div>
            </nav>
            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
                <!-- partial:partials/_sidebar.html -->
                <nav class="sidebar sidebar-offcanvas" id="sidebar">
                    <ul class="nav">
                        <!-- /* style=" position: fixed;
                                        left: 0;
                    width: 17%;
                    background-color: #333; 
                    z-index: 1000;" */ -->
                        <li class="nav-item">
                            <a class="nav-link" href="Report.html">
                                <span class="icon-bg"><svg viewBox="0 0 25 24"
                                                           fill="none"
                                                           xmlns="http://www.w3.org/2000/svg"><path
                                                           fill-rule="evenodd"
                                                           clip-rule="evenodd"
                                                           d="M7.002 22a3 3 0 0 1-3-3v-6h-1c-.89 0-1.337-1.077-.707-1.707l9-9a1 1 0 0 1 1.414 0l9 9c.63.63.184 1.707-.707 1.707h-1v6a3 3 0 0 1-3 3h-10Zm5-17.586-6.648 6.65a1 1 0 0 1 .649.936v7a1 1 0 0 0 1 1h2v-4a2 2 0 0 1 2-2h2a2 2 0 0 1 2 2v4h2a1 1 0 0 0 1-1v-7a1 1 0 0 1 .649-.937l-6.65-6.649Zm1 11.586h-2v4h2v-4Z"
                                                           fill="currentColor"></path></svg></i></span>
                                <span class="menu-title">Home</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Report.html">
                                <span class="icon-bg"><i class="fa fa-bell" style="font-size:24px"></i></span>
                                <span class="menu-title"> Notication</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"
                               href="#auth" aria-expanded="false"
                               aria-controls="auth">
                                <span class="icon-bg"><i class="fa fa-send-o" style="font-size:24px"></i></span>
                                <span class="menu-title"> Request </span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="auth">
                                <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <a class="nav-link"
                                                         href>
                                         Inbound Order </a></li>
                                <li class="nav-item"> <a class="nav-link"
                                                         href>
                                          Outbound Order </a></li>
                                <li class="nav-item"> <a class="nav-link"
                                                         href>
                                        Inventory Request </a></li>

                                </ul>
                            </div>
                        </li>
<!--                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"
                               href="#ui-basic" aria-expanded="false"
                               aria-controls="ui-basic">
                                <span class="icon-bg"><i class="fa fa-dropbox" style="font-size:24px"></i></span>
                                <span class="menu-title"> Your Product  </span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ui-basic">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a class="nav-link"
                                                             href="#">View List  </a></li>
                                </ul>
                            </div>
                        </li>-->
                     
                        <li class="nav-item sidebar-user-actions">
                            <div class="user-details">
                                <div
                                    class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <div class="d-flex align-items-center">
                                            <div class="sidebar-profile-text">
                                                <p class="mb-1"> SYSTEM </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"
                               href="#ut" aria-expanded="false"
                               aria-controls="ut">
                                <span class="icon-bg"><i
                                        class="mdi mdi-crosshairs-gps menu-icon"></i></span>
                                <span class="menu-title">Setting Configuration</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ut">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a
                                            class="nav-link"
                                            href>
                                            Genenal Diary</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item sidebar-user-actions">
                            <div class="sidebar-user-menu">
                                <a href="#" class="nav-link"><i
                                        class="mdi mdi-logout menu-icon"></i>
                                    <span class="menu-title">Log
                                        Out</span></a>
                            </div>
                        </li>
                    </ul>
                </nav>
                <script src="assets/vendors/js/vendor.bundle.base.js"></script>
            <!-- endinject -->
            <!-- Plugin js for this page -->
            <script src="assets/vendors/chart.js/Chart.min.js"></script>
            <script
            src="assets/vendors/jquery-circle-progress/js/circle-progress.min.js"></script>
            <!-- End plugin js for this page -->
            <!-- inject:js -->
            <script src="assets/js/off-canvas.js"></script>
            <script src="assets/js/hoverable-collapse.js"></script>
            <script src="assets/js/misc.js"></script>
            <!-- endinject -->
            <!-- Custom js for this page -->
            <script src="assets/js/dashboard.js"></script>
    </body>
</html>

