
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Home Page</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="assets/css/style.css">
           <link href="https://stackpath.bootstrapcdn.com/bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">

        <!-- End layout styles -->
                <link rel="shortcut icon" href="assets/images/logo-mini.svg" />
                <style>
                       .container {
                border: 2px solid #4CAF50; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px; /* Bo góc (tùy chọn) */
            }

                </style>
    </head>
    <body>
                <%@ include file="sidebarCenter.jsp" %>
 <div class="container mt-5">
     <h2 class="mb-4 " style="color: #007bff">Center  Dashboard</h2>
    
        <div class="row">
            <!-- Orders Section -->
            <div class="col-6 mb-4">
                <div class="card">
                    <div class="card-header bg-danger text-white">Orders</div>
                    <div class="card-body" style="border: 2px solid; box-shadow: 14px 14px 35px">
                        <form action="your_servlet_url" method="post">
                            <div class="mb-3 ">
                                <label for="orderStatus" class="form-label">Filter Order Status:</label>
                                  <input type="text" id="searchInput" style="border: 2px solid #4aad03eb; border-radius: 20px;">  </div>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Partner Name</th>
                                            <th>Number of orders</th>
                                            <th>Order completed</th>
                                            <th>Orders Pending Approval</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                             
                                        <tr>
                                            <td></td>
                                            <td>Backordered</td>
                                            <td>May 1, 2020</td>
                                            <td>Wellton</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Inventory Section -->
            <div class="col-6 mb-4">
                <div class="card">
                    <div class="card-header bg-info text-white">Inventory</div>
                    <div class="card-body" style="border: 2px solid; box-shadow: 14px 14px 35px">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Product</th>
                                        <th>SKU</th>
                                        <th>In Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Sample rows -->
                                    <tr>
                                        <td>Electronics</td>
                                        <td>Sony Bravia KDL-32V4000</td>
                                        <td>784560</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Electronics</td>
                                        <td>LG G2P60</td>
                                        <td>874841</td>
                                        <td>10</td>
                                    </tr>
                                    <!-- Add more rows as needed -->
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-around mt-3">
                            <div>
                                <p>Electronics: 514</p>
                                <p>Computers: 400</p>
                            </div>
                            <div>
                                <p>Appliances: 188</p>
                                <p>Automotive: 135</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- KPIs-Monthly Section -->
            <div class="col-6 mb-4">
                <div class="card">
                    <div class="card-header bg-info text-white">KPIs-Monthly</div>
                    <div class="card-body" style="border: 2px solid; box-shadow: 14px 14px 35px">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Metric</th>
                                        <th>This Month</th>
                                        <th>Past Month</th>
                                        <th>Change</th>
                                        <th>Past 30 Days</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Sample rows -->
                                    <tr>
                                        <td>Inventory</td>
                                        <td>$385,904</td>
                                        <td>$385,637</td>
                                        <td>11%</td>
                                        <td>$345,220</td>
                                    </tr>
                                    <tr>
                                        <td>Shipping Costs</td>
                                        <td>$25,091</td>
                                        <td>$28,747</td>
                                        <td>-12%</td>
                                        <td>$26,000</td>
                                    </tr>
                                    <!-- Add more rows as needed -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
  <!-- Volume Today Section -->
            <div class="col-6 mb-4">
                <div class="card">
                    <div class="card-header bg-danger text-white">Volume Today</div>
                    <div class="card-body" style="border: 2px solid; box-shadow: 14px 14px 35px">
                        <div class="row text-center">
                            <div class="col">
                                <p>Orders to Ship: <strong>105</strong></p>
                            </div>
                            <div class="col">
                                <p>Overdue Shipments: <strong>21</strong></p>
                            </div>
                            <div class="col">
                                <p>Open PCs: <strong>197</strong></p>
                            </div>
                            <div class="col">
                                <p>Late Vendor Shipments: <strong>11</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Global Financial Performances Section -->
            <div class="col-12 mb-4">
                <div class="card">
                    <div class="card-header bg-danger text-white">Global Financial Performances</div>
                    <div class="card-body" style="border: 2px solid; box-shadow: 14px 14px 35px">
                        <canvas id="financialChart" width="400" height="200"></canvas>
                    </div>
                </div>
            </div>

          
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        const ctx = document.getElementById('financialChart').getContext('2d');
        const financialChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['NA', 'EUA', 'ASIA'],
                datasets: [{
                    label: 'C-In-Do Cash Cycle Time',
                    data: [10, 20, 30],
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                }, {
                    label: 'Accounts Rec. Days',
                    data: [15, 25, 35],
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                }, {
                    label: 'Accounts Payable Days',
                    data: [20, 30, 40],
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
      </div>
                    <!-- page-body-wrapper ends -->
                </div>
                
                <!-- End custom js for this page -->
        </body>
    </html>
