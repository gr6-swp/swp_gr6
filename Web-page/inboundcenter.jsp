
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="model.WarehouseZone" %>
<%@ page import="dao.DAO" %>




<!DOCTYPE html>
<html xmlns:c="http://java.sun.com/jsp/jstl/core">
    <html>

        <style>
            /* CSS để tùy chỉnh giao diện */
            body {
                font-family: Arial, sans-serif;
                background-color: #f5f5f5;
                margin: 0;
                padding: 0;
                color: #333;
            }

            .container {
                width: 80%;
                margin: 20px auto;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                padding: 20px;
                border-radius: 8px;
            }

            form {
                display: flex;
                justify-content: space-between;
                margin-bottom: 20px;
            }

            form input[type="text"] {
                width: 80%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            form button {
                padding: 10px 20px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            .partner-section {
                margin-bottom: 20px;
            }

            .partner-section input[type="text"] {
                width: 100%;
                padding: 10px;
                margin-bottom: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            .btn {
                padding: 8px 12px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                margin-right: 5px;
            }

            .btn:hover {
                background-color: #45a049;
            }

            .outer-container {
                border: 3px solid #4CAF50; /* Màu xanh */
                padding: 17px;
                margin-left: 3%;
                margin-top: 1%;
                width: 91%;
                background-color: #f9f9f9; /* Màu nền cho khung chứa toàn bộ nội dung */
                border-radius: 8px; /* Bo tròn các góc của khung */
            }

            .inner-container {
                padding: 0 10px; /* Tạo khoảng cách giữa nội dung và khung ngoài */
            }

            .bordered-section {
                border: 1px solid #000; /* Màu đen */
                padding: 10px;
                margin-bottom: 20px;
                border-radius: 8px;
            }

            .partner-header {
                background-color: #4CAF50; /* Màu xanh */
                color: white;
                padding: 10px;
                border-top-left-radius: 8px;
                border-top-right-radius: 8px;
            }
            .search-container {
                margin-bottom: 20px; /* Khoảng cách giữa phần tìm kiếm và phần còn lại */
            }


            .search-container button {

                background-color: #4CAF50;
                color: white;
                border-radius: 7px;
            }

            .modal {
                display: none; /* Ẩn popup ban đầu */
                position: fixed; /* Ở vị trí cố định so với viewport */
                z-index: 1; /* Đảm bảo popup nằm trên các phần tử khác */
                left: 0;
                top: 0;
                width: 70%;
                height: 100%;
                overflow: auto;
                background-color: rgba(0, 0, 0, 0.4); /* Màu nền mờ */
            }

            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* Căn giữa popup */
                padding: 17px;
                border: 3px solid #888;
                width: 50%; /* Giới hạn chiều rộng tối đa */
                border-radius: 30px;
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }

            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }

            .popup-content {
                padding: 20px;
            }

            .popup-content h2 {
                color: #000;
            }

            .popup-content label {
                color: #000;
            }

            .popup-content table {
                width: 100%;
                border-collapse: collapse;
            }

            .popup-content th, .popup-content td {
                padding: 8px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }

            .popup-content button {
                background-color: #4CAF50;
                color: white;
                padding: 10px 20px;
                border: none;
                cursor: pointer;
            }

            .popup-content button:hover {
                background-color: #45a049;
            }

            #container > div {
                margin-bottom: 15px;
            }

            #container label {
                display: block;
                margin-bottom: 5px;
            }

            #container input[type="text"],
            #container select,
            #container input[type="number"] {
                padding: 2px;
                margin-bottom: 7px;
                border: 2px solid #ccc;
                border-radius: 4px;
            }

            #container table th {
                background-color: #4CAF50;
                color: white;
            }

            #container table input {
                width: 100%;
                padding: 5px;
                box-sizing: border-box;
            }

            .container {
                display: grid;
                grid-template-columns: 1fr 1fr;
                gap: 10px;
            }

            .container > div {
                display: flex; /* Sử dụng flexbox để căn chỉnh label và input */
                align-items: center;
            }

            .container label {
                width: 120px;
                margin-right: 10px; /* Khoảng cách giữa label và input */
            }



        </style>
        <head>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport"
                  content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <title>Trang chủ</title>
            <!-- plugins:css -->
            <link rel="stylesheet"
                  href="assets/vendors/mdi/css/materialdesignicons.min.css">
            <link rel="stylesheet"
                  href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
            <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
            <!-- endinject -->
            <!-- Plugin css for this page -->
            <link rel="stylesheet"
                  href="assets/vendors/font-awesome/css/font-awesome.min.css" />
            <link rel="stylesheet"
                  href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
            <!-- End plugin css for this page -->
            <!-- inject:css -->
            <!-- endinject -->
            <!-- Layout styles -->
            <link rel="stylesheet" href="assets/css/style.css">
            <!-- End layout styles -->
            <link rel="shortcut icon" href="assets/images/favicon.png" />
        </head>
        <body>
            <div class="container-scroller" style="width: 102%;">

                <%@ include file="sidebarCenter.jsp" %>
                <!-- partial -->
                <div class="container-scroller" style="width: 130%">
                    <div class="outer-container">
                        <form  action="inboundcenter" method="GET" style="display: flex; align-items: center; justify-content: space-between;">
                            <div class="search-container" style="display: flex; align-items: center;">
                                <h2 style="color: #007bff; margin: 0; font-size: 30px">
                                    <span style="margin-left: 30px;">List Order</span> </h2>
                            </div>

                            <c:if test="${not empty searchResultMessage}">
                                <p style="color: red; display: inline-block; margin-left: -35px; font-size: 19px;margin-top :10px; vertical-align: top;">${searchResultMessage}</p>
                            </c:if>

                            <div style="display: flex; align-items: center;">
                                <input type="text" id="searchInput"  name="keyword" placeholder="Tìm kiếm..." value="${param.keyword}">
                                <button type="submit">Search</button>
                            </div>


                        </form>


                        <div class="inner-container">
                            <c:forEach items="${proor}" var="entry">
                                <div class="bordered-section">
                                    <div class="partner-section">
                                        <div class="partner-header">
                                            <h3>PARTNER ID : ${entry.key.partnerID} | PARTNER NAME : ${entry.key.partnerName}</h3>
                                        </div>
                                        <table style="color: black">
                                            <thead>
                                                <tr style="color: black">
                                                    <th>ID</th>
                                                    <th>Product Name</th>
                                                    <th>Description</th>
                                                    <th>Quantity</th>
                                                    <th>Unit Price</th>                                                       
                                                    <th>Date</th>
                                                    <th>Status</th>
                                                    <th>Type</th>
                                                    <th>Zone</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${entry.value}" var="o">
                                                    <tr>
                                                        <td>${o.productID}</td>
                                                        <td>${o.productName}</td>
                                                        <td>${o.description}</td>
                                                        <td>${o.quantity}</td>
                                                        <td>${o.unitPrice}</td>
                                                        <td>${o.createdAt}</td>                                                                                                
                                                        <td>${o.orderStatus}</td>
                                                        <th>${o.orderType}</th>
                                                        <td>${o.zoneID}</td>
                                                        <td>
                                                            <c:if test="${o.orderStatus eq 'Chờ phê duyệt'}">
                                                                <button class="btn" style="background-color: #4CAF50; color: white;"
                                                                        onclick="openModal('${o.productID}', '${o.productName}', '${o.description}', '${o.quantity}', '${o.unitPrice}', '${o.orderType}', '${entry.key.partnerID}')">CONFIRM</button>
                                                            </c:if>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>


                <!-- The Modal -->
                <div id="myModal" class="modal">
                    <div class="modal-content" style="width: 59%; margin-left: 29%; margin-top: 8%; height: 65%;border-radius: 10px;">
                        <span class="close" onclick="closeModal()">&times;</span>
                        <div class="popup-content">
                            <h2 style="color: #000">Products Of Partners</h2>
                            <form id="createOrderForm" action="updatewarehouse" method="POST">
                                <div id="container">
                                    <div class="container">
                                        <div>
                                            <label for="productID">ID Product</label>
                                            <input type="text" id="productID" name="productID" required readonly>
                                        </div>

                                        <div>
                                            <label for="orderType">Order Type:</label>
                                            <input type="text" id="OrderType" name="OrderType"  required readonly>
                                        </div>

<%
    DAO dao = new DAO();
    List<WarehouseZone> zones = dao.getZones();
%>

<div id="zoneContainer" style="display: flex; align-items: center;">
    <label for="Zone" style="margin-right: 10px;">Zone:</label>
    <select id="Zone" name="Zone" required style="flex-grow: 1;">
        <option value="">Select Zone</option>
        <% for (WarehouseZone zone : zones) { %>
            <option value="<%= zone.getZoneID() %>"><%= zone.getZoneName() %></option>
        <% } %>
    </select>
</div>



                                        <div>
                                            <label for="orderStatus">Orders STATUS:</label>
                                            <select id="orderStatus" name="orderStatus" required onchange="toggleZoneInput()">
                                                <option value="Đang xử lý">Xác Nhận</option>
                                                <option value="Đã hủy">Đã hủy</option>
                                            </select>
                                        </div>
                                    </div>
                                    <h2 style="color: black">DETAILS</h2>
                                    <table class="order-table">
                                        <thead>
                                            <tr style="color:black">
                                                <th>Product Name</th>
                                                <th>Description</th>
                                                <th>Quantity</th>
                                                <th>Unit Price</th>
                                            </tr>
                                        </thead>
                                        <tbody id="orderDetails">
                                            <tr>
                                                <td>
                                                    <input type="text" id="ProductName" name="ProductName" required readonly>
                                                </td>
                                                <td>
                                                    <input type="text" id="Description" name="Description" required readonly>
                                                </td>
                                                <td>
                                                    <input type="number" id="Quantity" name="Quantity" required readonly>
                                                </td>
                                                <td>
                                                    <input type="number" id="UnitPrice" name="UnitPrice" required readonly>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button type="submit">CONFIRM</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <!-- container-scroller -->
                <!-- plugins:js -->

                <script>
                    function toggleZoneInput() {
                        const orderStatus = document.getElementById('orderStatus').value;
                        const zoneInput = document.getElementById('Zone');
                         const orderType = document.getElementById("OrderType").value; // Lấy giá trị của orderType
                        const cancelledOption = document.getElementById("orderStatus").querySelector('option[value="Đã hủy"]');


                        if (orderStatus === 'Đã hủy') {
                            zoneInput.required = false;
                            zoneInput.value = '';
                        } else {
                            zoneInput.required = true;
                        }

                        if (orderType === 'Xuất kho') {
                                            cancelledOption.style.display = 'none';
                                      } else {
                                            cancelledOption.style.display = 'block';
                                      }

                    }

                    document.addEventListener('DOMContentLoaded', (event) => {
                        toggleZoneInput();  // Set the initial state
                    });


                    // Get the modal
                    var modal = document.getElementById("myModal");

                    // Function to open the modal with data
                    function openModal(productID, productName, description, quantity, unitPrice, orderType) {
                        // Open the modal
                        modal.style.display = "block";
                        const zoneContainer = document.getElementById("zoneContainer");
                        const zoneSelect = document.getElementById("Zone");

                        // Set form fields with data
                        document.getElementById("productID").value = productID;
                        document.getElementById("ProductName").value = productName;
                        document.getElementById("Description").value = description;
                        document.getElementById("Quantity").value = quantity;
                        document.getElementById("UnitPrice").value = unitPrice;
                        document.getElementById("OrderType").value = orderType;

                        if (orderType === "Nhập kho") {
                            zoneContainer.style.display = "flex";
                            zoneSelect.removeAttribute("disabled");
                        } else {
                            zoneContainer.style.display = "none";
                            zoneSelect.setAttribute("disabled", "true");
                        }

                        toggleZoneInput();
                    }



                    // Function to close the modal
                    function closeModal() {
                        modal.style.display = "none";
                    }

                    // Close the modal if the user clicks outside of it
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }

                    function updateWarehouseStatus() {
                        // Điền dữ liệu vào form ẩn
                        document.getElementById("productIDHidden").value = document.getElementById("productID").value;
                        document.getElementById("WarehouseHidden").value = document.getElementById("Warehouse").value;
                        document.getElementById("orderStatusHidden").value = document.getElementById("orderStatus").value;

                        // Gửi form ẩn
                        document.getElementById("updateWarehouseStatusForm").submit();
                    }
                </script>


                <script src="assets/vendors/js/vendor.bundle.base.js"></script>
                <!-- endinject -->
                <!-- Plugin js for this page -->
                <script src="assets/vendors/chart.js/Chart.min.js"></script>
                <script
                src="assets/vendors/jquery-circle-progress/js/circle-progress.min.js"></script>
                <!-- End plugin js for this page -->
                <!-- inject:js -->
                <script src="assets/js/off-canvas.js"></script>
                <script src="assets/js/hoverable-collapse.js"></script>
                <script src="assets/js/misc.js"></script>
                <!-- endinject -->
                <!-- Custom js for this page -->
                <script src="assets/js/dashboard.js"></script>
                <!-- End custom js for this page -->
        </body>
    </html>
