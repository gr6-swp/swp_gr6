<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Partner</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="assets/images/logo-mini.svg" />
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            h2 {
                margin-bottom: 10px;
            }

            /* Ki?u dáng cho nút */
            button {
                background-color: #4CAF50; /* Màu xanh lá cây */
                color: white;
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Ki?u dáng cho ô tìm ki?m và select */
            input[type="text"], select {
                padding: 8px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* Ki?u dáng cho b?ng */
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 15px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            input[type="text"] {
                padding: 10px;
                border: 2px solid #4CAF50; /* Vi?n xanh lá cây */
                border-radius: 5px; /* Bo góc */
                width: 200px; /* ?i?u ch?nh ?? r?ng tùy ý */
            }

            button {
                /* ... (các ki?u dáng khác c?a nút) */
                margin-left: 5px; /* T?o kho?ng cách gi?a nút và ô tìm ki?m */
            }
            #container {
                border: 2px solid #4CAF50; /* Vi?n xanh d??ng */
                padding: 20px; /* Kho?ng cách bên trong khung */
                border-radius: 10px; /* Bo góc (tùy ch?n) */
            }


            body {
                font-family: sans-serif;
            }
            label {
                display: block;
                margin-bottom: 5px;
            }
            input, select {
                width: 100%;
                padding: 8px;
                margin-bottom: 10px;
                box-sizing: border-box;
            }
            button {
                padding: 10px;
                background-color: #4CAF50;
                color: white;
                border: none;
                cursor: pointer;
            }
            #orderDetails {
                margin-top: 20px;
            }

            .form-group {
                margin-bottom: 15px;
            }


            th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
                padding: 10px;
                border-bottom: 1px solid #dee2e6;
            }

            .form-row {
                display: flex;
                justify-content: space-between;
            }
            .modal-header {
                color: white;
            }
            .modal-footer {
                justify-content: center;
            }
            .modal-body{
                border: 2px solid #4CAF50; /* Vi?n xanh d??ng */
                padding: 20px; /* Kho?ng cách bên trong khung */
                border-radius: 10px;
                margin: 20px
            }
            .modal-body .form-group label {
                font-weight: bold;
            }
            .btn-custom {
                background-color: #28a745;
                color: white;
            }
            .btn-custom:hover {
                background-color: #218838;
            }
            .form-control {
                border-radius: 0;
            }
            .table th {
                background-color: #28a745;
                color: white;
            }
            .table input {
                width: 100%;
                border: none;
            }
            .form-container {
                display: flex;
                align-items: center;
                justify-content: space-between;
                border: 1px solid #ccc;
                border-radius: 5px;
                background-color: #f9f9f9;
            }
            .form-container select,
            .form-container .products-count {
                padding: 5px;
                font-size: 14px;
            }
            .form-container .sort-by {
                display: flex;
                align-items: center;
            }
            .form-container .sort-by select {
                margin-left: 5px;
            }
            .form-container .view-options {
                display: flex;
                align-items: center;
            }
            .form-container .view-options i {
                margin-left: 5px;
                cursor: pointer;
            }
        </style>

    </head>
    <body>
        <%@ include file="sidebarPartner.jsp" %>
        <%@ include file="profile.jsp" %>

    </body>
</html>
