<%-- 
    Document   : PartnerOderStatistic
    Created on : Jul 9, 2024, 1:00:26 PM
    Author     : songl
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<taglib-uri>http://java.sun.com/jsp/jstl/core</taglib-uri>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Trang chủ</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="assets/images/favicon.png" />
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            h2 {
                margin-bottom: 10px;
            }

            /* Kiểu dáng cho nút */
            button {
                background-color: #4CAF50; /* Màu xanh lá cây */
                color: white;
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Kiểu dáng cho ô tìm kiếm và select */
            input[type="text"], select {
                padding: 8px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* Kiểu dáng cho bảng */
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 15px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }
            .btn-primary{
                color: #fff;
                background-color: #4CAF50;
                border-color: #0062ff;
            }
            th {
                background-color: #f2f2f2;
            }

            input[type="text"] {
                padding: 10px;
                border: 2px solid #4CAF50; /* Viền xanh lá cây */
                border-radius: 5px; /* Bo góc */
                width: 200px; /* Điều chỉnh độ rộng tùy ý */
            }

            button {
                /* ... (các kiểu dáng khác của nút) */
                margin-left: 5px; /* Tạo khoảng cách giữa nút và ô tìm kiếm */
            }
            #container {
                border: 2px solid #4caf50;
                padding: 20px;
                border-radius: 10px;
            }


            body {
                font-family: sans-serif;
            }
            label {
                display: block;
                margin-bottom: 5px;
            }
            input, select {
                width: 100%;
                padding: 8px;
                margin-bottom: 10px;
                box-sizing: border-box;
            }
            button {
                padding: 10px;
                background-color: #4CAF50;
                color: white;
                border: none;
                cursor: pointer;
            }
            #orderDetails {
                margin-top: 20px;
            }

            .form-group {
                margin-bottom: 15px;
            }


            th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
                padding: 10px;
                border-bottom: 1px solid #dee2e6;
            }

            .form-row {
                display: flex;
                justify-content: space-between;
            }

            .navbar.fixed-top + .page-body-wrapper {
                padding-top: 46px;
            }
            .export-button-container {
                margin-top: 20px; /* Điều chỉnh khoảng cách giữa nút xuất Excel và bảng */
            }
            .btn-success{
                background-color: #4CAF50;
                border-color: #0062ff;
            }
        </style>
    </head>
    <body>
        <div class="container-scroller">
            <!-- partial:partials/_navbar.html -->
            <nav
                class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div
                    class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                    <a class="navbar-brand brand-logo" href="homepartner"><img
                            src="assets/images/logo.svg" alt="logo" /></a>
                    <a class="navbar-brand brand-logo-mini"
                       href="homepartner"><img src="assets/images/logo-mini.svg"
                                            alt="logo" /></a>
                </div>

                <div class="navbar-menu-wrapper d-flex align-items-stretch">
                    <button
                        class="navbar-toggler navbar-toggler align-self-center"
                        type="button" data-toggle="minimize">
                        <span class="mdi mdi-menu"></span>
                    </button>
                    <form class="d-flex align-items-center h-100"
                          style="font-size: larger;
                          padding-top: 10px;">
                        <h2>Report</h2>
                    </form>
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item  dropdown d-none d-md-block">

                            <a style="color: black"
                               href="trogiup"

                               ><svg width="24"
                                  height="24" fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                  font-size="24" class="jss220"
                                  color="#A2A8AF"><path
                                  d="M19.08 4.93A9.972 9.972 0 0 0 12 2a9.928 9.928 0 0 0-7.07 2.93A9.947 9.947 0 0 0 2 12a9.947 9.947 0 0 0 2.93 7.07A9.947 9.947 0 0 0 12 22a9.947 9.947 0 0 0 7.07-2.93A9.947 9.947 0 0 0 22 12a9.937 9.937 0 0 0-2.92-7.07ZM12 11.38a2.817 2.817 0 0 1 2.81 2.81c0 1.285-.86 2.374-2.048 2.701a.187.187 0 0 0-.141.185v.523c0 .338-.261.621-.6.643a.622.622 0 0 1-.653-.621v-.545a.187.187 0 0 0-.141-.185 2.83 2.83 0 0 1-2.048-2.69c0-.338.261-.632.61-.643a.626.626 0 0 1 .642.62 1.56 1.56 0 0 0 1.678 1.558 1.567 1.567 0 0 0 1.449-1.448A1.567 1.567 0 0 0 12 12.61c-1.547.022-2.81-1.242-2.81-2.8 0-1.274.871-2.363 2.048-2.701a.2.2 0 0 0 .141-.185V6.39c0-.338.261-.62.6-.643a.622.622 0 0 1 .653.621v.545c0 .087.054.163.141.185a2.83 2.83 0 0 1 2.048 2.69.637.637 0 0 1-.61.643.626.626 0 0 1-.642-.62 1.56 1.56 0 0 0-1.678-1.558 1.576 1.576 0 0 0-1.449 1.449A1.567 1.567 0 0 0 12 11.379Z"
                                  fill="#A2A8AF"></path></svg>
                                Vay vốn kinh doanh </a>
                        </li>

                        <li class="nav-item nav-profile dropdown">
                            <a class="nav-link dropdown-toggle"
                               id="profileDropdown" href="#"
                               data-toggle="dropdown" aria-expanded="false">
                                <span
                                    style="display: table-cell;
                                    vertical-align: middle;
                                    white-space: nowrap;"></span>
                                <div class="nav-profile-text">

                                    <p class="mb-1 text-black"> Your Name </p>
                                </div>
                            </a>
                            <div
                                class="dropdown-menu navbar-dropdown dropdown-menu-right p-0 border-0 font-size-sm"
                                aria-labelledby="profileDropdown"
                                data-x-placement="bottom-end">
                                <div class="p-2">
                                    <h5
                                        class="dropdown-header text-uppercase pl-2 text-dark">User
                                        Options</h5>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>Profile</span>
                                        <span class="p-0">
                                            <span
                                                class="badge badge-success">1</span>
                                            <i
                                                class="mdi mdi-account-outline ml-1"></i>
                                        </span>
                                    </a>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="javascript:void(0)">
                                        <span>Service pack information</span>
                                        <i class="mdi mdi-settings"></i>
                                    </a>
                                    <div role="separator"
                                         class="dropdown-divider"></div>
                                    <h5
                                        class="dropdown-header text-uppercase  pl-2 text-dark mt-2">Actions</h5>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>File import and export
                                            history</span>
                                        <svg width="24" height="24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg"
                                             font-size="24"><g
                                             clip-path="url(#file-icon_svg__a)"
                                             fill="currentColor"><path
                                             d="M20.02 7.443c.209 0 .398.045.588.1v-.136c0-.94-.768-1.708-1.708-1.708h-6.462l-1.455-1.446A.856.856 0 0 0 10.378 4H5.272c-.94 0-1.709.768-1.709 1.708v1.79c.136-.028.272-.046.407-.046h16.05v-.009Z"></path><path
                                             fill-rule="evenodd"
                                             clip-rule="evenodd"
                                             d="M3.97 8.546h16.05c1.094.009 1.971.84 1.98 1.88v.027l-1.139 8.097c-.018 1.022-.904 1.853-1.988 1.853H5.136c-1.094 0-1.97-.822-1.988-1.853L2 10.453v-.027c0-1.04.886-1.871 1.97-1.88Zm7.393 3.19v1.057c2.088.118 3.588 1.944 3.597 4.429 0 .1-.082.19-.18.208h-.037a.203.203 0 0 1-.199-.145c-.47-1.4-1.51-2.115-3.172-2.169v1.04a.22.22 0 0 1-.081.162.213.213 0 0 1-.299-.027l-1.898-2.314a.206.206 0 0 1 .01-.27L11 11.6a.193.193 0 0 1 .154-.073c.118 0 .208.09.208.208Z"></path></g><defs><clippath
                                             id="file-icon_svg__a"><path
                                                fill="#fff"
                                                transform="translate(2 4)"
                                                d="M0 0h20v16.412H0z"></path></clippath></defs></svg>
                                    </a>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>Lock Account</span>
                                        <i class="mdi mdi-lock ml-1"></i>
                                    </a>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>Log Out</span>
                                        <i class="mdi mdi-logout ml-1"></i>
                                    </a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link count-indicator dropdown-toggle"
                               id="notificationDropdown" href="#"
                               data-toggle="dropdown">
                                <i class="mdi mdi-bell-outline"></i>
                                <span class="count-symbol bg-danger"></span>
                            </a>
                            <div
                                class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list"
                                aria-labelledby="notificationDropdown">
                                <h6
                                    class="p-3 mb-0 bg-primary text-white py-4">Thông
                                    báo</h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-success">
                                            <i class="mdi mdi-calendar"></i>
                                        </div>
                                    </div>
                                    <div
                                        class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6
                                            class="preview-subject font-weight-normal mb-1">Sự
                                            kiện hôm nay
                                        </h6>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-warning">
                                            <i class="mdi mdi-settings"></i>
                                        </div>
                                    </div>
                                    <div
                                        class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6
                                            class="preview-subject font-weight-normal mb-1">Cập
                                            nhật </h6>
                                        <p class="text-gray ellipsis mb-0">
                                            Update website </p>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-info">
                                            <i class="mdi mdi-link-variant"></i>
                                        </div>
                                    </div>
                                    <div
                                        class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6
                                            class="preview-subject font-weight-normal mb-1">News
                                        </h6>
                                        <p class="text-gray ellipsis mb-0"> News
                                            everyday
                                            ! </p>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <h6 class="p-3 mb-0 text-center">
                                    Tất cả thông báo</h6>
                            </div>
                        </li>
                    </ul>
                    <button
                        class="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
                        type="button" data-toggle="offcanvas">
                        <span class="mdi mdi-menu"></span>
                    </button>
                </div>
            </nav>
            <!-- partial -->
            <div class="container-fluid page-body-wrapper">

                <nav class="sidebar sidebar-offcanvas" id="sidebar">
                    <ul class="nav">
                        <!-- /* style=" position: fixed;
                                        left: 0;
                    width: 17%;
                    background-color: #333; 
                    z-index: 1000;" */ -->
                        <li class="nav-item">
                            <a class="nav-link" href="Report.html">
                                <span class="icon-bg"><svg viewBox="0 0 25 24"
                                                           fill="none"
                                                           xmlns="http://www.w3.org/2000/svg"><path
                                                           fill-rule="evenodd"
                                                           clip-rule="evenodd"
                                                           d="M7.002 22a3 3 0 0 1-3-3v-6h-1c-.89 0-1.337-1.077-.707-1.707l9-9a1 1 0 0 1 1.414 0l9 9c.63.63.184 1.707-.707 1.707h-1v6a3 3 0 0 1-3 3h-10Zm5-17.586-6.648 6.65a1 1 0 0 1 .649.936v7a1 1 0 0 0 1 1h2v-4a2 2 0 0 1 2-2h2a2 2 0 0 1 2 2v4h2a1 1 0 0 0 1-1v-7a1 1 0 0 1 .649-.937l-6.65-6.649Zm1 11.586h-2v4h2v-4Z"
                                                           fill="currentColor"></path></svg></i></span>
                                <span class="menu-title">Tổng quan</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"
                               href="#au" aria-expanded="false"
                               aria-controls="au">
                                <span class="icon-bg"><i class="fa fa-bell" style="font-size:24px"></i></span>
                                <span class="menu-title">Notication</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="au">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a class="nav-link"
                                                             href>
                                            Partner  </a></li>
                                    <li class="nav-item"> <a class="nav-link"
                                                             href>
                                            Keeper  </a></li>
                                </ul>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="Report.html">
                                <span class="icon-bg"><i class="fa fa-desktop" style="font-size:24px"></i></span>
                                <span class="menu-title"> Warehouse</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Report.html">
                                <span class="icon-bg"><i class="fa fa-clipboard" style="font-size:24px"></i></span>
                                <span class="menu-title"> Order </span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link"  href="" >
                                <span class="icon-bg"><svg viewBox="0 0 24 24"
                                                           fill="none"
                                                           xmlns="http://www.w3.org/2000/svg"><path
                                                           d="m21.24 6.62-8.832-4.474-.003-.002a1.38 1.38 0 0 0-1.25.012l-3.789 2.01a.59.59 0 0 0-.088.047l-4.54 2.41A1.373 1.373 0 0 0 2 7.841v8.316c0 .514.283.981.738 1.22l8.413 4.465.003.002a1.377 1.377 0 0 0 1.25.011l8.836-4.477c.47-.235.761-.706.761-1.231V7.852c0-.525-.291-.997-.76-1.231Zm-9.543-3.426a.205.205 0 0 1 .184-.002l8.267 4.189-3.217 1.602-8.083-4.277 2.85-1.512Zm-.5 17.347-7.911-4.2-.004-.001a.204.204 0 0 1-.11-.182V8.371l8.025 4.184v7.986Zm.592-8.998L3.808 7.38l3.79-2.012 8.042 4.256-3.851 1.918Zm9.04 4.605a.204.204 0 0 1-.113.183l-8.348 4.23v-7.998l3.844-1.914v2.007a.586.586 0 0 0 1.171 0v-2.59L20.83 8.35v7.798Z"
                                                           fill="currentColor"
                                                           stroke="currentColor"
                                                           stroke-width="0.5"></path></svg></i></span>
                                <span class="menu-title">Product</span>
                            </a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"
                               href="#auds" aria-expanded="false"
                               aria-controls="auds">
                                <span class="icon-bg"><i class="fa fa-group" style="font-size:24px"></i></span>
                                <span class="menu-title">Report</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="auds">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> 
                                        <a class="nav-link" href="partnerinventorys">Inventory Statistics</a>
                                    </li>
                                    <li class="nav-item"> <a class="nav-link" href="parnerorders">
                                            Order Statistics  </a></li>                                   
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item sidebar-user-actions">
                            <div class="user-details">
                                <div
                                    class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <div class="d-flex align-items-center">
                                            <div class="sidebar-profile-text">
                                                <p class="mb-1"> SYSTEM </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"
                               href="#ut" aria-expanded="false"
                               aria-controls="ut">
                                <span class="icon-bg"><i
                                        class="mdi mdi-crosshairs-gps menu-icon"></i></span>
                                <span class="menu-title">Setting Configuration</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ut">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a
                                            class="nav-link"
                                            href>
                                            Shop Setup
                                        </a> </li>
                                    <li class="nav-item"> <a
                                            class="nav-link"
                                            href>
                                            Sales Setup</a></li>
                                    <li class="nav-item"> <a
                                            class="nav-link"
                                            href>
                                            Genenal Diary</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item sidebar-user-actions">
                            <div class="sidebar-user-menu">
                                <a href="#" class="nav-link"><i
                                        class="mdi mdi-logout menu-icon"></i>
                                    <span class="menu-title">Log
                                        Out</span></a>
                            </div>
                        </li>
                    </ul>
                </nav>

                <div style="margin-left: 5%; margin-top: 1%; margin-right: 0%; width: 71%">
                    <h2 style="color: #007bff">Inventory Statistics</h2>

                    <% Integer partnerID = (Integer) session.getAttribute("partnerID");
                       if (partnerID == null) {
                           // Handle case where partnerID is null
                    %>
                    <p>Partner ID is missing. Please <a href="login.jsp">log in</a> again.</p>
                    <% return; } %>

                    <div id="container">
                        <div class="search-container">
                            <form action="parnerorders" method="get" class="search-container">
                                <select name="productName" class="form-control" id="product-select">
                                    <option value="" disabled selected>Select Product</option>
                                    <option value="all">All Product</option>
                                    <c:forEach var="name" items="${ productNames}">
                                        <c:choose>
                                            <c:when test="${param.productName == name}">
                                                <option value="${name}" selected>${name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${name}">${name}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                    <!-- Add more options as needed -->
                                </select>
                                <button type="submit" class="btn btn-primary">Search</button>
                            </form>
                        </div>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Product Name</th>
                                    <th>Quantity Product</th>
                                    <th>Order Type</th>
                                    <th>Order Status</th>



                                </tr>
                            </thead>
                            <tbody>
                                <c:if test="${not empty partneroderlist}">
                                    <c:forEach var="item" items="${partneroderlist}" varStatus="status">
                                        <tr>
                                            <td>${status.index + 1}</td>
                                            <td>${item.product}</td>
                                            <td>${item.quantity}</td>
                                            <td>${item.orderType}</td>
                                            <td>${item.orderStatus}</td>


                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${empty partneroderlist}">
                                    <tr>
                                        <td colspan="3">No order data available</td>
                                    </tr>
                                </c:if>
                            </tbody>
                        </table>

                        <div class="export-button-container">
                            <form action="exparneroders" method="post" id="exportForm">
                                <input type="hidden" name="productNameSelected" id="productNameSelected">
                                <button type="button" class="btn btn-success" id="exportExcelBtn" onclick="exportToExcel()">Export to Excel</button>
                            </form>
                        </div>
                    </div>
                </div>



                <script>
                    document.getElementById('exportExcelBtn').addEventListener('click', function () {
                        var productNameSelect = document.querySelector('select[name="productName"]');
                        var productNameSelected = productNameSelect.options[productNameSelect.selectedIndex].value;
                        document.getElementById('productNameSelected').value = productNameSelected;
                        document.getElementById('exportForm').submit();
                    });
                </script>


                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
                <script>
                      function handleSearch(event) {
                          if (event.key === "Enter") {
                              performSearch();
                          }
                      }
                </script>          
                <script src="assets/vendors/js/vendor.bundle.base.js"></script>
                <!-- endinject -->
                <!-- Plugin js for this page -->
                <script src="assets/vendors/chart.js/Chart.min.js"></script>
                <script
                src="assets/vendors/jquery-circle-progress/js/circle-progress.min.js"></script>
                <!-- End plugin js for this page -->
                <!-- inject:js -->
                <script src="assets/js/off-canvas.js"></script>
                <script src="assets/js/hoverable-collapse.js"></script>
                <script src="assets/js/misc.js"></script>
                <!-- endinject -->
                <!-- Custom js for this page -->
                <script src="assets/js/dashboard.js"></script>
                </body>
                </html>

