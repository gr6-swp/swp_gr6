package dao;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import model.Notification;
import dal.DBConnection;

public class NotificationsDAO {

    private static Connection connection = DBConnection.getConnection();

    // Create a new notification
    public static boolean createNotification(Notification notification) {
        String sql = "INSERT INTO Notifications (relatedEntityId, senderRole, receiverRole, message, status, createdAt) VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, notification.getRelatedEntityId());
            statement.setString(2, notification.getSenderRole());
            statement.setString(3, notification.getReceiverRole());
            statement.setString(4, notification.getMessage());
            statement.setString(5, notification.getStatus());
            statement.setTimestamp(6, Timestamp.valueOf(notification.getCreatedAt()));

            int rowsInserted = statement.executeUpdate();
            return rowsInserted > 0;
        } catch (SQLException e) {
            e.printStackTrace(); // In chi tiết lỗi
        }
        return false;
    }

    // Retrieve all notifications
    public static List<Notification> getAllNotifications() {
        List<Notification> notifications = new ArrayList<>();
        String sql = "SELECT * FROM Notifications";
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Notification notification = new Notification();
                notification.setNotificationID(resultSet.getInt("notificationID"));
                notification.setRelatedEntityId(resultSet.getInt("relatedEntityId"));
                notification.setSenderRole(resultSet.getString("senderRole"));
                notification.setReceiverRole(resultSet.getString("receiverRole"));
                notification.setMessage(resultSet.getString("message"));
                notification.setStatus(resultSet.getString("status"));
                notification.setCreatedAt(resultSet.getTimestamp("createdAt").toLocalDateTime());
                notifications.add(notification);
            }
        } catch (SQLException e) {
            e.printStackTrace(); // In chi tiết lỗi
        }
        return notifications;
    }

    public static List<Notification> getAllNotificationsPartner() {
        List<Notification> notifications = new ArrayList<>();
        String sql = "SELECT NotificationID,\n"
                + "       SenderRole,\n"
                + "       ReceiverRole,\n"
                + "       Message,\n"
                + "       Status,\n"
                + "       CreatedAt,\n"
                + "       relatedEntityId\n"
                + "FROM Notifications\n"
                + "WHERE ReceiverRole = 'Partner'\n"
                + "ORDER BY CreatedAt DESC;";
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Notification notification = new Notification();
                notification.setNotificationID(resultSet.getInt("notificationID"));
                notification.setRelatedEntityId(resultSet.getInt("relatedEntityId"));
                notification.setSenderRole(resultSet.getString("senderRole"));
                notification.setReceiverRole(resultSet.getString("receiverRole"));
                notification.setMessage(resultSet.getString("message"));
                notification.setStatus(resultSet.getString("status"));
                notification.setCreatedAt(resultSet.getTimestamp("createdAt").toLocalDateTime());
                notifications.add(notification);
            }
        } catch (SQLException e) {
            e.printStackTrace(); // In chi tiết lỗi
        }
        return notifications;
    }
    public static List<Notification> getAllNotificationsCenter() {
        List<Notification> notifications = new ArrayList<>();
        String sql = "SELECT NotificationID,\n"
                + "       SenderRole,\n"
                + "       ReceiverRole,\n"
                + "       Message,\n"
                + "       Status,\n"
                + "       CreatedAt,\n"
                + "       relatedEntityId\n"
                + "FROM Notifications\n"
                + "WHERE ReceiverRole = 'Center'\n"
                + "ORDER BY CreatedAt DESC;";
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Notification notification = new Notification();
                notification.setNotificationID(resultSet.getInt("notificationID"));
                notification.setRelatedEntityId(resultSet.getInt("relatedEntityId"));
                notification.setSenderRole(resultSet.getString("senderRole"));
                notification.setReceiverRole(resultSet.getString("receiverRole"));
                notification.setMessage(resultSet.getString("message"));
                notification.setStatus(resultSet.getString("status"));
                notification.setCreatedAt(resultSet.getTimestamp("createdAt").toLocalDateTime());
                notifications.add(notification);
            }
        } catch (SQLException e) {
            e.printStackTrace(); // In chi tiết lỗi
        }
        return notifications;
    }
    public static List<Notification> getAllNotificationsKeeper() {
        List<Notification> notifications = new ArrayList<>();
        String sql = "SELECT NotificationID,\n"
                + "       SenderRole,\n"
                + "       ReceiverRole,\n"
                + "       Message,\n"
                + "       Status,\n"
                + "       CreatedAt,\n"
                + "       relatedEntityId\n"
                + "FROM Notifications\n"
                + "WHERE ReceiverRole = 'Keeper'\n"
                + "ORDER BY CreatedAt DESC;";
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Notification notification = new Notification();
                notification.setNotificationID(resultSet.getInt("notificationID"));
                notification.setRelatedEntityId(resultSet.getInt("relatedEntityId"));
                notification.setSenderRole(resultSet.getString("senderRole"));
                notification.setReceiverRole(resultSet.getString("receiverRole"));
                notification.setMessage(resultSet.getString("message"));
                notification.setStatus(resultSet.getString("status"));
                notification.setCreatedAt(resultSet.getTimestamp("createdAt").toLocalDateTime());
                notifications.add(notification);
            }
        } catch (SQLException e) {
            e.printStackTrace(); // In chi tiết lỗi
        }
        return notifications;
    }

    // Update the status of a notification
    public static boolean updateNotificationStatus(int notificationID, String status) {
        String sql = "UPDATE Notifications SET status = ? WHERE notificationID = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, status);
            statement.setInt(2, notificationID);

            int rowsUpdated = statement.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException e) {
            e.printStackTrace(); // In chi tiết lỗi
        }
        return false;
    }

    public static void main(String[] args) {
        List<Notification> notifications = new ArrayList<>();
        notifications = getAllNotificationsCenter();
        System.out.println(notifications.size());
    }
}
