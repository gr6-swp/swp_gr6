package dal;

import entity.PartnerOder;
import entity.InventoryItem;
import entity.OderIteam;
import entity.PartnerInventory;
import entity.PartnerItem;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ht
 */
public class StatisticDAO {

    private Connection connection;

    public StatisticDAO() {
        DBConnection dBConnection = new DBConnection();
        this.connection = dBConnection.getConnection();
    }

    public List<InventoryItem> fetchInventoryStatistics(String zoneName) throws SQLException {
        List<InventoryItem> poList = new ArrayList<>();
        String sql = "SELECT p.ProductName, z.ZoneName, i.Quantity AS TotalQuantity "
                + "FROM Inventory i "
                + "JOIN Products p ON i.ProductID = p.ProductID "
                + "JOIN WarehouseZones z ON i.WarehouseZoneID = z.ZoneID "
                + "WHERE 1=1";
        if (zoneName != null && !zoneName.isEmpty()) {
            sql += " AND z.ZoneName = ?";
        }
        try ( PreparedStatement ps = connection.prepareStatement(sql)) {
            int paramIndex = 1;
            if (zoneName != null && !zoneName.isEmpty()) {
                ps.setString(paramIndex++, zoneName);
            }
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    InventoryItem item = new InventoryItem();
                    item.setProductName(rs.getString("ProductName"));
                    item.setZoneName(rs.getString("ZoneName"));
                    item.setTotalQuantity(rs.getInt("TotalQuantity"));
                    poList.add(item);
                }
            }
        } catch (SQLException e) {

            throw e;
        }
        return poList;
    }

    public List<OderIteam> fetchOrderStatistics(String zoneName) throws SQLException {
        List<OderIteam> orderStatisticsList = new ArrayList<>();
        String sql = "SELECT "
                + "    wz.ZoneName, "
                + "    o.OrderType, "
                + "    o.OrderStatus, "
                + "    COUNT(DISTINCT o.OrderID) AS TotalQuantity "
                + "FROM "
                + "    Orders o "
                + "JOIN "
                + "    Inventory i ON o.ProductID = i.ProductID "
                + "JOIN "
                + "    WarehouseZones wz ON i.WarehouseZoneID = wz.ZoneID ";
        if (zoneName != null && !zoneName.isEmpty()) {
            sql += "WHERE wz.ZoneName = ? ";
        }
        sql += "GROUP BY wz.ZoneName, o.OrderType, o.OrderStatus "
                + "ORDER BY wz.ZoneName, o.OrderType, o.OrderStatus";

        try ( PreparedStatement ps = connection.prepareStatement(sql)) {
            if (zoneName != null && !zoneName.isEmpty()) {
                ps.setString(1, zoneName);
            }
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    OderIteam stats = new OderIteam();
                    stats.setZoneName(rs.getString("ZoneName"));
                    stats.setOder(rs.getString("OrderType"));
                    stats.setStatus(rs.getString("OrderStatus"));
                    stats.setTotalquantity(rs.getInt("TotalQuantity"));
                    orderStatisticsList.add(stats);
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return orderStatisticsList;
    }

    public List<PartnerItem> fetchPartnerStatistics(String partner) throws SQLException {
        List<PartnerItem> partnerStatisticsList = new ArrayList<>();
        String sql = "SELECT \n"
                + "    p.PartnerName,\n"
                + "    o.OrderType,\n"
                + "    o.OrderStatus,\n"
                + "    COUNT(o.OrderID) AS [Total Quantity]\n"
                + "FROM \n"
                + "    Partners p\n"
                + "JOIN \n"
                + "    Orders o ON p.PartnerID = o.PartnerID\n"
                + "WHERE 1=1"; // Điều kiện mặc định
        if (partner != null && !partner.isEmpty()) {
            sql += " AND p.PartnerName = ?";
        }
        sql += " GROUP BY \n"
                + "    p.PartnerName,\n"
                + "    o.OrderType,\n"
                + "    o.OrderStatus\n"
                + "ORDER BY \n"
                + "    p.PartnerName, \n"
                + "    o.OrderType, \n"
                + "    o.OrderStatus;";
        try ( PreparedStatement ps = connection.prepareStatement(sql)) {
            if (partner != null && !partner.isEmpty()) {
                ps.setString(1, partner);
            }
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    PartnerItem pi = new PartnerItem();
                    pi.setPartner(rs.getString("PartnerName"));
                    pi.setOder(rs.getString("OrderType"));
                    pi.setStatus(rs.getString("OrderStatus"));
                    pi.setTotalquantity(rs.getInt("Total Quantity"));
                    partnerStatisticsList.add(pi);
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return partnerStatisticsList;
    }

    public List<PartnerInventory> fetchPartnerInventoryStatistics(int partnerID, String productName) throws SQLException {
        List<PartnerInventory> partnerInventoryList = new ArrayList<>();
        String sql = "SELECT "
                + "    p.ProductName, "
                + "    SUM(i.Quantity) AS TotalQuantity "
                + "FROM "
                + "    Products p "
                + "JOIN "
                + "    Inventory i ON p.ProductID = i.ProductID "
                + "JOIN "
                + "    Orders o ON o.ProductID = p.ProductID "
                + "WHERE "
                + "    o.PartnerID = ? "
                + "    AND o.OrderStatus = 'Hoàn thành' ";

        if (productName != null && !productName.isEmpty()) {
            sql += "AND p.ProductName = ? ";
        }

        sql += "GROUP BY "
                + "    p.ProductName "
                + "ORDER BY "
                + "    p.ProductName;";

        try ( PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, partnerID);
            if (productName != null && !productName.isEmpty()) {
                ps.setString(2, productName);
            }

            // Debug: In câu truy vấn và các giá trị biến
            System.out.println("Query: " + ps.toString());

            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    PartnerInventory po = new PartnerInventory();
                    po.setProduct(rs.getString("ProductName"));
                    po.setQuantity(rs.getInt("TotalQuantity"));
                    partnerInventoryList.add(po);
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return partnerInventoryList;
    }

   public List<PartnerOder> fetchPartnerOrderStatistics(int partnerID, String productName) throws SQLException {
    List<PartnerOder> partnerOrderList = new ArrayList<>();
    String sql = "SELECT "
            + "    p.ProductName, "
            + "    o.OrderType, "
            + "    o.OrderStatus, "
            + "    SUM(i.Quantity) AS TotalQuantity, "
            + "    COUNT(*) AS OrderTypeQuantity "
            + "FROM "
            + "    Products p "
            + "JOIN "
            + "    Inventory i ON p.ProductID = i.ProductID "
            + "JOIN "
            + "    Orders o ON o.ProductID = p.ProductID "
            + "WHERE "
            + "    o.PartnerID = ? ";

    if (productName != null && !productName.isEmpty()) {
        sql += "AND p.ProductName = ? ";
    }

    sql += "GROUP BY "
            + "    p.ProductName, o.OrderType, o.OrderStatus "
            + "ORDER BY "
            + "    p.ProductName, o.OrderType, o.OrderStatus;";

    try (PreparedStatement ps = connection.prepareStatement(sql)) {
        ps.setInt(1, partnerID);
        if (productName != null && !productName.isEmpty()) {
            ps.setString(2, productName);
        }

        // Debug: In câu truy vấn và các giá trị biến
        System.out.println("SQL Query: " + ps.toString());

        try (ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                PartnerOder po = new PartnerOder();
                po.setPartnerID(partnerID);
                po.setProduct(rs.getString("ProductName"));
                po.setOrderType(rs.getString("OrderType"));
                po.setOrderStatus(rs.getString("OrderStatus"));
                po.setQuantity(rs.getInt("TotalQuantity"));
                po.setOrderTypeQuantity(rs.getInt("OrderTypeQuantity"));
                partnerOrderList.add(po);
            }
        }
    } catch (SQLException e) {
        throw e;
    }
    return partnerOrderList;
   }
       public Statistics getStatistics(int partnerID) throws SQLException {
        Statistics statistics = new Statistics();
        try ( Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {

            // Total Inventory Quantity
            String sql1 = "SELECT SUM(i.Quantity) AS TotalQuantity "
                    + "FROM Inventory i "
                    + "JOIN Products p ON i.ProductID = p.ProductID "
                    + "JOIN Orders o ON p.ProductID = o.ProductID "
                    + "WHERE o.PartnerID = ? AND o.OrderStatus = 'Hoàn thành';";
            try ( PreparedStatement ps1 = conn.prepareStatement(sql1)) {
                ps1.setInt(1, partnerID);
                try ( ResultSet rs1 = ps1.executeQuery()) {
                    if (rs1.next()) {
                        statistics.setTotalInventoryQuantity(rs1.getInt("TotalQuantity"));
                    }
                }
            }

            // Total Inventory Value
            String sql2 = "SELECT SUM(i.Quantity * p.UnitPrice) AS TotalValue "
                    + "FROM Inventory i "
                    + "JOIN Products p ON i.ProductID = p.ProductID "
                    + "JOIN Orders o ON p.ProductID = o.ProductID "
                    + "WHERE o.PartnerID = ? AND o.OrderStatus = 'Hoàn thành' AND o.OrderType = N'Nhập kho';";
            try ( PreparedStatement ps2 = conn.prepareStatement(sql2)) {
                ps2.setInt(1, partnerID);
                try ( ResultSet rs2 = ps2.executeQuery()) {
                    if (rs2.next()) {
                        statistics.setTotalInventoryValue(rs2.getDouble("TotalValue"));
                    }
                }
            }

            // Total Inbound Orders
            String sql3 = "SELECT COUNT(*) AS TotalInboundOrders "
                    + "FROM Orders "
                    + "WHERE PartnerID = ? AND OrderType = N'Nhập kho';";
            try ( PreparedStatement ps3 = conn.prepareStatement(sql3)) {
                ps3.setInt(1, partnerID);
                try ( ResultSet rs3 = ps3.executeQuery()) {
                    if (rs3.next()) {
                        statistics.setTotalInboundOrders(rs3.getInt("TotalInboundOrders"));
                    }
                }
            }

            // Total Outbound Orders
            String sql4 = "SELECT COUNT(*) AS TotalOutboundOrders "
                    + "FROM Orders "
                    + "WHERE PartnerID = ? AND OrderType = N'Xuất kho';";
            try ( PreparedStatement ps4 = conn.prepareStatement(sql4)) {
                ps4.setInt(1, partnerID);
                try ( ResultSet rs4 = ps4.executeQuery()) {
                    if (rs4.next()) {
                        statistics.setTotalOutboundOrders(rs4.getInt("TotalOutboundOrders"));
                    }
                }
            }
            String sql5 = "Select count(*) AS totalWaittingOrders "
                    + "FROM Orders "
                    + "WHERE PartnerID = ? AND OrderStatus = N'Chờ phê duyệt';";
            try ( PreparedStatement ps5 = conn.prepareStatement(sql5)) {
                ps5.setInt(1, partnerID);
                try ( ResultSet rs5 = ps5.executeQuery()) {
                    if (rs5.next()) {
                        statistics.setTotalWaittingOrders(rs5.getInt("totalWaittingOrders"));
                    }
                }
            }
            String sql6 = "Select count(*) AS totalSuccessfulOrders \n"
                    + "        FROM Orders \n"
                    + "        WHERE PartnerID = ? AND OrderStatus = N'Hoàn thành' ";
            try ( PreparedStatement ps6 = conn.prepareStatement(sql6)) {
                ps6.setInt(1, partnerID);
                try ( ResultSet rs6 = ps6.executeQuery()) {
                    if (rs6.next()) {
                        statistics.setTotalSuccessfulOrders(rs6.getInt("totalSuccessfulOrders"));
                    }
                }
            }
             String sql7 = "Select count(*) AS totalSuccessfulInboundOrders \n"
                    + "        FROM Orders \n"
                    + "        WHERE PartnerID = ? AND OrderStatus = N'Hoàn thành' AND OrderType = N'Nhập kho' ";
            try ( PreparedStatement ps7 = conn.prepareStatement(sql7)) {
                ps7.setInt(1, partnerID);
                try ( ResultSet rs7 = ps7.executeQuery()) {
                    if (rs7.next()) {
                        statistics.setTotalSuccessfulInboundOrders(rs7.getInt("totalSuccessfulInboundOrders"));
                    }
                }
            }
             String sql8 = "Select count(*) AS totalSuccessfulOutboundOrders \n"
                    + "        FROM Orders \n"
                    + "        WHERE PartnerID = ? AND OrderStatus = N'Hoàn thành' AND OrderType = N'Xuất kho' ";
            try ( PreparedStatement ps8 = conn.prepareStatement(sql8)) {
                ps8.setInt(1, partnerID);
                try ( ResultSet rs8 = ps8.executeQuery()) {
                    if (rs8.next()) {
                        statistics.setTotalSuccessfulOutboundOrders(rs8.getInt("totalSuccessfulOutboundOrders"));
                    }
                }
            }
        }

        return statistics;
    }
    public List<String> getAllZoneNames() throws SQLException {
        List<String> zoneNames = new ArrayList<>();
        String sql = "SELECT ZoneName FROM WarehouseZones";
        try ( PreparedStatement ps = connection.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String zoneName = rs.getString("zoneName");
                zoneNames.add(zoneName);
            }
        } catch (SQLException e) {
            throw e;
        }
        return zoneNames;
    }

    public List<String> getAllProduct(int partnerID) throws SQLException {
        List<String> productNames = new ArrayList<>();
        String sql = "SELECT p.ProductName "
                + "FROM Products p "
                + "JOIN Inventory i ON p.ProductID = i.ProductID "
                + "JOIN Orders o ON o.ProductID = p.ProductID "
                + "WHERE o.PartnerID = ? AND o.OrderStatus = 'Hoàn thành'";
        try ( PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, partnerID);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String productName = rs.getString("ProductName");
                    productNames.add(productName);
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return productNames;
    }

    public List<String> getAllPartner() throws SQLException {
        List<String> partnerNameS = new ArrayList<>();
        String sql = "Select PartnerName from Partners";
        try ( PreparedStatement ps = connection.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String partnerName = rs.getString("PartnerName");
                partnerNameS.add(partnerName);
            }
        } catch (SQLException e) {
            throw e;
        }
        return partnerNameS;
    }

    public Connection getConnection() {
        return connection;
    }

}
