/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ProOrCenter;
import model.Products;

/**
 *
 * @author thuct
 */
public class ProductDAO extends DBConnection {

    private static final Logger log = Logger.getLogger(ProductDAO.class.getName());
    //////////////////////////////////////////////////////Thuc//////////////////////////////////////////////////////

    // danh sách san phẩm
    public List<Products> getAllProduct() {
        List<Products> list = new ArrayList<>();
        String sql = "SELECT * FROM products";

        try (PreparedStatement st = connection.prepareStatement(sql); ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                list.add(new Products(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getDouble(5),
                        rs.getInt(6)));
            }

        } catch (SQLException e) {

            System.err.println("Er");
        }

        return list;
    }

    // xóa san phẩm
    public void deleteProduct(int productId) {
        try {
            String query = "DELETE FROM Products WHERE ProductID = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, productId);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // productCenter
    public List<ProOrCenter> getProductCenter() {
        List<ProOrCenter> proOrCenters = new ArrayList<>();
        String sql = "SELECT o.PartnerID, p.ProductID, p.ProductName, p.Description, "
                + "p.Quantity, p.UnitPrice, o.CreatedAt, o.OrderStatus, o.OrderType, "
                + "o.ZoneID FROM Products p LEFT JOIN Orders o ON p.ProductID = o.ProductID"
                + " WHERE o.OrderStatus = 'Hoàn thành'";

        try (PreparedStatement st = connection.prepareStatement(sql); ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                int partnerID = rs.getInt("PartnerID");
                int productId = rs.getInt("ProductID");
                ProOrCenter proOrCenter = new ProOrCenter(
                        partnerID,
                        productId,
                        rs.getString("ProductName"),
                        rs.getString("Description"),
                        rs.getInt("Quantity"),
                        rs.getDouble("UnitPrice"),
                        rs.getDate("CreatedAt"),
                        rs.getString("OrderType"),
                        rs.getString("OrderStatus"),
                        rs.getInt("ZoneID")
                );
                proOrCenters.add(proOrCenter);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return proOrCenters;
    }

    public List<ProOrCenter> searchProductCenter(String searchTerm) {
        List<ProOrCenter> proOrCenters = new ArrayList<>();
        String sql = "SELECT o.PartnerID, p.ProductID, p.ProductName, p.Description, "
                + "p.Quantity, p.UnitPrice, o.CreatedAt, o.OrderStatus, o.OrderType, "
                + "o.ZoneID FROM Products p LEFT JOIN Orders o ON p.ProductID = o.ProductID"
                + " WHERE o.OrderStatus = 'Hoàn thành' AND (p.ProductName LIKE ? OR p.Description LIKE ?)";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, "%" + searchTerm + "%");
            st.setString(2, "%" + searchTerm + "%");
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    int partnerID = rs.getInt("PartnerID");
                    int productId = rs.getInt("ProductID");
                    ProOrCenter proOrCenter = new ProOrCenter(
                            partnerID,
                            productId,
                            rs.getString("ProductName"),
                            rs.getString("Description"),
                            rs.getInt("Quantity"),
                            rs.getDouble("UnitPrice"),
                            rs.getDate("CreatedAt"),
                            rs.getString("OrderType"),
                            rs.getString("OrderStatus"),
                            rs.getInt("ZoneID")
                    );
                    proOrCenters.add(proOrCenter);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return proOrCenters;
    }

  public List<Products> getProductsByPartnerID(int partnerID) {
    List<Products> products = new ArrayList<>();
    String query = "SELECT p.* FROM Products p JOIN Orders o ON p.ProductID = o.ProductID WHERE o.PartnerID = ?";
    try (PreparedStatement pstmt = connection.prepareStatement(query)) {
        pstmt.setInt(1, partnerID);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Products product = new Products();
            product.setProductid(rs.getInt("ProductID"));
            product.setProductname(rs.getString("ProductName"));
            product.setDescription(rs.getString("Description"));
            product.setQuantity(rs.getInt("Quantity"));
            product.setUnitprice(rs.getDouble("UnitPrice"));
            product.setWarehouseZoneID(rs.getInt("WarehouseZoneID"));
            products.add(product);
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return products;
}

    public void updateProduct(int id, String name, String description) {
        try (PreparedStatement pstmt = connection.prepareStatement("UPDATE products SET productName =?, description =? WHERE productID =?")) {
            pstmt.setString(1, name);
            pstmt.setString(2, description);
            pstmt.setInt(3, id);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ProductDAO productDAO = new ProductDAO();
        List<Products> list = productDAO.getProductsByPartnerID(1);
        for (Products proOrCenter : list) {
            System.out.println(proOrCenter);
        }
    }
}
