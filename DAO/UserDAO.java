/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;

public class UserDAO {

    private Connection connection;

    public UserDAO() {
        DBConnection dbConnection = new DBConnection();
        this.connection = dbConnection.getConnection();
    }

    //login by username
    public Account loginByUserName(String user, String pass) {
        String sql = "SELECT * FROM Accounts WHERE Username = ? AND PasswordHash = HASHBYTES('SHA2_256', ?);";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user);
            statement.setString(2, pass);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getObject("IsCenter") != null ? rs.getInt("IsCenter") : null,
                        rs.getInt(8),
                        rs.getBoolean(9));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //check if username has already exsited when register
    public Account checkAccoutExist(String user) {
        String sql = "SELECT * FROM Accounts WHERE Username = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getBoolean(9));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //sign up  a new account
    public void SignUp(String partnerName, String contactName, String email, String phone, String address, String username, String password) throws SQLException {
        Account existingAccount = checkAccoutExist(username);
        if (existingAccount != null) {
            throw new SQLException("Username already exist");
        }

        connection.setAutoCommit(false);

        String insertPartnerSQL = "INSERT INTO Partners (PartnerName, ContactName, Email, PhoneNumber, Address) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement insertPartnerStmt = connection.prepareStatement(insertPartnerSQL, PreparedStatement.RETURN_GENERATED_KEYS)) {
            insertPartnerStmt.setString(1, partnerName);
            insertPartnerStmt.setString(2, contactName);
            insertPartnerStmt.setString(3, email);
            insertPartnerStmt.setString(4, phone);
            insertPartnerStmt.setString(5, address);
            insertPartnerStmt.executeUpdate();

            ResultSet rs = insertPartnerStmt.getGeneratedKeys();
            if (rs.next()) {
                int partnerID = rs.getInt(1);

                String insertAccountSQL = "INSERT INTO Accounts (Username, PasswordHash, Email, PhoneNumber, IsPartner, PartnerID) VALUES (?, HASHBYTES('SHA2_256', ?), ?, ?, 1, ?)";
                try (PreparedStatement insertAccountStmt = connection.prepareStatement(insertAccountSQL)) {
                    insertAccountStmt.setString(1, username);
                    insertAccountStmt.setString(2, password);
                    insertAccountStmt.setString(3, email);
                    insertAccountStmt.setString(4, phone);
                    insertAccountStmt.setInt(5, partnerID);
                    insertAccountStmt.executeUpdate();

                    connection.commit();
                }
            } else {
                connection.rollback();
                throw new SQLException("Creating partner failed, no ID obtained.");
            }
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.setAutoCommit(true);
        }
    }

    //login by gmail
    public Account LoginGmail(String email) throws SQLException {
        String sql = "SELECT * FROM Accounts WHERE email = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    System.out.println("User already exists: " + email);
                    return new Account(rs.getInt(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getInt(6),
                            rs.getInt(7),
                            rs.getInt(8),
                            rs.getBoolean(9));
                }
            }
        }
        return null;
    }

    //check if email has already exsited when register
    public boolean checkEmailExist(String email) throws SQLException {
        String sql = "SELECT * FROM Accounts WHERE email = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            try (ResultSet rs = statement.executeQuery()) {
                return rs.next();
            }
        }
    }

    //reset password by email
    public void updatePassword(String email, String newPassWord) throws SQLException {
        String sql = "UPDATE Accounts SET PasswordHash = HASHBYTES('SHA2_256', ?) WHERE Email = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, newPassWord);
            statement.setString(2, email);
            statement.executeUpdate();
        }
    }

    //list all accounts
    public List<Account> listAllAccounts() throws SQLException {
        List<Account> accounts = new ArrayList<>();
        String sql = "select UserID, Username, Email, PhoneNumber, IsPartner, IsCenter, isActive  from Accounts";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                accounts.add(new Account(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getBoolean(7)));
            }
        }
        return accounts;
    }

    // Enable and disable accounts
    public void AccountActivation(int userID, boolean isActive) throws SQLException {
        String sql = "UPDATE Accounts SET isActive = ? WHERE UserID = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, isActive);
            statement.setInt(2, userID);
            statement.executeUpdate();
        }
    }

    //add Account
    public void addAccount(String username, String password, String email, String phoneNumber, boolean isPartner, Integer partnerID) throws SQLException {
        String sql = "INSERT INTO Accounts (Username, PasswordHash, Email, PhoneNumber, IsPartner, IsCenter, PartnerID) VALUES (?, HASHBYTES('SHA2_256', ?), ?, ?, ?, 0, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, username);
            statement.setString(2, password);
            statement.setString(3, email);
            statement.setString(4, phoneNumber);
            statement.setBoolean(5, isPartner);
            if (partnerID != null) {
                statement.setInt(6, partnerID);
            } else {
                statement.setNull(6, java.sql.Types.INTEGER);
            }
            statement.executeUpdate();
        }
    }

    //Users' profile
    public Account getUserInfo(int userID) throws SQLException {
        String sql = "SELECT Username, Email, PhoneNumber FROM Accounts WHERE UserID = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, userID);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    return new Account(
                            userID,
                            rs.getString("Username"),
                            rs.getString("Email"),
                            rs.getString("PhoneNumber")
                    );
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //change password by UserID
    public void updatePasswordByUserID(int userID, String newPassword) throws SQLException {
        String sql = "UPDATE Accounts SET PasswordHash = HASHBYTES('SHA2_256', ?) WHERE UserID = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, newPassword);
            statement.setInt(2, userID);
            statement.executeUpdate();
        }
    }

    //change email by UserID
    public void updateEmailByUserID(int userID, String newEmail) throws SQLException {
        String sql = "UPDATE Accounts SET Email = ? WHERE UserID = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, newEmail);
            statement.setInt(2, userID);
            statement.executeUpdate();
        }
    }

    //change phone number by UserID
    public void updatePhoneByUserID(int userID, String newPhone) throws SQLException {
        String sql = "UPDATE Accounts SET PhoneNumber = ? WHERE UserID = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, newPhone);
            statement.setInt(2, userID);
            statement.executeUpdate();
        }
    }

    //lấy PartnerID theo username
    public Integer getPartnerIDByUsername(String username) throws SQLException {
        String sql = "SELECT PartnerID FROM Accounts WHERE Username = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, username);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt("PartnerID");
                } else {
                    return null; // Không tìm thấy tài khoản với tên đăng nhập này
                }
            }
        }
    }
}
