/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import dal.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Inventory;
import model.Products;
import model.WarehouseZone;

/**
 *
 * @author thuct
 */
public class WarehouseDAO extends DBConnection{
    
       //list Zone
public List<WarehouseZone> getAllWarehouseZones() {
    List<WarehouseZone> zones = new ArrayList<>();
    String sql = "SELECT wz.ZoneID, wz.WarehouseID, wz.ZoneName, wz.KeeperID, a.Username " +
                 "FROM WarehouseZones wz " +
                 "LEFT JOIN Accounts a ON wz.KeeperID = a.UserID";
    try (PreparedStatement statement = connection.prepareStatement(sql);
         ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
            WarehouseZone zone = new WarehouseZone();
            zone.setZoneID(rs.getInt("ZoneID"));
            zone.setWarehouseID(rs.getInt("WarehouseID"));
            zone.setZoneName(rs.getString("ZoneName"));
            zone.setKeeperID(rs.getInt("KeeperID"));
            zone.setKeeperUsername(rs.getString("Username")); // Add this line
            zone.setProducts(getProductsForZone(zone.getZoneID()));
            zone.setInventories(getInventoriesForZone(zone.getZoneID()));
            zones.add(zone);
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return zones;
}

private List<Products> getProductsForZone(int zoneID) {
    List<Products> products = new ArrayList<>();
    String sql = "SELECT * FROM Products WHERE WarehouseZoneID = ?";
    try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
        pstmt.setInt(1, zoneID);
        try (ResultSet rs = pstmt.executeQuery()) {
            while (rs.next()) {
                products.add(new Products(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                rs.getInt(4),
                rs.getDouble(5),
                rs.getInt(6)));
            }
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return products;
}
    public List<Inventory> getInventoriesForZone(int zoneID) {
        List<Inventory> inventories = new ArrayList<>();
        String sql = "SELECT * FROM Inventory WHERE WarehouseZoneID =?";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setInt(1, zoneID);
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    inventories.add(new Inventory(rs.getInt(1),
                            rs.getInt(2), 
                            rs.getInt(3), 
                            rs.getInt(4))); 
                            
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return inventories;
    }
    
public void addZone(String zoneName, int keeperID) {
    int defaultWarehouseID = 1; // Assuming this is the default value for WarehouseID
    String query = "INSERT INTO WarehouseZones (WarehouseID, ZoneName, KeeperID) VALUES (?,?,?)";
    try (PreparedStatement stmt = connection.prepareStatement(query)) {
        stmt.setInt(1, defaultWarehouseID);
        stmt.setString(2, zoneName);
        stmt.setInt(3, keeperID);
        stmt.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}

public void editZone(int zoneId, String zoneName, int keeperId) {
    String sql = "UPDATE WarehouseZones SET ZoneName = ?, KeeperID = ? WHERE ZoneID = ?";
    try (PreparedStatement statement = connection.prepareStatement(sql)) {
        statement.setString(1, zoneName);
        statement.setInt(2, keeperId);
        statement.setInt(3, zoneId);
        statement.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}

    public void deleteZone(int zoneId) {
        String query = "DELETE FROM WarehouseZones WHERE zoneID = ?";
        try (PreparedStatement pstmt = connection.prepareStatement(query)) {
            pstmt.setInt(1, zoneId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<WarehouseZone> searchZones(String zoneName, String keeperID) {
        List<WarehouseZone> zones = new ArrayList<>();
        String sql = "SELECT * FROM WarehouseZones WHERE ZoneName LIKE ?  OR keeperID LIKE ?";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(1, "%" + zoneName  + "%");
            pstmt.setString(2, "%" + keeperID  + "%");
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    WarehouseZone zone = new WarehouseZone();
                    zone.setZoneID(rs.getInt("zoneID"));
                    zone.setZoneName(rs.getString("zoneName"));
                    zone.setKeeperID(rs.getInt("keeperID"));
                    zones.add(zone);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return zones;
    }
    
public List<Account> getKeeperID() {
    List<Account> keeper = new ArrayList<>();
        String sql = "Select * from Accounts where IsCenter = 0 and IsPartner = 0";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                keeper.add( new Account(rs.getInt(1),
                                   rs.getString(2),
                                   rs.getString(3),
                                   rs.getString(4),
                                   rs.getString(5),
                                   rs.getInt(6),
                                   rs.getInt(7),
                                   rs.getInt(8),
                                   rs.getBoolean(9)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return keeper;
    }

   public WarehouseZone getZoneById(int zoneID) {
    WarehouseZone zone = null;
    String sql = "SELECT * FROM WarehouseZones WHERE ZoneID = ?";
    try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
        pstmt.setInt(1, zoneID);
        try (ResultSet rs = pstmt.executeQuery()) {
            if (rs.next()) {
                zone = new WarehouseZone();
                zone.setZoneID(rs.getInt("ZoneID"));
                zone.setWarehouseID(rs.getInt("WarehouseID"));
                zone.setZoneName(rs.getString("ZoneName"));
                zone.setKeeperID(rs.getInt("KeeperID"));
                zone.setProducts(getProductsForZone(zone.getZoneID()));
                zone.setInventories(getInventoriesForZone(zone.getZoneID()));
            }
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return zone;
}

}
