package dal;

import dal.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PartnerDAO extends DBConnection {

    public PartnerDAO() {
        super();
    }

    public int getCompletedProductsCount(int partnerID) {
        int count = 0;
        String query = "SELECT COUNT(*) FROM Products p LEFT JOIN Orders o ON p.ProductID = o.ProductID WHERE o.OrderStatus = 'Hoàn thành' AND o.PartnerID = ?";

        try (
            PreparedStatement st = connection.prepareStatement(query);
        ) {
            st.setInt(1, partnerID);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count;
    }
    public double getTotalProductCompleted(int partnerID) {
        double totalUnitPrice = 0.0;
        String query = "SELECT SUM(p.UnitPrice) AS TotalUnitPrice\n" +
"FROM Products p\n" +
"LEFT JOIN Orders o ON p.ProductID = o.ProductID\n" +
"WHERE o.OrderStatus = 'Hoàn thành' AND o.PartnerID = 1;";

        try (
            PreparedStatement st = connection.prepareStatement(query);
        ) {
            st.setInt(1, partnerID);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    totalUnitPrice = rs.getDouble("TotalUnitPrice");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return totalUnitPrice;
    }

    public static void main(String[] args) {
        PartnerDAO dao = new PartnerDAO();
        int completedCount = dao.getCompletedProductsCount(1);
        System.out.println("Completed Products Count: " + completedCount);
    }
}
