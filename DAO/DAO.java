/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import dal.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Partner;
import model.ProOrCenter;
import model.WarehouseZone;

/**
 *
 * @author X P S
 */
public class DAO extends DBConnection {

///////////////////////////////////////////// TRUNG - HE173480 ( ORDERS) ////////////////////////////////////////////////////////    
    // thêm đơn
    public void insertOrders(int partnerID, String orderType, String orderStatus) {
        String sql = "INSERT INTO Orders (PartnerID, OrderType, OrderStatus, CreatedAt, UpdatedAt) "
                + "VALUES (?, ?, ?, GETDATE(), NULL)";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, partnerID);
            st.setString(2, orderType);
            st.setString(3, orderStatus);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    /// thêm PRODUCT
    public void insertProduct(String productName, String description, int quantity, double unitPrice) {
        String sql = "INSERT INTO Products (ProductName, Description, Quantity, UnitPrice) VALUES (?, ?, ?, ?)";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, productName);
            st.setString(2, description);
            st.setInt(3, quantity);
            st.setDouble(4, unitPrice);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // lấy danh sách hàng và thông tin partner
    public List<ProOrCenter> getProductOrders2() {
        List<ProOrCenter> proOrCenters = new ArrayList<>();
        String sql = "SELECT o.PartnerID, p.ProductID, p.ProductName, p.Description, p.Quantity, p.UnitPrice, "
                + "o.CreatedAt, o.OrderType, o.OrderStatus, o.ZoneID "
                + "FROM Products p LEFT JOIN Orders o ON p.ProductID = o.ProductID";

        try (PreparedStatement st = connection.prepareStatement(sql); ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                int partnerID = rs.getInt("PartnerID");
                int productId = rs.getInt("ProductID");
                ProOrCenter proOrCenter = new ProOrCenter(
                        partnerID,
                        productId,
                        rs.getString("ProductName"),
                        rs.getString("Description"),
                        rs.getInt("Quantity"),
                        rs.getDouble("UnitPrice"),
                        rs.getDate("CreatedAt"),
                        rs.getString("OrderType"),
                        rs.getString("OrderStatus"),
                        rs.getInt("ZoneID")
                );
                proOrCenters.add(proOrCenter);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return proOrCenters;
    }

    // lấy danh danh sách partner
    public List<Partner> getAllPartners() {
        List<Partner> partners = new ArrayList<>();
        String sql = "SELECT * FROM Partners";

        try (PreparedStatement st = connection.prepareStatement(sql); ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                partners.add(new Partner(
                        rs.getInt("partnerID"),
                        rs.getString("partnerName"),
                        rs.getString("contactName"),
                        rs.getString("email"),
                        rs.getString("phoneNumber"),
                        rs.getString("address")
                ));
            }

        } catch (SQLException e) {

            System.err.println("Er");
        }

        return partners;
    }

    // láy tên và ID partner để phân loại
    public List<String> getPartnerNames() {
        List<String> partnerNames = new ArrayList<>();
        String sql = "SELECT PartnerID, PartnerName FROM Partners";

        try (PreparedStatement st = connection.prepareStatement(sql); ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                int partnerID = rs.getInt("PartnerID");
                String partnerName = rs.getString("PartnerName");
                partnerNames.add(partnerID + " - " + partnerName);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return partnerNames;
    }

    // update kho cho partner của center
    public void updatezoneOrder(int zoneID, int productId, String orderStatus) {
        String sql = "UPDATE Orders SET ZoneID = ?, OrderStatus = ? WHERE ProductID = ?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, zoneID);
            st.setString(2, orderStatus);
            st.setInt(3, productId);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // trường hợp nếu hủy đơn thì không cập nhập zone
    public void updateOrderStatus(int productId, String orderStatus) {
        String sql = "UPDATE Orders SET OrderStatus = ? WHERE ProductID = ?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, orderStatus);
            st.setInt(2, productId);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Update OUTBOUND cho partner
    public void updaOutboundPa(int productId, String orderType, String orderStatus) {
        String sql = "UPDATE Orders SET OrderType = ?, OrderStatus = ? WHERE ProductID = ?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, orderType);
            st.setString(2, orderStatus);
            st.setInt(3, productId);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // lấy quantity để so sánh
    public int getQuantityByProID(int productId) {
        int quantity = 0;
        String sql = "SELECT p.Quantity, o.OrderType, o.OrderStatus "
                + "FROM Orders o "
                + "JOIN Products p ON o.ProductID = p.ProductID "
                + "WHERE o.ProductID = ?;";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, productId);

            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    quantity = rs.getInt("Quantity");
                    String orderType = rs.getString("OrderType");
                    String orderStatus = rs.getString("OrderStatus");

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return quantity;
    }

    // lấy giá cũ để tính toán giá mới cho sản phẩm
    public double getUnitPriceByID(int productId) {
        double unitPrice = 0.0;
        String sql = "SELECT p.UnitPrice "
                + "FROM Products p "
                + "WHERE p.ProductID = ?;";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, productId);

            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    unitPrice = rs.getDouble("UnitPrice");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return unitPrice;
    }

    // update cho hàng còn lại 
    public void updateOrders(String orderType, String orderStatus, int productID) {
        String sql = "UPDATE Orders SET OrderType = ?, OrderStatus = ? WHERE ProductID = ?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setString(1, "Nhập kho");
            st.setString(2, "Hoàn thành");
            st.setInt(3, productID);

            int rowsUpdated = st.executeUpdate();
            System.out.println("Rows updated in Orders: " + rowsUpdated);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Phương thức để update Products
    public void updateProducts(int quantity, double unitPrice, int productID) {
        String sql = "UPDATE Products SET Quantity = ?, UnitPrice = ? WHERE ProductID = ?";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, quantity);
            st.setDouble(2, unitPrice);
            st.setInt(3, productID);

            int rowsUpdated = st.executeUpdate();
            System.out.println("Rows updated in Products: " + rowsUpdated);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // insert đơn xuất
    public void insertOrders2(int partnerID, String orderType, String orderStatus, int zoneID) {
        String sql = "INSERT INTO Orders (PartnerID, OrderType, OrderStatus, ZoneID, CreatedAt, UpdatedAt) "
                + "VALUES (?, ?, ?, ?, GETDATE(), NULL)";

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            st.setInt(1, partnerID);
            st.setString(2, orderType);
            st.setString(3, orderStatus);
            st.setInt(4, zoneID);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

public List<WarehouseZone> getZones() {
    List<WarehouseZone> zones = new ArrayList<>();
    String sql = "SELECT ZoneID, ZoneName FROM WarehouseZones";

    try (PreparedStatement st = connection.prepareStatement(sql); ResultSet rs = st.executeQuery()) {

        while (rs.next()) {
            WarehouseZone zone = new WarehouseZone();
            zone.setZoneID(rs.getInt("ZoneID"));
            zone.setZoneName(rs.getString("ZoneName"));
            zones.add(zone);
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return zones;
}

    // Test main method
    public static void main(String[] args) {
        DAO dao = new DAO();
 dao.updatezoneOrder(2, 1, "Đã phê duyệt");
    }


    public List<ProOrCenter> searchProducts(String keyword) {
        List<ProOrCenter> productList = new ArrayList<>();
        String sql = "SELECT o.PartnerID, p.ProductID, p.ProductName, p.Description, p.Quantity, p.UnitPrice, "
                + "o.CreatedAt, o.OrderType, o.OrderStatus, o.ZoneID "
                + "FROM Products p "
                + "LEFT JOIN Orders o ON p.ProductID = o.ProductID "
                + "WHERE p.ProductName LIKE N'%' + ? + '%' OR p.Description LIKE N'%' + ? + '%'";

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, keyword);
            ps.setString(2, keyword);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    ProOrCenter product = new ProOrCenter(
                            rs.getInt("PartnerID"),
                            rs.getInt("ProductID"),
                            rs.getString("ProductName"),
                            rs.getString("Description"),
                            rs.getInt("Quantity"),
                            rs.getDouble("UnitPrice"),
                            rs.getDate("CreatedAt"),
                            rs.getString("OrderType"),
                            rs.getString("OrderStatus"),
                            rs.getInt("ZoneID")
                    );
                    productList.add(product);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return productList;
    }
}
