package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    protected Connection connection;

    public DBConnection() {
        try {
            // Edit URL, username, password to authenticate with your MS SQL Server
            String url = "jdbc:sqlserver://localhost:1433;databaseName=Notifications";
            String username = "sa";
            String password = "123";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }
    
    public void testConnection() {
        try {
            if (connection != null && !connection.isClosed()) {
                System.out.println("Connection successful!");
            } else {
                System.out.println("Failed to establish connection!");
            }
        } catch (SQLException ex) {
            System.out.println("An error occurred while testing the connection: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        DBConnection dbContext = new DBConnection();
        dbContext.testConnection();
    }

    public Connection getConnection() {
        if (connection == null) {
            try {
                String url = "jdbc:sqlserver://localhost:1433;databaseName=Notifications"; // Thay thế bằng thông tin của bạn
                String user = "sa"; // Thay thế bằng thông tin của bạn
                String pass = "123"; // Thay thế bằng thông tin của bạn
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                connection = DriverManager.getConnection(url, user, pass);
            } catch (ClassNotFoundException | SQLException e) {
            }
        }
        return connection;
    }
}
