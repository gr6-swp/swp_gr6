package model;

import java.util.List;

public class WarehouseZone {
    private int zoneID;
    private int warehouseID;
    private String zoneName;
    private int keeperID;
    private String keeperUsername;
    private List<Products> products;
    private List<Inventory> inventories;

    public WarehouseZone(int zoneID, int warehouseID, String zoneName, int keeperID, List<Products> products, List<Inventory> inventories) {
        this.zoneID = zoneID;
        this.warehouseID = warehouseID;
        this.zoneName = zoneName;
        this.keeperID = keeperID;
        this.products = products;
        this.inventories = inventories;
    }

    public WarehouseZone(int zoneID, int warehouseID, String zoneName, int keeperID, String keeperUsername, List<Products> products, List<Inventory> inventories) {
        this.zoneID = zoneID;
        this.warehouseID = warehouseID;
        this.zoneName = zoneName;
        this.keeperID = keeperID;
        this.keeperUsername = keeperUsername;
        this.products = products;
        this.inventories = inventories;
    }
    
    
     public String getKeeperUsername() {
        return keeperUsername;
    }

    public void setKeeperUsername(String keeperUsername) {
        this.keeperUsername = keeperUsername;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }

    public int getZoneID() {
        return zoneID;
    }

    public void setZoneID(int zoneID) {
        this.zoneID = zoneID;
    }

    public int getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(int warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public int getKeeperID() {
        return keeperID;
    }

    public void setKeeperID(int keeperID) {
        this.keeperID = keeperID;
    }

    public List<Inventory> getInventories() {
        return inventories;
    }

    public void setInventories(List<Inventory> inventories) {
        this.inventories = inventories;
    }

    
    // getters and setters

    public WarehouseZone() {
    }
}
  

