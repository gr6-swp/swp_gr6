/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author thuct
 */
public class Accounts {
    private int UserID;
     private String   Username;
      private String  Password;
     private String  Email;
  private String     PhoneNumber;
    private boolean   isActive;
    private boolean   IsPartner;
    private boolean   IsCenter;
    private int   PartnerID;

    public Accounts() {
    }

    public Accounts(int UserID, String Username, String Password, String Email, String PhoneNumber, boolean isActive, boolean IsPartner, boolean IsCenter, int PartnerID) {
        this.UserID = UserID;
        this.Username = Username;
        this.Password = Password;
        this.Email = Email;
        this.PhoneNumber = PhoneNumber;
        this.isActive = isActive;
        this.IsPartner = IsPartner;
        this.IsCenter = IsCenter;
        this.PartnerID = PartnerID;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isIsPartner() {
        return IsPartner;
    }

    public void setIsPartner(boolean IsPartner) {
        this.IsPartner = IsPartner;
    }

    public boolean isIsCenter() {
        return IsCenter;
    }

    public void setIsCenter(boolean IsCenter) {
        this.IsCenter = IsCenter;
    }

    public int getPartnerID() {
        return PartnerID;
    }

    public void setPartnerID(int PartnerID) {
        this.PartnerID = PartnerID;
    }


}
