/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ht
 */
public class Partner {
    private int PartnerID;
    private String PartnerName;
    private String ContactName;
    private String Email;
    private String PhoneNumber;
    private String Address;

    public Partner() {
    }

    public Partner(int PartnerID, String PartnerName, String ContactName, String Email, String PhoneNumber, String Address) {
        this.PartnerID = PartnerID;
        this.PartnerName = PartnerName;
        this.ContactName = ContactName;
        this.Email = Email;
        this.PhoneNumber = PhoneNumber;
        this.Address = Address;
    }

    public int getPartnerID() {
        return PartnerID;
    }

    public void setPartnerID(int PartnerID) {
        this.PartnerID = PartnerID;
    }

    public String getPartnerName() {
        return PartnerName;
    }

    public void setPartnerName(String PartnerName) {
        this.PartnerName = PartnerName;
    }

    public String getContactName() {
        return ContactName;
    }

    public void setContactName(String ContactName) {
        this.ContactName = ContactName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    @Override
    public String toString() {
        return "Partner{" + "PartnerID=" + PartnerID + ", PartnerName=" + PartnerName + ", ContactName=" + ContactName + ", Email=" + Email + ", PhoneNumber=" + PhoneNumber + ", Address=" + Address + '}';
    }
}
