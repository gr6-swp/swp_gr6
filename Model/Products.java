/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author X P S
 */
public class Products {

    private int productid;
    private String productname;
    private String description;
    private int quantity;
    private double unitprice;
    private int warehouseZoneID;

    public Products() {
    }

    public Products(int productid, String productname, String description, int quantity, double unitprice, int warehouseZoneID) {
        this.productid = productid;
        this.productname = productname;
        this.description = description;
        this.quantity = quantity;
        this.unitprice = unitprice;
        this.warehouseZoneID = warehouseZoneID;
    }

    public int getWarehouseZoneID() {
        return warehouseZoneID;
    }

    public void setWarehouseZoneID(int warehouseZoneID) {
        this.warehouseZoneID = warehouseZoneID;
    }

    
    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    @Override
    public String toString() {
        return "Products{" + "productid=" + productid + ", productname=" + productname + ", description=" + description + ", quantity=" + quantity + ", unitprice=" + unitprice + '}';
    }

    
    
    
    
}
