/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author X P S
 */
public class Orders {

    private int orderid;
    private int partnerid;
    private String ordertype;
    private String orderstatus;
    private Date createdat;
    private Date updatedat;
    private int zoneID;

    public Orders() {
    }

    public Orders(int orderid, int partnerid, String ordertype, String orderstatus, Date createdat, Date updatedat, int zoneID) {
        this.orderid = orderid;
        this.partnerid = partnerid;
        this.ordertype = ordertype;
        this.orderstatus = orderstatus;
        this.createdat = createdat;
        this.updatedat = updatedat;
        this.zoneID = zoneID;
    }

    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public int getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(int partnerid) {
        this.partnerid = partnerid;
    }

    public String getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(String ordertype) {
        this.ordertype = ordertype;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    public Date getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Date createdat) {
        this.createdat = createdat;
    }

    public Date getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(Date updatedat) {
        this.updatedat = updatedat;
    }

    public int getZoneID() {
        return zoneID;
    }

    public void setZoneID(int zoneID) {
        this.zoneID = zoneID;
    }

      
    
}