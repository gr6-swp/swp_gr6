/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author X P S
 */
public class ProOrCenter {

    private int partnerID;
    private int productID;
    private String productName;
    private String description;
    private int quantity;
    private double unitPrice;
    private Date createdAt;
    private String orderType;
    private String orderStatus;
    private int zoneID;
    

    public ProOrCenter() {
    }

    public ProOrCenter(int partnerID, int productID, String productName, String description, int quantity, double unitPrice, Date createdAt, String orderType, String orderStatus, int zoneID) {
        this.partnerID = partnerID;
        this.productID = productID;
        this.productName = productName;
        this.description = description;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.createdAt = createdAt;
        this.orderType = orderType;
        this.orderStatus = orderStatus;
        this.zoneID = zoneID;
    }

    public int getPartnerID() {
        return partnerID;
    }

    public void setPartnerID(int partnerID) {
        this.partnerID = partnerID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getZoneID() {
        return zoneID;
    }

    public void setZoneID(int zoneID) {
        this.zoneID = zoneID;
    }

    @Override
    public String toString() {
        return "ProOrCenter{" + "partnerID=" + partnerID + ", productID=" + productID + ", productName=" + productName + ", description=" + description + ", quantity=" + quantity + ", unitPrice=" + unitPrice + ", createdAt=" + createdAt + ", orderType=" + orderType + ", orderStatus=" + orderStatus + ", zoneID=" + zoneID + '}';
    }

    

}
