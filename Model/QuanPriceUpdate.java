/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author X P S
 */
public class QuanPriceUpdate {

    private int productID;
    private int quantity;
    private double unitPrice;
    private String orderType;
    private String orderStatus;

    public QuanPriceUpdate() {
    }

    public QuanPriceUpdate(int productID, int quantity, double unitPrice, String orderType, String orderStatus) {
        this.productID = productID;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.orderType = orderType;
        this.orderStatus = orderStatus;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return "QuanPriceUpdate{" + "productID=" + productID + ", quantity=" + quantity + ", unitPrice=" + unitPrice + ", orderType=" + orderType + ", orderStatus=" + orderStatus + '}';
    }

}
