/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author thuct
 */
public class Inventory {
    private int id;
    private int productId;
    private int warehouseZoneId;
    private int quantity;

    public Inventory() {
    }

    public Inventory(int id, int productId, int warehouseZoneId, int quantity) {
        this.id = id;
        this.productId = productId;
        this.warehouseZoneId = warehouseZoneId;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getWarehouseZoneId() {
        return warehouseZoneId;
    }

    public void setWarehouseZoneId(int warehouseZoneId) {
        this.warehouseZoneId = warehouseZoneId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    
}
