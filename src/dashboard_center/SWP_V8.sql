﻿-- Bảng Kho: Lưu trữ thông tin về các kho hàng.
CREATE TABLE Warehouses (
    WarehouseID INT PRIMARY KEY IDENTITY(1,1),   -- Mã định danh duy nhất của kho
    WarehouseName NVARCHAR(255) NOT NULL,        -- Tên kho
    Address NVARCHAR(255)                         -- Địa chỉ kho
);
-- Bảng Đối tác: Lưu trữ thông tin về các đối tác.
CREATE TABLE Partners (
    PartnerID INT PRIMARY KEY IDENTITY(1,1),       -- Mã định danh duy nhất của đối tác
    PartnerName NVARCHAR(255) NOT NULL,           -- Tên đối tác
    ContactName NVARCHAR(255),                     -- Tên người liên hệ
    Email NVARCHAR(255) UNIQUE NOT NULL,           -- Email (duy nhất)
    PhoneNumber NVARCHAR(20),                     -- Số điện thoại
    Address NVARCHAR(255)                          -- Địa chỉ
);
-- Bảng Tài khoản: Lưu trữ thông tin tài khoản người dùng.
CREATE TABLE Accounts (
    UserID INT PRIMARY KEY IDENTITY(1,1),          -- Mã định danh duy nhất của tài khoản
    Username NVARCHAR(255) UNIQUE NOT NULL,       -- Tên đăng nhập (duy nhất)
    PasswordHash NVARCHAR(255) NOT NULL,          -- Mật khẩu đã băm (hash)
    Email NVARCHAR(255) UNIQUE NOT NULL,          -- Email (duy nhất)
    PhoneNumber NVARCHAR(20),                     -- Số điện thoại
    IsCenter BIT NOT NULL DEFAULT 0,              -- 1: Center, 0: Keeper
    IsPartner BIT NOT NULL DEFAULT 0,             -- 1: Partner, 0: Không phải Partner
    PartnerID INT,                                -- ID đối tác (nếu là Partner)
    WarehouseID INT,                              -- ID kho (nếu là Keeper)
    FOREIGN KEY (PartnerID) REFERENCES Partners(PartnerID),
    FOREIGN KEY (WarehouseID) REFERENCES Warehouses(WarehouseID)
);



-- Bảng Khu vực kho: Lưu trữ thông tin về các khu vực trong kho.
CREATE TABLE WarehouseZones (
    ZoneID INT PRIMARY KEY IDENTITY(1,1),        -- Mã định danh duy nhất của khu vực kho
    WarehouseID INT NOT NULL,                    -- ID kho chứa khu vực này
    ZoneName NVARCHAR(255) NOT NULL,             -- Tên khu vực kho
    KeeperID INT,                                -- ID thủ kho phụ trách khu vực này
    FOREIGN KEY (WarehouseID) REFERENCES Warehouses(WarehouseID),
    FOREIGN KEY (KeeperID) REFERENCES Accounts(UserID)
);

-- Bảng Sản phẩm: Lưu trữ thông tin về các sản phẩm.
CREATE TABLE Products (
    ProductID INT PRIMARY KEY IDENTITY(1,1),     -- Mã định danh duy nhất của sản phẩm
    ProductName NVARCHAR(255) NOT NULL,          -- Tên sản phẩm
    Description NVARCHAR(MAX),                   -- Mô tả sản phẩm
	Quantity INT NOT NULL,                       -- Số lượng sản phẩm
    UnitPrice DECIMAL(18,2) NOT NULL             -- Đơn giá sản phẩm
);

-- Bảng Đơn hàng: Lưu trữ thông tin về các đơn hàng nhập/xuất kho.
CREATE TABLE Orders (
    OrderID INT PRIMARY KEY IDENTITY(1,1),       -- Mã định danh duy nhất của đơn hàng
    PartnerID INT NOT NULL,                      -- ID đối tác tạo đơn hàng
    WarehouseID INT,                    -- ID kho liên quan đến đơn hàng
    ProductID INT,                      -- ID sản phẩm
    OrderType NVARCHAR(50) NOT NULL CHECK (OrderType IN (N'Nhập kho',N'Xuất kho')), -- Loại đơn hàng (nhập hoặc xuất)
    OrderStatus NVARCHAR(50) NOT NULL CHECK (OrderStatus IN (N'Chờ phê duyệt', N'Đã phê duyệt', N'Đang xử lý', N'Hoàn thành', N'Đã hủy')), -- Trạng thái đơn hàng
    CreatedAt DATETIME NOT NULL DEFAULT GETDATE(), -- Thời gian tạo đơn hàng
    UpdatedAt DATETIME,                          -- Thời gian cập nhật đơn hàng (nếu có)
    FOREIGN KEY (PartnerID) REFERENCES Partners(PartnerID),
    FOREIGN KEY (WarehouseID) REFERENCES Warehouses(WarehouseID),
    FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
);

-- Bảng Tồn kho: Lưu trữ thông tin về số lượng sản phẩm tồn kho tại từng khu vực.
CREATE TABLE Inventory (
    InventoryID INT PRIMARY KEY IDENTITY(1,1),    -- Mã định danh duy nhất của bản ghi tồn kho
    ProductID INT NOT NULL,                      -- ID sản phẩm
    WarehouseZoneID INT NOT NULL,                -- ID khu vực kho
    Quantity INT NOT NULL,                       -- Số lượng tồn kho
    FOREIGN KEY (ProductID) REFERENCES Products(ProductID),
    FOREIGN KEY (WarehouseZoneID) REFERENCES WarehouseZones(ZoneID)
);

-- Bảng Thông báo: Lưu trữ thông báo cho người dùng.
CREATE TABLE Notifications (
    NotificationID INT PRIMARY KEY IDENTITY(1,1), -- Mã định danh duy nhất của thông báo
    UserID INT NOT NULL,                         -- ID người dùng nhận thông báo
    ActorID INT NOT NULL,                        -- ID người dùng tạo thông báo
    Type NVARCHAR(50) NOT NULL,                  -- Loại thông báo
    Message NVARCHAR(MAX) NOT NULL,             -- Nội dung thông báo
    CreatedAt DATETIME NOT NULL DEFAULT GETDATE(), -- Thời gian tạo thông báo
    IsRead BIT NOT NULL DEFAULT 0,               -- Trạng thái đã đọc (0: chưa đọc, 1: đã đọc)
    RelatedEntityID INT,                         -- ID thực thể liên quan (đơn hàng, sản phẩm,...)
    RelatedEntityType NVARCHAR(50)               -- Loại thực thể liên quan (Order, Product,...)
    FOREIGN KEY (UserID) REFERENCES Accounts(UserID),
    FOREIGN KEY (ActorID) REFERENCES Accounts(UserID)
);