/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;
import java.sql.*;
import dal.DBContext;
import entity.PartnerItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author songl
 */
public class PartnerService {

    private static final Logger LOGGER = Logger.getLogger(OrderService.class.getName());
    private DBContext dbContext;

    public PartnerService(DBContext dbContext) {
        this.dbContext = dbContext;
    }

    public List<PartnerItem> getPartnerStatistics(String partner) throws SQLException {
        return dbContext.fetchPartnerStatistics(partner);
    }

    public PartnerService() throws ClassNotFoundException, SQLException {
        try {
            dbContext = new DBContext();
            LOGGER.log(Level.INFO, "DBContext initialized successfully.");
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, "Error initializing DBContext", e);
            throw e;
        }
    }

    public List<PartnerItem> fetchPartnerStatistics(String partner) throws SQLException {
        List<PartnerItem> partnerStatisticsList = new ArrayList<>();
        String sql = "SELECT \n"
                + "    p.PartnerName,\n"
                + "    o.OrderType,\n"
                + "    o.OrderStatus,\n"
                + "    COUNT(o.OrderID) AS [Total Quantity]\n"
                + "FROM \n"
                + "    Partners p\n"
                + "JOIN \n"
                + "    Orders o ON p.PartnerID = o.PartnerID\n"
                + "GROUP BY \n"
                + "    p.PartnerName,\n"
                + "    o.OrderType,\n"
                + "    o.OrderStatus\n"
                + "ORDER BY \n"
                + "    p.PartnerName, \n"
                + "    o.OrderType, \n"
                + "    o.OrderStatus;";
        if (partner != null && !partner.isEmpty()) {
            sql += "and p.PartnerName = ?";
        }
        try ( Connection conn = dbContext.getConnection();  PreparedStatement ps = conn.prepareStatement(sql)) {
            int paramIndex = 1;
            if (partner != null && !partner.isEmpty()) {
                ps.setString(paramIndex++, partner);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PartnerItem pi = new PartnerItem();
                pi.setPartner(rs.getString("PartnerName"));
                pi.setOder(rs.getString("OrderType"));
                pi.setStatus(rs.getString("OrderStatus"));
                pi.setTotalquantity(rs.getInt("Total Quantity"));
                partnerStatisticsList.add(pi);
            }
        }
        return partnerStatisticsList;
    }

    public List<String> getAllPartner() throws SQLException {
        List<String> partnerNameS = new ArrayList<>();
        String sql = "Select PartnerName from Partners";
        try ( PreparedStatement ps = dbContext.getConnection().prepareStatement(sql);  
              ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String partnerName = rs.getString("PartnerName");
                partnerNameS.add(partnerName);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Error fetching partner names.", e);
            throw e;
        }
        return partnerNameS;
    }
}
