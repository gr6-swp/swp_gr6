/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dal.DBContext;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import entity.OderIteam;

public class OrderService {

    private static final Logger LOGGER = Logger.getLogger(OrderService.class.getName());
    private DBContext dbContext;

    public OrderService(DBContext dbContext) {
        this.dbContext = dbContext;
    }
    public List<OderIteam> getOrderStatistics(String warehouse) throws SQLException {
        return dbContext.fetchOrderStatistics(warehouse);
    }

    public OrderService() throws ClassNotFoundException, SQLException {
        try {
            dbContext = new DBContext();
            LOGGER.log(Level.INFO, "DBContext initialized successfully.");
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, "Error initializing DBContext", e);
            throw e;
        }
    }
    public List<OderIteam> fetchOrderStatistics(String warehouse) throws SQLException {
        List<OderIteam> orderStatisticsList = new ArrayList<>();
        String sql = "SELECT\n"
                + "    w.WarehouseName,\n"
                + "    o.OrderType,\n"
                + "    o.OrderStatus,\n"
                + "    COUNT(o.OrderID) as [Total Quantity]\n"
                + "FROM\n"
                + "    Orders o\n"
                + "JOIN\n"
                + "    Warehouses w ON o.WarehouseID = w.WarehouseID\n"
                + "GROUP BY\n"
                + "    w.WarehouseName, o.OrderType, o.OrderStatus\n"
                + "ORDER BY\n"
                + "    w.WarehouseName, o.OrderType, o.OrderStatus;";
        if (warehouse != null && !warehouse.isEmpty()) {
            sql += " AND w.WarehouseName = ?";
        }
        try ( Connection conn = dbContext.getConnection();  PreparedStatement ps = conn.prepareStatement(sql)) {
            int paramIndex = 1;
            if (warehouse != null && !warehouse.isEmpty()) {
                ps.setString(paramIndex++, warehouse);
            }
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                OderIteam stats = new OderIteam();
                stats.setWarehouse(rs.getString("WarehouseName"));
                stats.setOder(rs.getString("OrderType"));
                stats.setStatus(rs.getString("OrderStatus"));
                stats.setTotalquantity(rs.getInt("Total Quantity"));
                orderStatisticsList.add(stats);
            }
        }
        return orderStatisticsList;
    }

        public List<String> getAllWarehouseNames() throws SQLException {
        List<String> warehouseNames = new ArrayList<>();
        String sql = "SELECT WarehouseName FROM Warehouses";

        try (PreparedStatement ps = dbContext.getConnection().prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String warehouseName = rs.getString("WarehouseName");
                warehouseNames.add(warehouseName);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Error fetching warehouse names.", e);
            throw e;
        }
        return warehouseNames;
    }   

}
