/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ht
 */
public class Account {

    private int userID;
    private String userName;
    private String email;
    private String phoneNumber;
    private String passWord;
    private int WHID;//warehouseID
    private Integer isCenter;
    private int isPartner;
    private int partnerID;
    private boolean isActive;

    public Account() {
    }

    public Account(int userID, String userName, String email, String phoneNumber, String passWord, int WHID, Integer isCenter, int isPartner, int partnerID, boolean isActive) {
        this.userID = userID;
        this.userName = userName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.passWord = passWord;
        this.WHID = WHID;
        this.isCenter = isCenter;
        this.isPartner = isPartner;
        this.partnerID = partnerID;
        this.isActive = isActive;
    }

    public Account(int userID, String userName, String email, String phoneNumber, Integer isCenter, int isPartner, int WHID, boolean isActive) {
        this.userID = userID;
        this.userName = userName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.isCenter = isCenter;
        this.isPartner = isPartner;
        this.WHID = WHID;
        this.isActive = isActive;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public int getWHID() {
        return WHID;
    }

    public void setWHID(int WHID) {
        this.WHID = WHID;
    }

    public Integer getIsCenter() {
        return isCenter;
    }

    public void setIsCenter(Integer isCenter) {
        this.isCenter = isCenter;
    }

    public int getIsPartner() {
        return isPartner;
    }

    public void setIsPartner(int isPartner) {
        this.isPartner = isPartner;
    }

    public int getPartnerID() {
        return partnerID;
    }

    public void setPartnerID(int partnerID) {
        this.partnerID = partnerID;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "Account{" + "userID=" + userID + ", userName=" + userName + ", email=" + email + ", phoneNumber=" + phoneNumber + ", passWord=" + passWord + ", WHID=" + WHID + ", isCenter=" + isCenter + ", isPartner=" + isPartner + ", partnerID=" + partnerID + ", isActive=" + isActive + '}';
    }
}
