/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DBContext;
import entity.OderIteam;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import service.OrderService;

/**
 *
 * @author songl
 */
@WebServlet(name = "OrderStatisticsServlet", urlPatterns = {"/orderStatistics"})
public class OrderStatisticsServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(OrderStatisticsServlet.class.getName());
    private OrderService orderService;

    @Override
    public void init() throws ServletException {
        try {
            DBContext dbContext = new DBContext();
            orderService = new OrderService(dbContext);
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, "Cannot initialize OrderService", e);
            throw new ServletException("Cannot initialize OrderService", e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String warehouse = request.getParameter("warehouse");

        try {
            DBContext dbContext = new DBContext();
            List<String> warehouseNames = dbContext.getAllWarehouseNames();
            request.setAttribute("warehouseNames", warehouseNames);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "SQL exception while fetching warehouse names.", e);
            throw new ServletException("Error fetching warehouse names", e);
        }

        try {
            List<OderIteam> orderList;
            orderList = orderService.getOrderStatistics(warehouse);
            request.setAttribute("orderList", orderList);
            request.getRequestDispatcher("oderstatistic.jsp").forward(request, response);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Error fetching order statistics", e);
            throw new ServletException("Error fetching order statistics", e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }
}
