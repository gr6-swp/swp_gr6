package controller;

import dal.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;

public class UserServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if ("toggleUserStatus".equals(action)) {
            userStatus(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        UserDAO userDAO = new UserDAO();

        if ("toggleUserStatus".equals(action)) {
            int userID = Integer.parseInt(request.getParameter("id"));
            boolean status = Boolean.parseBoolean(request.getParameter("status"));
            try {
                userDAO.AccountActivation(userID, status);
            } catch (SQLException e) {
            }
            response.sendRedirect("User.jsp");
        } else {
            if (action == null || action.isEmpty()) {
                listUser(request, response, userDAO);
            }
        }
    }

    private void listUser(HttpServletRequest request, HttpServletResponse response, UserDAO userDAO) throws ServletException, IOException {
        try {
            request.setAttribute("users", userDAO.listAllAccounts());
            request.getRequestDispatcher("User.jsp").forward(request, response);
        } catch (ServletException | IOException | SQLException e) {
        }
    }

    private void userStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int userID = Integer.parseInt(request.getParameter("id"));
        boolean newStatus = Boolean.parseBoolean(request.getParameter("status"));

        UserDAO userDAO = new UserDAO();

        try {
            userDAO.AccountActivation(userID, newStatus);
            request.setAttribute("users", userDAO.listAllAccounts());
            request.getRequestDispatcher("User.jsp").forward(request, response);
        } catch (ServletException | IOException | SQLException e) {
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phoneNumber");
        String role = request.getParameter("role");
        int warehouseID = Integer.parseInt(request.getParameter("warehouseID"));
        UserDAO userDAO = new UserDAO();
        try {
            if ("Partner".equals(role)) {
                String partnerName = request.getParameter("partnerName");
                String contactName = request.getParameter("contactName");
                String address = request.getParameter("address");

                int partnerID = userDAO.addPartner(partnerName, contactName, email, phoneNumber, address);
                userDAO.addAccount(username, password, email, phoneNumber, warehouseID, true, partnerID);
            } else if ("Keeper".equals(role)) {
                userDAO.addAccount(username, password, email, phoneNumber, warehouseID, false, null);
            }

            response.sendRedirect("userManagement.jsp");
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
