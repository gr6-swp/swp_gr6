package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import model.Constants;
import model.GoogleUserDto;
import dal.UserDAO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.PrintWriter;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

@WebServlet(name = "LoginGoogleServlet", urlPatterns = {"/logingoogle"})
public class LoginGoogleServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginGoogleServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginGoogleServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String code = request.getParameter("code");
        if (code == null || code.isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing code parameter.");
            return;
        }

        try {
            String accessToken = getToken(code);
            GoogleUserDto googleUser = getUserInfo(accessToken);

            if (googleUser.getEmail() == null || googleUser.getEmail().isEmpty()) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing email information from Google.");
                return;
            }

            UserDAO db = new UserDAO();
            Account account = db.LoginGmail(googleUser.getEmail());
            if (account == null) {
                response.sendRedirect("login.jsp?warningG=Your email hasn't been signed-up. Please sign up a new account");
                return;
            }
            HttpSession session = request.getSession();
            session.setAttribute("acc", account);
            int isPartner = account.getIsPartner();
            Integer isCenter = account.getIsCenter();

            if (isPartner != 1 && isCenter == 0) {
                response.sendRedirect("partner.jsp");
            } else if (isPartner != 1 && isCenter == 1) {
                response.sendRedirect("home.jsp");
            } else {
                response.sendRedirect("keeper.jsp");
            }

        } catch (SQLException ex) {
            Logger.getLogger(LoginGoogleServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "An error occurred while processing your request.");
        } catch (IOException ex) {
            Logger.getLogger(LoginGoogleServlet.class.getName()).log(Level.SEVERE, "Unexpected error", ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "An unexpected error occurred.");
        }
    }

    private String getToken(String code) throws ClientProtocolException, IOException {
        String response = Request
                .Post(Constants.GOOGLE_LINK_GET_TOKEN)
                .bodyForm(Form.form().add("client_id", Constants.GOOGLE_CLIENT_ID)
                        .add("client_secret", Constants.GOOGLE_CLIENT_SECRET)
                        .add("redirect_uri", Constants.GOOGLE_REDIRECT_URI)
                        .add("code", code)
                        .add("grant_type", Constants.GOOGLE_GRANT_TYPE)
                        .build())
                .execute().returnContent().asString();

        Logger.getLogger(LoginGoogleServlet.class.getName()).log(Level.INFO, "Token Response: {0}", response);

        JsonObject jobj = new Gson().fromJson(response, JsonObject.class);
        return jobj.get("access_token").getAsString();
    }

    private GoogleUserDto getUserInfo(final String accessToken) throws ClientProtocolException, IOException {
        String link = Constants.GOOGLE_LINK_GET_USER_INFO + "?access_token=" + accessToken;
        String response = Request.Get(link).execute().returnContent().asString();

        Logger.getLogger(LoginGoogleServlet.class.getName()).log(Level.INFO, "User Info Response: {0}", response);

        return new Gson().fromJson(response, GoogleUserDto.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
