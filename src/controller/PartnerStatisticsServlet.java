/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DBContext;
import entity.PartnerItem;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import service.PartnerService;

/**
 *
 * @author songl
 */
@WebServlet(name = "PartnerStatisticsServlet", urlPatterns = {"/partnerStatistics"})
public class PartnerStatisticsServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(OrderStatisticsServlet.class.getName());
    private PartnerService partnerService;

    @Override
    public void init() throws ServletException {
        try {
            DBContext dbContext = new DBContext();
            partnerService = new PartnerService(dbContext);
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, "Cannot initialize PartnerService", e);
            throw new ServletException("Cannot initialize PartnerService", e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String partner = request.getParameter("partner");
        try {
            DBContext dBContext = new DBContext();
            List<String> partnerNameS = dBContext.getAllPartner();
            request.setAttribute("partnerNameS", partnerNameS);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "SQL exception while fetching partner names.", e);
            throw new ServletException("Error fetching partner names", e);
        }
        try {
            List<PartnerItem> partnerList ;
            partnerList= partnerService.getPartnerStatistics(partner);
            request.setAttribute("partnerList", partnerList);
            request.getRequestDispatcher("partnerstatistic.jsp").forward(request, response);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Error fetching partner statistics", e);
            throw new ServletException("Error fetching partner statistics", e);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PartnerStatisticsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PartnerStatisticsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
}
