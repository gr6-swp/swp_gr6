﻿--Trigger của Trung
CREATE TRIGGER trg_UpdateOrderIDOnProductInsert
ON Products
AFTER INSERT
AS
BEGIN
    -- Lấy ProductID mới nhất từ bảng Products
    DECLARE @latestProductID INT;
    SELECT TOP 1 @latestProductID = ProductID FROM Products ORDER BY ProductID DESC;

    -- Lấy OrderID mới nhất từ bảng Orders
    DECLARE @latestOrderID INT;
    SELECT TOP 1 @latestOrderID = OrderID FROM Orders ORDER BY OrderID DESC;

    -- Cập nhật ProductID trong bảng Orders với OrderID mới nhất và ProductID mới nhất
    UPDATE Orders
    SET ProductID = @latestProductID
    WHERE OrderID = @latestOrderID;
END;

--Trigger của Thực
CREATE TRIGGER trg_UpdateOrderIDOnProductInsert
ON Products
AFTER INSERT
AS
BEGIN
    -- Lấy ProductID mới nhất từ bảng Products
    DECLARE @latestProductID INT;
    SELECT TOP 1 @latestProductID = ProductID FROM Products ORDER BY ProductID DESC;

    -- Lấy OrderID mới nhất từ bảng Orders
    DECLARE @latestOrderID INT;
    SELECT TOP 1 @latestOrderID = OrderID FROM Orders ORDER BY OrderID DESC;

    -- Cập nhật ProductID trong bảng Orders với OrderID mới nhất và ProductID mới nhất
    UPDATE Orders
    SET ProductID = @latestProductID
    WHERE OrderID = @latestOrderID;
END;

-- Trigger kiểm tra mỗi Keeper chỉ quản lý một kho duy nhất
CREATE TRIGGER trg_CheckSingleWarehouseKeeper
ON WarehouseZones
AFTER INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM inserted i
        JOIN WarehouseZones w ON i.KeeperID = w.KeeperID AND i.WarehouseID <> w.WarehouseID
        WHERE i.KeeperID IS NOT NULL
    )
    BEGIN
        RAISERROR ('Invalid KeeperID: The user is already managing a different warehouse.', 16, 1);
        ROLLBACK TRANSACTION;
    END
END;
GO

CREATE TRIGGER trg_UpdateProductWarehouseZoneID
ON Orders
AFTER UPDATE
AS
BEGIN
    UPDATE p
    SET p.WarehouseZoneID = i.ZoneID
    FROM Products p
    INNER JOIN inserted i ON p.ProductID = i.ProductID
    WHERE p.ProductID = i.ProductID;
END;
