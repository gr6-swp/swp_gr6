/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.StatisticDAO;
import entity.InventoryItem;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import service.InventoryService;

/**
 *
 * @author songl
 */
@WebServlet(name = "ExportInventoryExel", urlPatterns = {"/exportInventoryExel"})
public class ExportInventoryExel extends HttpServlet {

    private InventoryService inventoryService;

    @Override
    public void init() throws ServletException {
        StatisticDAO dbContext = new StatisticDAO();
        inventoryService = new InventoryService(dbContext);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String zoneName = request.getParameter("zoneName");
        List<InventoryItem> inventoryList = null;
        try {
            inventoryList = inventoryService.getInventoryStatistics(zoneName);
        } catch (SQLException ex) {
            Logger.getLogger(ExportInventoryExel.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (inventoryList == null || inventoryList.isEmpty()) {
            response.getWriter().write("No data available to export.");
            return;
        }

        // Thiết lập response để xuất file Excel
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=inventory_statistics.xlsx");

        try ( Workbook workbook = new XSSFWorkbook();  OutputStream out = response.getOutputStream()) {
            Sheet sheet = workbook.createSheet("Inventory Statistics");

            // Tiêu đề cho các cột
            String[] columns = {"No", "Product Name", "Zone Name", "Total Quantity"};
            Row headerRow = sheet.createRow(0);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }
            int rowNum = 1;
            for (InventoryItem item : inventoryList) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(rowNum - 1);
                row.createCell(1).setCellValue(item.getProductName());
                row.createCell(2).setCellValue(item.getZoneName());
                row.createCell(3).setCellValue(item.getTotalQuantity());
            }
            workbook.write(out);
        } catch (Exception e) {
            throw new ServletException("Exception in creating Excel file", e);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String zoneName = request.getParameter("zoneNameSelected");
        List<InventoryItem> inventoryList = null;
        try {
            inventoryList = inventoryService.getInventoryStatistics(zoneName);
        } catch (SQLException e) {
            response.getWriter().write("Error retrieving data from database.");
            return;
        }
        if(inventoryList == null || inventoryList.isEmpty()){
            response.getWriter().write("No data available for selected zoneName.");
            return;
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=inventory_report.xlsx");
        try ( Workbook workbook = new XSSFWorkbook();  OutputStream out = response.getOutputStream()) {
            Sheet sheet = workbook.createSheet("Inventory Report");
            Row titleRow = sheet.createRow(0);
            Cell titleCell = titleRow.createCell(0);
            titleCell.setCellValue("Bảng thống kê số lượng sản phầm tồn kho tại các khu");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4)); // Từ dòng 0, cột 0 đến dòng 0, cột 4
            Row emptyRow = sheet.createRow(1);
            String[] columns = {"No", "Product Name", "Zone Name", "Total Quantity"};
            Row headerRow = sheet.createRow(2);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }
            int rowNum = 3;
            for (InventoryItem item : inventoryList) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(rowNum - 1);
                row.createCell(1).setCellValue(item.getProductName());
                row.createCell(2).setCellValue(item.getZoneName());
                row.createCell(3).setCellValue(item.getTotalQuantity());
            }
            workbook.write(out);
        } catch (Exception e) {
            throw new ServletException("Exception in creating Excel file", e);
        }
    }

    @Override
    public String getServletInfo() {
        return "Servlet to export Inventory Statistics to Excel";
    }// </editor-fold>

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);

    }
}
