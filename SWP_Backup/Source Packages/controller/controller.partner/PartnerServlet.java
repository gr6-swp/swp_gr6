/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.partner;

import dao.PartnerDAO;
import dao.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Products;

/**
 *
 * @author thuct
 */
@WebServlet(name="PartnerServlet", urlPatterns={"/partner"})
public class PartnerServlet extends HttpServlet {
     private final ProductDAO dao = new ProductDAO(); // Instantiate ProductDAO

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ProductPartner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
            try {
                // Handle other actions if needed
                processRequest(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(ProductPartner.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        HttpSession session = request.getSession();
        Account partner = (Account) session.getAttribute("acc"); // Get the partner object from session

        if (partner == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        int partnerId = partner.getPartnerID();

        PartnerDAO partnerDAO = new PartnerDAO();
        int completedProducts = partnerDAO.getCompletedProductsCount(partnerId);
// double totalUnitPrice = partnerDAO.getTotalProductCompleted(partnerId);
        request.setAttribute("completedProducts", completedProducts);
//        request.setAttribute("totalUnitPrice", totalUnitPrice);
        request.getRequestDispatcher("partner.jsp").forward(request, response);
    }

}