/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.partner;

import dao.StatisticDAO;
import entity.InventoryItem;
import entity.PartnerInventory;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author songl
 */
@WebServlet(name = "exparnerinventorys", urlPatterns = {"/exparnerinventorys"})
public class ExportPartnerInventory extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StatisticDAO dbcontext = new StatisticDAO();
        HttpSession session = request.getSession();
        Integer partnerID = (Integer) session.getAttribute("partnerID");
        if (partnerID == null) {
            response.sendRedirect("login.jsp?error=missingPartnerID");
            return;
        }

        String productName = request.getParameter("productName");
        List<PartnerInventory> partnerinventorylist = null;
        try {
            partnerinventorylist = dbcontext.fetchPartnerInventoryStatistics(partnerID, productName);
        } catch (SQLException e) {
            throw new ServletException("Error retrieving data from database", e);
        }

        if (partnerinventorylist == null || partnerinventorylist.isEmpty()) {
            response.getWriter().write("No data available to export");
            return;
        }

        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=partnerinventory_statistics.xlsx");

        try ( Workbook workbook = new XSSFWorkbook();  OutputStream out = response.getOutputStream()) {
            Sheet sheet = workbook.createSheet("Inventory Statistics");

            // Tiêu đề cho các cột
            String[] columns = {"No", "Product Name", "Total Quantity"};
            Row headerRow = sheet.createRow(0);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }

            int rowNum = 1;
            for (PartnerInventory item : partnerinventorylist) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(rowNum - 1);
                row.createCell(1).setCellValue(item.getProduct());

                row.createCell(2).setCellValue(item.getQuantity());

            }
            workbook.write(out);
        } catch (Exception e) {
            throw new ServletException("Exception in creating Excel file", e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StatisticDAO dbcontext = new StatisticDAO();
        String productName = request.getParameter("productNameSelected");
        List<PartnerInventory> partnerinventorylist = null;
        HttpSession session = request.getSession();
        Integer partnerID = (Integer) session.getAttribute("partnerID");
        if (partnerID == null) {
            response.sendRedirect("login.jsp?error=missingPartnerID");
            return;
        }

        try {
            partnerinventorylist = dbcontext.fetchPartnerInventoryStatistics(partnerID, productName);
        } catch (SQLException e) {
            // Log the error for debugging purposes
            System.out.println("SQL Exception: " + e.getMessage());
            e.printStackTrace();

            response.getWriter().write("Error retrieving data from database: " + e.getMessage());
            return;
        }
        if (partnerinventorylist == null || partnerinventorylist.isEmpty()) {
            response.getWriter().write("No data available for selected productName.");
            return;
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=partnerinventory_report.xlsx");
        try ( Workbook workbook = new XSSFWorkbook();  OutputStream out = response.getOutputStream()) {
            Sheet sheet = workbook.createSheet("Inventory Report");
            Row titleRow = sheet.createRow(0);
            Cell titleCell = titleRow.createCell(0);
            titleCell.setCellValue("Bảng thống kê số lượng sản phẩm tồn kho");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4)); // Từ dòng 0, cột 0 đến dòng 0, cột 4
            Row emptyRow = sheet.createRow(1);
            String[] columns = {"No", "Product Name", "Total Quantity"};
            Row headerRow = sheet.createRow(2);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }
            int rowNum = 3;
            for (PartnerInventory item : partnerinventorylist) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(rowNum - 3);
                row.createCell(1).setCellValue(item.getProduct());
                row.createCell(2).setCellValue(item.getQuantity());
            }
            workbook.write(out);
        } catch (Exception e) {
            throw new ServletException("Exception in creating Excel file", e);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
