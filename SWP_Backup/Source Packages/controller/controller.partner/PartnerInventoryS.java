/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.partner;

import dao.StatisticDAO;
import entity.PartnerInventory;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author songl
 */
@WebServlet(name = "parnerinventorys", urlPatterns = {"/partnerinventorys"})
public class PartnerInventoryS extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PartnerInventoryS</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PartnerInventoryS at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   @Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    StatisticDAO stdao = new StatisticDAO();

    String productName = request.getParameter("productName");
    HttpSession session = request.getSession();
    Integer partnerID = (Integer) session.getAttribute("partnerID");

    // Kiểm tra xem partnerID có tồn tại không
    if (partnerID == null) {
        response.sendRedirect("login.jsp?error=missingPartnerID");
        return;
    }

    try {
        // Lấy danh sách tên sản phẩm của partner
        List<String> productNames = stdao.getAllProduct(partnerID);
        request.setAttribute("productNames", productNames);
    } catch (SQLException e) {
        throw new ServletException("Error fetching product names", e);
    }

    try {
        List<PartnerInventory> partnerinventorylist;
        if ("all".equals(productName)) {
            partnerinventorylist = stdao.fetchPartnerInventoryStatistics(partnerID, null);
        } else {
            partnerinventorylist = stdao.fetchPartnerInventoryStatistics(partnerID, productName);
        }
        request.setAttribute("partnerinventorylist", partnerinventorylist);
        request.getRequestDispatcher("PartnerInventory.jsp").forward(request, response);
    } catch (SQLException e) {
        throw new ServletException("Error fetching inventory statistics", e);
    }
}


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
