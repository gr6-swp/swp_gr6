package controller.keeper;



import dao.OrderDAO;
import model.OrderKeeper;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "OrderController", urlPatterns = {"/orderList", "/confirmOrder"})
public class OrderKeeperServlet extends HttpServlet {

    private OrderDAO orderDAO;

    public void init() {
        orderDAO = new OrderDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
//            HttpSession session = request.getSession();
//            Integer UserID = (Integer) session.getAttribute("UserID");


        switch (action) {
            case "/orderList":
                listOrders(request, response);
                break;
            case "/confirmOrder":
                confirmOrder(request, response);
                break;
            default:
                response.sendRedirect("orderKeeper.jsp");
                break;
        }
    }

    private void listOrders(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<OrderKeeper> orders = OrderDAO.getAllProcessingOrders();
        request.setAttribute("orders", orders);
        RequestDispatcher dispatcher = request.getRequestDispatcher("orderKeeper.jsp");
        dispatcher.forward(request, response);
    }

    private void confirmOrder(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int orderId = Integer.parseInt(request.getParameter("orderId"));
        orderDAO.updateOrderStatus(orderId, "Hoàn thành");
        response.sendRedirect("orderList");
    }
    
    
}
