package controller.keeper;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import dao.NotificationsDAO;
import model.Account;
import model.Notification;

@WebServlet("/notificationsKeeper")
public class NotificationsKeeperServlet extends HttpServlet {

        private NotificationsDAO notificationDAO;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }

        switch (action) {
            case "create":
                showCreateForm(request, response);
                break;
            case "list":
            default:
                listNotifications(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        if (action == null) {
            action = "list";
        }

        switch (action) {
//            case "createNotification":
//                createNotification(request, response);
//                break;
            case "updateStatus":
                updateNotificationStatus(request, response);
                break;
            default:
                listNotifications(request, response);
                break;
        }
    }

    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("new.jsp");
        dispatcher.forward(request, response);
    }


    private void listNotifications(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("acc"); // Get the partner object from session

        if (user == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        int userId = user.getUserID();
        List<Notification> notifications = notificationDAO.getNotificationsByReceiverRole(userId); // Change receiver role ID as needed
        request.setAttribute("notifications", notifications);
        RequestDispatcher dispatcher = request.getRequestDispatcher("notificationsKeeper.jsp");
        dispatcher.forward(request, response);
    }

    private void updateNotificationStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int notificationID = Integer.parseInt(request.getParameter("notificationID"));
        String status = request.getParameter("status");
        NotificationsDAO.markAsRead(notificationID);

        //notificationsDAO.updateNotificationStatus(notificationID, status);
        response.sendRedirect("notificationsKeeper?action=list");
    }
}
