package controller.center;

import dao.WarehouseDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Account;
import model.WarehouseZone;

public class WarehouseServlet extends HttpServlet {
    WarehouseDAO dao = new WarehouseDAO();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        List<WarehouseZone> zones = dao.getAllWarehouseZones();
        List<Account> keeper = dao.getKeeperID();
        
        request.setAttribute("userID", keeper);
        request.setAttribute("zones", zones);
        request.getRequestDispatcher("warehouse.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String action = request.getParameter("action");

        if (action != null) {
            switch (action) {
                case "addZone":
                    handleAddZone(request, response);
                    break;
                case "editZone":
                    handleEditZone(request, response);
                    break;
                case "deleteZone":
                    handleDeleteZone(request, response);
                    break;
                case "searchZones":
                    handleSearchZones(request, response);
                    break;
                default:
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid action");
            }
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Action parameter missing");
        }
    }

    
    //--------------------Thêm zone----------------
private void handleAddZone(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String zoneName = request.getParameter("name");
    int keeperID = 0;

    try {
        keeperID = Integer.parseInt(request.getParameter("keeperID"));
    } catch (NumberFormatException e) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid number format for keeperID or total");
        return;
    }
    if (zoneName != null && !zoneName.isEmpty()) {
        dao.addZone(zoneName, keeperID);
        response.sendRedirect("warehouse");
    } else {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Zone name is required");
    }
}
       

//--------------Chỉnh sửa Zone-----------------
private void handleEditZone(HttpServletRequest request, HttpServletResponse response) throws IOException {
    int keeperId = 0;
    int zoneId = 0;
    String zoneName = request.getParameter("name");

    try {
        keeperId = Integer.parseInt(request.getParameter("keeperID"));
        zoneId = Integer.parseInt(request.getParameter("zoneID"));
    } catch (NumberFormatException e) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid number format");
        return;
    }
    if (zoneName!= null &&!zoneName.isEmpty()) {
        dao.editZone(zoneId, zoneName, keeperId);
        response.sendRedirect("warehouse");
    } else {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Zone name is required");
    }
}
  //-----------Xóa Zone=--------------
  private void handleDeleteZone(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int zoneId = 0;
        try {
            zoneId = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid zone ID");
            return;
        }
        dao.deleteZone(zoneId);
        response.sendRedirect("warehouse");
    }

  
  //-----------TÌm kiếm Zone======--------------
 private void handleSearchZones(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    String zoneName = request.getParameter("zoneName");
    String keeperID = request.getParameter("keeperID");
    List<WarehouseZone> zones = dao.searchZones(zoneName, keeperID);
    request.setAttribute("zones", zones);
    request.getRequestDispatcher("warehouse.jsp").forward(request, response);
}
 



    @Override
    public String getServletInfo() {
        return "";
    }
}
