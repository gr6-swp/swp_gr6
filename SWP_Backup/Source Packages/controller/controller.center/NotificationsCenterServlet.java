package controller.center;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import dao.NotificationsDAO;
import model.Account;
import model.Notification;

@WebServlet("/notificationsCenter")

public class NotificationsCenterServlet extends HttpServlet {

    private NotificationsDAO notificationDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        notificationDAO = new NotificationsDAO(); // Khởi tạo DAO trong phương thức init
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }

        switch (action) {
            case "create":
                showCreateForm(request, response);
                break;
            case "list":
            default:
                listNotifications(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        if (action == null) {
            action = "list";
        }

        switch (action) {
            case "createNotification":
                createNotification(request, response);
                break;
            case "updateStatus":
                updateNotificationStatus(request, response);
                break;
            default:
                listNotifications(request, response);
                break;
        }
    }

    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("new.jsp");
        dispatcher.forward(request, response);
    }

    private void createNotification(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int senderId = 1; // Assuming sender ID is known or retrieved from session
            int receiverId = Integer.parseInt(request.getParameter("receiverId")); // Assuming receiver ID is passed from request
            String content = request.getParameter("content");
            int relatedEntityId = Integer.parseInt(request.getParameter("relatedEntityId"));

            Notification notification = new Notification();
            notification.setIDSender(senderId);
            notification.setIDReceiver(receiverId);
            notification.setContent(content);
            notification.setRelatedEntityID(relatedEntityId);
            notification.setCreatedAt(LocalDateTime.now());

            notificationDAO.createNotification(notification);
            response.sendRedirect("notificationsCenter?action=list");
        } catch (NumberFormatException e) {
            // Handle number format exception (e.g., invalid input for ID)
            response.sendRedirect("notificationsCenter?action=list"); // Redirect to list page or show error message
        }
    }

    private void listNotifications(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("acc"); // Get the partner object from session

        if (user == null) {
            response.sendRedirect("login.jsp");
            return;
        }
        int userId = user.getUserID();
        List<Notification> notifications = notificationDAO.getNotificationsByReceiverRole(1); // Change receiver role ID as needed
        request.setAttribute("notifications", notifications);
        RequestDispatcher dispatcher = request.getRequestDispatcher("notificationsCenter.jsp");
        dispatcher.forward(request, response);
    }

    private void updateNotificationStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int notificationID = Integer.parseInt(request.getParameter("notificationID"));
            notificationDAO.markAsRead(notificationID);
            response.sendRedirect("notificationsCenter?action=list");
        } catch (NumberFormatException e) {
            // Handle number format exception (e.g., invalid notification ID)
            response.sendRedirect("notificationsCenter?action=list"); // Redirect to list page or show error message
        }
    }

    @Override
    public void destroy() {
        notificationDAO = null; // Cleanup DAO in destroy method if needed
        super.destroy();
    }
    public static void main(String[] args) {
        NotificationsDAO notice = new NotificationsDAO();
        List<Notification> list = notice.getNotificationsByReceiverRole(1);
        System.out.println(list.get(0));
    }
}
