<%-- 
    Document   : partner
    Created on : May 30, 2024, 11:08:28 PM
    Author     : X P S
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Orders</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="assets/images/logo-mini.svg" />

        <style>
            @media print {
                body {
                    width: 100%;
                    margin: 0;
                    padding: 0;
                    box-sizing: border-box;
                }
                .popup-content {
                    width: 100%;
                    margin: 0;
                    padding: 0;
                    text-align: left;
                }
                .invoice-container {
                    width: 100%;
                    margin: 0;
                    padding: 0;
                    box-sizing: border-box;
                    text-align: left;
                }
                table {
                    width: 100%;
                    margin: 0;
                    padding: 0;
                    border-collapse: collapse;
                }
                th, td {
                    border: 1px solid black;
                    padding: 8px;
                    text-align: left;
                }
                .row {
                    display: flex;
                    justify-content: space-between;
                    margin-top: 10px;
                }
                .col-6 {
                    width: 50%;
                }
                .text-black {
                    color: black;
                }
                .invoice-header h1 {
                    margin: 0;
                }
                button {
                    display: none;
                }

            }

            .order-table {
                border-collapse: collapse;
                width: 10%;
            }

            .order-table th,
            .order-table td {
                border: 1px solid #ddd;
                padding: 8px;
            }

            .order-table tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            .order-table tr:hover {
                background-color: #ddd;
            }

            .order-table th {
                background-color: #4CAF50;
                color: white;
            }

            body {
                font-family: Arial, sans-serif;
            }
            h2 {
                margin-bottom: 10px;
            }

            /* Kiểu dáng cho nút */
            button {
                background-color: #4CAF50; /* Màu xanh lá cây */
                color: white;
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Kiểu dáng cho ô tìm kiếm và select */
            input[type="text"], input[type="number"], select {
                padding: 8px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 4px;
                padding: 10px;
                border-radius: 5px; /* Bo góc */
                width: 170px; /* Điều chỉnh độ rộng tùy ý */
            }


            /* Kiểu dáng cho bảng */
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 15px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }



            button {
                /* ... (các kiểu dáng khác của nút) */
                margin-left: 5px; /* Tạo khoảng cách giữa nút và ô tìm kiếm */
            }
            #container {
                border: 2px solid #44ce42; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px; /* Bo góc (tùy chọn) */
            }

            .popup {
                display: none; /* Ẩn popup ban đầu */
                position: fixed; /* Ở vị trí cố định so với viewport */
                z-index: 1; /* Đảm bảo popup nằm trên các phần tử khác */
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: auto;
                background-color: rgba(0, 0, 0, 0.4); /* Màu nền mờ */
            }

            .popup-content {
                background-color: #fefefe;
                margin: 10% auto; /* Căn giữa popup */
                padding: 17px;
                border: 3px solid #888;
                width: 73%;
                max-width: 833px; /* Giới hạn chiều rộng tối đa */
                min-height: 453px;
                border-radius: 10px;
                margin-left: 28%;
            }

            .close-btn {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close-btn:hover,
            .close-btn:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 15px; /* Tăng padding cho các ô */
                text-align: left;
            }

            body {
                font-family: sans-serif;
            }

            button {
                padding: 10px;
                background-color: #4CAF50;
                color: white;
                border: none;
                cursor: pointer;
            }
            input, select {
                width: 100%;
                padding: 8px;
                box-sizing: border-box;
            }
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }
            th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
            }
            th {
                background-color: #f4f4f4;
            }
            button {
                background: #28a745;
                color: #fff;
                border: none;
                padding: 10px 15px;
                cursor: pointer;
                border-radius: 5px;
            }

            .nav-link.active {
                background-color: #28a745 !important;
                color: white !important;
            }

        </style>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">

    </head>
    <body>
<div class="container-scroller">
            <!-- partial:partials/_navbar.html -->
            <nav
                class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div
                    class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                    <a class="navbar-brand brand-logo" href="homepartner"><img
                            src="assets/images/logo.svg" alt="logo" /></a>
                    <a class="navbar-brand brand-logo-mini"
                       href="homepartner"><img src="assets/images/logo-mini.svg"
                                            alt="logo" /></a>
                </div>

                <div class="navbar-menu-wrapper d-flex align-items-stretch">
                    <button
                        class="navbar-toggler navbar-toggler align-self-center"
                        type="button" data-toggle="minimize">
                        <span class="mdi mdi-menu"></span>
                    </button>
                    <form class="d-flex align-items-center h-100"
                          style="font-size: larger;
                          padding-top: 10px;">
                        <h2>ORDER PARTNER</h2>
                    </form>
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item  dropdown d-none d-md-block">

                            <a style="color: black"
                               href="trogiup"

                               ><svg width="24"
                                  height="24" fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                  font-size="24" class="jss220"
                                  color="#A2A8AF"><path
                                  d="M19.08 4.93A9.972 9.972 0 0 0 12 2a9.928 9.928 0 0 0-7.07 2.93A9.947 9.947 0 0 0 2 12a9.947 9.947 0 0 0 2.93 7.07A9.947 9.947 0 0 0 12 22a9.947 9.947 0 0 0 7.07-2.93A9.947 9.947 0 0 0 22 12a9.937 9.937 0 0 0-2.92-7.07ZM12 11.38a2.817 2.817 0 0 1 2.81 2.81c0 1.285-.86 2.374-2.048 2.701a.187.187 0 0 0-.141.185v.523c0 .338-.261.621-.6.643a.622.622 0 0 1-.653-.621v-.545a.187.187 0 0 0-.141-.185 2.83 2.83 0 0 1-2.048-2.69c0-.338.261-.632.61-.643a.626.626 0 0 1 .642.62 1.56 1.56 0 0 0 1.678 1.558 1.567 1.567 0 0 0 1.449-1.448A1.567 1.567 0 0 0 12 12.61c-1.547.022-2.81-1.242-2.81-2.8 0-1.274.871-2.363 2.048-2.701a.2.2 0 0 0 .141-.185V6.39c0-.338.261-.62.6-.643a.622.622 0 0 1 .653.621v.545c0 .087.054.163.141.185a2.83 2.83 0 0 1 2.048 2.69.637.637 0 0 1-.61.643.626.626 0 0 1-.642-.62 1.56 1.56 0 0 0-1.678-1.558 1.576 1.576 0 0 0-1.449 1.449A1.567 1.567 0 0 0 12 11.379Z"
                                  fill="#A2A8AF"></path></svg>
                                Sales support  </a>
                        </li>

                        <li class="nav-item nav-profile dropdown">
                            <a class="nav-link dropdown-toggle"
                               id="profileDropdown" href="#"
                               data-toggle="dropdown" aria-expanded="false">
                                <span
                                    style="display: table-cell;
                                    vertical-align: middle;
                                    white-space: nowrap;"></span>
                                <div class="nav-profile-text">

                                    <p class="mb-1 text-black"> Your Name </p>
                                </div>
                            </a>
                            <div
                                class="dropdown-menu navbar-dropdown dropdown-menu-right p-0 border-0 font-size-sm"
                                aria-labelledby="profileDropdown"
                                data-x-placement="bottom-end">
                                <div class="p-2">
                                    <h5
                                        class="dropdown-header text-uppercase pl-2 text-dark">User
                                        Options</h5>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>Profile</span>
                                        <span class="p-0">
                                            <span
                                                class="badge badge-success">1</span>
                                            <i
                                                class="mdi mdi-account-outline ml-1"></i>
                                        </span>
                                    </a>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="javascript:void(0)">
                                        <span>Service pack information</span>
                                        <i class="mdi mdi-settings"></i>
                                    </a>
                                    <div role="separator"
                                         class="dropdown-divider"></div>
                                    <h5
                                        class="dropdown-header text-uppercase  pl-2 text-dark mt-2">Actions</h5>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>File import and export
                                            history</span>
                                        <svg width="24" height="24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg"
                                             font-size="24"><g
                                             clip-path="url(#file-icon_svg__a)"
                                             fill="currentColor"><path
                                             d="M20.02 7.443c.209 0 .398.045.588.1v-.136c0-.94-.768-1.708-1.708-1.708h-6.462l-1.455-1.446A.856.856 0 0 0 10.378 4H5.272c-.94 0-1.709.768-1.709 1.708v1.79c.136-.028.272-.046.407-.046h16.05v-.009Z"></path><path
                                             fill-rule="evenodd"
                                             clip-rule="evenodd"
                                             d="M3.97 8.546h16.05c1.094.009 1.971.84 1.98 1.88v.027l-1.139 8.097c-.018 1.022-.904 1.853-1.988 1.853H5.136c-1.094 0-1.97-.822-1.988-1.853L2 10.453v-.027c0-1.04.886-1.871 1.97-1.88Zm7.393 3.19v1.057c2.088.118 3.588 1.944 3.597 4.429 0 .1-.082.19-.18.208h-.037a.203.203 0 0 1-.199-.145c-.47-1.4-1.51-2.115-3.172-2.169v1.04a.22.22 0 0 1-.081.162.213.213 0 0 1-.299-.027l-1.898-2.314a.206.206 0 0 1 .01-.27L11 11.6a.193.193 0 0 1 .154-.073c.118 0 .208.09.208.208Z"></path></g><defs><clippath
                                             id="file-icon_svg__a"><path
                                                fill="#fff"
                                                transform="translate(2 4)"
                                                d="M0 0h20v16.412H0z"></path></clippath></defs></svg>
                                    </a>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="#">
                                        <span>Lock Account</span>
                                        <i class="mdi mdi-lock ml-1"></i>
                                    </a>
                                    <a
                                        class="dropdown-item py-1 d-flex align-items-center justify-content-between"
                                        href="logout">
                                        <span>Log Out</span>
                                        <i class="mdi mdi-logout ml-1"></i>
                                    </a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link count-indicator dropdown-toggle"
                               id="notificationDropdown" href="#"
                               data-toggle="dropdown">
                                <i class="mdi mdi-bell-outline"></i>
                                <span class="count-symbol bg-danger"></span>
                            </a>
                            <div
                                class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list"
                                aria-labelledby="notificationDropdown">
                                <h6
                                    class="p-3 mb-0 bg-primary text-white py-4">Thông
                                    báo</h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-success">
                                            <i class="mdi mdi-calendar"></i>
                                        </div>
                                    </div>
                                    <div
                                        class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6
                                            class="preview-subject font-weight-normal mb-1">Sự
                                            kiện hôm nay
                                        </h6>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-warning">
                                            <i class="mdi mdi-settings"></i>
                                        </div>
                                    </div>
                                    <div
                                        class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6
                                            class="preview-subject font-weight-normal mb-1">Cập
                                            nhật </h6>
                                        <p class="text-gray ellipsis mb-0">
                                            Update website </p>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item preview-item">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-info">
                                            <i class="mdi mdi-link-variant"></i>
                                        </div>
                                    </div>
                                    <div
                                        class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                        <h6
                                            class="preview-subject font-weight-normal mb-1">News
                                        </h6>
                                        <p class="text-gray ellipsis mb-0"> News
                                            everyday
                                            ! </p>
                                    </div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <h6 class="p-3 mb-0 text-center">
                                    Tất cả thông báo</h6>
                            </div>
                        </li>
                    </ul>
                    <button
                        class="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
                        type="button" data-toggle="offcanvas">
                        <span class="mdi mdi-menu"></span>
                    </button>
                </div>
            </nav>





            <!-- partial -->
            <div class="container-fluid page-body-wrapper">

                <nav class="sidebar sidebar-offcanvas" id="sidebar">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="homepartner">
                                <span class="icon-bg"><svg viewBox="0 0 25 24"
                                                           fill="none"
                                                           xmlns="http://www.w3.org/2000/svg"><path
                                                           fill-rule="evenodd"
                                                           clip-rule="evenodd"
                                                           d="M7.002 22a3 3 0 0 1-3-3v-6h-1c-.89 0-1.337-1.077-.707-1.707l9-9a1 1 0 0 1 1.414 0l9 9c.63.63.184 1.707-.707 1.707h-1v6a3 3 0 0 1-3 3h-10Zm5-17.586-6.648 6.65a1 1 0 0 1 .649.936v7a1 1 0 0 0 1 1h2v-4a2 2 0 0 1 2-2h2a2 2 0 0 1 2 2v4h2a1 1 0 0 0 1-1v-7a1 1 0 0 1 .649-.937l-6.65-6.649Zm1 11.586h-2v4h2v-4Z"
                                                           fill="currentColor"></path></svg></i></span>
                                <span class="menu-title">Home</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="notificationsPartner">
                                <span class="icon-bg"><i class="fa fa-bell" style="font-size:24px"></i></span>
                                <span class="menu-title"> Notications</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="inboundpart">
                                <span class="icon-bg"><i class="fa fa-clipboard" style="font-size:24px"></i></span> 
                                <span class="menu-title"> Order</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  href="productPartner" >
                                <span class="icon-bg"><svg viewBox="0 0 24 24"
                                                           fill="none"
                                                           xmlns="http://www.w3.org/2000/svg"><path
                                                           d="m21.24 6.62-8.832-4.474-.003-.002a1.38 1.38 0 0 0-1.25.012l-3.789 2.01a.59.59 0 0 0-.088.047l-4.54 2.41A1.373 1.373 0 0 0 2 7.841v8.316c0 .514.283.981.738 1.22l8.413 4.465.003.002a1.377 1.377 0 0 0 1.25.011l8.836-4.477c.47-.235.761-.706.761-1.231V7.852c0-.525-.291-.997-.76-1.231Zm-9.543-3.426a.205.205 0 0 1 .184-.002l8.267 4.189-3.217 1.602-8.083-4.277 2.85-1.512Zm-.5 17.347-7.911-4.2-.004-.001a.204.204 0 0 1-.11-.182V8.371l8.025 4.184v7.986Zm.592-8.998L3.808 7.38l3.79-2.012 8.042 4.256-3.851 1.918Zm9.04 4.605a.204.204 0 0 1-.113.183l-8.348 4.23v-7.998l3.844-1.914v2.007a.586.586 0 0 0 1.171 0v-2.59L20.83 8.35v7.798Z"
                                                           fill="currentColor"
                                                           stroke="currentColor"
                                                           stroke-width="0.5"></path></svg></i></span>
                                <span class="menu-title">Product</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"
                               href="#auds" aria-expanded="false"
                               aria-controls="auds">
                                <span class="icon-bg"><i class="fa fa-group" style="font-size:24px"></i></span>
                                <span class="menu-title">Report</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="auds">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> 
                                        <a class="nav-link" href="partnerinventorys">Inventory Statistics</a>
                                    </li>
                                    <li class="nav-item"> <a class="nav-link" href="parnerorders">
                                            Order Statistics  </a></li>                                   
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item sidebar-user-actions">
                            <div class="user-details">
                                <div
                                    class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <div class="d-flex align-items-center">
                                            <div class="sidebar-profile-text">
                                                <p class="mb-1"> SYSTEM </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"
                               href="#ut" aria-expanded="false"
                               aria-controls="ut">
                                <span class="icon-bg"><i
                                        class="mdi mdi-crosshairs-gps menu-icon"></i></span>
                                <span class="menu-title">Setting Configuration</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ut">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a
                                            class="nav-link"
                                            href>
                                            Genenal Diary</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item sidebar-user-actions">
                            <div class="sidebar-user-menu">
                                <a href="logout" class="nav-link"><i
                                        class="mdi mdi-logout menu-icon"></i>
                                    <span class="menu-title">Log
                                        Out</span></a>
                            </div>
                        </li>
                    </ul>
                </nav>

        <div style="margin-left: 5%; margin-top: 1%; margin-right: 0%; width: 71%">

            <ul class="nav nav-tabs" id="orderTabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#inbound" onclick="showTab('inbound')">Inbound</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#outbound" onclick="showTab('outbound')">Outbound</a>
                </li>
            </ul>
            <%
                                   Integer partnerID = (Integer) session.getAttribute("partnerID");
            %>
            <div class="tab-content">
                <div id="inbound" class="tab-pane active">
                    <div id="container">
                        <div class="search-container">

                            <form action="inboundpart" method="GET"> 
                                <button href="inboundpart" type="button">Create</button>
                                <input type="text" name="keyword" placeholder="Tìm kiếm..." value="${param.keyword}">
                                <button type="submit">Search</button>

                                <c:if test="${not empty searchResultMessage}">
                                    <p style="color: red; display: inline-block; margin-left: 90px; font-size: 19px;margin-top :10px; vertical-align: top;">${searchResultMessage}</p>
                                </c:if>
                            </form> 


                        </div>

                        <div id="createPopup" class="popup">
                            <div class="popup-content">
                                <span class="close-btn" onclick="closePopup()">&times;</span>
                                <h2 style="color: #000">Create Order Inbound</h2>
                                <form id="createOrderForm" action="insertdonpart" method="POST">
                                    <div id="container">
                                        <div>
                                            <label style="color: black" for="partnerID">ID Partner:</label>
                                            <input type="number" id="partnerID" name="partnerID" value="<%= partnerID %>" required readonly>
                                        </div>

                                        <div>
                                            <label style="color: black" for="orderType">Loại Đơn Hàng:</label>
                                            <select id="orderType" name="orderType" required>
                                                <option value="Nhập kho">Inbound</option>
                                            </select>
                                        </div>
                                        <div>
                                            <label style="color: black" for="orderStatus">Trạng Thái Đơn Hàng:</label>
                                            <select id="orderStatus" name="orderStatus" required>
                                                <option value="Chờ phê duyệt">Chờ phê duyệt</option>
                                            </select>
                                        </div>
                                        <h2 style="color: black">Order Detail</h2>
                                        <table class="order-table">
                                            <thead>
                                                <tr style="color:black">
                                                    <th>Product Name</th>
                                                    <th>Description</th>
                                                    <th>Quantity</th>
                                                    <th>UnitPrice</th>
                                                </tr>
                                            </thead>
                                            <tbody id="orderDetails">
                                                <tr>
                                                    <td>
                                                        <input type="text" id="ProductName" name="ProductName" required>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="Description" name="Description" required>
                                                    </td>
                                                    <td>
                                                        <input type="number" id="Quantity" name="Quantity" required min="1" max="100" oninput="validateInput(this)">
                                                    </td>
                                                    <td>
                                                        <input type="number" id="UnitPrice" name="UnitPrice" required min="1" max="1000000000" oninput="validateInput(this)">
                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>
                                        <button type="submit">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <table style="color: black;">
                            <thead>
                                <tr>

                                    <!--<th>ID Partner</th>-->
                                    <!--<th>ID Product</th>-->
                                    <th>Product Name</th>
                                    <th>Description</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Time</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${product}" var="o">
                                    <c:if test="${o.orderType == 'Nhập kho'&& o.partnerID == partnerID}">
                                        <tr>

<!--                                                    <th>${o.partnerID}</th>
                                                    <th>${o.productID}</th>-->
                                            <td>${o.productName}</td>
                                            <td>${o.description}</td>
                                            <td>${o.quantity}</td>
                                            <td>${o.unitPrice}</td>
                                            <td>${o.createdAt}</td>
                                            <td>${o.orderStatus}</td>


                                            <td>
                                                <c:if test="${o.orderStatus eq 'Hoàn thành'}">
                                                    <button class="openExportPopupBtn" type="button" data-partner-id="${o.partnerID}" 
                                                            data-product-id="${o.productID}" data-product-name="${o.productName}"
                                                            data-description="${o.description}" data-quantity="${o.quantity}"
                                                            data-unit-price="${o.unitPrice}" data-zone-id="${o.zoneID}">Export</button>
                                                </c:if>

                                                <c:if test="${o.orderStatus eq 'Hoàn thành'}">
                                                    <button class="openBillPopupBtn" type="button"
                                                            data-product-name="${o.productName}"
                                                            data-quantity="${o.quantity}"
                                                            data-unit-price="${o.unitPrice}"
                                                            data-date="${o.createdAt}">Bill</button>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </tbody>
                        </table>

                        <!--Hóa don nhap kho -->
                        <div id="invoicePopup" class="popup" style="display: none;">
                            <div class="popup-content">
                                <span class="close-btn" onclick="closeInvoicePopup()">&times;</span>
                                <div class="invoice-container">
                                    <div class="invoice-header">
                                        <h1 style="color: black; text-align: center;">Im-Warehouse Invoice</h1>
                                        <h5 style="color: #000; text-align: center;" id="invoiceDate" class="small-text"></h5>
                                        <div class="text-black">

                                            <p>
                                                <b>Seller:</b> <b id="sellerName">${sessionScope.acc.userName}</b>
                                            </p>

                                            <p>
                                                <b>Address Seller:</b>
                                                <input type="text" id="addressInput" oninput="updateDisplayValue('displayAddress', this.value)">
                                                <span id="displayAddress" class="displaySpan"></span>
                                            </p>
                                            <p>
                                                <b>Receiver:</b>
                                                <input type="text" id="buyerNameInput" oninput="updateDisplayValue('displayBuyerName', this.value)">
                                                <span id="displayBuyerName" class="displaySpan"></span>
                                            </p>
                                            <p>
                                                <b>Company Name:</b>
                                                <input type="text" id="companyNameInput" oninput="updateDisplayValue('displayCompanyName', this.value)">
                                                <span id="displayCompanyName" class="displaySpan"></span>
                                            </p>

                                            <p>
                                                <b>Address Receiver:</b>
                                                <input type="text" id="addressInput2" oninput="updateDisplayValue('displayAddress2', this.value)">
                                                <span id="displayAddress2" class="displaySpan"></span>
                                            </p>
                                        </div>
                                        <table style="color: black; width: 100%;"> <tr>
                                                <th style="text-align: left;">Product Name</th>
                                                <th style="text-align: left;">Quantity</th>
                                                <th style="text-align: left;">Unit Price(VNĐ)</th>                                                       
                                                <th style="text-align: left;">Total(VNĐ)</th>
                                                <th style="text-align: left;">Amount in words</th>
                                            </tr>
                                            <tr>
                                                <td id="invoiceProductName"></td>
                                                <td id="invoiceQuantity"></td>
                                                <td id="invoiceUnitPrice"></td>
                                                <td id="invoiceTotalPrice"></td>
                                                <td id="invoiceTotalPriceInWordsOutbound"></td>

                                            </tr>
                                        </table>

                                        <div class="row text-black">
                                            <div class="col-6">
                                                <p><b>Consignee's signature</b></p>
                                            </div>
                                            <div class="col-6 text-right">
                                                <p><b>Salesperson's signature</b></p>
                                            </div>
                                        </div>
                                        <button onclick="printInvoice()">Print Invoice</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script>
                            function updateDisplayValue(elementId, value) {
                                document.getElementById(elementId).innerText = value;
                            }

                            function printInvoice() {
                                // Cập nhật các giá trị từ input sang các span tương ứng
                                updateDisplayValue('displayAddress', document.getElementById('addressInput').value);
                                updateDisplayValue('displayBuyerName', document.getElementById('buyerNameInput').value);
                                updateDisplayValue('displayCompanyName', document.getElementById('companyNameInput').value);
                                updateDisplayValue('displayAddress2', document.getElementById('addressInput2').value);

                                // Ẩn các input và chỉ hiển thị các giá trị span
                                document.querySelectorAll('input').forEach(input => {
                                    input.style.display = 'none';
                                });
                                document.querySelectorAll('.displaySpan').forEach(span => {
                                    span.style.display = 'inline';
                                });

                                var printContent = document.getElementById("invoicePopup").innerHTML;
                                var originalContent = document.body.innerHTML;
                                document.body.innerHTML = printContent;

                                // Thực hiện in
                                window.print();

                                // Đặt lại nội dung ban đầu của trang
                                document.body.innerHTML = originalContent;

                                // Hiển thị lại các input sau khi in
                                document.querySelectorAll('input').forEach(input => {
                                    input.style.display = 'inline';
                                });
                                document.querySelectorAll('.displaySpan').forEach(span => {
                                    span.style.display = 'none';
                                });
                            }
                        </script>

                        <!-- Màn hình popup xác nhận outbound -->
                        <div id="exportPopup" class="popup" style="display: none;">
                            <div class="popup-content" style="width: 30%; margin-left: 606px; border: 20px solid #28a745;">
                                <span class="close-btn" onclick="closeExportPopup()">&times;</span>
                                <h2 style="color: #000">EXPORT PRODUCT</h2>
                                <img src="https://png.pngtree.com/png-clipart/20230925/original/pngtree-free-shipping-green-delivery-van-icon-for-your-business-vector-png-image_12858455.png" alt="Export Product Image" style="width: 87%; margin-bottom: -20px;">
                                <label style="font-size:20px; color: black">Quantity available: <span id="quantityAvailable" style="font-weight: bold;"></span></label><br>
                                <label style="font-size:20px; color: black">The Quantity you want to export: </label>

                                <form id="exportForm" action="exportpart" method="POST" onsubmit="return validateExportQuantity()">
                                    <input type="number" id="quantityExport" name="quantityExport" required>
                                    <input type="hidden" id="partnerIDExport" name="partnerID">
                                    <input type="hidden" id="productIDExport" name="productID">
                                    <input type="hidden" id="orderTypeExport" name="orderType" value="Xuất kho"> 
                                    <input type="hidden" id="orderStatusExport" name="orderStatus" value="Chờ phê duyệt"> 
                                    <input type="hidden" id="productNameExport" name="productName">
                                    <input type="hidden" id="descriptionExport" name="description">
                                    <input type="hidden" id="zoneIDExport" name="zoneID">
                                    <input type="hidden" id="unitPriceExport" name="unitPrice">
                                    <button type="submit" style="margin-left: 140px; margin-top: 20px;">Yes</button>
                                    <button type="button" onclick="closeExportPopup()">No</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>



                <div id="outbound" class="tab-pane" >
                    <div id="container">
                        <form action="inboundpart" method="GET"> 
                            <button href="inboundpart" type="button">Creat</button>
                            <input type="text" name="keyword" placeholder="Tìm kiếm..." value="${param.keyword}">
                            <button type="submit">Search</button>

                            <c:if test="${not empty searchResultMessage}">
                                <p style="color: red; display: inline-block; margin-left: 90px; font-size: 19px;margin-top :10px; vertical-align: top;">${searchResultMessage}</p>
                            </c:if>
                        </form> 


                        <table style="color: black;">
                            <thead>
                                <tr>
                                    <th>ID Partner</th>
                                    <th>ID Product</th>
                                    <th>Product Name</th>
                                    <th>Description</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th> 
                                    <th>Time</th>  
                                    <th>Status</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${product}" var="o">
                                    <c:if test="${o.orderType == 'Xuất kho'}">
                                        <tr>
                                            <th>${o.partnerID}</th>
                                            <th>${o.productID}</th>
                                            <td>${o.productName}</td>
                                            <td>${o.description}</td>
                                            <td>${o.quantity}</td>
                                            <td>${o.unitPrice}</td>
                                            <td>${o.createdAt}</td>
                                            <td>${o.orderStatus}</td>
                                            <td>
                                                <c:if test="${o.orderStatus eq 'Hoàn thành'}">
                                                    <button class="openBillPopupBtnOutbound" type="button"
                                                            data-product-name="${o.productName}"
                                                            data-quantity="${o.quantity}"
                                                            data-unit-price="${o.unitPrice}"
                                                            data-date="${o.createdAt}">Bill</button>
                                                </c:if></td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="invoicePopup" class="popup" style="display: none;">
                <div class="popup-content">
                    <span class="close-btn" onclick="closeInvoicePopup()">&times;</span>
                    <div class="invoice-container">
                        <div class="invoice-header">
                            <h1 style="color: black; text-align: center;">HÓA ĐƠN NHẬP KHO</h1>
                            <h5 style="color: #000; text-align: center;" id="invoiceDate" class="small-text"></h5>
                            <div class="text-black">
                                <p class="text-right">
                                    <b>Ký hiệu (Serial):</b> <input type="text" id="taxSerialInput">
                                    <b>Số (No):</b> <input type="text" id="taxNoInput">
                                </p>
                                <p>
                                    <b>Đơn vị bán hàng (Seller):</b> <b id="sellerName">${sessionScope.acc.userName}</b>
                                </p>
                                <p>
                                    <b>Mã số thuế (Tax code):</b>
                                    <input type="text" id="taxCodeInput"><span id="displayTaxCode" class="displaySpan"></span>
                                </p>
                                <p>
                                    <b>Địa chỉ (Address):</b>
                                    <input type="text" id="addressInput"><span id="displayAddress" class="displaySpan"></span>
                                </p>
                                <p>
                                    <b>Họ tên người mua hàng (Buyer):</b>
                                    <input type="text" id="buyerNameInput"><span id="displayBuyerName" class="displaySpan"></span>
                                </p>
                                <p>
                                    <b>Tên đơn vị (Company name):</b>
                                    <input type="text" id="companyNameInput"><span id="displayCompanyName" class="displaySpan"></span>
                                </p>
                                <p>
                                    <b>Mã số thuế (Tax code):</b>
                                    <input type="text" id="taxCodeInput2"><span id="displayTaxCode2" class="displaySpan"></span>
                                </p>
                                <p>
                                    <b>Địa chỉ (Address):</b>
                                    <input type="text" id="addressInput2"><span id="displayAddress2" class="displaySpan"></span>
                                </p>
                            </div>
                            <table style="color: black; width: 100%;">
                                <tr>
                                    <th style="text-align: left;">Tên Sản Phẩm:</th>
                                    <th style="text-align: left;">Số Lượng:</th>
                                    <th style="text-align: left;">Giá Đơn Vị (VNĐ):</th>
                                    <th style="text-align: left;">Tổng Giá (VNĐ)</th>

                                </tr>
                                <tr>
                                    <td id="invoiceProductName"></td>
                                    <td id="invoiceQuantity"></td>
                                    <td id="invoiceUnitPrice"></td>
                                    <td id="invoiceTotalPrice"></td>

                                </tr>
                            </table>

                            <div class="row text-black">
                                <div class="col-6">
                                    <p><b>Người mua hàng (Buyer)</b></p>

                                </div>
                                <div class="col-6 text-right">
                                    <p><b>Người bán hàng (Seller)</b></p>


                                </div>
                            </div>
                            <button onclick="printInvoice()">Print Invoice</button>
                        </div>
                    </div>
                </div>
            </div>          
            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    const billButtonsOutbound = document.querySelectorAll('.openBillPopupBtnOutbound');
                    billButtonsOutbound.forEach(button => {
                        button.addEventListener('click', function () {
                            const productName = this.getAttribute('data-product-name');
                            const quantity = this.getAttribute('data-quantity');
                            const unitPrice = this.getAttribute('data-unit-price');
                            const date = this.getAttribute('data-date');

                            document.getElementById('invoiceProductNameOutbound').textContent = productName;
                            document.getElementById('invoiceQuantityOutbound').textContent = quantity;
                            document.getElementById('invoiceUnitPriceOutbound').textContent = unitPrice;
                            document.getElementById('invoiceDateOutbound').textContent = date;

                            const totalPrice = quantity * unitPrice;
                            document.getElementById('invoiceTotalPriceOutbound').textContent = totalPrice.toLocaleString('vi-VN', {style: 'currency', currency: 'VND'});
                            const totalPriceInWords = convertNumberToWords(totalPrice);
                            document.getElementById('invoiceTotalPriceInWordsOutbound').textContent = totalPriceInWords;

                            document.getElementById('invoicePopupOutbound').style.display = 'block';
                        });
                    });
                });

                function closeInvoicePopupOutbound() {
                    document.getElementById('invoicePopupOutbound').style.display = 'none';
                }

                function convertNumberToWords(number) {
                    const units = ["không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín"];
                    const teens = ["mười", "mười một", "mười hai", "mười ba", "mười bốn", "mười lăm", "mười sáu", "mười bảy", "mười tám", "mười chín"];
                    const tens = ["", "", "hai mươi", "ba mươi", "bốn mươi", "năm mươi", "sáu mươi", "bảy mươi", "tám mươi", "chín mươi"];
                    const scales = ["", "nghìn", "triệu", "tỷ"];

                    function chunkNumber(number) {
                        let chunked = [];
                        while (number > 0) {
                            chunked.push(number % 1000);
                            number = Math.floor(number / 1000);
                        }
                        return chunked.reverse();
                    }

                    function convertChunk(chunk) {
                        let chunkStr = '';
                        if (chunk >= 100) {
                            chunkStr += units[Math.floor(chunk / 100)] + ' trăm ';
                            chunk %= 100;
                        }
                        if (chunk >= 20) {
                            chunkStr += tens[Math.floor(chunk / 10)] + ' ';
                            chunk %= 10;
                        } else if (chunk >= 10) {
                            chunkStr += teens[chunk - 10] + ' ';
                            chunk = 0;
                        }
                        if (chunk > 0) {
                            chunkStr += units[chunk] + ' ';
                        }
                        return chunkStr.trim();
                    }

                    const chunks = chunkNumber(number);
                    let wordArray = [];
                    for (let i = 0; i < chunks.length; i++) {
                        if (chunks[i] > 0) {
                            wordArray.push(convertChunk(chunks[i]) + ' ' + scales[chunks.length - 1 - i]);
                        }
                    }
                    return wordArray.join(' ').trim() + ' đồng';
                }
            </script>
            <script>
                        document.addEventListener('DOMContentLoaded', function () {
                            const billButtons = document.querySelectorAll('.openBillPopupBtn');
                            billButtons.forEach(button => {
                                button.addEventListener('click', function () {
                                    const productName = this.getAttribute('data-product-name');
                                    const quantity = this.getAttribute('data-quantity');
                                    const unitPrice = this.getAttribute('data-unit-price');
                                    const date = new Date(this.getAttribute('data-date'));

                                    document.getElementById('invoiceProductName').textContent = productName;
                                    document.getElementById('invoiceQuantity').textContent = quantity;
                                    document.getElementById('invoiceUnitPrice').textContent = unitPrice;

                                    const totalPrice = quantity * unitPrice;
                                    document.getElementById('invoiceTotalPrice').textContent = totalPrice;
                                    document.getElementById('invoiceDate').textContent = formatDate(date);
                                    document.getElementById('invoicePopup').style.display = 'block';
                                    const totalPriceInWords = convertNumberToWords(totalPrice);
                                    document.getElementById('invoiceTotalPriceInWordsOutbound').textContent = totalPriceInWords;
                                });
                            });
                        });
                        function closeInvoicePopup() {
                            document.getElementById('invoicePopup').style.display = 'none';
                        }
                        function formatDate(date) {
                            const options = {year: 'numeric', month: '2-digit', day: '2-digit'};
                            return date.toLocaleDateString('en-US', options);
                        }
                        function convertNumberToWords(number) {
                            const units = ["không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín"];
                            const teens = ["mười", "mười một", "mười hai", "mười ba", "mười bốn", "mười lăm", "mười sáu", "mười bảy", "mười tám", "mười chín"];
                            const tens = ["", "", "hai mươi", "ba mươi", "bốn mươi", "năm mươi", "sáu mươi", "bảy mươi", "tám mươi", "chín mươi"];
                            const scales = ["", "nghìn", "triệu", "tỷ"];

                            function chunkNumber(number) {
                                let chunked = [];
                                while (number > 0) {
                                    chunked.push(number % 1000);number = Math.floor(number / 1000);
                                }
                                return chunked.reverse();
                            }

                            function convertChunk(chunk) {
                                let chunkStr = '';
                                if (chunk >= 100) {
                                    chunkStr += units[Math.floor(chunk / 100)] + ' trăm ';
                                    chunk %= 100;
                                }
                                if (chunk >= 20) {
                                    chunkStr += tens[Math.floor(chunk / 10)] + ' ';
                                    chunk %= 10;
                                } else if (chunk >= 10) {
                                    chunkStr += teens[chunk - 10] + ' ';
                                    chunk = 0;
                                }
                                if (chunk > 0) {
                                    chunkStr += units[chunk] + ' ';
                                }
                                return chunkStr.trim();
                            }

                            const chunks = chunkNumber(number);
                            let wordArray = [];
                            for (let i = 0; i < chunks.length; i++) {
                                if (chunks[i] > 0) {
                                    wordArray.push(convertChunk(chunks[i]) + ' ' + scales[chunks.length - 1 - i]);
                                }
                            }
                            return wordArray.join(' ').trim() + ' đồng';
                        }
                    </script>

            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



            <script>
                // Hàm này để ẩn/hiện nội dung của tab tương ứng
                function showTab(tabId) {
                    var tabs = document.querySelectorAll('.tab-pane'); // Chọn các phần tử tab-pane
                    tabs.forEach(function (tab) {
                        tab.classList.remove('active'); // Ẩn tất cả các tab
                        if (tab.id === tabId) {
                            tab.classList.add('active'); // Hiển thị tab được chọn
                        }
                    });
                }

                document.addEventListener('DOMContentLoaded', function () {
                    // Variables for the Create popup
                    var createPopup = document.getElementById("createPopup");
                    var createButton = document.querySelector(".search-container button"); // Create button
                    var closeButton = document.querySelector(".close-btn"); // Close button for the Create popup

                    // Variables for the Export popup
                    const openExportPopupBtns = document.querySelectorAll('.openExportPopupBtn');
                    const exportPopup = document.getElementById('exportPopup');
                    const partnerIDExport = document.getElementById('partnerIDExport');
                    const productIDExport = document.getElementById('productIDExport');
                    const productNameExport = document.getElementById('productNameExport');
                    const descriptionExport = document.getElementById('descriptionExport');
                    const quantityExport = document.getElementById('quantityExport');
                    const unitPriceExport = document.getElementById('unitPriceExport');
                    const orderTypeExport = document.getElementById('orderTypeExport');
                    const orderStatusExport = document.getElementById('orderStatusExport');
                    const zoneIDExport = document.getElementById('zoneIDExport');
                    const quantityAvailable = document.getElementById('quantityAvailable');
                    const closeExportButton = document.querySelector("#exportPopup .close-btn"); // Close button for the Export popup
                    const noButton = document.querySelector("#exportPopup button[type='button']"); // No button for the Export popup

                    openExportPopupBtns.forEach(btn => {
                        btn.addEventListener('click', function () {

                            const partnerID = this.dataset.partnerId;
                            const productID = this.dataset.productId;
                            const productName = this.dataset.productName;
                            const description = this.dataset.description;
                            const quantity = this.dataset.quantity;
                            const unitPrice = this.dataset.unitPrice;
                            const zoneID = this.dataset.zoneId;



                            partnerIDExport.value = partnerID;
                            productIDExport.value = productID;
                            productNameExport.value = productName;
                            descriptionExport.value = description;
                            quantityAvailable.textContent = quantity;
                            unitPriceExport.value = unitPrice;
                            zoneIDExport.value = zoneID;


                            exportPopup.style.display = 'block';
                        });
                    });


                    // Handle Create button click
                    createButton.addEventListener('click', function () {
                        createPopup.style.display = "block";
                    });

                    // Handle close button click for the Create popup
                    closeButton.onclick = function () {
                        createPopup.style.display = "none";
                    }

                    // Handle click outside the Create popup
                    window.onclick = function (event) {
                        if (event.target == createPopup) {
                            createPopup.style.display = "none";
                        } else if (event.target == exportPopup) {
                            exportPopup.style.display = "none";
                        }
                    }




                    // Handle close button click for the Export popup
                    closeExportButton.onclick = closeExportPopup;
                    // Handle "No" button click for the Export popup
                    noButton.onclick = closeExportPopup;

                    // Function to close the Export popup
                    function closeExportPopup() {
                        exportPopup.style.display = 'none';
                    }

                    // Handle form submission for Export
                    const exportForm = document.getElementById('exportForm');
                    exportForm.addEventListener('submit', function (event) {
                        if (!validateExportQuantity()) {
                            event.preventDefault(); // Prevent form submission if validation fails
                        }
                    });

                    // Validate export quantity
                    window.validateExportQuantity = function () {
                        const quantityToExport = parseInt(quantityExport.value);
                        const availableQuantity = parseInt(quantityAvailable.textContent);

                        if (isNaN(quantityToExport) || quantityToExport <= 0) {
                            Swal.fire({
                                icon: 'error',
                                text: 'Please enter a valid quantity to export.',
                                confirmButtonText: 'OK',
                                confirmButtonColor: '#28a745',
                            });
                            return false;
                        }

                        if (quantityToExport > availableQuantity) {
                            Swal.fire({
                                icon: 'error',
                                text: 'The quantity to export cannot be more than the available quantity.',
                                confirmButtonText: 'OK',
                                confirmButtonColor: '#28a745',
                            });
                            return false;
                        }

                        return true;
                    }

                    // Handle search (You need to define performSearch function)
                    function handleSearch(event) {
                        if (event.key === "Enter") {
                            performSearch(); // Implement your performSearch function here
                        }
                    }
                }
                );



            </script>



        </div>



        
    </body>
</html>