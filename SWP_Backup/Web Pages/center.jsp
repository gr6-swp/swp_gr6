
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Home Page</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
                <link rel="shortcut icon" href="assets/images/logo-mini.svg" />

    </head>
    <body>
                <%@ include file="sidebarCenter.jsp" %>

                    <div class=" main-panel">
                        <div class="col-md-12 content-wrapper"
                             style="    padding: 2.875rem 1rem 0.875rem;">
                            <div
                                class="d-xl-flex justify-content-between align-items-start">
                                <h2 class="text-dark font-weight-bold mb-2">
                                    Recent business results </h2>
                                <div
                                    class="d-sm-flex justify-content-xl-between align-items-center mb-2">
                                    <div class="btn-group bg-white p-3"
                                         role="group"
                                         aria-label="Basic example">
                                        <button type="button"
                                                class="btn btn-link text-light py-0 border-right">7
                                            Days</button>
                                        <button type="button"
                                                class="btn btn-link text-dark py-0 border-right">1
                                            Month</button>
                                        <button type="button"
                                                class="btn btn-link text-light py-0">3
                                            Month</button>
                                    </div>
                                    <div
                                        class="dropdown ml-0 ml-md-4 mt-2 mt-lg-0">
                                        <button
                                            class="btn bg-white dropdown-toggle p-3 d-flex align-items-center"
                                            type="button"
                                            id="dropdownMenuButton1"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"> <i
                                                class="mdi mdi-calendar mr-1"></i>

                                        </button>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div
                                        class="d-sm-flex justify-content-between align-items-center transaparent-tab-border {">
                                            <ul class="nav nav-tabs tab-transparent"
                                                role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link"
                                                       id="home-tab"
                                                       data-toggle="tab" href="user"
                                                       role="tab"
                                                       aria-selected="true">Users</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link active"
                                                       id="business-tab"
                                                       data-toggle="tab"
                                                       href="#business-1"
                                                       role="tab"
                                                       aria-selected="false">Business</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link"
                                                       id="performance-tab"
                                                       data-toggle="tab" href="#"
                                                       role="tab"
                                                       aria-selected="false">Performance</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link"
                                                       id="conversion-tab"
                                                       data-toggle="tab" href="#"
                                                       role="tab"
                                                       aria-selected="false">Conversion</a>
                                                </li>
                                            </ul>
                                            <div class="d-md-block d-none">
                                                <a href="#"
                                                   class="text-light p-1"><i
                                                        class="mdi mdi-view-dashboard"></i></a>
                                                <a href="#"
                                                   class="text-light p-1"><i
                                                        class="mdi mdi-dots-vertical"></i></a>
                                            </div>
                                        </div>
                                        <div
                                            class="tab-content tab-transparent-content">
                                            <div class="tab-pane fade show active"
                                                 id="business-1" role="tabpanel"
                                                 aria-labelledby="business-tab">
                                                <div class="row">
                                                    <div
                                                        class="col-xl-3 col-lg-6 col-sm-6 grid-margin stretch-card">
                                                        <div class="card">
                                                            <div
                                                                class="card-body text-center">
                                                                <h5
                                                                    class="mb-2 text-dark font-weight-normal">Revenue</h5>
                                                                <h2
                                                                    class="mb-4 text-dark font-weight-bold">932.00</h2>
                                                                <div
                                                                    class="dashboard-progress dashboard-progress-1 d-flex align-items-center justify-content-center item-parent"><i
                                                                        class=" absolute-center text-dark"><svg
                                                                            viewBox="0 0 24 24"
                                                                            fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="24"
                                                                            height="24"
                                                                            font-size="24"><path
                                                                            d="M9.04 5.717h6.49l1.4-1.144a1.446 1.446 0 0 0 .453-1.607A1.4 1.4 0 0 0 16.05 2H8.52a1.4 1.4 0 0 0-1.333.968 1.445 1.445 0 0 0 .453 1.606l1.4 1.143ZM15.642 7.133H8.93C6.719 9.348 5 13.233 5 16.425 5 19.2 6.439 22 9.655 22h5.465c2.747 0 4.453-2.137 4.453-5.575 0-3.193-1.72-7.077-3.931-9.292Zm-3.663 6.814h.615c.946 0 1.716.785 1.716 1.75 0 .87-.613 1.583-1.417 1.729v.65c0 .343-.271.62-.607.62a.613.613 0 0 1-.607-.62v-.62h-.81a.613.613 0 0 1-.607-.619c0-.342.272-.619.607-.619h1.725c.277 0 .501-.23.501-.512a.51.51 0 0 0-.501-.52h-.616c-.946 0-1.716-.785-1.716-1.751 0-.869.613-1.582 1.417-1.728v-.65c0-.343.272-.62.607-.62.336 0 .607.277.607.62v.619h.81c.336 0 .607.277.607.62 0 .341-.271.619-.607.619H11.98a.508.508 0 0 0-.502.512c0 .29.225.52.502.52Z"
                                                                            fill="currentColor"></path></svg></i></div>
                                                                <p
                                                                    class="mt-4 mb-0">Completed</p>
                                                                <h3
                                                                    class="mb-0 font-weight-bold mt-2 text-dark">5443</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="col-xl-3 col-lg-6 col-sm-6 grid-margin stretch-card">
                                                        <div class="card">
                                                            <div
                                                                class="card-body text-center">
                                                                <h5
                                                                    class="mb-2 text-dark font-weight-normal">Unique
                                                                    Visitors</h5>
                                                                <h2
                                                                    class="mb-4 text-dark font-weight-bold">756,00</h2>
                                                                <div
                                                                    class="dashboard-progress dashboard-progress-2 d-flex align-items-center justify-content-center item-parent"><i
                                                                        class=" absolute-center text-dark"><svg
                                                                            viewBox="0 0 24 24"
                                                                            fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="24"
                                                                            height="24"
                                                                            font-size="24"><path
                                                                            d="m11.574 2.018-4.515.02c-.585 0-1.133.33-1.411.852l-2.36 4.36 7.831-.02.455-5.212ZM21.29 7.212l-2.386-4.36A1.627 1.627 0 0 0 17.483 2l-4.514.019.473 5.212 7.849-.019ZM21.584 10.21l.005-1.537h-.011v-.047l-8.593.029h-1.381L3.01 8.626v.047H3l.005 1.536L3 11.666c.003-.002.007-.002.01-.003l.027 7.841c0 .825.66 1.497 1.459 1.497h.01l7.788-.021 7.788.021h.01c.799 0 1.459-.672 1.459-1.497l.027-7.841a.048.048 0 0 0 .01.003l-.004-1.456Z"
                                                                            fill="currentColor"></path></svg></i></div>
                                                                <p
                                                                    class="mt-4 mb-0">Increased
                                                                    since
                                                                    yesterday</p>
                                                                <h3
                                                                    class="mb-0 font-weight-bold mt-2 text-dark">50%</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="col-xl-3  col-lg-6 col-sm-6 grid-margin stretch-card">
                                                        <div class="card">
                                                            <div
                                                                class="card-body text-center">
                                                                <h5
                                                                    class="mb-2 text-dark font-weight-normal">Impressions</h5>
                                                                <h2
                                                                    class="mb-4 text-dark font-weight-bold">100,38</h2>
                                                                <div
                                                                    class="dashboard-progress dashboard-progress-3 d-flex align-items-center justify-content-center item-parent"><i
                                                                        class="absolute-center text-dark">
                                                                        <svg
                                                                            viewBox="0 0 24 24"
                                                                            fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="24"
                                                                            height="24"
                                                                            font-size="24"><path
                                                                            d="m8.918 2.018-4.168.017c-.54 0-1.046.3-1.303.771L1.269 6.751l7.229-.017.42-4.716ZM17.886 6.717l-2.203-3.945A1.507 1.507 0 0 0 14.37 2l-4.168.017.438 4.717 7.245-.017ZM8.926 15.622c.002-.502.21-.99.568-1.34l3.958-3.82c.35-.339.814-.526 1.303-.526a1.874 1.874 0 0 1 1.874 1.88l-.001.61a8.06 8.06 0 0 1 1.516.476l.008-2.16.01.004-.005-1.318.005-1.39h-.01v-.043l-7.933.026H8.943L1.01 7.996v.042H1l.005 1.39L1 10.747a.046.046 0 0 1 .01-.003l.025 7.096c0 .746.609 1.355 1.346 1.355h.009l7.19-.02 2.178.006-2.267-2.212a1.89 1.89 0 0 1-.565-1.347Z"
                                                                            fill="currentColor"></path><path
                                                                            d="m15.41 13.826.006-2.182a.387.387 0 0 0-.655-.279l-4.388 4.223a.385.385 0 0 0-.001.552l4.364 4.247a.39.39 0 0 0 .42.08c.143-.06.236-.2.236-.354l.007-2.182.73.002a6.571 6.571 0 0 1 5.733 3.389l.011.02a.386.386 0 0 0 .725-.183c.01-3.99-3.202-7.254-7.188-7.333Z"
                                                                            fill="currentColor"></path></svg>
                                                                    </i></div>
                                                                <p
                                                                    class="mt-4 mb-0">Increased
                                                                    since
                                                                    yesterday</p>
                                                                <h3
                                                                    class="mb-0 font-weight-bold mt-2 text-dark">35%</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="col-xl-3 col-lg-6 col-sm-6 grid-margin stretch-card">
                                                        <div class="card">
                                                            <div
                                                                class="card-body text-center">
                                                                <h5
                                                                    class="mb-2 text-dark font-weight-normal">Followers</h5>
                                                                <h2
                                                                    class="mb-4 text-dark font-weight-bold">4250k</h2>
                                                                <div
                                                                    class="dashboard-progress dashboard-progress-4 d-flex align-items-center justify-content-center item-parent"><i
                                                                        class="absolute-center text-dark">
                                                                        <svg
                                                                            viewBox="0 0 24 24"
                                                                            fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="24"
                                                                            height="24"
                                                                            font-size="24"><path
                                                                            d="M9.307 1h-4.32c-.54 0-1.08.332-1.412.748L1.332 5.985h7.56L9.307 1ZM18.696 5.985 16.41 1.872c-.29-.54-.83-.872-1.37-.872h-4.32l.415 4.985h7.56ZM19.028 9.432a8.44 8.44 0 0 0-2.285-.332c-4.195 0-7.685 3.448-7.685 7.685 0 .747.125 1.495.333 2.284H2.412C1.665 19.028 1 18.404 1 17.615V7.355h18.028v2.077Z"
                                                                            fill="currentColor"></path><path
                                                                            d="M20.854 12.548c-2.284-2.285-5.94-2.285-8.307 0-2.285 2.284-2.285 5.94 0 8.307 2.284 2.368 5.94 2.285 8.307 0 2.285-2.367 2.285-6.064 0-8.307Zm-2.783 6.355-1.287-1.288-1.288 1.288c-.208.208-.665.208-.955 0-.208-.208-.208-.665 0-.955l1.287-1.288-1.287-1.288c-.25-.249-.25-.664-.042-.997.208-.207.665-.207.955 0l1.288 1.288 1.288-1.288c.207-.207.664-.207.955 0 .208.208.208.665 0 .956l-1.287 1.287 1.287 1.288c.208.208.208.665 0 .956-.29.249-.706.249-.914.041Z"
                                                                            fill="currentColor"></path></svg></i></div>
                                                                <p
                                                                    class="mt-4 mb-0">Decreased
                                                                    since
                                                                    yesterday</p>
                                                                <h3
                                                                    class="mb-0 font-weight-bold mt-2 text-dark">25%</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="MuiBox-root jss427 jss423">
                                                    <div class="MuiBox-root jss428"
                                                         style="position: relative;">
                                                        <div class="sc-hKpBwk keodMK">
                                                            <div id="scroller"
                                                                 class="sc-ewDcJz loyJZh">
                                                                <div
                                                                    class="sc-djvmMF jQKRyF">
                                                                    <button
                                                                        id="isActiveRevenueSale"
                                                                        class="sc-ehMyHa btWoFz">Revenue </button>
                                                                    <button
                                                                        id="isActiveProportionSale"
                                                                        class="sc-ehMyHa cInVmR">Proportion </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="MuiBox-root jss429"
                                                             style="position: absolute;
                                                             right: 16px;
                                                             top: 14px;">
                                                            <div
                                                                class="MuiBox-root jss430 jss426">
                                                                <div
                                                                    class="MuiBox-root jss535"
                                                                    style="margin-right: 15px;
                                                                    max-width: calc(100% - 124px);">
                                                                    <div width="225px"
                                                                         class="sc-idiyUo hccqMn">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div
                                                        class="col-sm-4 grid-margin stretch-card">
                                                        <div
                                                            class="card card-danger-gradient"
                                                            style="box-shadow: 15px 20px 20px #ff9aab;">
                                                            <div
                                                                class="card-body mb-4"
                                                                style="padding: 1.5rem 0.5rem;
                                                                display: inline-flex;
                                                                position: relative;
                                                                flex-direction: row;">
                                                                <h4
                                                                    class="card-title text-white">TOP
                                                                    PRODUCTS</h4>
                                                                <div width="120px"
                                                                     class="sc-idiyUo gbGLmd"
                                                                     style="padding-left: 100px;">
                                                                    <button
                                                                        type="button"
                                                                        id="Sapo-Button-de74f544-d9ac-45f5-ba37-2f7dfad2e2cd"
                                                                        class="sc-dmRaPn hNatIi">7 days ago<svg
                                                                            viewBox="0 0 24 24"
                                                                            fill="none"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="24"
                                                                            height="24"
                                                                            font-size="24"
                                                                            active="false"
                                                                            variant="filled"
                                                                            class="sc-hHLeRK chxSTE"><path
                                                                            d="m7 9.5 5 5 5-5H7Z"
                                                                            fill="currentColor"></path></svg></button></div>
                                                            </div>
                                                            <div
                                                                class="card-body bg-white pt-4">
                                                                <div
                                                                    class="row pt-4">
                                                                    <div
                                                                        class="col-sm-6">
                                                                        <div
                                                                            class="text-center border-right border-md-0">
                                                                            <h4>Conversion</h4>
                                                                            <h1
                                                                                class="text-dark font-weight-bold mb-md-3">$306</h1>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="col-sm-6">
                                                                        <div
                                                                            class="text-center">
                                                                            <h4>Cancellation</h4>
                                                                            <h1
                                                                                class="text-dark font-weight-bold">$1,520</h1>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="col-sm-4  grid-margin stretch-card">
                                                        <div class="card"
                                                             style="box-shadow: 10px 15px 20px aqua;
                                                             width: 100%;
                                                             min-width: 100%;">
                                                            <div class="card-body">
                                                                <div
                                                                    class="d-xl-flex justify-content-between mb-2">
                                                                    <h3
                                                                        class="card-title"
                                                                        style="font-size: 1rem;">
                                                                        INFORMATION WAREHOUSE</h3>
                                                                    <div
                                                                        width="225px"
                                                                        class="d-xl-flex justify-content-between mb-4"
                                                                        style="padding-left: 30%"><button
                                                                                type="button"
                                                                                id="Sapo-Button-0d459a2e-f423-48be-9a13-ee1f3cad9c19"
                                                                                class="sc-dmRaPn hNatIi"
                                                                                style="display: contents;     ">ALL ZONE<svg
                                                                                viewBox="0 0 24 24"
                                                                                fill="none"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                width="24"
                                                                                height="24"
                                                                                font-size="24"
                                                                                active="false"
                                                                                variant="filled"
                                                                                class="sc-hHLeRK chxSTE"><path
                                                                                d="m7 9.5 5 5 5-5H7Z"
                                                                                fill="currentColor"></path></svg></button></div>
                                                                    <hr>
                                                                    <div
                                                                        class="graph-custom-legend primary-dot"
                                                                        id="pageViewAnalyticLengend"></div>
                                                                </div>
                                                                <canvas
                                                                    id="page-view-analytic"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="col-sm-4  grid-margin stretch-card"> 
                                                        <div class="card"
                                                             style="box-shadow: 10px 15px 20px yellow;
                                                             width: 100%;
                                                             min-width: 100%;">
                                                            <div class="card-body">
                                                                Hello World
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- content-wrapper ends -->
                            <!-- partial:partials/_footer.html -->
                            <footer class="footer">
                                <div class="MuiBox-root jss506 jss502 ">
                                    <div class="MuiBox-root jss507">
                                        <div class="MuiBox-root jss508 jss504">

                                            <div class="MuiBox-root jss511 jss505">
                                                <div class="MuiBox-root jss512" style="height: 1px;
                                                     background: rgb(24, 37, 55);
                                                     z-index: 10;
                                                     margin-top: 13px;
                                                     position: absolute;
                                                     width: 130px;
                                                     margin-left: -20px;"></div>
                                                <!-- <p class="MuiTypography-root MuiTypography-body1" style="font-weight: 500; font-size: 40px; text-align: center;">1</p> -->
                                            </div>
                                        </div>
                                        <h6 class="MuiTypography-root MuiTypography-subtitle1" style="z-index: 12;
                                            margin-left: 5px;
                                            color: rgb(163, 168, 175);"> Have nice days!</h6>
                                        <button id="Sapo-Button-81819337-7d27-490c-8617-adc738a2784b" class="sc-kDDrLX knccNY">
                                            <span class="sc-jqUVSM cMXJOk">Contact: 0123456</span>
                                        </button>
                                    </div>

                                </div>
                            </footer>
                            <!-- partial -->
                        </div>
                        <!-- main-panel ends -->
                    </div>
                    <!-- page-body-wrapper ends -->
                </div>
                
                <!-- End custom js for this page -->
        </body>
    </html>
