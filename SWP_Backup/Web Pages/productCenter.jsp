
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>



<!DOCTYPE html>
<html xmlns:c="http://java.sun.com/jsp/jstl/core">
    <html>

        <style>
            /* CSS để tùy chỉnh giao diện */
            body {
                font-family: Arial, sans-serif;
                background-color: #f5f5f5;
                margin: 0;
                padding: 0;
                color: #333;
            }

            .container {
                width: 80%;
                margin: 20px auto;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                padding: 20px;
                border-radius: 8px;
            }



            form input[type="text"] {
                width: 80%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            form button {
                padding: 10px 20px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            .partner-section {
                margin-bottom: 20px;
            }

            .partner-section input[type="text"] {
                width: 100%;
                padding: 10px;
                margin-bottom: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            .btn {
                padding: 8px 12px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                margin-right: 5px;
            }

            .btn:hover {
                background-color: #45a049;
            }

            .outer-container {
                border: 3px solid #4CAF50; /* Màu xanh */
                padding: 17px;
                margin-top: 1%;
                width: 91%;
                background-color: #f9f9f9; /* Màu nền cho khung chứa toàn bộ nội dung */
                border-radius: 8px; /* Bo tròn các góc của khung */
            }

            /*            .inner-container {
                            padding: 0 10px;  Tạo khoảng cách giữa nội dung và khung ngoài 
                        }*/

            .bordered-section {
                border: 1px solid #000; /* Màu đen */
                padding: 10px;
                margin-bottom: 20px;
                border-radius: 8px;
            }

            .partner-header {
                color: white;
                padding: 10px;
                border-top-left-radius: 8px;
                border-top-right-radius: 8px;
            }
            .search-container {
                margin-bottom: 20px; /* Khoảng cách giữa phần tìm kiếm và phần còn lại */
            }


            .search-container button {

                background-color: #4CAF50;
                color: white;
                border-radius: 7px;
            }

            .modal {
                display: none; /* Ẩn popup ban đầu */
                position: fixed; /* Ở vị trí cố định so với viewport */
                z-index: 1; /* Đảm bảo popup nằm trên các phần tử khác */
                left: 0;
                top: 0;
                width: 70%;
                height: 100%;
                overflow: auto;
                background-color: rgba(0, 0, 0, 0.4); /* Màu nền mờ */
            }

            .modal-content {
                background-color: #fefefe;
                margin: 10% auto; /* Căn giữa popup */
                padding: 17px;
                border: 3px solid #888;
                width: 50%; /* Giới hạn chiều rộng tối đa */
                border-radius: 30px;
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }

            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }

            .popup-content {
                padding: 20px;
            }

            .popup-content h2 {
                color: #000;
            }

            .popup-content label {
                color: #000;
            }

            .popup-content table {
                width: 100%;
                border-collapse: collapse;
            }

            .popup-content th, .popup-content td {
                padding: 8px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }

            .popup-content button {
                background-color: #4CAF50;
                color: white;
                padding: 10px 20px;
                border: none;
                cursor: pointer;
            }

            .popup-content button:hover {
                background-color: #45a049;
            }

            #container > div {
                margin-bottom: 15px;
            }

            #container label {
                display: block;
                margin-bottom: 5px;
            }

            #container select,
            #container input[type="number"] {
                padding: 2px;
                margin-bottom: -10px;
                border: 2px solid #ccc;
                border-radius: 4px;
            }

            #container table th {
                background-color: #4CAF50;
                color: white;
            }

            #container table input {
                width: 100%;
                padding: 5px;
                box-sizing: border-box;
            }

            .container {
                display: grid;
                grid-template-columns: 1fr 1fr;
                gap: 10px;
            }

            .container > div {
                display: flex; /* Sử dụng flexbox để căn chỉnh label và input */
                align-items: center;
            }

            .container label {
                width: 120px;
                margin-right: 10px; /* Khoảng cách giữa label và input */
            }

        </style>
        <head>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport"
                  content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <title>Products</title>
            <!-- plugins:css -->
            <link rel="stylesheet"
                  href="assets/vendors/mdi/css/materialdesignicons.min.css">
            <link rel="stylesheet"
                  href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
            <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
            <!-- endinject -->
            <!-- Plugin css for this page -->
            <link rel="stylesheet"
                  href="assets/vendors/font-awesome/css/font-awesome.min.css" />
            <link rel="stylesheet"
                  href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
            <!-- End plugin css for this page -->
            <!-- inject:css -->
            <!-- endinject -->
            <!-- Layout styles -->
            <link rel="stylesheet" href="assets/css/style.css">
            <!-- End layout styles -->
                    <link rel="shortcut icon" href="assets/images/logo-mini.svg" />

        </head>
        <body>
            <div class="container-scroller" style="width: 102%;">
                <%@include file="sidebarCenter.jsp" %>

                <div style="margin-left: 3%;
                     margin-top: 0%;
                     margin-right: 0%;
                     width: 75%">
                    <div id="container">

                        <div class="container-scroller" style="width: 110%">
                            <div class="outer-container bg-light-50">
                                <div class="search-container justify-content-between" style="display: flex; align-items: center;">
                                    <h2 style="color: #007bff; margin-right: 50%;">List Product</h2>
                                    <form action="${pageContext.request.contextPath}/productCenter" method="get" class="d-flex">
                                        <input type="text" id="searchInput" name="searchTerm" class="form-control mt-1" placeholder="Search..." style="max-width: 210px; height: 30px; border-radius: 40px; margin-bottom: 0px">
                                    </form>
                                </div>
                                <div class="inner-container">
                                    <div class="bordered-section">
                                        <div class="partner-section">
                                            <div class="partner-header d-flex bg-light" style="height: 50px">
                                                <div class="col-md-6">
                                                    <span class="text-black-50 text-small pl-2">Showing ${proCenter.size()} Products</span>
                                                </div>
                                                <div class="d-flex col-md-6 justify-content-end">
                                                    <form class="form-inline justify-content-md-end">
                                                        <div class="form-group mr-3">
                                                            <label for="sortSelect" class="text-black pt-2">Sort by:</label>
                                                            <select class="form-control" id="sortSelect" style="height: 30px; margin: 5px">
                                                                <option value="priceLowToHigh">Price: Low to High</option>
                                                                <option value="priceHighToLow">Price: High to Low</option>
                                                            </select>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table id="productTable" class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>PartnerID</th>
                                                            <th>Name</th>
                                                            <th>Description</th>
                                                            <th>Quantity</th>
                                                            <th>Unit Price</th>
                                                            <th>Date</th>
                                                            <th>ZoneID</th>
                                                            <th>Type</th>
                                                            <th>Status</th>
                                                            <!-- <th>Action</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-black" id="productTbody">
                                                        <c:forEach items="${proCenter}" var="p">
                                                            <tr>
                                                                <td>${p.productID}</td>
                                                                <td>${p.partnerID}</td>
                                                                <td>${p.productName}</td>
                                                                <td>${p.description}</td>
                                                                <td>${p.quantity}</td>
                                                                <td>${p.unitPrice}</td>
                                                                <td>${p.createdAt}</td>
                                                                <td>${p.zoneID}</td>
                                                                <td>${p.orderType}</td>
                                                                <td>${p.orderStatus}</td>
<!--                                                                <td>
                                                                    <c:choose>
                                                                        <c:when test="${p.orderType == 'Xuất kho'}">
                                                                            <button class="btn btn-danger delete-btn" data-id="${p.productID}">
                                                                                <i class="fa fa-trash"></i>
                                                                            </button>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <button class="btn btn-primary edit-btn" data-id="${p.productID}" data-name="${p.productName}" data-description="${p.description}" data-quantity="${p.quantity}" data-price="${p.unitPrice}">
                                                                                <i class="fa fa-pencil"></i>
                                                                            </button>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </td>-->
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Edit Modal -->
                    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-black" id="editModalLabel">Edit Product</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form id="editForm">
                                        <div class="form-group text-black">
                                            <label for="editProductName">Name</label>
                                            <input type="text" id="editProductName" name="name" class="form-control" placeholder="Product Name" required>
                                            <small id="editProductNameError" class="form-text text-danger" style="display: none;">Name cannot contain special characters</small>
                                        </div>
                                        <div class="form-group text-black">
                                            <label for="editDescription">Description</label>
                                            <textarea id="editDescription" name="description" class="form-control" placeholder="Description" required></textarea>
                                            <small id="editDescriptionError" class="form-text text-danger" style="display: none;">Description cannot contain special characters</small>
                                        </div>
                                        <div class="form-group text-black">
                                            <label for="editQuantity">Quantity</label>
                                            <input type="number" id="editQuantity" name="quantity" class="form-control" required>
                                        </div>
                                        <div class="form-group text-black">
                                            <label for="editPrice">Unit Price</label>
                                            <input type="number" id="editPrice" name="price" class="form-control" step="0.01" required>
                                        </div>
                                        <input type="hidden" id="editProductId" name="productID">
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
                    <script>
                        $(document).ready(function () {
                            const editForm = $('#editForm');
                            const productTbody = $('#productTbody');

                            // Function to show edit modal and populate fields
                            $(document).on('click', '.edit-btn', function () {
                                const productID = $(this).data('id');
                                const productName = $(this).data('name');
                                const productDescription = $(this).data('description');
                                const productQuantity = $(this).data('quantity');
                                const productPrice = $(this).data('price');

                                $('#editModal').modal('show');
                                $('#editProductId').val(productID);
                                $('#editProductName').val(productName);
                                $('#editDescription').val(productDescription);
                                $('#editQuantity').val(productQuantity);
                                $('#editPrice').val(productPrice);
                            });

                            // Edit form submission
                            editForm.on('submit', function (event) {
                                event.preventDefault();

                                const formData = {
                                    productID: $('#editProductId').val(),
                                    name: $('#editProductName').val(),
                                    description: $('#editDescription').val(),
                                    quantity: $('#editQuantity').val(),
                                    price: $('#editPrice').val()
                                };

                                // AJAX request to update product
                                $.ajax({
                                    url: '/productCenter', // Adjust URL as needed
                                    type: 'POST',
                                    data: formData,
                                    success: function (response) {
                                        if (response.success) {
                                            $('#editModal').modal('hide'); // Hide modal on success
                                            // Update the row with new data
                                            const row = productTbody.find('tr').filter(function () {
                                                return $(this).find('td:eq(0)').text() === formData.productID.toString();
                                            });
                                            row.find('td:eq(2)').text(formData.name);
                                            row.find('td:eq(3)').text(formData.description);
                                            row.find('td:eq(4)').text(formData.quantity);
                                            row.find('td:eq(5)').text(formData.price);
                                            alert('Product updated successfully.');
                                        } else {
                                            alert('Failed to edit product: ' + response.message); // Handle error
                                        }
                                    },
                                    error: function (xhr, status, error) {
                                        alert('Failed to edit product: ' + error); // Handle error
                                    }
                                });
                            });

                            // Delete button functionality
                            $(document).on('click', '.delete-btn', function () {
                                if (confirm('Are you sure you want to delete this product?')) {
                                    const productID = $(this).data('id');
                                    const row = $(this).closest('tr');

                                    // AJAX request to delete product
                                    $.ajax({
                                        url: '/productCenter', // Adjust URL as needed
                                        type: 'POST',
                                        data: {productID: productID},
                                        success: function (response) {
                                            if (response.success) {
                                                row.remove(); // Remove row from table
                                                alert('Product deleted successfully.');
                                            } else {
                                                alert('Failed to delete product: ' + response.message); // Handle error
                                            }
                                        },
                                        error: function (xhr, status, error) {
                                            alert('Failed to delete product: ' + error); // Handle error
                                        }
                                    });
                                }
                            });

                            // Search and sort functionality
                            $('#searchInput').on('keyup', function () {
                                const searchTerm = $(this).val().toLowerCase();
                                productTbody.find('tr').each(function () {
                                    const productName = $(this).find('td:eq(2)').text().toLowerCase();
                                    $(this).toggle(productName.includes(searchTerm));
                                });
                            });

                            $('#sortSelect').on('change', function () {
                                const sortOption = $(this).val();
                                const rows = productTbody.find('tr').get();
                                rows.sort(function (a, b) {
                                    switch (sortOption) {
                                        case 'priceLowToHigh':
                                            return parseFloat($(a).find('td:eq(5)').text()) - parseFloat($(b).find('td:eq(5)').text());
                                        case 'priceHighToLow':
                                            return parseFloat($(b).find('td:eq(5)').text()) - parseFloat($(a).find('td:eq(5)').text());
                                        case 'partnerID':
                                            return $(a).find('td:eq(1)').text().localeCompare($(b).find('td:eq(1)').text());
                                        case 'zone':
                                            return $(a).find('td:eq(7)').text().localeCompare($(b).find('td:eq(7)').text());
                                        default:
                                            return 0;
                                    }
                                });
                                $.each(rows, function (index, row) {
                                    productTbody.append(row);
                                });
                            });
                        });
                    </script>
                    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
                    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
                    <script src="assets/vendors/js/vendor.bundle.base.js"></script>
                    <!-- endinject -->
                    <!-- Plugin js for this page -->
                    <script src="assets/vendors/chart.js/Chart.min.js"></script>
                    <script
                    src="assets/vendors/jquery-circle-progress/js/circle-progress.min.js"></script>
                    <!-- End plugin js for this page -->
                    <!-- inject:js -->
                    <script src="assets/js/off-canvas.js"></script>
                    <script src="assets/js/hoverable-collapse.js"></script>
                    <script src="assets/js/misc.js"></script>
                    <!-- endinject -->
                    <!-- Custom js for this page -->
                    <script src="assets/js/dashboard.js"></script>
                    <!-- End custom js for this page -->
                    </body>
                    </html>
