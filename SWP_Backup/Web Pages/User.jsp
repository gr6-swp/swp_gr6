<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>User Management</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            .status-message {
                color: red;
                font-weight: bold;
            }
        </style>
                <link rel="shortcut icon" href="assets/images/logo-mini.svg" />

    </head>
    <body>
        <%@ include file="sidebarCenter.jsp" %>

        <div class="container">
            <h2>User Management</h2>
            <% String warningUsername = (String) request.getAttribute("warningUsername"); %>
            <% if (warningUsername != null) { %>
            <p class="status-message error"><%= warningUsername %></p>
            <% } %>

            <% String warningEmail = (String) request.getAttribute("warningEmail"); %>
            <% if (warningEmail != null) { %>
            <p class="status-message error"><%= warningEmail %></p>
            <% } %>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="user" items="${users}">
                        <c:if test="${user.getIsCenter() != 1 && user.getIsCenter() != null}">
                            <tr>
                                <td>${user.getUserID()}</td>
                                <td>${user.getUserName()}</td>
                                <td>${user.getEmail()}</td>
                                <td>${user.getPhoneNumber()}</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${user.getIsPartner() == 1}">
                                            Partner
                                        </c:when>
                                        <c:when test="${user.getIsPartner() == 0 && user.getIsCenter() == 0}">
                                            Keeper
                                        </c:when>
                                    </c:choose>
                                </td>
                                <td>
                                    <a href="user?action=toggleUserStatus&id=${user.userID}&status=${!user.isActive}"
                                       class="btn ${user.isActive ? 'btn-danger' : 'btn-success'} btn-sm">
                                        ${user.isActive ? 'Deactivate' : 'Activate'}
                                    </a>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUserModal">Add User</button>
        </div>

        <!-- Add User Modal -->
        <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="user" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addUserModalLabel">Add User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username" name="username" pattern="[a-zA-Z0-9]{4,}" title="Username must contain at least 4 characters" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" pattern=".{6,}" title="Password must contain at least 6 characters" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" pattern="^(?=.*[a-zA-Z0-9]){4,}@gmail\\.com" title="Please enter the right form of email" required>
                            </div>
                            <div class="form-group">
                                <label for="phoneNumber">Phone Number</label>
                                <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" pattern="0[0-9]{9}" title="Please enter a 10-digit phone number">
                            </div>
                            <div class="form-group">
                                <label for="role">Role</label>
                                <select class="form-control" id="role" name="role" onchange="showHidePartnerFields()">
                                    <option value="Keeper">Keeper</option>
                                    <option value="Partner">Partner</option>
                                </select>
                            </div>
                            <!-- Các trường partner name, contact name và address -->
                            <div class="form-group" id="partnerFields" style="display: none;">
                                <label for="partnerName">Partner Name</label>
                                <input type="text" class="form-control" id="partnerName" name="partnerName">
                            </div>
                            <div class="form-group" id="contactNameFields" style="display: none;">
                                <label for="contactName">Contact Name</label>
                                <input type="text" class="form-control" id="contactName" name="contactName">
                            </div>
                            <div class="form-group" id="addressFields" style="display: none;">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" name="address">
                            </div>
                            <!-- Kết thúc các trường partner -->
                        </div>
                        <script>
                            function showHidePartnerFields() {
                                var roleSelect = document.getElementById("role");
                                var partnerFields = document.getElementById("partnerFields");
                                var contactNameFields = document.getElementById("contactNameFields");
                                var addressFields = document.getElementById("addressFields");

                                if (roleSelect.value === "Partner") {
                                    partnerFields.style.display = "block";
                                    contactNameFields.style.display = "block";
                                    addressFields.style.display = "block";
                                } else {
                                    partnerFields.style.display = "none";
                                    contactNameFields.style.display = "none";
                                    addressFields.style.display = "none";
                                }
                            }
                        </script>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
</html>
