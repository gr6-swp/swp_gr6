<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Account</title>
        <link rel="stylesheet" type="text/css" href="css/profile.css">
        <script>
            function togglePasswordSection() {
                var passwordSection = document.getElementById("password-section");
                passwordSection.style.display = passwordSection.style.display === "none" ? "block" : "none";
            }

            function toggleEditSection(sectionId) {
                var editSection = document.getElementById(sectionId);
                editSection.style.display = editSection.style.display === "none" ? "block" : "none";
            }

            function validatePasswordForm() {
                var newPassword = document.getElementById("newPassword").value;
                var reenterPassword = document.getElementById("reenterPassword").value;

                if (newPassword !== reenterPassword) {
                    alert("Mật khẩu mới và xác nhận mật khẩu không trùng nhau!");
                    return false;
                }
                return true;
            }
        </script>
                <link rel="shortcut icon" href="assets/images/logo-mini.svg" />

    </head>
    <body>
        <c:choose>
            <c:when test="${acc.isPartner == 1}">
                <%@ include file="sidebarPartner.jsp" %>
            </c:when>
            <c:when test="${acc.isPartner == 0 && acc.isCenter == 1}">
                <%@ include file="sidebarCenter.jsp" %>
            </c:when>
            <c:otherwise>
                <%@ include file="sidebarKeeper.jsp" %>
            </c:otherwise>
        </c:choose>
        <div class="container">
            <h1 class="success-text">Thông tin tài khoản</h1>
            <div class="profile-info">
                <c:choose>
                    <c:when test="${not empty acc}">
                        <p>
                            <strong>Tên đăng nhập:</strong> ${acc.userName}
                        </p>
                        <p>
                            <strong>Email:</strong> ${acc.email}
                            <button onclick="toggleEditSection('email-section')" class="edit-btn">Change email</button>
                        </p>
                        <div id="email-section" class="edit-section">
                            <form action="changeEmail" method="post">
                                <p>
                                    <label for="newEmail">Enter New Email:</label>
                                    <input type="email" id="newEmail" name="newEmail" pattern="^(?=.*[a-zA-Z0-9]){4,}@gmail\\.com" title="Please enter the right form of email" required>
                                </p>
                                <button type="submit" class="success-btn">Submit</button>
                            </form>
                        </div>
                        <c:if test="${not empty errorMessage}">
                            <div class="error-message">${errorMessage}</div>
                        </c:if>

                        <c:if test="${not empty param.message}">
                            <div class="success-message">${param.message}</div>
                        </c:if>
                        <p>
                            <strong>Số điện thoại:</strong> ${acc.phoneNumber}
                            <button onclick="toggleEditSection('phone-section')" class="edit-btn">Change phone number</button>
                        </p>
                        <div id="phone-section" class="edit-section">
                            <form action="changePhone" method="post">
                                <p>
                                    <label for="newPhoneNumber">Enter New Phone Number:</label>
                                    <input type="tel" id="newPhoneNumber" name="newPhoneNumber" pattern="^\d{10}$" title="Phone number must be 10 digits." required>
                                </p>
                                <button type="submit" class="success-btn">Submit</button>
                            </form>
                        </div>
                        <button onclick="togglePasswordSection()" class="edit-btn">Change Password</button>
                        <div id="password-section" class="edit-section">
                            <form action="changePassword" method="post" onsubmit="return validatePasswordForm()">
                                <p>
                                    <label for="newPassword">Enter New Password:</label>
                                    <input type="password" id="newPassword" name="newPassword" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9]{6,}" title="Password must contain at least 1 lowercase letter, 1 uppercase letter, 1 number and 6 characters." required>
                                </p>
                                <p>
                                    <label for="reenterPassword">Re-enter New Password:</label>
                                    <input type="password" id="reenterPassword" name="reenterPassword" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9]{6,}" title="Re-Password must contain at least 1 lowercase letter, 1 uppercase letter, 1 number and 6 characters." required>
                                </p>
                                <button type="submit" class="success-btn">Change password</button>
                            </form>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <p>Không có thông tin người dùng.</p>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </body>
</html>
