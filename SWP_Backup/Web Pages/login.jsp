<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css">
        <title>Login </title>
        <style>
            .btn-social {
                display: block;
                width: 100%;
                padding: 10px;
                margin: 10px 0;
                font-size: 16px;
                text-align: center;
                border: none;
                cursor: pointer;
                color: white;
                text-decoration: none;
            }
            .btn-gmail {
                background-color: #db4437;
                color: white;
            }
            a {
                text-decoration: none;
            }
            .no-style-link {
                color: inherit;
                text-decoration: none;
                cursor: pointer;
            }
            .status-message {
                padding: 10px;
                margin: 10px 0;
                border-radius: 5px;
                font-size: 16px;
                text-align: center;
            }
            .success {
                background-color: #d4edda;
                color: #155724;
            }
            .error {
                background-color: #f8d7da;
                color: #721c24;
            }
            .mismatch {
                background-color: #fff3cd;
                color: #856404;
            }
        </style>
                <link rel="shortcut icon" href="assets/images/logo-mini.svg" />

    </head>
    <body>
        <div id="container" class="container">
            <!-- FORM SECTION -->
            <div class="row">
                <!-- SIGN UP -->
                <div class="col align-items-center flex-col sign-up">
                    <div class="form-wrapper align-items-center">
                        <div class="form sign-up">
                            <form action="signup" method="post" onsubmit="return validatePasswords()">
                                <!-- Username -->
                                <div class="input-group">
                                    <i class='bx bxs-user'></i>
                                    <input name="user" type="text" placeholder="Username" pattern="[a-zA-Z0-9]{4,}" title="Username must contain at least 4 characters" required>
                                </div>
                                <!-- Partner's name -->
                                <div class="input-group">
                                    <i class='bx bxs-partnerName'></i>
                                    <input name="partnerName" type="text" placeholder="Name" pattern="[a-zA-Z\s]{4,}" title="Name must contain at least 4 characters" required>
                                </div>
                                <!-- Contact name -->
                                <div class="input-group">
                                    <i class='bx bxs-contactName'></i>
                                    <input name="contactName" type="text" placeholder="Contact name" pattern="[a-zA-Z\s]{4,}" title="Contact Name must contain at least 4 characters" required>
                                </div>
                                <!-- Address -->
                                <div class="input-group">
                                    <i class='bx bxs-address'></i>
                                    <input name="address" type="text" placeholder="Address" pattern="[a-zA-Z0-9\s]{4,}" title="Address must contain at least 4 characters" required>
                                </div>
                                <!-- Email -->
                                <div class="input-group">
                                    <i class='bx bx-mail-send'></i>
                                    <input name="email" type="email" placeholder="Email" pattern="^(?=.*[a-zA-Z0-9]){4,}@gmail\\.com" title="Please enter the right form of email" required>
                                </div>
                                <!-- Phone Number -->
                                <div class="input-group">
                                    <i class='bx bxs-phone'></i>
                                    <input name="phone" type="tel" placeholder="Phone Number" pattern="0[0-9]{9}" title="Please enter a 10-digit phone number" required>
                                </div>
                                <!-- Password -->
                                <div class="input-group">
                                    <i class='bx bxs-lock-alt'></i>
                                    <input id="pass" name="pass" type="password" placeholder="Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9]{6,}" title="Password must contain 
                                           at least 1 lowercase letter, 1 uppercase letter, 1 number and 6 characters." required>
                                </div>
                                <!-- Confirm password -->
                                <div class="input-group">
                                    <i class='bx bxs-lock-alt'></i>
                                    <input id="Cpass" name="Cpass" type="password" placeholder="Confirm password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9]{6,}" title="Confirm Password must contain 
                                           at least 1 lowercase letter, 1 uppercase letter, 1 number and 6 characters." required>
                                </div>
                                <button>
                                    Sign up
                                </button>
                                <p>
                                    <span>
                                        Already have an account?
                                    </span>
                                    <b onclick="toggle()" class="pointer">
                                        Sign in here
                                    </b>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END SIGN UP -->

                <script>
                    function validatePasswords() {
                        var password = document.getElementById("pass").value;
                        var confirmPassword = document.getElementById("Cpass").value;

                        if (password !== confirmPassword) {
                            alert("Password and Confirm Password do not match");
                            return false;
                        }

                        return true;
                    }
                </script>

                <!-- SIGN IN -->
                <div class="col align-items-center flex-col sign-in">
                    <div class="form-wrapper align-items-center">
                        <div class="form sign-in">
                            <% String warning = (String) request.getAttribute("warning"); %>
                            <% if (warning != null) { %>
                            <p class="status-message error"><%= warning %></p>
                            <% } %>
                            <% String warningLogin = (String) request.getAttribute("warningLogin"); %>
                            <% if (warningLogin != null) { %>
                            <p class="status-message error"><%= warningLogin %></p>
                            <% } %>
                            <% String warningEmail = (String) request.getAttribute("warningEmail"); %>
                            <% if (warningEmail != null) { %>
                            <p class="status-message error"><%= warningEmail %></p>
                            <% } %>
                            <form action="login" method="post">
                                <div class="input-group">
                                    <i class='bx bxs-user'></i>
                                    <input name="username" type="text" placeholder="Username" pattern="[a-zA-Z0-9]{4,}" title="Username must contain at least 4 characters" required>
                                </div>
                                <div class="input-group">
                                    <i class='bx bxs-lock-alt'></i>
                                    <input name="pass" type="password" placeholder="Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9]{6,}" title="Password must contain 
                                           at least 1 lowercase letter, 1 uppercase letter, 1 number and 6 characters." required>
                                </div>
                                <button>Sign in</button>
                                <!-- Social login buttons -->
                                <div class="social-login">
                                    <a href="https://accounts.google.com/o/oauth2/auth?scope=email profile openid&redirect_uri=http://localhost:8080/Login/logingoogle&response_type=code
                                       &client_id=1076933820260-849b32vqug0gtgrpuvhbi5n66i06n7oc.apps.googleusercontent.com&approval_prompt=force">
                                        <button type="button" class="btn-social btn-gmail">
                                            <i class='bx bxl-google'></i> Sign in with Gmail
                                        </button>
                                    </a>
                                </div>
                                <p>
                                    <a href="forgotPassword.jsp" class="no-style-link">
                                        Forgot password?
                                    </a>
                                </p>
                                <p>
                                    <span>
                                        Don't have an account?
                                    </span>
                                    <b onclick="toggle()" class="pointer">
                                        Sign up here
                                    </b>
                                </p>
                            </form>
                        </div>
                    </div>
                    <!-- END SIGN IN -->
                </div>
                <!-- END FORM SECTION -->
                <!-- CONTENT SECTION -->
                <div class="row content-row">
                    <!-- SIGN IN CONTENT -->
                    <div class="col align-items-center flex-col">
                        <div class="text sign-in">
                            <h2>
                                Welcome
                            </h2>

                        </div>
                        <div class="img sign-in">

                        </div>
                    </div>
                    <!-- END SIGN IN CONTENT -->
                    <!-- SIGN UP CONTENT -->
                    <div class="col align-items-center flex-col">
                        <div class="img sign-up">

                        </div>
                        <div class="text sign-up">
                            <h2>
                                Join with us
                            </h2>

                        </div>
                    </div>
                    <!-- END SIGN UP CONTENT -->
                </div>
                <!-- END CONTENT SECTION -->
            </div>
            <% if (request.getAttribute("status") != null) { %>
            <div>
                <% if (request.getAttribute("status").equals("resetSuccess")) { %>
                <p>Password reset successfully.</p>
                <% } else if (request.getAttribute("status").equals("resetFailed")) { %>
                <p>Password reset failed. Please try again.</p>
                <% } else if (request.getAttribute("status").equals("passwordMismatch")) { %>
                <p>Passwords do not match. Please try again.</p>
                <% } %>
            </div>
            <% } %>
            <script src="js/style.js"></script>
    </body>
</html>
