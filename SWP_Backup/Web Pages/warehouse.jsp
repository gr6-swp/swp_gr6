
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title> Warehouses</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
                <link rel="shortcut icon" href="assets/images/logo-mini.svg" />

        <style>
            body {
                font-family: Arial, sans-serif;
            }
            h2 {
                margin-bottom: 10px;
            }

            /* Kiểu dáng cho nút */
            button {
                background-color: #4CAF50; /* Màu xanh lá cây */
                color: white;
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Kiểu dáng cho ô tìm kiếm và select */
            input[type="text"], select {
                padding: 8px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            button {
                /* ... (các kiểu dáng khác của nút) */
                margin-left: 5px; /* Tạo khoảng cách giữa nút và ô tìm kiếm */
            }
            #container {
                border: 2px solid #4aad03eb; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px; /* Bo góc (tùy chọn) */
            }



            body {
                font-family: sans-serif;
            }

            .card-container {
                display: flex;
                flex-wrap: wrap;
                gap: 20px;
            }
            .card {
                flex: 1 0 300px; /* Adjust width as needed */
                min-width: 200px;
                height:200px;
                max-width: 200px;
                box-shadow: 1px 1px 20px #938787;
                border-radius: 15%
            }

            .add-card {
                display: flex;
                align-items: center;
                justify-content: center;
                border: 2px dashed #007bff;
                height: 150px;
                min-width: 150px;
                max-width: 150px;
                box-shadow: 1px 1px 20px #938787;
                cursor: pointer;
                transition: all 0.3s ease-in-out;
            }
            .add-card:hover {
                border-color: #0056b3;
            }
            .add-card i {
                font-size: 2rem;
            }
            .modal input {
                margin-bottom: 10px;
            }

        </style>
    </head>
    <body>
        <div class="container-scroller">
            <%@ include file="sidebarCenter.jsp" %>


            <div class="container-fluid page-body-wrapper">
                <div style="margin-left: 2%; width: 95%" >
                    <div id="container">
                        <div class="container mt-1">
                            <div class="d-flex justify-content-between" style="height: 50px">
                                <h2 class="text-success mt-2">Warehouse Center</h2>
                                <div class="form-group mt-2">
                                    <div class="search-container" style="text-align: end">
                                        <input type="text" id="searchInput" placeholder="Tìm kiếm..." style="border: 2px solid #4aad03eb; border-radius: 20px;">
                                    </div>
                                </div>
                            </div>
                            <div class="container mt-2">
                                <h3 class="text-black">Warehouse Zones</h3>
                                <h6 style="font-style: italic;" class="text-black-50">
                                    Recently Updated (Để xem chi tiết về các thay đổi cập nhật gần đây, vui lòng nhấp vào đây)
                                </h6>
                                <div class="card-container">
                                    <c:forEach items="${zones}" var="z">
                                        <div class="card bg-gradient-light" style="border-radius: 15%;">
                                            <div class="card-body" style="padding: 1.5rem;">
                                                <h5 class="card-title" style="margin-bottom: 1rem;">Zone: ${z.zoneName}</h5>
                                                <p class="card-text text-black-50">
                                                    Keeper ID: ${z.keeperID} <br>
                                                    <c:set var="totalProducts" value="0"/>
                                                    <c:forEach items="${z.products}" var="p">
                                                        <c:set var="totalProducts" value="${totalProducts + p.quantity}"/>
                                                    </c:forEach>
                                                    <c:set var="totalInventories" value="0"/>
                                                    <c:forEach items="${z.inventories}" var="inventory">
                                                        <c:set var="totalInventories" value="${totalInventories + inventory.quantity}"/>
                                                    </c:forEach>
                                                    <c:set var="total" value="${totalProducts + totalInventories}"/>
                                                    Total: ${total}
                                                </p>
                                                <div class="d-flex pl-3">
                                                    <button class="btn btn-success btn-sm detail-zone" 
                                                            data-id="${z.zoneID}" 
                                                            data-title="${z.zoneName}" 
                                                            data-keeper="${z.keeperID}" 
                                                            data-total="${total}" 
                                                            data-inventory="${z.inventories}"
                                                            data-products="${z.products}">
                                                        <i class="fa fa-eye"></i>
                                                    </button>
                                                    <c:if test="${empty z.products && empty z.inventories}">
                                                        <button class="btn btn-danger btn-sm remove-card" data-id="${z.zoneID}">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </c:if>
                                                    <button class="btn btn-primary btn-sm edit-zone" data-id="${z.zoneID}" data-title="${z.zoneName}" 
                                                            data-keeper="${z.keeperID}" data-total="${total}">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    <div class="add-card mt-4" id="addCardButton">
                                        <i class="fa fa-plus text-primary"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal for adding new zone -->
                            <div class="modal fade" id="addCardModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <h5 class="modal-title text-black" id="exampleModalLabel">Add New Zone</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="addCardForm" method="post" action="warehouse">
                                                <input type="hidden" name="action" value="addZone">
                                                <div class="form-group">
                                                    <label for="zoneTitle">Zone Name</label>
                                                    <input type="text" id="zoneTitle" name="name" class="form-control" placeholder="Zone Title" required>
                                                    <small id="zoneTitleError" class="form-text text-danger" style="display: none;">Title cannot contain special characters</small>
                                                </div>
                                                <div class="form-group">
                                                    <label for="keeperID">Keeper</label>
                                                    <select id="keeperID" name="keeperID" class="form-control" required>
                                                        <option value="">Select a Keeper</option>
                                                        <%-- Populate keeper ID dropdown list --%>
                                                        <c:forEach items="${userID}" var="u">
                                                            <option value="${u.userID}">${u.userName}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <button type="submit" class="btn btn-primary">Add Zone</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal for editing zone -->
                            <div class="modal fade" id="editCardModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <h5 class="modal-title text-black" id="exampleModalLabel">Edit Zone</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="editCardForm" method="post" action="warehouse">
                                                <input type="hidden" name="action" value="editZone">
                                                <input type="hidden" name="id" id="zoneId">
                                                <div class="form-group">
                                                    <label for="editZoneTitle">Zone Name</label>
                                                    <input type="text" id="editZoneTitle" name="name" class="form-control" placeholder="Zone Title" required>
                                                    <small id="editZoneTitleError" class="form-text text-danger" style="display: none;">Title cannot contain special characters</small>
                                                </div>
                                                <div class="form-group">
                                                    <label for="editZoneKeeper">Keeper</label>
                                                    <select id="editZoneKeeper" name="keeperID" class="form-control" required>
                                                        <option value="">Select a Keeper</option>
                                                        <!-- Replace with your server-side logic to populate options -->
                                                        <c:forEach items="${userID}" var="u">
                                                            <option value="${u.userID}">${u.userID}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <button type="submit" class="btn btn-primary">Edit Zone</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal for zone details -->
                            <div class="modal fade" id="detailCardModal" tabindex="-1" role="dialog" aria-labelledby="detailCardModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <h5 class="modal-title text-black" id="detailCardModalLabel">Zone Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p><strong>Zone Name:</strong> <span id="zone-name"></span></p>
                                            <p><strong>Keeper ID:</strong> <span id="keeper-id"></span></p>
                                            <p style="margin-bottom: 0px"><strong>Total Product:</strong> <span id="total"></span></p>
<!--                                            <p class="text-black" style="text-align: center; margin-bottom: 0px"><strong>PRODUCT </strong> <span id="total"></span></p>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Product Name</th>
                                                        <th>Description</th>
                                                        <th>Quantity</th>
                                                        <th>Unit Price</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="inventory-list">
                                                     Product details will be inserted here 
                                                </tbody>
                                            </table>-->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
                            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
                            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
                            <script>
                                $(document).ready(function () {
                                    $('#addCardButton').click(function () {
                                        $('#addCardModal').modal('show');
                                    });

                                    $('#addCardForm').submit(function (event) {
                                        event.preventDefault();

                                        var name = $('#zoneTitle').val();
                                        var keeperID = $('#keeperID').val();
                                        var errorMessage = '';

                                        // Check if title contains special characters
                                        if (!/^[a-zA-Z0-9\s]+$/.test(name)) {
                                            errorMessage = 'Title cannot contain special characters';
                                            $('#zoneTitleError').text(errorMessage).show();
                                            return; // Prevent the form from submitting
                                        }

                                        $.ajax({
                                            url: 'warehouse',
                                            type: 'POST',
                                            data: {
                                                action: 'addZone',
                                                name: name,
                                                keeperID: keeperID
                                            },
                                            success: function (response) {
                                                location.reload();
                                            },
                                            error: function (error) {
                                                alert('Failed to add zone.');
                                            }
                                        });
                                    });

                                    $('#zoneTitle').on('input', function () {
                                        $('#zoneTitleError').hide();
                                    });
                                });
//----------------
                                $(document).ready(function () {
                                    $('.detail-zone').on('click', function () {
                                        var zoneTitle = $(this).data('title');
                                        var zoneKeeper = $(this).data('keeper');
                                        var total = $(this).data('total');
                                        var productData = $(this).data('products');

                                        $('#zone-name').text(zoneTitle);
                                        $('#keeper-id').text(zoneKeeper);
                                        $('#total').text(total); // Display total products

                                        // Function to parse the product data string
                                        function parseProductData(dataString) {
                                            var resultArray = [];
                                            if (dataString) {
                                                var items = dataString.split(';');
                                                items.forEach(function (item) {
                                                    if (item) { // Check if item is not empty
                                                        var details = item.split(',');
                                                        resultArray.push({
                                                            productname: details[1],
                                                            description: details[2],
                                                            quantity: parseInt(details[3]),
                                                            unitprice: parseFloat(details[4])
                                                        });
                                                    }
                                                });
                                            }
                                            return resultArray;
                                        }

                                        // Parse product data
                                        var products = parseProductData(productData);

                                        // Generate HTML for products table
                                        var productsHtml = '';
                                        products.forEach(function (product) {
                                            productsHtml += `
                                    <tr>
                                        <td>${product.productname}</td>
                                        <td>${product.description}</td>
                                        <td>${product.quantity}</td>
                                        <td>${product.unitprice}</td>
                                    </tr>
                                `;
                                        });

                                        // Display products table in modal
                                        $('#inventory-list').html(productsHtml);

                                        // Show the modal
                                        $('#detailCardModal').modal('show');
                                    });
                                });
// ------------------------------------
                                $('.remove-card').on('click', function () {
                                    var zoneID = $(this).data('id');
                                    var confirmed = confirm("Are you sure you want to delete this zone?");
                                    if (confirmed) {
                                        $.ajax({
                                            url: 'warehouse',
                                            type: 'POST',
                                            data: {
                                                id: zoneID,
                                                action: 'deleteZone'
                                            },
                                            success: function (response) {
                                                location.reload();
                                            },
                                            error: function (error) {
                                                alert('Failed to delete.');
                                            }
                                        });
                                    }
                                });
// ------------------------------------
                                $('#searchInput').on('input', function () {
                                    var searchValue = $(this).val().toLowerCase();
                                    var zones = $('.card-container .card');
                                    zones.hide();
                                    zones.filter(function () {
                                        var zoneName = $(this).find('.card-title').text().toLowerCase();
                                        var keeperID = $(this).find('.card-text').text().toLowerCase();
                                        return zoneName.includes(searchValue) || keeperID.includes(searchValue);
                                    }).show();
                                });
//======================
                                // Edit zone functionality
                                $(document).ready(function () {
                                    // Function to show edit modal and populate fields
                                    $(document).on('click', '.edit-zone', function () {
                                        const zoneId = $(this).data('id');
                                        const zoneTitle = $(this).data('title');
                                        const zoneKeeper = $(this).data('keeper');

                                        $('#editCardModal').modal('show');
                                        $('#editZoneTitle').val(zoneTitle);
                                        $('#editZoneKeeper').val(zoneKeeper);
                                        $('#zoneId').val(zoneId);
                                    });

                                    // Edit zone form submission
                                    $(document).on('submit', '#editCardForm', function (event) {
                                        event.preventDefault();

                                        var zoneId = $('#zoneId').val();
                                        var zoneName = $('#editZoneTitle').val();
                                        var keeperId = $('#editZoneKeeper').val();
                                        var errorMessage = '';

                                        // Validate zoneName - allow only alphanumeric characters and spaces
                                        if (!/^[a-zA-Z0-9\s]+$/.test(zoneName)) {
                                            errorMessage = 'Title cannot contain special characters';
                                            $('#editZoneTitleError').text(errorMessage).show(); // Show error message
                                            return; // Prevent form submission
                                        } else {
                                            $('#editZoneTitleError').hide(); // Hide error message if validation passes
                                        }

                                        // AJAX request to update zone
                                        $.ajax({
                                            url: 'warehouse', // Adjust URL as per your server-side endpoint
                                            type: 'POST',
                                            data: {
                                                action: 'editZone',
                                                zoneID: zoneId,
                                                name: zoneName,
                                                keeperID: keeperId
                                            },
                                            success: function (response) {
                                                $('#editCardModal').modal('hide'); // Hide modal on success
                                                location.reload(); // Reload page or update UI as needed
                                            },
                                            error: function (error) {
                                                alert('Failed to edit zone.'); // Handle error
                                            }
                                        });
                                    });

                                    // Clear error message when editing zone title
                                    $('#editZoneTitle').on('input', function () {
                                        $('#editZoneTitleError').hide();
                                    });
                                });

                            </script>
                        </div>
                    </div>
                </div>

                <!-- container-scroller -->
                <!-- plugins:js -->
                <script src="assets/vendors/js/vendor.bundle.base.js"></script>
                <!-- endinject -->
                <!-- Plugin js for this page -->
                <script src="assets/vendors/chart.js/Chart.min.js"></script>
                <script
                src="assets/vendors/jquery-circle-progress/js/circle-progress.min.js"></script>
                <!-- End plugin js for this page -->
                <!-- inject:js -->
                <script src="assets/js/off-canvas.js"></script>
                <script src="assets/js/hoverable-collapse.js"></script>
                <script src="assets/js/misc.js"></script>
                <!-- endinject -->
                <!-- Custom js for this page -->
                <script src="assets/js/dashboard.js"></script>
                </body>
                </html>

