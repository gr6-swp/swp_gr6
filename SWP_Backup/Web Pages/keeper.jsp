<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Home Pages</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet" href="assets/vendors/font-awesome/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <!-- Custom CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
                <link rel="shortcut icon" href="assets/images/logo-mini.svg" />

    <style>
        .card-header {
            background-color: #f8f9fa;
            font-weight: bold;
        }
        .card {
            margin-bottom: 20px;
            box-shadow: 4px 6px 10px;
        }
        .gauge {
            height: 200px;
        }
        .chart-container {
            position: relative;
            height: 300px;
        }
        .data-table th, .data-table td {
            text-align: center;
            vertical-align: middle;
        }
        .text-success, .text-danger {
            font-size: 1.2rem;
        }
        .card-value {
            font-size: 1.5rem;
            font-weight: bold;
        }
        .card-label {
            font-size: 0.75rem;
        }
        .form-control{
            height: auto;
        }
        .card-body{
            border: 1px solid;
            
        }
                  .body {
                border: 2px solid #4CAF50; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px; /* Bo góc (tùy chọn) */
            }
    </style>
        </head>
    <body>
        <div class="container-scroller">
            <%@ include file="sidebarKeeper.jsp" %>

            <div class="container body" style="  max-width: 77%;">
                <h1 class="text-center text-black">Dashboard Keeper</h1>

                

    <div class="row mt-3 ml-3 ">
        <div class="col-md-2" style="box-shadow: 10px 12px; border: 1px solid">
            <div class="form-group">
                <label for="warehouseSelect" class="text-black pt-4 " style="font-size: large"  >Keeper  <b>${sessionScope.acc.userName} </b></label>
               
            </div>
        </div>
        <div class="col-md-2 ml-4" style="box-shadow: 10px 12px; border: 1px solid; ">
            <div class="form-group">
                <label for="dateSelect" class="text-black">Date</label>
                <input type="date" class="form-control" id="dateSelect" style="height: 35px">
            </div>
        </div>
        <div class="col-md-8"></div>
    </div>
    <div class="row mt-4">
        <div class="col-md-2 mt-5">
            <div class="card text-center">
                <div class="card-body ">
                    <div class="card-value text-success">6.67%</div>
                    <div class="card-label">Rate Finished Products </div>
                    <div class="card-value">${completedProducts}</div>
                    <div class="card-label" > Finished Products</div>
                    <hr/>
                    <div class="card-value"></div>
                    <div class="card-label">Total Unit Price</div>
                </div>
            </div>
        </div>
        <div class="col-md-2 mt-5">
            <div class="card text-center">
                <div class="card-body">
                    <div class="card-value text-success">2.17%</div>
                    <div class="card-label">Rate Processing Products </div>
                    <div class="card-value">248</div>
                    <div class="card-label">Processing Products</div>
                    <hr/>
                    <div class="card-value">11,434</div>
                    <div class="card-label">Total Unit Price</div>
                </div>
            </div>
        </div>
        <div class="col-md-2 mt-5">
            <div class="card text-center">
                <div class="card-body">
                    <div class="card-value text-success">1.11%</div>
                    <div class="card-label"> Rate inventory products </div>
                    <div class="card-value">1</div>
                    <div class="card-label">Inventory products</div>
                    <hr/>
                    <div class="card-value">90</div>
                    <div class="card-label">Total Unit Price</div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header text-black">Inventory Days of Supply</div>
                <div class="card-body chart-container">
                    <canvas id="inventoryDaysChart"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-black">Product Stock Details</div>
                <div class="card-body">
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>ZoneID</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${products}" var="product">
                                        <tr>
  
                                            <td>${product.productname}</td>
                                            <td>${product.description}</td>
                                            <td>${product.quantity}</td>
                                            <td>${product.unitprice}</td>
                                            <td>${product.warehouseZoneID}</td>
                                        </tr>
                                    </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jQuery, Popper.js, Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<!-- Chart.js -->
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<!-- Google Charts for gauges -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    // Initialize Chart.js charts
    var ctx1 = document.getElementById('inventoryDaysChart').getContext('2d');
    var inventoryDaysChart = new Chart(ctx1, {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
            datasets: [{
                label: 'Days of Supply',
                data: [30, 35, 40, 45, 50, 55, 60],
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    // Initialize Google Charts for gauges
    google.charts.load('current', {'packages':['gauge']});
    google.charts.setOnLoadCallback(drawGauges);

    function drawGauges() {
        // Inventory Carrying Cost Rate
        var data1 = google.visualization.arrayToDataTable([
            ['Label', 'Value'],
            ['Cost Rate', 15.79]
        ]);
        var options1 = {
            width: 400, height: 120,
            redFrom: 90, redTo: 100,
            yellowFrom: 75, yellowTo: 90,
            minorTicks: 5
        };
        var chart1 = new google.visualization.Gauge(document.getElementById('carryingCostRate'));
        chart1.draw(data1, options1);

        // Inventory Turnover Ratio
        var data2 = google.visualization.arrayToDataTable([
            ['Label', 'Value'],
            ['Ratio', 3.76]
        ]);
        var options2 = {
            width: 400, height: 120,
            redFrom: 90, redTo: 100,
            yellowFrom: 75, yellowTo: 90,
            minorTicks: 5
        };
        var chart2 = new google.visualization.Gauge(document.getElementById('turnoverRatio'));
        chart2.draw(data2, options2);

        // Inventory To Sales Ratio
        var data3 = google.visualization.arrayToDataTable([
            ['Label', 'Value'],
            ['Ratio', 18.33]
        ]);
        var options3 = {
            width: 400, height: 120,
            redFrom: 90, redTo: 100,
            yellowFrom: 75, yellowTo: 90,
            minorTicks: 5
        };
        var chart3 = new google.visualization.Gauge(document.getElementById('salesRatio'));
        chart3.draw(data3, options3);
    }
</script>
</body>
</html>