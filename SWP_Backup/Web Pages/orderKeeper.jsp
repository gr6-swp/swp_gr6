<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Notifications</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="assets/images/logo-mini.svg" />
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            h2 {
                margin-bottom: 10px;
            }

            /* Kiểu dáng cho nút */
            button {
                background-color: #4CAF50; /* Màu xanh lá cây */
                color: white;
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Kiểu dáng cho ô tìm kiếm và select */
            input[type="text"], select {
                padding: 8px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* Kiểu dáng cho bảng */
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 15px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            input[type="text"] {
                padding: 10px;
                border: 2px solid #4CAF50; /* Viền xanh lá cây */
                border-radius: 5px; /* Bo góc */
                width: 200px; /* Điều chỉnh độ rộng tùy ý */
            }

            button {
                /* ... (các kiểu dáng khác của nút) */
                margin-left: 5px; /* Tạo khoảng cách giữa nút và ô tìm kiếm */
            }
            #container {
                border: 2px solid #4CAF50; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px; /* Bo góc (tùy chọn) */
            }


            body {
                font-family: sans-serif;
            }
            label {
                display: block;
                margin-bottom: 5px;
            }
            input, select {
                width: 100%;
                padding: 8px;
                margin-bottom: 10px;
                box-sizing: border-box;
            }
            button {
                padding: 10px;
                background-color: #4CAF50;
                color: white;
                border: none;
                cursor: pointer;
            }
            #orderDetails {
                margin-top: 20px;
            }

            .form-group {
                margin-bottom: 15px;
            }


            th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
                padding: 10px;
                border-bottom: 1px solid #dee2e6;
            }

            .form-row {
                display: flex;
                justify-content: space-between;
            }
            .modal-header {
                color: white;
            }
            .modal-footer {
                justify-content: center;
            }
            .modal-body{
                border: 2px solid #4CAF50; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px;
                margin: 20px
            }
            .modal-body .form-group label {
                font-weight: bold;
            }
            .btn-custom {
                background-color: #28a745;
                color: white;
            }
            .btn-custom:hover {
                background-color: #218838;
            }
            .form-control {
                border-radius: 0;
            }
            .table th {
                background-color: #28a745;
                color: white;
            }
            .table input {
                width: 100%;
                border: none;
            }
            .form-container {
                display: flex;
                align-items: center;
                justify-content: space-between;
                border: 1px solid #ccc;
                border-radius: 5px;
                background-color: #f9f9f9;
            }
            .form-container select,
            .form-container .products-count {
                padding: 5px;
                font-size: 14px;
            }
            .form-container .sort-by {
                display: flex;
                align-items: center;
            }
            .form-container .sort-by select {
                margin-left: 5px;
            }
            .form-container .view-options {
                display: flex;
                align-items: center;
            }
            .form-container .view-options i {
                margin-left: 5px;
                cursor: pointer;
            }
        </style>

    </head>


    <!DOCTYPE html>
    <html xmlns:c="http://java.sun.com/jsp/jstl/core">
        <html>

            <style>
                /* CSS để tùy chỉnh giao diện */
                body {
                    font-family: Arial, sans-serif;
                    background-color: #f5f5f5;
                    margin: 0;
                    padding: 0;
                    color: #333;
                }

                .container {
                    width: 80%;
                    margin: 20px auto;
                    background-color: #fff;
                    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                    padding: 20px;
                    border-radius: 8px;
                }

                form {
                    display: flex;
                    justify-content: space-between;
                    margin-bottom: 20px;
                }

                form input[type="text"] {
                    width: 80%;
                    padding: 10px;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                }

                form button {
                    padding: 10px 20px;
                    background-color: #4CAF50;
                    color: white;
                    border: none;
                    border-radius: 4px;
                    cursor: pointer;
                }

                .partner-section {
                    margin-bottom: 20px;
                }

                .partner-section input[type="text"] {
                    width: 100%;
                    padding: 10px;
                    margin-bottom: 10px;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                }

                table {
                    width: 100%;
                    border-collapse: collapse;
                    margin-bottom: 20px;
                }

                th, td {
                    border: 1px solid #ddd;
                    padding: 12px;
                    text-align: left;
                }

                th {
                    background-color: #f2f2f2;
                }

                .btn {
                    padding: 8px 12px;
                    background-color: #4CAF50;
                    color: white;
                    border: none;
                    border-radius: 4px;
                    cursor: pointer;
                    margin-right: 5px;
                }

                .btn:hover {
                    background-color: #45a049;
                }

                .outer-container {
                    border: 3px solid #4CAF50; /* Màu xanh */
                    padding: 17px;
                    margin-left: 3%;
                    margin-top: 1%;
                    width: 91%;
                    background-color: #f9f9f9; /* Màu nền cho khung chứa toàn bộ nội dung */
                    border-radius: 8px; /* Bo tròn các góc của khung */
                }

                .inner-container {
                    padding: 0 10px; /* Tạo khoảng cách giữa nội dung và khung ngoài */
                }

                .bordered-section {
                    border: 1px solid #000; /* Màu đen */
                    padding: 10px;
                    margin-bottom: 20px;
                    border-radius: 8px;
                }

                .partner-header {
                    background-color: #4CAF50; /* Màu xanh */
                    color: white;
                    padding: 10px;
                    border-top-left-radius: 8px;
                    border-top-right-radius: 8px;
                }
                .search-container {
                    margin-bottom: 20px; /* Khoảng cách giữa phần tìm kiếm và phần còn lại */
                }


                .search-container button {

                    background-color: #4CAF50;
                    color: white;
                    border-radius: 7px;
                }

                .modal {
                    display: none; /* Ẩn popup ban đầu */
                    position: fixed; /* Ở vị trí cố định so với viewport */
                    z-index: 1; /* Đảm bảo popup nằm trên các phần tử khác */
                    left: 0;
                    top: 0;
                    width: 70%;
                    height: 100%;
                    overflow: auto;
                    background-color: rgba(0, 0, 0, 0.4); /* Màu nền mờ */
                }

                .modal-content {
                    background-color: #fefefe;
                    margin: 10% auto; /* Căn giữa popup */
                    padding: 17px;
                    border: 3px solid #888;
                    width: 50%; /* Giới hạn chiều rộng tối đa */
                    border-radius: 30px;
                    box-shadow: 0 5px 15px rgba(0,0,0,.5);
                }

                .close {
                    color: #aaa;
                    float: right;
                    font-size: 28px;
                    font-weight: bold;
                }

                .close:hover,
                .close:focus {
                    color: black;
                    text-decoration: none;
                    cursor: pointer;
                }

                .popup-content {
                    padding: 20px;
                }

                .popup-content h2 {
                    color: #000;
                }

                .popup-content label {
                    color: #000;
                }

                .popup-content table {
                    width: 100%;
                    border-collapse: collapse;
                }

                .popup-content th, .popup-content td {
                    padding: 8px;
                    text-align: left;
                    border-bottom: 1px solid #ddd;
                }

                .popup-content button {
                    background-color: #4CAF50;
                    color: white;
                    padding: 10px 20px;
                    border: none;
                    cursor: pointer;
                }

                .popup-content button:hover {
                    background-color: #45a049;
                }

                #container > div {
                    margin-bottom: 15px;
                }

                #container label {
                    display: block;
                    margin-bottom: 5px;
                }

                #container input[type="text"],
                #container select,
                #container input[type="number"] {
                    padding: 2px;
                    margin-bottom: -10px;
                    border: 2px solid #ccc;
                    border-radius: 4px;
                }

                #container table th {
                    background-color: #4CAF50;
                    color: white;
                }

                #container table input {
                    width: 100%;
                    padding: 5px;
                    box-sizing: border-box;
                }

                .container {
                    display: grid;
                    grid-template-columns: 1fr 1fr;
                    gap: 10px;
                }

                .container > div {
                    display: flex; /* Sử dụng flexbox để căn chỉnh label và input */
                    align-items: center;
                }

                .container label {
                    width: 120px;
                    margin-right: 10px; /* Khoảng cách giữa label và input */
                }

            </style>
            <body>
                <%@ include file="sidebarKeeper.jsp" %>
                <!-- partial -->
                <div style="margin-left: 5%; margin-top: 1%; margin-right: 0%; width: 71%">
                    <h2>List Order</h2>
                    <table>
                        <thead>
                            <tr>
<!--                                <th>ID</th>
                                <th>Partner ID</th>-->
                                <!--<th>Warehouse ID</th>-->
                                <th>Product ID</th>
                                <th>Order Type</th>
                                <th>Order Status</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="order" items="${orders}">
                                <tr>
                                    <!--<td>${order.orderId}</td>-->
                                    <!--<td>${order.partnerId}</td>-->
                                    <!--<td>${order.zoneId}</td>-->
                                    <td>${order.productId}</td>
                                    <td>${order.orderType}</td>
                                    <td>${order.orderStatus}</td>
                                    <td>${order.createdAt}</td>
                                    <td>${order.updatedAt}</td>
                                    <td>
                                        <c:if test="${order.orderStatus == 'Đang xử lý' }">
                                            <form action="confirmOrder" method="get">
                                                <input type="hidden" name="orderId" value="${order.orderId}">
                                                <button type="submit">Confirm</button>
                                            </form>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </body>
        </html>
