<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Notifications</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
        <link rel="shortcut icon" href="assets/images/logo-mini.svg" />
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            h2 {
                margin-bottom: 10px;
            }

            /* Kiểu dáng cho nút */
            button {
                background-color: #4CAF50; /* Màu xanh lá cây */
                color: white;
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Kiểu dáng cho ô tìm kiếm và select */
            input[type="text"], select {
                padding: 8px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* Kiểu dáng cho bảng */
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 15px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            input[type="text"] {
                padding: 10px;
                border: 2px solid #4CAF50; /* Viền xanh lá cây */
                border-radius: 5px; /* Bo góc */
                width: 200px; /* Điều chỉnh độ rộng tùy ý */
            }

            button {
                /* ... (các kiểu dáng khác của nút) */
                margin-left: 5px; /* Tạo khoảng cách giữa nút và ô tìm kiếm */
            }
            #container {
                border: 2px solid #4CAF50; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px; /* Bo góc (tùy chọn) */
            }


            body {
                font-family: sans-serif;
            }
            label {
                display: block;
                margin-bottom: 5px;
            }
            input, select {
                width: 100%;
                padding: 8px;
                margin-bottom: 10px;
                box-sizing: border-box;
            }
            button {
                padding: 10px;
                background-color: #4CAF50;
                color: white;
                border: none;
                cursor: pointer;
            }
            #orderDetails {
                margin-top: 20px;
            }

            .form-group {
                margin-bottom: 15px;
            }


            th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
                padding: 10px;
                border-bottom: 1px solid #dee2e6;
            }

            .form-row {
                display: flex;
                justify-content: space-between;
            }
            .modal-header {
                color: white;
            }
            .modal-footer {
                justify-content: center;
            }
            .modal-body{
                border: 2px solid #4CAF50; /* Viền xanh dương */
                padding: 20px; /* Khoảng cách bên trong khung */
                border-radius: 10px;
                margin: 20px
            }
            .modal-body .form-group label {
                font-weight: bold;
            }
            .btn-custom {
                background-color: #28a745;
                color: white;
            }
            .btn-custom:hover {
                background-color: #218838;
            }
            .form-control {
                border-radius: 0;
            }
            .table th {
                background-color: #28a745;
                color: white;
            }
            .table input {
                width: 100%;
                border: none;
            }
            .form-container {
                display: flex;
                align-items: center;
                justify-content: space-between;
                border: 1px solid #ccc;
                border-radius: 5px;
                background-color: #f9f9f9;
            }
            .form-container select,
            .form-container .products-count {
                padding: 5px;
                font-size: 14px;
            }
            .form-container .sort-by {
                display: flex;
                align-items: center;
            }
            .form-container .sort-by select {
                margin-left: 5px;
            }
            .form-container .view-options {
                display: flex;
                align-items: center;
            }
            .form-container .view-options i {
                margin-left: 5px;
                cursor: pointer;
            }
        </style>

    </head>
    <body>
        <%@ include file="sidebarPartner.jsp" %>

        <!--<div class="container">-->
                        <div style="margin-left: 5%; margin-top: 1%; margin-right: 0%; width: 71%;">

            <!--<h1>Partner - Notifications</h1>-->
            <!-- Nút để mở popup tạo thông báo -->
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createNotificationModal">
                Send Request To Center
            </button>


            <!-- Modal popup để tạo thông báo mới -->
            <div class="modal fade" id="createNotificationModal" tabindex="-1" role="dialog" aria-labelledby="createNotificationModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="createNotificationModalLabel">Send Request</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="notificationsPartner" method="post">
                                <input type="hidden" name="action" value="createNotification">

                                <!--                                <div class="form-group">
                                                                    <label for="senderRole">Vai trò Người gửi:</label>
                                                                    <input type="text" class="form-control" id="senderRole" name="senderRole" required>
                                                                </div>-->
                                <!--                                <div class="form-group">
                                                                    <label for="receiverRole">Vai trò Người nhận:</label>
                                                                    <input type="text" class="form-control" id="receiverRole" name="receiverRole" required>
                                                                </div>-->
                                <div class="form-group">
                                    <label for="message">Nội dung:</label>
                                    <textarea class="form-control" id="message" name="message" rows="3" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="relatedEntityId">ID Liên quan:</label>
                                    <input type="number" class="form-control" id="relatedEntityId" name="relatedEntityId" required>
                                </div>
                                <button type="submit" class="btn btn-primary">Send</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Danh sách thông báo -->

            <h2>Notifications List</h2>
            <table class="table table-striped ">
                <thead>
                    <tr>
                        <th>ID Thông báo</th>
                        <th>Vai trò Người gửi</th>
                        <th>Vai trò Người nhận</th>
                        <th>Nội dung</th>
                        <th>Trạng thái</th>
                        <th>Ngày tạo</th>
                        <th>ID liên quan</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="notification" items="${notifications}">
                        <tr>
                            <td>${notification.notificationID}</td>
                            <td>${notification.senderRole}</td>
                            <td>${notification.receiverRole}</td>
                            <td>${notification.message}</td>
                            <td>${notification.status}</td>
                            <td>${notification.createdAt}</td>
                            <td>${notification.relatedEntityId}</td>

                            <td>
                                <c:if test="${notification.status == 'Unread'}">
                                    <form action="notificationsPartner" method="post" style="display:inline;">
                                        <input type="hidden" name="action" value="updateStatus">
                                        <input type="hidden" name="notificationID" value="${notification.notificationID}">
                                        <input type="hidden" name="status" value="Read">
                                        <button type="submit" class="btn btn-success">Read</button>
                                    </form>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <form action="notificationsPartner" method="doPost">
                <button type="submit" class="btn btn-success">View Notifications</button>
            </form>
        </div>
    </body>
</html>
