<%-- 
    Document   : PartnerOderStatistic
    Created on : Jul 9, 2024, 1:00:26 PM
    Author     : songl
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<taglib-uri>http://java.sun.com/jsp/jstl/core</taglib-uri>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Order Statistic</title>
        <!-- plugins:css -->
        <link rel="stylesheet"
              href="assets/vendors/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet"
              href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet"
              href="assets/vendors/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet"
              href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- End layout styles -->
                <link rel="shortcut icon" href="assets/images/logo-mini.svg" />

        <style>
            body {
                font-family: Arial, sans-serif;
            }
            h2 {
                margin-bottom: 10px;
            }

            /* Kiểu dáng cho nút */
            button {
                background-color: #4CAF50; /* Màu xanh lá cây */
                color: white;
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Kiểu dáng cho ô tìm kiếm và select */
            input[type="text"], select {
                padding: 8px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* Kiểu dáng cho bảng */
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 15px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 12px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            input[type="text"] {
                padding: 10px;
                border: 2px solid #4CAF50; /* Viền xanh lá cây */
                border-radius: 5px; /* Bo góc */
                width: 200px; /* Điều chỉnh độ rộng tùy ý */
            }

            button {
                /* ... (các kiểu dáng khác của nút) */
                margin-left: 5px; /* Tạo khoảng cách giữa nút và ô tìm kiếm */
            }
            #container {
                border: 2px solid #4CAF50;
                padding: 20px;
                border-radius: 10px;
            }


            body {
                font-family: sans-serif;
            }
            label {
                display: block;
                margin-bottom: 5px;
            }
            input, select {
                width: 100%;
                padding: 8px;
                margin-bottom: 10px;
                box-sizing: border-box;
            }
            button {
                padding: 10px;
                background-color: #4CAF50;
                color: white;
                border: none;
                cursor: pointer;
            }
            #orderDetails {
                margin-top: 20px;
            }

            .form-group {
                margin-bottom: 15px;
            }

            .btn-primary{
                color: #fff;
                background-color: #4CAF50;
                border-color: #0062ff;
            }
            th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
                padding: 10px;
                border-bottom: 1px solid #dee2e6;
            }

            .form-row {
                display: flex;
                justify-content: space-between;
            }
            .export-button-container {
                margin-top: 20px; /* Điều chỉnh khoảng cách giữa nút xuất Excel và bảng */
            }
            .btn-success{
                background-color: #4CAF50;
                border-color: #0062ff;
            }
        </style>
    </head>
    <body>
                <%@ include file="sidebarPartner.jsp" %>


                <div style="margin-left: 5%; margin-top: 1%; margin-right: 0%; width: 71%">
                    <h2 style="color: #007bff">Inventory Statistics</h2>

                    <% Integer partnerID = (Integer) session.getAttribute("partnerID");
                       if (partnerID == null) {
                           // Handle case where partnerID is null
                    %>
                    <p>Partner ID is missing. Please <a href="login.jsp">log in</a> again.</p>
                    <% return; } %>

                    <div id="container">
                        <div class="search-container">
                            <form action="parnerorders" method="get" class="search-container">
                                <select name="productName" class="form-control" id="product-select">
                                    <option value="" disabled selected>Select Product</option>
                                    <option value="all">All Product</option>
                                    <c:forEach var="name" items="${ productNames}">
                                        <c:choose>
                                            <c:when test="${param.productName == name}">
                                                <option value="${name}" selected>${name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${name}">${name}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                    <!-- Add more options as needed -->
                                </select>
                                <button type="submit" class="btn btn-primary">Search</button>
                            </form>
                        </div>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Product Name</th>
                                    <th>Quantity Product</th>
                                    <th>Order Type</th>
                                    <th>Order Status</th>



                                </tr>
                            </thead>
                            <tbody>
                                <c:if test="${not empty partneroderlist}">
                                    <c:forEach var="item" items="${partneroderlist}" varStatus="status">
                                        <tr>
                                            <td>${status.index + 1}</td>
                                            <td>${item.product}</td>
                                            <td>${item.quantity}</td>
                                            <td>${item.orderType}</td>
                                            <td>${item.orderStatus}</td>


                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${empty partneroderlist}">
                                    <tr>
                                        <td colspan="3">No order data available</td>
                                    </tr>
                                </c:if>
                            </tbody>
                        </table>

                        <div class="export-button-container">
                            <form action="exparneroders" method="post" id="exportForm">
                                <input type="hidden" name="productNameSelected" id="productNameSelected">
                                <button type="button" class="btn btn-success" id="exportExcelBtn" onclick="exportToExcel()">Export to Excel</button>
                            </form>
                        </div>
                    </div>
                </div>



                <script>
                    document.getElementById('exportExcelBtn').addEventListener('click', function () {
                        var productNameSelect = document.querySelector('select[name="productName"]');
                        var productNameSelected = productNameSelect.options[productNameSelect.selectedIndex].value;
                        document.getElementById('productNameSelected').value = productNameSelected;
                        document.getElementById('exportForm').submit();
                    });
                </script>


                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
                <script>
                    function handleSearch(event) {
                        if (event.key === "Enter") {
                            performSearch();
                        }
                    }
                </script>          
                
                </body>
                </html>

