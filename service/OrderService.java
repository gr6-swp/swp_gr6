/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dal.StatisticDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import entity.OderIteam;

public class OrderService {

    private StatisticDAO dbContext;

    public OrderService(StatisticDAO dbContext) {
        this.dbContext = dbContext;
    }

    public List<OderIteam> getOrderStatistics(String zoneName) throws SQLException {
        return dbContext.fetchOrderStatistics(zoneName);
    }

    public OrderService() throws ClassNotFoundException, SQLException {
        dbContext = new StatisticDAO();
    }

    public List<OderIteam> fetchOrderStatistics(String zoneName) throws SQLException {
        List<OderIteam> orderList = new ArrayList<>();
        String sql = "SELECT \n"
                + "    wz.ZoneName,\n"
                + "    o.OrderType,\n"
                + "    o.OrderStatus,\n"
                + "    COUNT(DISTINCT o.OrderID) AS [Total Quantity]\n"
                + "FROM \n"
                + "    Orders o\n"
                + "JOIN \n"
                + "    OrderDetails od ON o.OrderID = od.OrderID\n"
                + "JOIN \n"
                + "    Inventory i ON od.ProductID = i.ProductID\n"
                + "JOIN \n"
                + "    WarehouseZones wz ON i.WarehouseZoneID = wz.ZoneID\n"                
                + "GROUP BY \n"
                + "    wz.ZoneName, o.OrderType, o.OrderStatus\n"
                + "ORDER BY \n"
                + "    wz.ZoneName, o.OrderType, o.OrderStatus";
        try ( Connection conn = dbContext.getConnection();  PreparedStatement ps = conn.prepareStatement(sql)) {
            int paramIndex = 1;
            if (zoneName != null && !zoneName.isEmpty()) {
                ps.setString(paramIndex++, zoneName);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                OderIteam stats = new OderIteam();
                stats.setZoneName(rs.getString("ZoneName"));
                stats.setOder(rs.getString("OrderType"));
                stats.setStatus(rs.getString("OrderStatus"));
                stats.setTotalquantity(rs.getInt("Total Quantity"));
                orderList.add(stats);
            }
        }
        return orderList;
    }

    public List<String> getAllZoneNames() throws SQLException {
        List<String> zoneNames = new ArrayList<>();
        String sql = "SELECT ZoneName FROM WarehouseZones";
        try ( PreparedStatement ps = dbContext.getConnection().prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String zoneName = rs.getString("ZoneName");
                zoneNames.add(zoneName);
            }
        } catch (SQLException e) {
            throw e;
        }
        return zoneNames;
    }
}
