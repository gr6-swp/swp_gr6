/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dal.StatisticDAO;
import entity.PartnerItem;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PartnerService {

    private StatisticDAO dbContext;

    public PartnerService(StatisticDAO dbContext) {
        this.dbContext = dbContext;
    }

    public List<PartnerItem> getPartnerStatistics(String partner) throws SQLException {
        return dbContext.fetchPartnerStatistics(partner);
    }

    public PartnerService() throws ClassNotFoundException, SQLException {
        dbContext = new StatisticDAO();

    }   

    public List<PartnerItem> fetchPartnerStatistics(String partner) throws SQLException {
        List<PartnerItem> partnerStatisticsList = new ArrayList<>();
        String sql = "SELECT \n"
                + "    p.PartnerName,\n"
                + "    o.OrderType,\n"
                + "    o.OrderStatus,\n"
                + "    COUNT(o.OrderID) AS [Total Quantity]\n"
                + "FROM \n"
                + "    Partners p\n"
                + "JOIN \n"
                + "    Orders o ON p.PartnerID = o.PartnerID\n"
                + "GROUP BY \n"
                + "    p.PartnerName,\n"
                + "    o.OrderType,\n"
                + "    o.OrderStatus\n"
                + "ORDER BY \n"
                + "    p.PartnerName, \n"
                + "    o.OrderType, \n"
                + "    o.OrderStatus;"; 
        if (partner != null && !partner.isEmpty()) {
            sql += " AND p.PartnerName = ?";
        }
        try ( Connection conn = dbContext.getConnection();  PreparedStatement ps = conn.prepareStatement(sql)) {
            int paramIndex = 1;
            if (partner != null && !partner.isEmpty()) {
                ps.setString(paramIndex++, partner);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PartnerItem pi = new PartnerItem();
                pi.setPartner(rs.getString("PartnerName"));
                pi.setOder(rs.getString("OrderType"));
                pi.setStatus(rs.getString("OrderStatus"));
                pi.setTotalquantity(rs.getInt("Total Quantity"));
                partnerStatisticsList.add(pi);
            }
        }
        return partnerStatisticsList;
    }

    public List<String> getAllPartner() throws SQLException {
        List<String> partnerNameS = new ArrayList<>();
        String sql = "Select PartnerName from Partners";
        try ( PreparedStatement ps = dbContext.getConnection().prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String partnerName = rs.getString("PartnerName");
                partnerNameS.add(partnerName);
            }
        } catch (SQLException e) {
            throw e;
        }
        return partnerNameS;
    }
}
