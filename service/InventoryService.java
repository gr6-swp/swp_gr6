package service;
import dal.DBConnection;
import dal.StatisticDAO;
import entity.InventoryItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InventoryService {

    private StatisticDAO dbContext;

    public InventoryService(StatisticDAO dbContext) {
        this.dbContext = dbContext;
    }

    public List<InventoryItem> getInventoryStatistics(String warehouse, String zoneName) throws SQLException {
        return dbContext.fetchInventoryStatistics(warehouse, zoneName);
    }

    public InventoryService() throws ClassNotFoundException, SQLException {
        StatisticDAO dbContext = new StatisticDAO();
    }

    public List<InventoryItem> fetchInventoryStatistics(String warehouse, String zoneName) throws SQLException {
        List<InventoryItem> inventoryList = new ArrayList<>();
        String sql = "SELECT p.ProductName, w.WarehouseName, z.ZoneName, i.Quantity AS TotalQuantity "
                + "FROM Inventory i "
                + "JOIN Products p ON i.ProductID = p.ProductID "
                + "JOIN WarehouseZones z ON i.WarehouseZoneID = z.ZoneID "
                + "JOIN Warehouses w ON z.WarehouseID = w.WarehouseID "
                + "WHERE 1=1";
        if (warehouse != null && !warehouse.isEmpty()) {
            sql += " AND w.WarehouseName = ?";
        }
        if (zoneName != null && !zoneName.isEmpty()) {
            sql += " AND z.ZoneName = ?";
        }

        try ( Connection conn = dbContext.getConnection();  PreparedStatement ps = conn.prepareStatement(sql)) {

            int paramIndex = 1;
            if (warehouse != null && !warehouse.isEmpty()) {
                ps.setString(paramIndex++, warehouse);
            }
            if (zoneName != null && !zoneName.isEmpty()) {
                ps.setString(paramIndex++, zoneName);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                InventoryItem item = new InventoryItem();
                item.setProductName(rs.getString("ProductName"));
                item.setWarehouse(rs.getString("WarehouseName"));
                item.setZoneName(rs.getString("ZoneName"));
                item.setTotalQuantity(rs.getInt("TotalQuantity"));
                inventoryList.add(item);
            }
        }
        return inventoryList;
    }

    public List<String> getAllWarehouseNames() throws SQLException {
        List<String> warehouseNames = new ArrayList<>();
        String sql = "SELECT WarehouseName FROM Warehouses";
        try ( PreparedStatement ps = dbContext.getConnection().prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String warehouseName = rs.getString("WarehouseName");
                warehouseNames.add(warehouseName);
            }
        } catch (SQLException e) {

            throw e;
        }
        return warehouseNames;
    }

    public List<String> getAllZoneNames() throws SQLException {
        List<String> zoneNames = new ArrayList<>();
        String sql = "SELECT ZoneName FROM WarehouseZones";
        try ( PreparedStatement ps = dbContext.getConnection().prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                String zoneName = rs.getString("ZoneName");
                zoneNames.add(zoneName);
            }
        } catch (SQLException e) {
            throw e;
        }
        return zoneNames;
    }

}
